﻿using System;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Storage;
using Microsoft.EntityFrameworkCore.SqlServer;
using System.Runtime.CompilerServices;
using System.Linq;
using System.Data.Common;
using Microsoft.Data.SqlClient;

[assembly: InternalsVisibleTo("NURV.Migrations")]
namespace NURV.Data.Context
{
    internal class NURVDbContext : DbContext, INURVDbContext 
    {
        private static NURVContext.CreateContextDelegate _createDbContext;
        
        /// <summary>
        /// Mock initialiser to override DbContext creation for unit tests ONLY.
        /// </summary>
        internal static void MockInit(NURVContext.CreateContextDelegate createDbContext)
        {
            _createDbContext = createDbContext;
        }

        /// <summary>
        /// Creates a DbContext using either supplied method (to support mocking) or the internal constructor.
        /// </summary>
        internal static INURVDbContext CreateDbContext(DataAccess.Common.ContextSettings contextSettings)
        {
            INURVDbContext context;

            if (_createDbContext == null)
            {
                context = new NURVDbContext(contextSettings, contextSettings?.DbOptions);
            }
            else
            {
                context = _createDbContext(contextSettings?.TrackingOn ?? false);
            }

            return context;
        }

       /// <remarks>Only used when adding migrations via Package Manager Console</remarks>
       public NURVDbContext() 
        {
            ChangeTracker.AutoDetectChangesEnabled = false;
        }

        private NURVDbContext(DataAccess.Common.ContextSettings contextSettings, DbContextOptions options) : base(options)
        {
            if (contextSettings?.Transaction != null)
            {
                Database.UseTransaction(contextSettings.Transaction);
            }

            if (!(contextSettings?.TrackingOn ?? false))
            {
                ChangeTracker.AutoDetectChangesEnabled = false;
            }
        }

        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        {
            if (!optionsBuilder.IsConfigured)
            {
                optionsBuilder.UseSqlServer(@"Data Source=localhost;Initial Catalog=NURVTEST;Integrated Security=SSPI;",
                                x => x.MigrationsAssembly("NURV.Migrations"));
            }
        }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            // Force all string-based columns to non-unicode equivalent 
            // when no column type is explicitly set.
            // https://benjaminray.com/codebase/force-strings-to-use-varchar-in-entity-framework-core/
            foreach (var property in modelBuilder.Model.GetEntityTypes()
                .SelectMany(t => t.GetProperties())
                .Where(
                       p => p.ClrType == typeof(string)    // Entity is a string
                    && p.GetColumnType() == null           // No column type is set
                ))
            {
                property.SetIsUnicode(false);
            }

            // Set the default value for the CreatedDate column on all inherited classes of EntityObjectWithCreatedBase.
            // ...in a perfect world we would have just done the following...
            // modelBuilder.Entity<Model.Common.EntityObjectWithCreatedBase>().Property(x => x.CreatedDate).HasDefaultValueSql("getutcdate()");
            // ...but we have to apply the default to each concrete class ...can't just do it directly on the base class.
            // ...and this is much better than having to remember to do it on each concrete class definition.
            modelBuilder.Model.GetEntityTypes()
                .Where(entityType => typeof(Model.Common.EntityObjectWithCreatedBase).IsAssignableFrom(entityType.ClrType))
                .ToList()
                .ForEach(entityType =>
                {
                    modelBuilder.Entity(entityType.ClrType).Property<DateTimeOffset>(nameof(Data.Model.Common.EntityObjectWithCreatedBase.CreatedDate)).HasDefaultValueSql("getutcdate()");
                });

            // Register stored procedure 'entity objects' ...HasNoKey and ToView(null) avoids it adding as a table or view in the database.
            _ = modelBuilder.Entity<Model.StoredProcedures.TransactionPolicyHistory>().HasNoKey().ToView(null);

            modelBuilder.Entity<Model.ArrearsClause>(entity =>
            {
                entity.HasKey(e => e.ArrearsClauseId)
                    .HasName("PK_ArrearsClause");

                entity.HasIndex(e => new { e.TenantInArrears, e.ArrearsClauseId })
                    .HasName("ix_tblArrearsClause_cover1")
                    .IsUnique();

                entity.Property(e => e.ArrearsClauseId).ValueGeneratedNever();
            });

            modelBuilder.Entity<Model.ArrearsWeekRange>(entity =>
            {
                entity.HasKey(e => e.ArrearsWeekRangeId)
                    .HasName("PK_ArrearsWeekRangeID");
            });

            modelBuilder.Entity<Model.BankCategory>(entity =>
            {
                entity.HasIndex(e => e.BcId)
                    .HasName("ixBankCategory_bc_id");

                entity.Property(e => e.TimeStamp)
                    .IsRowVersion()
                    .IsConcurrencyToken();
            });

            modelBuilder.Entity<Model.Banking>(entity =>
            {
                entity.HasIndex(e => e.Bank)
                    .HasName("ixBanking_Bank");

                entity.HasIndex(e => e.BankCategoryId)
                    .HasName("ixBanking_BankCategoryID");

                entity.HasIndex(e => e.BankDate)
                    .HasName("ixBanking_bankDate");

                entity.HasIndex(e => e.BankSubCategoryId)
                    .HasName("ixBanking_BankSubCategoryID");

                entity.HasIndex(e => e.ChequeNumber)
                    .HasName("ixBanking_chequeNumber");

                entity.HasIndex(e => e.ContactForCreditId)
                    .HasName("ixBanking_ContactForCreditID");

                entity.HasIndex(e => e.CreationDate)
                    .HasName("ix_tblBanking_creationDate");

                entity.HasIndex(e => e.TransactionNumber)
                    .HasName("ixBanking_TransactionNumber");

                entity.HasIndex(e => e.TrustId)
                    .HasName("ixBanking_TrustID");

                entity.Property(e => e.TimeStamp)
                    .IsRowVersion()
                    .IsConcurrencyToken();
            });

            modelBuilder.Entity<Model.BonusBackup>(entity =>
            {
                entity.Property(e => e.Id).ValueGeneratedNever();
            });

            modelBuilder.Entity<Model.BonusOffer>(entity =>
            {
                entity.HasKey(e => e.BonusOfferId)
                    .HasName("PK_tblBonusOfferNew");

                entity.HasIndex(e => new { e.Code, e.BonusTimeId, e.BonusOfferId })
                    .HasName("ix_tblBonusOffer_cover1")
                    .IsUnique();
            });

            modelBuilder.Entity<Model.BonusTime>(entity =>
            {
                entity.HasIndex(e => new { e.Description, e.BonusTimeId })
                    .HasName("ix_tblBonusTime_cover1")
                    .IsUnique();
            });

            modelBuilder.Entity<Model.BushFireRiskLevel>(entity =>
            {
                entity.HasNoKey();
            });

            modelBuilder.Entity<Model.Claim>(entity =>
            {
                entity.HasIndex(e => e.CreationDate)
                    .HasName("ixClaim_CreationDate");

                entity.HasIndex(e => e.DateOfLoss)
                    .HasName("ixClaim_DateOfLoss");

                entity.HasIndex(e => e.DisputeOutcomeId)
                    .HasName("ix_tblClaim_disputeOutcomeID");

                entity.HasIndex(e => e.LandlordId)
                    .HasName("IX_tblClaim_landlordID");

                entity.HasIndex(e => e.PolicyNumber)
                    .HasName("ixClaim_policyNumber");

                entity.HasIndex(e => e.ReahandlingClaim)
                    .HasName("ixClaim_reaHandlingClaim");

                entity.HasIndex(e => e.StaffName)
                    .HasName("ix_tblClaim_staffName");

                entity.HasIndex(e => e.TransactionNumber)
                    .HasName("ixClaim_TransactionNumber");

                entity.HasIndex(e => new { e.ClaimTypeId, e.StatusOfClaimId, e.AssignedToUser })
                    .HasName("ix_tblClaim_cover6");

                entity.HasIndex(e => new { e.UwCode, e.ClaimStatusId, e.ClaimVersion, e.ClaimNumber })
                    .HasName("ixClaim_uwCode")
                    .IsUnique();

                entity.HasIndex(e => new { e.Id, e.StaffName, e.Deleted, e.ClaimVersion, e.AssignedToUser, e.ClaimStatusId })
                    .HasName("ix_tblClaim_cover2");

                entity.HasIndex(e => new { e.UwCode, e.StatusOfClaimId, e.ClaimVersion, e.ClaimStatusId, e.TransactionNumber, e.ClaimNumber, e.Id })
                    .HasName("ix_tblClaim_cover1")
                    .IsUnique();

                entity.HasIndex(e => new { e.Id, e.CreationDate, e.StaffName, e.Deleted, e.AssignedToUser, e.ClaimVersion, e.StatusOfClaimId, e.ClaimNumber })
                    .HasName("ix_tblClaim_claimVersion")
                    .IsUnique();

                entity.HasIndex(e => new { e.CreationDate, e.CreationUser, e.AssignedToUser, e.ClaimPriorityId, e.ClaimTypeId, e.LodgedById, e.LodgedHowId, e.Id, e.ClaimNumber, e.ClassOfRiskId, e.TransactionNumber })
                    .HasName("ix_tblClaim_cover5")
                    .IsUnique();

                entity.HasIndex(e => new { e.EventCode, e.NaturalHazard, e.Deleted, e.ClaimStatusId, e.LandlordId, e.StatusOfClaimId, e.CreationDate, e.ClaimVersion, e.ContactId, e.ClassOfRiskId, e.Id, e.ClaimNumber, e.TransactionNumber })
                    .HasName("ixClaim_ClaimNumber")
                    .IsUnique();

                entity.HasIndex(e => new { e.CreationDate, e.LodgementDate, e.FinalisedDate, e.ReopenedDate, e.DateOfLoss, e.Id, e.StaffName, e.ClaimVersion, e.ClaimNumber, e.AssignedToUser, e.ClaimTypeId, e.LodgedHowId, e.LodgedById, e.CreationUser })
                    .HasName("ix_tblClaim_cover7")
                    .IsUnique();

                entity.HasIndex(e => new { e.ClaimVersion, e.ClaimTypeId, e.LodgedById, e.LodgedHowId, e.EventCode, e.ClassOfRiskId, e.DateOfLoss, e.PolicyNumber, e.LandlordId, e.TransactionNumber, e.StatusOfClaimId, e.Deleted, e.ClaimStatusId, e.Id, e.ClaimNumber })
                    .HasName("ix_tblClaim_cover3")
                    .IsUnique();

                entity.HasIndex(e => new { e.PolicyNumber, e.ClaimVersion, e.LandlordAddress, e.Landlord, e.FollowUpDate, e.AssignedToUser, e.ClaimStatusId, e.StatusOfClaimId, e.ClaimTypeId, e.ClaimNumber, e.ContactId, e.LandlordId, e.DateOfLoss, e.TransactionNumber, e.Id })
                    .HasName("ixClaim_ContactID")
                    .IsUnique();

                entity.HasIndex(e => new { e.Id, e.ClaimNumber, e.CreationDate, e.PolicyNumber, e.StaffName, e.TransactionNumber, e.ClaimStatusId, e.DateStatusChanged, e.FollowUpDate, e.IsAssessedClaim, e.EventCode, e.LodgedHowId, e.ReahandlingClaimContactId, e.ClaimTypeId, e.PropertyManagerId, e.ClaimVersion, e.ClaimPriorityId, e.NextScheduleOfLossRowNumber, e.StatusOfClaimId, e.AssignedToUser, e.Untenantable, e.Deleted })
                    .HasName("ixClaim_Deleted");

                entity.Property(e => e.BoldUwmemo).HasDefaultValueSql("(0)");

                entity.Property(e => e.BondCreditApplies).HasDefaultValueSql("(0)");

                entity.Property(e => e.TimeStamp)
                    .IsRowVersion()
                    .IsConcurrencyToken();
            });

            modelBuilder.Entity<Model.ContactSpecialCondition>(entity =>
            {
                entity.HasKey(e => new { e.SpecialConditionId, e.PremiumTypeId, e.CreatedDate });
            });

            modelBuilder.Entity<Model.DisclosureClaim>(entity =>
            {
                entity.HasKey(e => e.DisclosureClaimsId)
                    .HasName("PK_DisclosureClaimsID");

                entity.HasIndex(e => e.DisclosureQuestionsId)
                    .HasName("ix_tblDisclosureClaims_disclosureQuestionsID");
            });

            modelBuilder.Entity<Model.DisclosureClass>(entity =>
            {
                entity.HasKey(e => e.DisclosureClassId)
                    .HasName("PK_tblDisclosueClass");

                entity.Property(e => e.DisclosureClassId).ValueGeneratedNever();
            });

            modelBuilder.Entity<Model.DisclosureLossAmount>(entity =>
            {
                entity.Property(e => e.DisclosureLossAmountId).ValueGeneratedNever();
            });

            modelBuilder.Entity<Model.DisclosureQuestion>(entity =>
            {
                entity.HasKey(e => e.DisclosureQuestionId)
                    .HasName("PK_DisclosureQuestionsID");

                entity.HasIndex(e => e.TransactionNumber)
                    .HasName("IX_tblDisclosureQuestions");
            });

            modelBuilder.Entity<Model.DisclosureType>(entity =>
            {
                entity.Property(e => e.DisclosureTypeId).ValueGeneratedNever();
            });

            modelBuilder.Entity<Model.Embargo>(entity =>
            {
                entity.HasIndex(e => e.EffectiveDate)
                    .HasName("ix_tblEmbargo_effectiveDate");

                entity.HasIndex(e => e.EmbargoLifted)
                    .HasName("ix_tblEmbargo_embargoLifted");

                entity.HasIndex(e => e.PostCode)
                    .HasName("ix_tblEmbargo_postcode");
            });

            modelBuilder.Entity<Model.Endorsement>(entity =>
            {
                entity.HasNoKey();

                entity.HasIndex(e => e.EndorsementDate)
                    .HasName("ixEndorsement_endorsementDate");

                entity.HasIndex(e => e.Id)
                    .HasName("pkEndorsement")
                    .IsUnique()
                    .IsClustered();

                entity.HasIndex(e => e.MultipleEndorsementId)
                    .HasName("ixEndorsement_multipleEndorsementID");

                entity.HasIndex(e => e.TransactionNumber)
                    .HasName("ixEndorsement_TransactionNumber");

                entity.Property(e => e.TimeStamp)
                    .IsRowVersion()
                    .IsConcurrencyToken();
            });

            modelBuilder.Entity<Model.FloodRiskLevel>(entity =>
            {
                entity.HasNoKey();

                entity.HasIndex(e => e.FloodRiskLevelId)
                    .HasName("ix_tblFloodRiskLevel_floodRiskLevelID")
                    .IsUnique()
                    .IsClustered();
            });

            modelBuilder.Entity<Model.HoldCoverInfo>(entity =>
            {
                entity.HasNoKey();

                entity.HasIndex(e => e.WljoiningId)
                    .HasName("IX_tblHoldCoverInfo_wlJoiningID");

                entity.HasIndex(e => new { e.TransactionNumber, e.ArrearsClauseId })
                    .HasName("ix_tblHoldCoverInfo_cover2");

                entity.HasIndex(e => new { e.AttachPds, e.AttachFsg, e.HeardAboutTsi, e.TransactionNumber, e.WhoRequested, e.HowRequested })
                    .HasName("ix_tblHoldCoverInfo_cover1");
            });

            modelBuilder.Entity<Model.LossRatioAnalysisPenalty>(entity =>
            {
                entity.HasNoKey();

                entity.HasIndex(e => e.PenaltyId)
                    .HasName("ix_tblLossRatioAnalysisPenalty_penaltyID");

                entity.HasIndex(e => e.PenaltyShortDescription)
                    .HasName("ix_tblLossRatioAnalysisPenalty_penaltyShortDescription");

                entity.HasIndex(e => new { e.PenaltyDescription, e.RuleId, e.Default, e.Active })
                    .HasName("ix_tblLossRatioAnalysisPenalty_cover1");
            });

            modelBuilder.Entity<Model.MessageAction>(entity =>
            {
                entity.HasNoKey();
            });

            modelBuilder.Entity<Model.PolicyNumber>(entity =>
            {
                entity.HasNoKey();

                entity.Property(e => e.TimeStamp)
                    .IsRowVersion()
                    .IsConcurrencyToken();
            });

            modelBuilder.Entity<Model.Postcode>(entity =>
            {
                entity.HasIndex(e => e.Code)
                .HasName("IX_tblPostcode_Postcode");

                entity.HasIndex(e => e.State);

                entity.HasIndex(e => e.Suburb);

                entity.Property(e => e.TimeStamp)
                    .IsRowVersion()
                    .IsConcurrencyToken();
            });

            modelBuilder.Entity<Model.Quotation>(entity =>
            {
                entity.HasNoKey();

                entity.HasIndex(e => e.OnlinePostcode)
                    .HasName("ix_tblQuotation_onlinePostcode");

                entity.HasIndex(e => e.OnlineQuotationNumber)
                    .HasName("ix_tblQuotation_onlineQuotationNumber");

                entity.HasIndex(e => e.QuotationCode)
                    .HasName("ixQuotation_quotationCode");

                entity.HasIndex(e => e.QuotationCreatedByUserId)
                    .HasName("ixQuotation_quotationCreatedByUserID");

                entity.HasIndex(e => e.QuotationCreatedDate)
                    .HasName("ixQuotation_quotationCreatedDate");

                entity.HasIndex(e => e.QuotationVoiddate)
                    .HasName("ixQuotation_quotationVoidDate");

                entity.HasIndex(e => new { e.QuotationPrintedDate, e.QuotationCode, e.OnlineQuotationNumber, e.QuotationCreatedDate, e.QuotationExpiryDate, e.TransactionNumber, e.QuotationCreatedByUserId })
                    .HasName("ix_tblQuotation_cover1");

                entity.Property(e => e.Id).ValueGeneratedOnAdd();
            });

            modelBuilder.Entity<Model.RatesMpd>(entity =>
            {
                entity.HasNoKey();

                entity.HasIndex(e => new { e.MinProperties, e.MaxProperties, e.Id, e.ClassOfRiskId })
                    .HasName("ix_tblRatesMpd_id");
            });

            modelBuilder.Entity<Model.ReaGroupDiscount>(entity =>
            {
                entity.HasIndex(e => e.Name)
                    .IsUnique();
            });

            modelBuilder.Entity<Model.Refund>(entity =>
            {
                entity.HasIndex(e => e.CreatedDate)
                    .HasName("IX_tblRefund_createdDate");

                entity.HasIndex(e => e.HowInformedId)
                    .HasName("ix_tblRefund_howInformedID");

                entity.HasIndex(e => e.OffsetTfId)
                    .HasName("IX_tblRefund_offset_tf_id");

                entity.HasIndex(e => e.PayMethod)
                    .HasName("IX_tblRefund_payMethod");

                entity.HasIndex(e => e.PrintedDate)
                    .HasName("IX_tblRefund_printedDate");

                entity.HasIndex(e => e.Type)
                    .HasName("IX_tblRefund_type");

                entity.HasIndex(e => new { e.CancelReasonId, e.TransactionNumber })
                    .HasName("IX_tblRefund_cancelReasonID");

                entity.HasIndex(e => new { e.TransactionNumber, e.HowInformedId, e.CancelReasonId })
                    .HasName("ix_tblRefund_cover2");

                entity.HasIndex(e => new { e.PayeeName, e.RefundAmount, e.Bsb, e.Account, e.BankTfId, e.PayMethod })
                    .HasName("IX_tblRefund_bank_tf_id");

                entity.HasIndex(e => new { e.TransactionNumber, e.BankTfId, e.PrintedBy, e.ApprovedBy, e.CreatedBy, e.CancelReasonId, e.Type, e.PayMethod, e.PrintedDate, e.TransferTfId })
                    .HasName("ix_tblRefund_cover3");
            });

            modelBuilder.Entity<Model.BaseRate>(entity =>
            {
                entity.HasIndex(e => new { e.Rate, e.ClassOfRiskId, e.PremiumTypeId, e.State, e.StartEffectiveDate, e.EndEffectiveDate })
                    .HasName("ix_tblRFBaseRate_cover1");
            });

            modelBuilder.Entity<Model.BushFire>(entity =>
            {
                entity.HasIndex(e => e.Postcode)
                    .HasName("ix_tblRFBushFire_Postcode");

                entity.HasIndex(e => new { e.BushFireId, e.BushFireZone, e.BushFireRiskRatingFactor, e.BushFireRiskLevelId, e.Suburb, e.Postcode, e.ClassOfRiskId, e.PremiumTypeId, e.StartEffectiveDate, e.EndEffectiveDate })
                    .HasName("ix_tblRFBushFire_cover2");
            });

            modelBuilder.Entity<Model.PetDamageExcess>(entity =>
            {
                entity.HasIndex(e => new { e.Excess, e.PetDamageExcessId })
                    .HasName("ix_tblRFPetDamageExcess_cover1")
                    .IsUnique();
            });

            modelBuilder.Entity<Model.TenantDamageExcess>(entity =>
            {
                entity.HasIndex(e => new { e.Excess, e.TenantDamageExcessId })
                    .HasName("ix_tblRFTenantDamageExcess_cover1")
                    .IsUnique();
            });

            modelBuilder.Entity<Model.UnoccupiedExcess>(entity =>
            {
                entity.HasNoKey();

                entity.HasIndex(e => new { e.Excess, e.UnoccupiedExcessId })
                    .HasName("ix_tblRFUnoccupiedExcess_cover1")
                    .IsUnique();
            });

            modelBuilder.Entity<Model.WeeklyRentCover>(entity =>
            {
                entity.HasNoKey();

                entity.HasIndex(e => e.EndEffectiveDate)
                    .HasName("ixRfWeeklyRentCover_endEffectiveDate");

                entity.HasIndex(e => e.Rfstate)
                    .HasName("ixRfWeeklyRentCover_rfState");

                entity.HasIndex(e => new { e.Urange, e.RentRange, e.TopWeeklyRental, e.Id, e.ClassOfRiskId, e.PremiumTypeId, e.StartEffectiveDate, e.EndEffectiveDate })
                    .HasName("ix_tblRFWeeklyRentCover_cover1")
                    .IsUnique();

                entity.HasIndex(e => new { e.Id, e.BaseAmount, e.Rate, e.ClassOfRiskId, e.StartEffectiveDate, e.EndEffectiveDate, e.PremiumTypeId, e.Urange, e.Lrange })
                    .HasName("ix_tblRFWeeklyRentCover_cover3");

                entity.HasIndex(e => new { e.Id, e.Rate, e.Urange, e.Lrange, e.Rfstate, e.StartEffectiveDate, e.EndEffectiveDate, e.ClassOfRiskId, e.PremiumTypeId })
                    .HasName("ix_tblRFWeeklyRentCover_cover2");
            });

            modelBuilder.Entity<Model.Settings>(entity =>
            {
                entity.Property(e => e.MinimumImposedExcess).HasDefaultValueSql("((500))");

                entity.Property(e => e.TimeStamp)
                    .IsRowVersion()
                    .IsConcurrencyToken();
            });

            modelBuilder.Entity<Model.Staff>(entity =>
            {
                entity.HasIndex(e => e.ClaimNumber)
                    .HasName("ix_tblStaff_ClaimNumber");

                entity.HasIndex(e => e.EmailAddress)
                    .HasName("ixStaff_EmailAddress");

                entity.HasIndex(e => e.Position)
                    .HasName("ixStaff_Position");

                entity.HasIndex(e => new { e.FirstName, e.Surname, e.MiddleName, e.PreferredMailingName, e.Salutation, e.ContactId, e.RemovedDate, e.Position, e.Deleted, e.Id })
                    .HasName("ix_tblStaff_cover1")
                    .IsUnique();

                entity.Property(e => e.HolidayLetting).HasDefaultValueSql("(0)");

                entity.Property(e => e.TimeStamp)
                    .IsRowVersion()
                    .IsConcurrencyToken();
            });

            modelBuilder.Entity<Model.StatusOfClaim>(entity =>
            {
                entity.HasIndex(e => e.StatusTypeId)
                    .HasName("ix_tblStatusOfClaim_statusTypeID");

                entity.HasIndex(e => new { e.Status, e.StatusOfClaimId })
                    .HasName("ix_tblStatusOfClaim_cover1")
                    .IsUnique();
            });

            modelBuilder.Entity<Model.Transaction>(entity =>
            {
                entity.HasIndex(e => e.Balance)
                    .HasName("ix_tblTransaction_balance");

                entity.HasIndex(e => e.BonusOfferId)
                    .HasName("ix_tblTransaction_BonusOfferID");

                entity.HasIndex(e => e.CreationDate)
                    .HasName("ixTransaction_creationDate");

                entity.HasIndex(e => e.CreditCardTransId);

                entity.HasIndex(e => e.CreditControlFinalReminderPrinted)
                    .HasName("ixTransaction_creditControl_lapsedSent");

                entity.HasIndex(e => e.CreditControlLapseNoticePrinted)
                    .HasName("ixTransaction_lapsePrinted");

                entity.HasIndex(e => e.CreditControlReminderBatchNumber)
                    .HasName("ix_tblTransaction_CreditControlReminderBatchNumber");

                entity.HasIndex(e => e.DoNotRenew)
                    .HasName("ixTransaction_DoNotRenew");

                entity.HasIndex(e => e.DoNotRenewReason)
                    .HasName("ix_tblTransaction_doNotRenew_reason");

                entity.HasIndex(e => e.ExtraReturnPremiumId)
                    .HasName("ix_tblTransaction_extraReturnPremiumID");

                entity.HasIndex(e => e.HowPayId)
                    .HasName("ix_tblTransaction_howPayID");

                entity.HasIndex(e => e.LandSizeId)
                    .HasName("ix_tblTransaction_landSizeID");

                entity.HasIndex(e => e.LandlordState)
                    .HasName("ixTransaction_LandlordState");

                entity.HasIndex(e => e.LandlordSuburb)
                    .HasName("IX_tblTransaction_landlordSuburb");

                entity.HasIndex(e => e.LapsingBatch)
                    .HasName("ixTransaction_LapsingBatch");

                entity.HasIndex(e => e.LinkedToLppTransaction)
                    .HasName("ixTransaction_LinkedToLppTransaction");

                entity.HasIndex(e => e.ParentTransactionNumber)
                    .HasName("ix_tblTransaction_ParentTransactionNumber");

                entity.HasIndex(e => e.PolicyNumber)
                    .HasName("ixTransaction_PolicyNumber");

                entity.HasIndex(e => e.PolicyPaymentReference)
                    .HasName("ix_tblTransaction_PolicyPaymentReference");

                entity.HasIndex(e => e.PremiumTotalPr)
                    .HasName("ix_tblTransaction_premiumTotalPr");

                entity.HasIndex(e => e.RenewalDate)
                    .HasName("ixTransaction_RenewalDate");

                entity.HasIndex(e => e.RenewalDeclinedPrinted)
                    .HasName("ix_tblTransaction_renewalDeclinedPrinted");

                entity.HasIndex(e => e.RptransactionNumber)
                    .HasName("ix_tblTransaction_rpTransactionNumber");

                entity.HasIndex(e => e.Rptype)
                    .HasName("ixTransaction_RPType");

                entity.HasIndex(e => e.TransactionNumber)
                    .HasName("ixTransaction_TransactionNumber")
                    .IsUnique();

                entity.HasIndex(e => e.TrnCancelDate)
                    .HasName("ix_tblTransaction_trn_cancelDate");

                entity.HasIndex(e => e.TrnCancelledOn)
                    .HasName("IX_tblTransaction_trn_cancelledOn");

                entity.HasIndex(e => e.TrnCrId)
                    .HasName("ixTransaction_trn_cr_id");

                entity.HasIndex(e => e.TrnPremiumPaid)
                    .HasName("ixTransaction_trn_PremiumPaid");

                entity.HasIndex(e => new { e.InsurerCode, e.EffectiveDate })
                    .HasName("ix_tblTransaction_cover7");

                entity.HasIndex(e => new { e.RenewalBatch, e.RenewalOnHold })
                    .HasName("ix_tblTransaction_RenewalOnHold");

                entity.HasIndex(e => new { e.RenewedTransaction, e.TransactionNumber })
                    .HasName("ix_tblTransaction_cover2")
                    .IsUnique();

                entity.HasIndex(e => new { e.ContactId, e.StatusOfTransaction, e.Deleted })
                    .HasName("ixTransaction_deleted");

                entity.HasIndex(e => new { e.EffectiveDate, e.TransactionNumber, e.CombinedPolicyDiscountPolicyLink })
                    .HasName("IX_CombinedPolicyDiscountPolicyLink_EffectiveDateTransactionNumber");

                entity.HasIndex(e => new { e.PremiumTypeId, e.StatusOfTransaction, e.TrnCancelledOn })
                    .HasName("ixTransaction_PremiumTypeID");

                entity.HasIndex(e => new { e.ClassOfRiskId, e.LandlordPostcode, e.LandlordSuburb, e.PremiumTypeId, e.StatusOfTransaction })
                    .HasName("t1");

                entity.HasIndex(e => new { e.Id, e.PremiumTypeId, e.StatusOfTransaction, e.CreationUser, e.CreationDate })
                    .HasName("ixTransaction_creationUser");

                entity.HasIndex(e => new { e.LandlordId, e.PolicyNumber, e.MultiplePropertyDiscount, e.ClassOfRiskId, e.StatusOfTransaction })
                    .HasName("ix_tblTransaction_cover9");

                entity.HasIndex(e => new { e.PolicyNumber, e.LandlordId, e.StatusOfTransaction, e.PremiumTypeId, e.ClassOfRiskId })
                    .HasName("ix_tblTransaction_cover11");

                entity.HasIndex(e => new { e.Id, e.TransactionNumber, e.TimeStamp, e.RenewalBatch, e.RenewedByTransaction, e.RenewalOnHold })
                    .HasName("ix_tblTransaction_cover6");

                entity.HasIndex(e => new { e.PremiumTypeId, e.StatusOfTransaction, e.OwnerBranch, e.ContactId, e.CreationDate, e.Deleted })
                    .HasName("ixTransaction_OwnerBranch");

                entity.HasIndex(e => new { e.ClassOfRiskId, e.MultiplePropertyDiscount, e.Mpdid, e.PolicyNumber, e.SelfManaged, e.PremiumTypeId, e.LandlordId, e.TransactionNumber })
                    .HasName("ixTransaction_LandlordID")
                    .IsUnique();

                entity.HasIndex(e => new { e.ClassOfRiskId, e.LandlordState, e.OwnerBranch, e.PremiumPaidBy, e.PremiumTypeId, e.SumInsured, e.ContactId, e.StatusOfTransaction, e.TrnAccrualCreated })
                    .HasName("ixTransaction_StatusOfTransaction");

                entity.HasIndex(e => new { e.ClassOfRiskId, e.OwnerBranch, e.PremiumTypeId, e.LandlordState, e.ContactId, e.LandlordId, e.LapsingBatch, e.CreditControlReminderBatchNumber, e.Deleted })
                    .HasName("ix_tblTransaction_cover4");

                entity.HasIndex(e => new { e.CreationDate, e.CreationUser, e.PremiumTotalPr, e.PremiumTypeId, e.StatusOfTransaction, e.TransactionNumber, e.HowPayId, e.ClassOfRiskId, e.InvoicePrinted, e.SelfManaged, e.EffectiveDate })
                    .HasName("ixTransaction_EffectiveDate");

                entity.HasIndex(e => new { e.TrnCrId, e.TrnCancelledOn, e.SelfManaged, e.LandlordId, e.EffectiveDate, e.CreditedTransactionNumber, e.ExtraPremiumParentTransaction, e.PremiumTypeId, e.StatusOfTransaction, e.TransactionNumber, e.ClassOfRiskId })
                    .HasName("ix_tblTransaction_cover5")
                    .IsUnique();

                entity.HasIndex(e => new { e.ClassOfRiskId, e.LandlordId, e.SelfManaged, e.UnoccupancyPremium, e.DoNotRenew, e.ExtraPremiumParentTransaction, e.LapsingBatch, e.PremiumTypeId, e.RenewalBatch, e.RenewalDate, e.RenewedByTransaction, e.StatusOfTransaction, e.TransactionNumber })
                    .HasName("ix_tblTransaction_renewals")
                    .IsUnique();

                entity.HasIndex(e => new { e.Balance, e.LandlordId, e.PremiumTotalPr, e.TrnCancelDate, e.CreditedTransactionNumber, e.ExtraPremiumParentTransaction, e.PolicyNumber, e.MultiplePropertyDiscount, e.StatusOfTransaction, e.SelfManaged, e.PremiumPaidBy, e.ClassOfRiskId, e.PremiumTypeId, e.TransactionNumber, e.EffectiveDate, e.ContactId })
                    .HasName("ix_tblTransaction_cover3")
                    .IsUnique();

                entity.HasIndex(e => new { e.Id, e.Balance, e.LandlordId, e.LandlordAddress, e.LandlordPostcode, e.LandlordState, e.LandlordSuburb, e.OwnerBranch, e.PremiumTotalPr, e.RenewalDate, e.SelfManaged, e.Rptype, e.Deleted, e.ContactId, e.TransactionNumber, e.EffectiveDate, e.StatusOfTransaction, e.ClassOfRiskId, e.PremiumTypeId })
                    .HasName("ixTransaction_ContactID")
                    .IsUnique();

                entity.HasIndex(e => new { e.ClassOfRiskId, e.ContactId, e.DoNotRenewReason, e.RenewalPremiumPaidBy, e.SelfManaged, e.LandlordId, e.DoNotRenew, e.RenewalDate, e.Deleted, e.RenewalOnHold, e.ExtraPremiumParentTransaction, e.PremiumTypeId, e.StatusOfTransaction, e.TransactionNumber, e.LapsingBatch, e.RenewalBatch, e.RenewedByTransaction, e.UnoccupancyPremium, e.TrnCrId, e.Id, e.ExtraReturnPremiumId })
                    .HasName("ix_tblTransaction_cover1")
                    .IsUnique();

                entity.HasIndex(e => new { e.BasePr, e.BasePrGst, e.BfPr, e.FslPr, e.FslPrGst, e.GstPrBf, e.GstPrMf, e.GstPrRw, e.LegalLiabilityPr, e.LegalLiabilityPrGst, e.MfPr, e.PolDocFeePr, e.PolDocFeePrGst, e.PremiumTotalPr, e.RwFeePr, e.SaCommissionRate, e.SdPr, e.RoundingError, e.TrnAccrualCreated, e.InsurerCode, e.StatusOfTransaction, e.ContactId, e.OwnerBranch, e.PremiumTypeId, e.TransactionNumber })
                    .HasName("ixTransaction_trn_AccrualCreated")
                    .IsUnique();

                entity.HasIndex(e => new { e.Balance, e.BasePr, e.ClosingDate, e.CocDate, e.CreationDate, e.CreationUser, e.Deleted, e.InsurerCode, e.LandlordAddress, e.LandlordPostcode, e.LandlordState, e.LandlordSuburb, e.OwnerBranch, e.PolicyNumber, e.PremiumTotalPr, e.RenewalDate, e.TrnProposalReceived, e.CreditedTransactionNumber, e.ExtraPremiumParentTransaction, e.StatusOfTransaction, e.NonReareferrerId, e.TrnCancelledOn, e.PremiumPaidBy, e.WeeklyRentPremiumId, e.ContentsCoverId, e.TransactionNumber, e.ContactId, e.LandlordId, e.EffectiveDate, e.ClassOfRiskId, e.PremiumTypeId, e.TrnReceivableCreated, e.TrnCancelDate, e.TrnCrId, e.SelfManaged })
                    .HasName("ix_tblTransaction_cover10")
                    .IsUnique();

                entity.Property(e => e.BonusId).HasDefaultValueSql("(0)");

                entity.Property(e => e.CommissionPaidFlag).HasDefaultValueSql("(0)");

                entity.Property(e => e.CommonGrounds).HasDefaultValueSql("(0)");

                entity.Property(e => e.Deleted).HasDefaultValueSql("(0)");

                entity.Property(e => e.DeletionOption).HasDefaultValueSql("(0)");

                entity.Property(e => e.GstSaPaidFlag).HasDefaultValueSql("(0)");

                entity.Property(e => e.GstToMfPaidFlag).HasDefaultValueSql("(0)");

                entity.Property(e => e.GstTsibPaidFlag).HasDefaultValueSql("(0)");

                entity.Property(e => e.LegacyData).HasComment("");

                entity.Property(e => e.Locked).HasDefaultValueSql("(0)");

                entity.Property(e => e.MfFlag).HasDefaultValueSql("(0)");

                entity.Property(e => e.MfRwGst).HasDefaultValueSql("(0)");

                entity.Property(e => e.OtherSchemesPaid).HasDefaultValueSql("(0)");

                entity.Property(e => e.PaidFlag).HasDefaultValueSql("(0)");

                entity.Property(e => e.PaidUw).HasDefaultValueSql("(0)");

                entity.Property(e => e.PayPrMf).HasDefaultValueSql("(0)");

                entity.Property(e => e.Rewired).HasDefaultValueSql("(0)");

                entity.Property(e => e.TimeStamp)
                    .IsRowVersion()
                    .IsConcurrencyToken();

                entity.Property(e => e.Version).HasDefaultValueSql("(0)");

                entity.Property(e => e.YearBuilt).IsFixedLength();
            });

            modelBuilder.Entity<Model.TransactionPDSandFSG>(entity =>
            {
                entity.HasNoKey();

                entity.HasIndex(e => new { e.TransactionNumber, e.DocVersion })
                    .HasName("ix_tblTransactionPDSandFSG_cover1");
            });

            modelBuilder.Entity<Model.TransactionPenalty>(entity =>
            {
                entity.HasIndex(e => e.TransactionNumber)
                    .HasName("ix_tblTransactionPenalty_transactionNumber");
            });

            modelBuilder.Entity<Model.User>(entity =>
            {
                entity.HasIndex(e => e.OwnerBranch)
                    .HasName("ix_tblUser_ownerBranch");

                entity.HasIndex(e => e.SigningId)
                    .HasName("ixSigningID")
                    .IsUnique();

                entity.HasIndex(e => e.SuncorpId)
                    .HasName("ix_tblUser_suncorpID");

                entity.HasIndex(e => e.UsrFullName)
                    .HasName("ix_tblUser_usr_fullName");

                entity.HasIndex(e => new { e.Deleted, e.UserGroupId, e.UsrFullName, e.SuncorpId, e.ClaimTeamId, e.UsrLogonName })
                    .HasName("ix_tblUser_cover1")
                    .IsUnique();

                entity.Property(e => e.Deleted).HasDefaultValueSql("(0)");

                entity.Property(e => e.TimeStamp)
                    .IsRowVersion()
                    .IsConcurrencyToken();
            });
        }

        #region Data - Entity Objects

        public virtual DbSet<Model.ArrearsClause> ArrearsClauses { get; set; }
        public virtual DbSet<Model.ArrearsWeekRange> ArrearsWeekRanges { get; set; }
        public virtual DbSet<Model.BankCategory> BankCategories { get; set; }
        public virtual DbSet<Model.Banking> Bankings { get; set; }
        public virtual DbSet<Model.BaseRate> BaseRates { get; set; }
        public virtual DbSet<Model.BonusBackup> BonusBackups { get; set; }
        public virtual DbSet<Model.BonusOffer> BonusOffers { get; set; }
        public virtual DbSet<Model.BonusTime> BonusTimes { get; set; }
        public virtual DbSet<Model.Branch> Branches { get; set; }
        public virtual DbSet<Model.BuildingSumInsuredExcess> BuildingSumInsuredExcesses { get; set; }
        public virtual DbSet<Model.BushFire> BushFires { get; set; }
        public virtual DbSet<Model.BushFireRiskLevel> BushFireRiskLevels { get; set; }
        public virtual DbSet<Model.CCTV> CCTVs { get; set; }
        public virtual DbSet<Model.Claim> Claim { get; set; }
        public virtual DbSet<Model.ClassOfRisk> ClassOfRisks { get; set; }
        public virtual DbSet<Model.ConstructionType> ConstructionTypes { get; set; }
        public virtual DbSet<Model.ContactSpecialCondition> ContactSpecialConditions { get; set; }
        public virtual DbSet<Model.ContentsCover> ContentCovers { get; set; }
        public virtual DbSet<Model.ContentsCoverExcess> ContentCoverExcesses { get; set; }
        public virtual DbSet<Model.DisclosureClaim> DisclosureClaims { get; set; }
        public virtual DbSet<Model.DisclosureClass> DisclosureClasses { get; set; }
        public virtual DbSet<Model.DisclosureLossAmount> DisclosureLossAmounts { get; set; }
        public virtual DbSet<Model.DisclosureQuestion> DisclosureQuestions { get; set; }
        public virtual DbSet<Model.DisclosureType> DisclosureTypes { get; set; }
        public virtual DbSet<Model.DwellingType> DwellingTypes { get; set; }
        public virtual DbSet<Model.ElectronicAccess> ElectronicAccesses { get; set; }
        public virtual DbSet<Model.Embargo> Embargos { get; set; }
        public virtual DbSet<Model.EmbargoReason> EmbargoReasons { get; set; }
        public virtual DbSet<Model.Endorsement> Endorsements { get; set; }
        public virtual DbSet<Model.FloodCover> FloodCovers { get; set; }
        public virtual DbSet<Model.FloodRiskLevel> FloodRiskLevels { get; set; }
        public virtual DbSet<Model.Furnished> Furnished { get; set; }
        public virtual DbSet<Model.HeardAboutTSIB> HeardAboutTSIBs { get; set; }
        public virtual DbSet<Model.HoldCoverInfo> HoldCoverInfos { get; set; }
        public virtual DbSet<Model.Holiday> Holidays { get; set; }
        public virtual DbSet<Model.InformedByWho> InformedByWhos { get; set; }
        public virtual DbSet<Model.InformedHow> InformedHows { get; set; }
        public virtual DbSet<Model.LandSize> LandSizes { get; set; }
        public virtual DbSet<Model.LegalLiability> LegalLiabilities { get; set; }
        public virtual DbSet<Model.LossRatioAnalysisPenalty> LossRatioAnalysisPenalties { get; set; }
        public virtual DbSet<Model.Message> Messages { get; set; }
        public virtual DbSet<Model.MessageAction> MessageActions { get; set; }
        public virtual DbSet<Model.OtherClaimsExcess> OtherClaimsExcesses { get; set; }
        public virtual DbSet<Model.PaymentMethod> PaymentMethods { get; set; }
        public virtual DbSet<Model.PetDamageExcess> PetDamageExcesses { get; set; }
        public virtual DbSet<Model.PolicyNumber> PolicyNumbers { get; set; }
        public virtual DbSet<Model.Postcode> Postcodes { get; set; }
        public virtual DbSet<Model.PremiumPaidBy> PremiumPaidBys { get; set; }
        public virtual DbSet<Model.PremiumType> PremiumTypes { get; set; }
        public virtual DbSet<Model.PromoCode> PromoCodes { get; set; }
        public virtual DbSet<Model.PromoCodeState> PromoCodeStates { get; set; }
        public virtual DbSet<Model.Quotation> Quotations { get; set; }
        public virtual DbSet<Model.RatesMpd> RatesMpds { get; set; }
        public virtual DbSet<Model.ReaGroupDiscount> ReaGroupDiscounts { get; set; }
        public virtual DbSet<Model.Refund> Refunds { get; set; }
        public virtual DbSet<Model.SelfManaged> SelfManaged { get; set; }
        public virtual DbSet<Model.Settings> Settings { get; set; }
        public virtual DbSet<Model.Staff> Staff { get; set; }
        public virtual DbSet<Model.StatusOfClaim> StatusOfClaims { get; set; }
        public virtual DbSet<Model.StatusType> StatusTypes { get; set; }
        public virtual DbSet<Model.TenantDamageExcess> TenantDamageExcesses { get; set; }
        public virtual DbSet<Model.Tenanted> Tenanted { get; set; }
        public virtual DbSet<Model.Transaction> Transactions { get; set; }
        public virtual DbSet<Model.TransactionPDSandFSG> TransactionPDSandFSGs { get; set; }
        public virtual DbSet<Model.TransactionPenalty> TransactionPenalties { get; set; }
        public virtual DbSet<Model.UnoccupiedExcess> UnoccupiedExcesses { get; set; }
        public virtual DbSet<Model.User> Users { get; set; }
        public virtual DbSet<Model.WeeklyRentCover> WeeklyRentCovers { get; set; }

        #endregion

        #region Stored Procedure - Entity Objects

        public DbSet<Model.StoredProcedures.TransactionPolicyHistory> TransactionPolicyHistories { get; set; }

        #endregion
    }
}
