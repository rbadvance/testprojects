﻿using System;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Storage;
using System.Runtime.CompilerServices;
using System.Linq;
using Microsoft.Data.SqlClient;

[assembly: InternalsVisibleTo("NURV.Data.UnitTest")]
namespace NURV.Data.Context
{
    public class NURVContext : IDisposable
    {
        internal delegate INURVDbContext CreateContextDelegate(bool trackingOn);

        private readonly DataAccess.Common.ContextSettings _contextSettings = new DataAccess.Common.ContextSettings();

        internal static void MockInit(CreateContextDelegate createDbContext)
        {
            NURVDbContext.MockInit(createDbContext);
        }

        public NURVContext(bool trackingOn, bool useTransaction = false)
        {
            // this will come from somewhere else, but for now...
            string connectionString = @"Data Source=localhost;Initial Catalog=NURV_TEST;Integrated Security=SSPI;";

            _contextSettings.TrackingOn = trackingOn;
            _contextSettings.UseTransaction = useTransaction;

            if (useTransaction)
            {
                // Setup shared connection and transaction to covering multiple dbContext calls
                _contextSettings.SqlConnection = new SqlConnection(connectionString);
                _contextSettings.SqlConnection.Open();
                _contextSettings.Transaction = _contextSettings.SqlConnection.BeginTransaction();

                _contextSettings.DbOptions = new DbContextOptionsBuilder<NURVDbContext>()
                                    .UseSqlServer(_contextSettings.SqlConnection,
                                         x => x.MigrationsAssembly("NURV.Migrations"))
                                    .Options;
            }
            else
            {
                _contextSettings.DbOptions = new DbContextOptionsBuilder<NURVDbContext>()
                        .UseSqlServer(connectionString,
                                x => x.MigrationsAssembly("NURV.Migrations"))
                        .Options;
            }
        }

        /// <summary>Commit Db Changes (...still requires DbContext.SaveChanges() to have been called on the DbContext)</summary>
        /// <remarks>Only usable if useTransaction was set to TRUE on the constructor</remarks>
        public void Commit()
        {
            if (_contextSettings?.Transaction != null)
            {
                _contextSettings.Transaction.Commit(); 
            }
        }

        /// <summary>Rollback Db Changes</summary>
        /// <remarks>Only usable if useTransaction was set to TRUE on the constructor</remarks>
        public void Rollback()
        {
            if (_contextSettings?.Transaction != null)
            {
                _contextSettings.Transaction.Rollback();
            }
        }

        #region command/Query instances (Lazy)

        public Lazy<DataAccess.ArrearsClauses.IArrearsClauseQuery> ArrearsClauseQuery 
            => new Lazy<DataAccess.ArrearsClauses.IArrearsClauseQuery>(() => new DataAccess.ArrearsClauses.ArrearsClauseQuery(_contextSettings));

        public Lazy<DataAccess.ArrearsWeekRanges.IArrearsWeekRangeQuery> ArrearsWeekRangeQuery
            => new Lazy<DataAccess.ArrearsWeekRanges.IArrearsWeekRangeQuery>(() => new DataAccess.ArrearsWeekRanges.ArrearsWeekRangeQuery(_contextSettings));

        public Lazy<DataAccess.Branches.IBranchQuery> BranchQuery
            => new Lazy<DataAccess.Branches.IBranchQuery>(() => new DataAccess.Branches.BranchQuery(_contextSettings));

        public Lazy<DataAccess.BuildingSumInsuredExcesses.IBuildingSumInsuredExcessQuery> BuildingSumInsuredExcessQuery
            => new Lazy<DataAccess.BuildingSumInsuredExcesses.IBuildingSumInsuredExcessQuery>(() => new DataAccess.BuildingSumInsuredExcesses.BuildingSumInsuredExcessQuery(_contextSettings));

        public Lazy<DataAccess.CCTVs.ICCTVQuery> CCTVQuery
            => new Lazy<DataAccess.CCTVs.ICCTVQuery>(() => new DataAccess.CCTVs.CCTVQuery(_contextSettings));

        public Lazy<DataAccess.ClassesOfRisk.IClassOfRiskCommand> ClassOfRiskCommand
            => new Lazy<DataAccess.ClassesOfRisk.IClassOfRiskCommand>(() => new DataAccess.ClassesOfRisk.ClassOfRiskCommand(_contextSettings));

        public Lazy<DataAccess.ClassesOfRisk.IClassOfRiskQuery> ClassOfRiskQuery
            => new Lazy<DataAccess.ClassesOfRisk.IClassOfRiskQuery>(() => new DataAccess.ClassesOfRisk.ClassOfRiskQuery(_contextSettings));

        public Lazy<DataAccess.ConstructionTypes.IConstructionTypeQuery> ConstructionTypeQuery
            => new Lazy<DataAccess.ConstructionTypes.IConstructionTypeQuery>(() => new DataAccess.ConstructionTypes.ConstructionTypeQuery(_contextSettings));

        public Lazy<DataAccess.ContentsCoverExcesses.IContentsCoverExcessQuery> ContentsCoverExcessQuery
            => new Lazy<DataAccess.ContentsCoverExcesses.IContentsCoverExcessQuery>(() => new DataAccess.ContentsCoverExcesses.ContentsCoverExcessQuery(_contextSettings));

        public Lazy<DataAccess.ContentsCovers.IContentsCoverQuery> ContentsCoverQuery
            => new Lazy<DataAccess.ContentsCovers.IContentsCoverQuery>(() => new DataAccess.ContentsCovers.ContentsCoverQuery(_contextSettings));

        public Lazy<DataAccess.DwellingTypes.IDwellingTypeQuery> DwellingTypeQuery
            => new Lazy<DataAccess.DwellingTypes.IDwellingTypeQuery>(() => new DataAccess.DwellingTypes.DwellingTypeQuery(_contextSettings));

        public Lazy<DataAccess.ElectronicAccesses.IElectronicAccessQuery> ElectronicAccessQuery
            => new Lazy<DataAccess.ElectronicAccesses.IElectronicAccessQuery>(() => new DataAccess.ElectronicAccesses.ElectronicAccessQuery(_contextSettings));

        public Lazy<DataAccess.Furnished.IFurnishedQuery> FurnishedQuery
            => new Lazy<DataAccess.Furnished.IFurnishedQuery>(() => new DataAccess.Furnished.FurnishedQuery(_contextSettings));

        public Lazy<DataAccess.HeardAboutTSIBs.IHeardAboutTSIBQuery> HeardAboutTSIBQuery
            => new Lazy<DataAccess.HeardAboutTSIBs.IHeardAboutTSIBQuery>(() => new DataAccess.HeardAboutTSIBs.HeardAboutTSIBQuery(_contextSettings));

        public Lazy<DataAccess.HoldCoverInfo.IHoldCoverInfoQuery> HoldCoverInfoQuery
            => new Lazy<DataAccess.HoldCoverInfo.IHoldCoverInfoQuery>(() => new DataAccess.HoldCoverInfo.HoldCoverInfoQuery(_contextSettings));

        public Lazy<DataAccess.Holidays.IHolidayQuery> HolidayQuery
            => new Lazy<DataAccess.Holidays.IHolidayQuery>(() => new DataAccess.Holidays.HolidayQuery(_contextSettings));

        public Lazy<DataAccess.InformedByWhos.IInformedByWhoQuery> InformedByWhoQuery
            => new Lazy<DataAccess.InformedByWhos.IInformedByWhoQuery>(() => new DataAccess.InformedByWhos.InformedByWhoQuery(_contextSettings));

        public Lazy<DataAccess.InformedHows.IInformedHowQuery> InformedHowQuery
            => new Lazy<DataAccess.InformedHows.IInformedHowQuery>(() => new DataAccess.InformedHows.InformedHowQuery(_contextSettings));

        public Lazy<DataAccess.LandSizes.ILandSizeQuery> LandSizeQuery
            => new Lazy<DataAccess.LandSizes.ILandSizeQuery>(() => new DataAccess.LandSizes.LandSizeQuery(_contextSettings));

        public Lazy<DataAccess.OtherClaimsExcesses.IOtherClaimsExcessQuery> OtherClaimsExcessQuery
            => new Lazy<DataAccess.OtherClaimsExcesses.IOtherClaimsExcessQuery>(() => new DataAccess.OtherClaimsExcesses.OtherClaimsExcessQuery(_contextSettings));

        public Lazy<DataAccess.PremiumPaidBys.IPremiumPaidByQuery> PremiumPaidByQuery
            => new Lazy<DataAccess.PremiumPaidBys.IPremiumPaidByQuery>(() => new DataAccess.PremiumPaidBys.PremiumPaidByQuery(_contextSettings));

        public Lazy<DataAccess.PremiumTypes.IPremiumTypeQuery> PremiumTypeQuery
            => new Lazy<DataAccess.PremiumTypes.IPremiumTypeQuery>(() => new DataAccess.PremiumTypes.PremiumTypeQuery(_contextSettings));

        public Lazy<DataAccess.PromoCodes.IPromoCodeQuery> PromoCodeQuery
            => new Lazy<DataAccess.PromoCodes.IPromoCodeQuery>(() => new DataAccess.PromoCodes.PromoCodeQuery(_contextSettings));

        public Lazy<DataAccess.PromoCodeStates.IPromoCodeStateQuery> PromoCodeStateQuery
            => new Lazy<DataAccess.PromoCodeStates.IPromoCodeStateQuery>(() => new DataAccess.PromoCodeStates.PromoCodeStateQuery(_contextSettings));

        public Lazy<DataAccess.Quotations.IQuotationQuery> QuotationQuery
            => new Lazy<DataAccess.Quotations.IQuotationQuery>(() => new DataAccess.Quotations.QuotationQuery(_contextSettings));

        public Lazy<DataAccess.SelfManaged.ISelfManagedQuery> SelfManagedQuery
            => new Lazy<DataAccess.SelfManaged.ISelfManagedQuery>(() => new DataAccess.SelfManaged.SelfManagedQuery(_contextSettings));

        public Lazy<DataAccess.Tenanted.ITenantedQuery> TenantedQuery
            => new Lazy<DataAccess.Tenanted.ITenantedQuery>(() => new DataAccess.Tenanted.TenantedQuery(_contextSettings));

        public Lazy<DataAccess.TransactionPolicyHistories.ITransactionPolicyHistoryQuery> TransactionPolicyHistoryQuery
            => new Lazy<DataAccess.TransactionPolicyHistories.ITransactionPolicyHistoryQuery>(() => new DataAccess.TransactionPolicyHistories.TransactionPolicyHistoryQuery(_contextSettings));

        #endregion

        public void Dispose()
        {
            if (_contextSettings != null)
            {
                if (_contextSettings.Transaction != null)
                {
                    _contextSettings.Transaction.Dispose();
                    _contextSettings.Transaction = null;
                }
                if (_contextSettings.SqlConnection != null)
                {
                    _contextSettings.SqlConnection.Close();
                    _contextSettings.SqlConnection = null;
                }
            }
        }

    }
}
