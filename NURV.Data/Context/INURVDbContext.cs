﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;
using System.Runtime.CompilerServices;

[assembly: InternalsVisibleTo("UnitTestCode")]
namespace NURV.Data.Context
{
    internal interface INURVDbContext : IDisposable
    {
        #region Data - Entity Objects

        DbSet<Model.ArrearsClause> ArrearsClauses { get; set; }
        DbSet<Model.ArrearsWeekRange> ArrearsWeekRanges { get; set; }
        DbSet<Model.BankCategory> BankCategories { get; set; }
        DbSet<Model.Banking> Bankings { get; set; }
        DbSet<Model.BaseRate> BaseRates { get; set; }
        DbSet<Model.BonusBackup> BonusBackups { get; set; }
        DbSet<Model.BonusOffer> BonusOffers { get; set; }
        DbSet<Model.BonusTime> BonusTimes { get; set; }
        DbSet<Model.Branch> Branches { get; set; }
        DbSet<Model.BuildingSumInsuredExcess> BuildingSumInsuredExcesses { get; set; }
        DbSet<Model.BushFire> BushFires { get; set; }
        DbSet<Model.BushFireRiskLevel> BushFireRiskLevels { get; set; }
        DbSet<Model.CCTV> CCTVs { get; set; }
        DbSet<Model.Claim> Claim { get; set; }
        DbSet<Model.ClassOfRisk> ClassOfRisks { get; set; }
        DbSet<Model.ConstructionType> ConstructionTypes { get; set; }
        DbSet<Model.ContactSpecialCondition> ContactSpecialConditions { get; set; }
        DbSet<Model.ContentsCover> ContentCovers { get; set; }
        DbSet<Model.ContentsCoverExcess> ContentCoverExcesses { get; set; }
        DbSet<Model.DisclosureClaim> DisclosureClaims { get; set; }
        DbSet<Model.DisclosureClass> DisclosureClasses { get; set; }
        DbSet<Model.DisclosureLossAmount> DisclosureLossAmounts { get; set; }
        DbSet<Model.DisclosureQuestion> DisclosureQuestions { get; set; }
        DbSet<Model.DisclosureType> DisclosureTypes { get; set; }
        DbSet<Model.DwellingType> DwellingTypes { get; set; }
        DbSet<Model.ElectronicAccess> ElectronicAccesses { get; set; }
        DbSet<Model.Embargo> Embargos { get; set; }
        DbSet<Model.EmbargoReason> EmbargoReasons { get; set; }
        DbSet<Model.Endorsement> Endorsements { get; set; }
        DbSet<Model.FloodCover> FloodCovers { get; set; }
        DbSet<Model.FloodRiskLevel> FloodRiskLevels { get; set; }
        DbSet<Model.Furnished> Furnished { get; set; }
        DbSet<Model.HeardAboutTSIB> HeardAboutTSIBs { get; set; }
        DbSet<Model.HoldCoverInfo> HoldCoverInfos { get; set; }
        DbSet<Model.Holiday> Holidays { get; set; }
        DbSet<Model.InformedByWho> InformedByWhos { get; set; }
        DbSet<Model.InformedHow> InformedHows { get; set; }
        DbSet<Model.LandSize> LandSizes { get; set; }
        DbSet<Model.LegalLiability> LegalLiabilities { get; set; }
        DbSet<Model.LossRatioAnalysisPenalty> LossRatioAnalysisPenalties { get; set; }
        DbSet<Model.Message> Messages { get; set; }
        DbSet<Model.MessageAction> MessageActions { get; set; }
        DbSet<Model.OtherClaimsExcess> OtherClaimsExcesses { get; set; }
        DbSet<Model.PaymentMethod> PaymentMethods { get; set; }
        DbSet<Model.PetDamageExcess> PetDamageExcesses { get; set; }
        DbSet<Model.PolicyNumber> PolicyNumbers { get; set; }
        DbSet<Model.Postcode> Postcodes { get; set; }
        DbSet<Model.PremiumPaidBy> PremiumPaidBys { get; set; }
        DbSet<Model.PremiumType> PremiumTypes { get; set; }
        DbSet<Model.PromoCode> PromoCodes { get; set; }
        DbSet<Model.PromoCodeState> PromoCodeStates { get; set; }
        DbSet<Model.Quotation> Quotations { get; set; }
        DbSet<Model.RatesMpd> RatesMpds { get; set; }
        DbSet<Model.ReaGroupDiscount> ReaGroupDiscounts { get; set; }
        DbSet<Model.Refund> Refunds { get; set; }
        DbSet<Model.SelfManaged> SelfManaged { get; set; }
        DbSet<Model.Settings> Settings { get; set; }
        DbSet<Model.Staff> Staff { get; set; }
        DbSet<Model.StatusOfClaim> StatusOfClaims { get; set; }
        DbSet<Model.StatusType> StatusTypes { get; set; }
        DbSet<Model.TenantDamageExcess> TenantDamageExcesses { get; set; }
        DbSet<Model.Tenanted> Tenanted { get; set; }
        DbSet<Model.Transaction> Transactions { get; set; }
        DbSet<Model.TransactionPDSandFSG> TransactionPDSandFSGs { get; set; }
        DbSet<Model.TransactionPenalty> TransactionPenalties { get; set; }
        DbSet<Model.UnoccupiedExcess> UnoccupiedExcesses { get; set; }
        DbSet<Model.User> Users { get; set; }
        DbSet<Model.WeeklyRentCover> WeeklyRentCovers { get; set; }

        #endregion

        #region Stored Procedure - Entity Objects

        DbSet<Model.StoredProcedures.TransactionPolicyHistory> TransactionPolicyHistories { get; set; }

        #endregion
    }
}
