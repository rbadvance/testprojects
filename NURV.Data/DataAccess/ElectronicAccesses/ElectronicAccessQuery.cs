﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;

namespace NURV.Data.DataAccess.ElectronicAccesses
{
    internal class ElectronicAccessQuery : Common.RepositoryBase, IElectronicAccessQuery
    {
        internal ElectronicAccessQuery(Common.ContextSettings contextSettings) : base(contextSettings)
        {

        }

        public async Task<Common.IResult<IList<DTO.DTElectronicAccessForLookup>>> GetElectronicAccessForLookupAsync(int classOfRiskId, bool includeUnknown, DateTime? effectiveDate, string state, Types.PremiumType premiumType, CancellationToken cancelToken)
        {
            var result = new Common.Result<IList<DTO.DTElectronicAccessForLookup>>();

            try
            {
                using (Context.INURVDbContext context = Context.NURVDbContext.CreateDbContext(_contextSettings))
                {
                    IQueryable<Model.ElectronicAccess> electronicAccessQuery = context.ElectronicAccesses
                        .Where(e => e.ClassOfRiskID == classOfRiskId
                                        && e.PremiumTypeID == (int)premiumType
                                        && e.State == (state ?? "NSW"));

                    if (!includeUnknown)
                    {
                        electronicAccessQuery = electronicAccessQuery.Where(e => e.IsUnknown == false);
                    }
                    if (effectiveDate.HasValue)
                    {
                        electronicAccessQuery = electronicAccessQuery.Where(e => effectiveDate.Value >= e.StartEffectiveDate && effectiveDate.Value <= e.EndEffectiveDate);
                    }

                    result.Value = await (from e in electronicAccessQuery
                                          select new DTO.DTElectronicAccessForLookup()
                                          {
                                              Id = e.ID,
                                              Name = e.ElectronicAccessName
                                          }).ToListAsync(cancelToken).ConfigureAwait(continueOnCapturedContext: false);
                }

                result.HasSucceeded = true;
            }
            catch (Exception ex)
            {
                result.Message = "Error loading Electronic Access for look up";
                result.ExceptionDetail = ex.ToString();
            }

            return result;
        }
    }
}
