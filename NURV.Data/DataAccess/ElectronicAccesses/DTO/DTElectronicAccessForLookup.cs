﻿using System;
using System.Collections.Generic;
using System.Text;

namespace NURV.Data.DataAccess.ElectronicAccesses.DTO
{
    public class DTElectronicAccessForLookup
    {
        public int Id { get; set; }
        public string Name { get; set; }
    }
}
