﻿using System;
using System.Collections.Generic;
using System.Text;

namespace NURV.Data.DataAccess.PremiumPaidBys.DTO
{
    public class DTPremiumPaidByForLookup
    {
        public int Id { get; set; }
        public string Name { get; set; }
    }
}
