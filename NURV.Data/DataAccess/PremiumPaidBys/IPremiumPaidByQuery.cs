﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace NURV.Data.DataAccess.PremiumPaidBys
{
    public interface IPremiumPaidByQuery
    {
        Task<Common.IResult<IList<DTO.DTPremiumPaidByForLookup>>> GetPremiumPaidByForLookupAsync(CancellationToken cancelToken);
    }
}
