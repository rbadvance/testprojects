﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Storage;
namespace NURV.Data.DataAccess.PremiumPaidBys
{
    internal class PremiumPaidByQuery : Common.RepositoryBase, IPremiumPaidByQuery
    {
        internal PremiumPaidByQuery(Common.ContextSettings contextSettings) : base(contextSettings)
        {

        }

        public async Task<Common.IResult<IList<DTO.DTPremiumPaidByForLookup>>> GetPremiumPaidByForLookupAsync(CancellationToken cancelToken)
        {
            var result = new Common.Result<IList<DTO.DTPremiumPaidByForLookup>>();

            try
            {
                using (Context.INURVDbContext context = Context.NURVDbContext.CreateDbContext(_contextSettings))
                {
                    result.Value = await (from p in context.PremiumPaidBys
                                          select new DTO.DTPremiumPaidByForLookup()
                                          {
                                              Id = p.PremiumPaidByID,
                                              Name = p.PremiumPaidByName
                                          }).ToListAsync(cancelToken).ConfigureAwait(continueOnCapturedContext: false);
                }

                result.HasSucceeded = true;
            }
            catch (Exception ex)
            {
                result.Message = "Error loading Premium Paid By for look up";
                result.ExceptionDetail = ex.ToString();
            }

            return result;
        }
    }
}
