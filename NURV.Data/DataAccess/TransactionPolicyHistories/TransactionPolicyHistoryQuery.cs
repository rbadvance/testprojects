﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using Microsoft.Data.SqlClient;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Storage;

namespace NURV.Data.DataAccess.TransactionPolicyHistories
{
    internal class TransactionPolicyHistoryQuery : Common.RepositoryBase, ITransactionPolicyHistoryQuery
    {
        internal TransactionPolicyHistoryQuery(Common.ContextSettings contextSettings) : base(contextSettings)
        {

        }

        public async Task<Common.IResult<IList<DTO.DTTransactionPolicyHistory>>> GetTransactionPolicyHistoryAsync(int transactionNumber, CancellationToken cancelToken)
        {
            var result = new Common.Result<IList<DTO.DTTransactionPolicyHistory>>();

            try
            {
                using (Context.INURVDbContext context = Context.NURVDbContext.CreateDbContext(_contextSettings))
                {
                    List<Model.StoredProcedures.TransactionPolicyHistory> query = await context.TransactionPolicyHistories
                    .FromSqlRaw("EXECUTE dbo.[procTransaction_PolicyHistory] {0}", transactionNumber)
                    .ToListAsync(cancelToken).ConfigureAwait(continueOnCapturedContext: false);

                    // convert the entity result to a DTO result
                    result.Value = query.Select(h => new DTO.DTTransactionPolicyHistory()
                    {
                        TransactionNumber = h.TransactionNumber,
                        ParentTransaction = h.ParentTransaction
                    }).ToList();
                }

                result.HasSucceeded = true;
            }
            catch (Exception ex)
            {
                result.Message = "Error loading Transaction History";
                result.ExceptionDetail = ex.ToString();
            }

            return result;
        }
    }
}
