﻿using System;
using System.Collections.Generic;
using System.Text;
using System.ComponentModel.DataAnnotations;

namespace NURV.Data.DataAccess.TransactionPolicyHistories.DTO
{
    public class DTTransactionPolicyHistory
    {
        public int TransactionNumber { get; set; }
        public int? OriginalTransactionNumber { get; set; }
        public int? ParentTransaction { get; set; }
    }
}
