﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace NURV.Data.DataAccess.TransactionPolicyHistories
{
    public interface ITransactionPolicyHistoryQuery
    {
        Task<Common.IResult<IList<DTO.DTTransactionPolicyHistory>>> GetTransactionPolicyHistoryAsync(int transactionNumber, CancellationToken cancelToken);
    }
}
