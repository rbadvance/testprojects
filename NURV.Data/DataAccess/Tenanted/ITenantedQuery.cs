﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace NURV.Data.DataAccess.Tenanted
{
    public interface ITenantedQuery
    {
        Task<Common.IResult<IList<DTO.DTTenantedForLookup>>> GetTenantedForLookupAsync(int classOfRiskId, bool includeUnknown, DateTime? effectiveDate, CancellationToken cancelToken);
    }
}
