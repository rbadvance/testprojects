﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;

namespace NURV.Data.DataAccess.Tenanted
{
    internal class TenantedQuery : Common.RepositoryBase, ITenantedQuery
    {
        internal TenantedQuery(Common.ContextSettings contextSettings) : base(contextSettings)
        {

        }

        public async Task<Common.IResult<IList<DTO.DTTenantedForLookup>>> GetTenantedForLookupAsync(int classOfRiskId, bool includeUnknown, DateTime? effectiveDate, CancellationToken cancelToken)
        {
            var result = new Common.Result<IList<DTO.DTTenantedForLookup>>();

            try
            {
                using (Context.INURVDbContext context = Context.NURVDbContext.CreateDbContext(_contextSettings))
                {
                    IQueryable<Model.Tenanted> tenantedQuery = context.Tenanted
                        .Where(t => t.ClassOfRiskID == classOfRiskId);

                    if (!includeUnknown)
                    {
                        tenantedQuery = tenantedQuery.Where(t => t.IsUnknown == false);
                    }
                    if (effectiveDate.HasValue)
                    {
                        tenantedQuery = tenantedQuery.Where(t => effectiveDate.Value >= t.StartEffectiveDate && effectiveDate.Value <= t.EndEffectiveDate);
                    }

                    result.Value = await (from t in tenantedQuery
                                          select new DTO.DTTenantedForLookup()
                                          {
                                              Id = t.TenantedID,
                                              Name = t.TenantedName
                                          }).ToListAsync(cancelToken).ConfigureAwait(continueOnCapturedContext: false);
                }

                result.HasSucceeded = true;
            }
            catch (Exception ex)
            {
                result.Message = "Error loading Tenanted for look up";
                result.ExceptionDetail = ex.ToString();
            }

            return result;
        }
    }
}
