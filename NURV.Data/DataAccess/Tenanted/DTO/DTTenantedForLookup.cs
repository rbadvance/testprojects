﻿using System;
using System.Collections.Generic;
using System.Text;

namespace NURV.Data.DataAccess.Tenanted.DTO
{
    public class DTTenantedForLookup
    {
        public int Id { get; set; }
        public string Name { get; set; }
    }
}
