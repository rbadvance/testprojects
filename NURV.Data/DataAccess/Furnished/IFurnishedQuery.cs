﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace NURV.Data.DataAccess.Furnished
{
    public interface IFurnishedQuery
    {
        Task<Common.IResult<IList<DTO.DTFurnishedForLookup>>> GetFurnishedForLookupAsync(int classOfRiskId, bool includeUnknown, DateTime? effectiveDate, string state, Types.PremiumType premiumType, CancellationToken cancelToken);
    }
}
