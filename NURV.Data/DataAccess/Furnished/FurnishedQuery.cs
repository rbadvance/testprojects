﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;

namespace NURV.Data.DataAccess.Furnished
{
    internal class FurnishedQuery : Common.RepositoryBase, IFurnishedQuery
    {
        internal FurnishedQuery(Common.ContextSettings contextSettings) : base(contextSettings)
        {

        }

        public async Task<Common.IResult<IList<DTO.DTFurnishedForLookup>>> GetFurnishedForLookupAsync(int classOfRiskId, bool includeUnknown, DateTime? effectiveDate, string state, Types.PremiumType premiumType, CancellationToken cancelToken)
        {
            var result = new Common.Result<IList<DTO.DTFurnishedForLookup>>();

            try
            {
                using (Context.INURVDbContext context = Context.NURVDbContext.CreateDbContext(_contextSettings))
                {
                    IQueryable<Model.Furnished> furnishedQuery = context.Furnished
                        .Where(f => f.ClassOfRiskID == classOfRiskId
                                        && f.PremiumTypeID == (int)premiumType
                                        && f.State == (state ?? "NSW"));

                    if (!includeUnknown)
                    {
                        furnishedQuery = furnishedQuery.Where(f => f.IsUnknown == false);
                    }
                    if (effectiveDate.HasValue)
                    {
                        furnishedQuery = furnishedQuery.Where(f => effectiveDate.Value >= f.StartEffectiveDate && effectiveDate.Value <= f.EndEffectiveDate);
                    }

                    result.Value = await (from f in furnishedQuery
                                          select new DTO.DTFurnishedForLookup()
                                          {
                                              Id = f.ID,
                                              Name = f.FurnishedName
                                          }).ToListAsync(cancelToken).ConfigureAwait(continueOnCapturedContext: false);
                }

                result.HasSucceeded = true;
            }
            catch (Exception ex)
            {
                result.Message = "Error loading Furnished for look up";
                result.ExceptionDetail = ex.ToString();
            }

            return result;
        }
    }
}
