﻿using System;
using System.Collections.Generic;
using System.Text;

namespace NURV.Data.DataAccess.Furnished.DTO
{
    public class DTFurnishedForLookup
    {
        public int Id { get; set; }
        public string Name { get; set; }
    }
}
