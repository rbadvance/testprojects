﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace NURV.Data.DataAccess.PromoCodes
{
    public interface IPromoCodeQuery
    {
        Task<Common.IResult<IList<DTO.DTPromoCodeForLookup>>> GetPromoCodeForLookupAsync(CancellationToken cancelToken);
    }
}
