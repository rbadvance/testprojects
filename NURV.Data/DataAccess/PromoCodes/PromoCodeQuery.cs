﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;

namespace NURV.Data.DataAccess.PromoCodes
{
    internal class PromoCodeQuery : Common.RepositoryBase, IPromoCodeQuery
    {
        internal PromoCodeQuery(Common.ContextSettings contextSettings) : base(contextSettings)
        {

        }

        public async Task<Common.IResult<IList<DTO.DTPromoCodeForLookup>>> GetPromoCodeForLookupAsync(CancellationToken cancelToken)
        {
            var result = new Common.Result<IList<DTO.DTPromoCodeForLookup>>();

            try
            {
                using (Context.INURVDbContext context = Context.NURVDbContext.CreateDbContext(_contextSettings))
                {
                    result.Value = await (from c in context.PromoCodes
                                          select new DTO.DTPromoCodeForLookup()
                                          {
                                              Id = c.PromoCodeID,
                                              Name = c.PromoCodeName
                                          }).ToListAsync(cancelToken).ConfigureAwait(continueOnCapturedContext: false);
                }

                result.HasSucceeded = true;
            }
            catch (Exception ex)
            {
                result.Message = "Error loading Promo Codes for look up";
                result.ExceptionDetail = ex.ToString();
            }

            return result;
        }
    }
}
