﻿using System;
using System.Collections.Generic;
using System.Text;

namespace NURV.Data.DataAccess.PromoCodes.DTO
{
    public class DTPromoCodeForLookup
    {
        public short Id { get; set; }
        public string Name { get; set; }
    }
}
