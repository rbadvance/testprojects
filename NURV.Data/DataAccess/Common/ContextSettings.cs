﻿using System;
using System.Collections.Generic;
using System.Text;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Storage;
using System.Data.Common;
using Microsoft.Data.SqlClient;

namespace NURV.Data.DataAccess.Common
{
    internal class ContextSettings
    {
        public bool TrackingOn { get; set; }

        public bool UseTransaction { get; set; }
        
        internal DbContextOptions DbOptions { get; set; }
        internal SqlConnection SqlConnection { get; set; }

        /// <summary>
        /// Use a transaction, which can be shared across multiple instances of the same DbContext.
        /// ...allows changes to be tied together in a single transaction scope (using commit or rollback).
        /// </summary>
        internal SqlTransaction Transaction { get; set; }
    }
}
