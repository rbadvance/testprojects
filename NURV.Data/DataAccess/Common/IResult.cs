﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NURV.Data.DataAccess.Common
{
    /// <summary>
    /// Base class for Result properties
    /// </summary>
    public interface IResult
    {
        bool HasSucceeded { get; set; }

        string Message { get; set; }

        /// <remarks>Exception.ToString()</remarks>
        string ExceptionDetail { get; set; }
    }

    /// <summary>
    /// Base class for Result properties using a generic return type value
    /// </summary>
    public interface IResult<T> : IResult 
    {
        T Value { get; set; }
    }
}
