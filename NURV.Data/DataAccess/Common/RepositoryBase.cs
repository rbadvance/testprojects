﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore.Storage;

namespace NURV.Data.DataAccess.Common
{
    internal class RepositoryBase
    {
        protected private ContextSettings _contextSettings;

        internal RepositoryBase(ContextSettings contextSettings)
        {
            _contextSettings = contextSettings ?? new ContextSettings();
        }
    }
}
