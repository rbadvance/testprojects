﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NURV.Data.DataAccess.Common
{
    /// <inheritdoc/>
    internal class Result : IResult
    {
        public bool HasSucceeded { get; set; }

        public string Message { get; set; }

        /// <inheritdoc/>
        public string ExceptionDetail { get; set; }
    }

    /// <inheritdoc/>
    internal class Result<T> : Result, IResult<T> 
    {
        public T Value { get; set; }
    }
}
