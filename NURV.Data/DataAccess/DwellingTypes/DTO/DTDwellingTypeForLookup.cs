﻿using System;
using System.Collections.Generic;
using System.Text;

namespace NURV.Data.DataAccess.DwellingTypes.DTO
{
    public class DTDwellingTypeForLookup
    {
        public int Id { get; set; }
        public string Name { get; set; }
    }
}
