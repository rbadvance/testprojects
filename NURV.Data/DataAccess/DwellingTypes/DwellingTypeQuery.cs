﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Storage;

namespace NURV.Data.DataAccess.DwellingTypes
{
    internal class DwellingTypeQuery : Common.RepositoryBase, IDwellingTypeQuery
    {
        internal DwellingTypeQuery(Common.ContextSettings contextSettings) : base(contextSettings)
        {

        }

        public async Task<Common.IResult<IList<DTO.DTDwellingTypeForLookup>>> GetDwellingTypeForLookupAsync(CancellationToken cancelToken)
        {
            var result = new Common.Result<IList<DTO.DTDwellingTypeForLookup>>();

            try
            {
                using (Context.INURVDbContext context = Context.NURVDbContext.CreateDbContext(_contextSettings))
                {
                    result.Value = await (from t in context.DwellingTypes
                                          select new DTO.DTDwellingTypeForLookup()
                                          {
                                              Id = t.DwellingTypeID,
                                              Name = t.DwellingTypeName
                                          }).ToListAsync(cancelToken).ConfigureAwait(continueOnCapturedContext: false);
                }

                result.HasSucceeded = true;
            }
            catch (Exception ex)
            {
                result.Message = "Error loading Dwelling Type for look up";
                result.ExceptionDetail = ex.ToString();
            }

            return result;
        }
    }
}
