﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace NURV.Data.DataAccess.DwellingTypes
{
    public interface IDwellingTypeQuery
    {
        Task<Common.IResult<IList<DTO.DTDwellingTypeForLookup>>> GetDwellingTypeForLookupAsync(CancellationToken cancelToken);
    }
}
