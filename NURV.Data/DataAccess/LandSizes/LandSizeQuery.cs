﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Storage;

namespace NURV.Data.DataAccess.LandSizes
{
    internal class LandSizeQuery : Common.RepositoryBase, ILandSizeQuery
    {
        internal LandSizeQuery(Common.ContextSettings contextSettings) : base(contextSettings)
        {

        }

        public async Task<Common.IResult<IList<DTO.DTLandSizeForLookup>>> GetLandSizeForLookupAsync(int classOfRiskId, bool includeUnknown, DateTime? effectiveDate,  CancellationToken cancelToken)
        {
            var result = new Common.Result<IList<DTO.DTLandSizeForLookup>>();

            try
            {
                using (Context.INURVDbContext context = Context.NURVDbContext.CreateDbContext(_contextSettings))
                {
                    IQueryable<Model.LandSize> landSizeQuery = context.LandSizes.Where(s => s.ClassOfRiskID == classOfRiskId);

                    if (!includeUnknown)
                    {
                        landSizeQuery = landSizeQuery.Where(s => s.IsUnknown == false);
                    }
                    if (effectiveDate.HasValue)
                    {
                        landSizeQuery = landSizeQuery.Where(s => effectiveDate.Value >= s.StartEffectiveDate && effectiveDate.Value <= s.EndEffectiveDate);
                    }    

                    result.Value = await (from s in landSizeQuery
                                          select new DTO.DTLandSizeForLookup()
                                          {
                                              Id = s.LandSizeId,
                                              Name = s.LandSizeName
                                          }).ToListAsync(cancelToken).ConfigureAwait(continueOnCapturedContext: false);
                }

                result.HasSucceeded = true;
            }
            catch (Exception ex)
            {
                result.Message = "Error loading Land Size for look up";
                result.ExceptionDetail = ex.ToString();
            }

            return result;
        }
    }
}
