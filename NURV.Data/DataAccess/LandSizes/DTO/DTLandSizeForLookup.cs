﻿using System;
using System.Collections.Generic;
using System.Text;

namespace NURV.Data.DataAccess.LandSizes.DTO
{
    public class DTLandSizeForLookup
    {
        public int Id { get; set; }
        public string Name { get; set; }
    }
}
