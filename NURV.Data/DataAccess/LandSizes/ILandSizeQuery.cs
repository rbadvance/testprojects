﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace NURV.Data.DataAccess.LandSizes
{
    public interface ILandSizeQuery
    {
        Task<Common.IResult<IList<DTO.DTLandSizeForLookup>>> GetLandSizeForLookupAsync(int classOfRiskId, bool includeUnknown, DateTime? effectiveDate, CancellationToken cancelToken);
    }
}
