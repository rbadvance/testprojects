﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;

namespace NURV.Data.DataAccess.InformedByWhos
{
    internal class InformedByWhoQuery : Common.RepositoryBase, IInformedByWhoQuery
    {
        internal InformedByWhoQuery(Common.ContextSettings contextSettings) : base(contextSettings)
        {

        }

        public async Task<Common.IResult<IList<DTO.DTInformedByWhoForLookup>>> GetInformedByWhoForLookupAsync(CancellationToken cancelToken)
        {
            var result = new Common.Result<IList<DTO.DTInformedByWhoForLookup>>();

            try
            {
                using (Context.INURVDbContext context = Context.NURVDbContext.CreateDbContext(_contextSettings))
                {
                    result.Value = await (from i in context.InformedByWhos
                                          orderby i.ID
                                          select new DTO.DTInformedByWhoForLookup()
                                          {
                                              Id = i.ID,
                                              Data = i.Data
                                          }).ToListAsync(cancelToken).ConfigureAwait(continueOnCapturedContext: false);
                }

                result.HasSucceeded = true;
            }
            catch (Exception ex)
            {
                result.Message = "Error loading Informed By Who for look up";
                result.ExceptionDetail = ex.ToString();
            }

            return result;
        }
    }
}
