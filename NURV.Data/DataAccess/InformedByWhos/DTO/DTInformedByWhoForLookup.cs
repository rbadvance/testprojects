﻿using System;
using System.Collections.Generic;
using System.Text;

namespace NURV.Data.DataAccess.InformedByWhos.DTO
{
    public class DTInformedByWhoForLookup
    {
        public int Id { get; set; }
        public string Data { get; set; }
    }
}
