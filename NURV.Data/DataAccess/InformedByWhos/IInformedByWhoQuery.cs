﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace NURV.Data.DataAccess.InformedByWhos
{
    public interface IInformedByWhoQuery
    {
        Task<Common.IResult<IList<DTO.DTInformedByWhoForLookup>>> GetInformedByWhoForLookupAsync(CancellationToken cancelToken);
    }
}
