﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace NURV.Data.DataAccess.PremiumTypes
{
    public interface IPremiumTypeQuery
    {
        Task<Common.IResult<IList<DTO.DTPremiumTypeForLookup>>> GetPremiumTypeForLookupAsync(CancellationToken cancelToken);
    }
}
