﻿using System;
using System.Collections.Generic;
using System.Text;

namespace NURV.Data.DataAccess.PremiumTypes.DTO
{
    public class DTPremiumTypeForLookup
    {
        public byte Id { get; set; }
        public string Data { get; set; }
    }
}
