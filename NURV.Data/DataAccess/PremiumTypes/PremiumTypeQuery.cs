﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;

namespace NURV.Data.DataAccess.PremiumTypes
{
    internal class PremiumTypeQuery : Common.RepositoryBase, IPremiumTypeQuery
    {
        internal PremiumTypeQuery(Common.ContextSettings contextSettings) : base(contextSettings)
        {

        }

        public async Task<Common.IResult<IList<DTO.DTPremiumTypeForLookup>>> GetPremiumTypeForLookupAsync(CancellationToken cancelToken)
        {
            var result = new Common.Result<IList<DTO.DTPremiumTypeForLookup>>();

            try
            {
                using (Context.INURVDbContext context = Context.NURVDbContext.CreateDbContext(_contextSettings))
                {
                    result.Value = await (from t in context.PremiumTypes
                                          select new DTO.DTPremiumTypeForLookup()
                                          {
                                              Id = t.ID,
                                              Data = t.Data
                                          }).ToListAsync(cancelToken).ConfigureAwait(continueOnCapturedContext: false);
                }

                result.HasSucceeded = true;
            }
            catch (Exception ex)
            {
                result.Message = "Error loading Premium Types for look up";
                result.ExceptionDetail = ex.ToString();
            }

            return result;
        }
    }
}
