﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace NURV.Data.DataAccess.ConstructionTypes
{
    public interface IConstructionTypeQuery
    {
        Task<Common.IResult<IList<DTO.DTConstructionTypeForLookup>>> GetConstructionTypeForLookupAsync(int classOfRiskId, bool includeUnknown, DateTime? effectiveDate, CancellationToken cancelToken);
    }
}
