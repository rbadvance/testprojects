﻿using System;
using System.Collections.Generic;
using System.Text;

namespace NURV.Data.DataAccess.ConstructionTypes.DTO
{
    public class DTConstructionTypeForLookup
    {
        public int Id { get; set; }
        public string Name { get; set; }
    }
}
