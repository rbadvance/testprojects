﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;

namespace NURV.Data.DataAccess.ConstructionTypes
{
    internal class ConstructionTypeQuery : Common.RepositoryBase, IConstructionTypeQuery
    {
        internal ConstructionTypeQuery(Common.ContextSettings contextSettings) : base(contextSettings)
        {

        }

        public async Task<Common.IResult<IList<DTO.DTConstructionTypeForLookup>>> GetConstructionTypeForLookupAsync(int classOfRiskId, bool includeUnknown, DateTime? effectiveDate, CancellationToken cancelToken)
        {
            var result = new Common.Result<IList<DTO.DTConstructionTypeForLookup>>();

            try
            {
                using (Context.INURVDbContext context = Context.NURVDbContext.CreateDbContext(_contextSettings))
                {
                    IQueryable<Model.ConstructionType> constructionTypeQuery = context.ConstructionTypes.Where(t => t.ClassOfRiskID == classOfRiskId);

                    if (!includeUnknown)
                    {
                        constructionTypeQuery = constructionTypeQuery.Where(t => t.IsUnknown == false);
                    }
                    if (effectiveDate.HasValue)
                    {
                        constructionTypeQuery = constructionTypeQuery.Where(t => effectiveDate.Value >= t.StartEffectiveDate && effectiveDate.Value <= t.EndEffectiveDate);
                    }

                    result.Value = await (from t in constructionTypeQuery
                                          select new DTO.DTConstructionTypeForLookup()
                                          {
                                              Id = t.ConstructionTypeID,
                                              Name = t.ConstructionTypeName
                                          }).ToListAsync(cancelToken).ConfigureAwait(continueOnCapturedContext: false);
                }

                result.HasSucceeded = true;
            }
            catch (Exception ex)
            {
                result.Message = "Error loading Construction Types for look up";
                result.ExceptionDetail = ex.ToString();
            }

            return result;
        }
    }
}
