﻿using System;
using System.Collections.Generic;
using System.Text;

namespace NURV.Data.DataAccess.Transactions.DTO
{
    public interface IDTTransaction
    {
        int Id { get; set; }
    }
}
