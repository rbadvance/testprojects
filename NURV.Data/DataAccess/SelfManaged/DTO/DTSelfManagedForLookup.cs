﻿using System;
using System.Collections.Generic;
using System.Text;

namespace NURV.Data.DataAccess.SelfManaged.DTO
{
    public class DTSelfManagedForLookup
    {
        public int Id { get; set; }

        public string Name { get; set; }
    }
}
