﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace NURV.Data.DataAccess.SelfManaged
{
    public interface ISelfManagedQuery
    {
        Task<Common.IResult<IList<DTO.DTSelfManagedForLookup>>> GetSelfManagedForLookupAsync(int classOfRiskId, DateTime? effectiveDate, string state, Types.PremiumType premiumType, CancellationToken cancelToken);
    }
}
