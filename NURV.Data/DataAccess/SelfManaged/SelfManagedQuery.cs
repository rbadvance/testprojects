﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;

namespace NURV.Data.DataAccess.SelfManaged
{
    internal class SelfManagedQuery : Common.RepositoryBase, ISelfManagedQuery
    {
        internal SelfManagedQuery(Common.ContextSettings contextSettings) : base(contextSettings)
        {

        }

        public async Task<Common.IResult<IList<DTO.DTSelfManagedForLookup>>> GetSelfManagedForLookupAsync(int classOfRiskId, DateTime? effectiveDate, string state, Types.PremiumType premiumType, CancellationToken cancelToken)
        {
            var result = new Common.Result<IList<DTO.DTSelfManagedForLookup>>();

            try
            {
                using (Context.INURVDbContext context = Context.NURVDbContext.CreateDbContext(_contextSettings))
                {
                    IQueryable<Model.SelfManaged> selfManagedQuery = context.SelfManaged
                        .Where(s => s.ClassOfRiskID == classOfRiskId
                                        && s.PremiumTypeID == (int)premiumType
                                        && s.State == (state ?? "NSW"));

                    if (effectiveDate.HasValue)
                    {
                        selfManagedQuery = selfManagedQuery.Where(s => effectiveDate.Value >= s.StartEffectiveDate && effectiveDate.Value <= s.EndEffectiveDate);
                    }

                    result.Value = await (from s in selfManagedQuery
                                          orderby s.SelfManagedName descending
                                          select new DTO.DTSelfManagedForLookup()
                                          {
                                              Id = s.SelfManagedID,
                                              Name = s.SelfManagedName
                                          }).ToListAsync(cancelToken).ConfigureAwait(continueOnCapturedContext: false);
                }

                result.HasSucceeded = true;
            }
            catch (Exception ex)
            {
                result.Message = "Error loading Self Managed for look up";
                result.ExceptionDetail = ex.ToString();
            }

            return result;
        }
    }
}
