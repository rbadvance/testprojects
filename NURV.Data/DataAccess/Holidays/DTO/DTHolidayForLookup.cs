﻿using System;
using System.Collections.Generic;
using System.Text;

namespace NURV.Data.DataAccess.Holidays.DTO
{
    public class DTHolidayForLookup
    {
        public int Id { get; set; }
        public string Name { get; set; }
    }
}
