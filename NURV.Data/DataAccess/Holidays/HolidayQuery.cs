﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;

namespace NURV.Data.DataAccess.Holidays
{
    internal class HolidayQuery : Common.RepositoryBase, IHolidayQuery
    {
        internal HolidayQuery(Common.ContextSettings contextSettings) : base(contextSettings)
        {

        }

        public async Task<Common.IResult<IList<DTO.DTHolidayForLookup>>> GetHolidayForLookupAsync(int classOfRiskId, bool includeUnknown, DateTime? effectiveDate, string state, Types.PremiumType premiumType, CancellationToken cancelToken)
        {
            var result = new Common.Result<IList<DTO.DTHolidayForLookup>>();

            try
            {
                using (Context.INURVDbContext context = Context.NURVDbContext.CreateDbContext(_contextSettings))
                {
                    IQueryable<Model.Holiday> holidayQuery = context.Holidays
                        .Where(h => h.ClassOfRiskID == classOfRiskId 
                                        && h.PremiumTypeID == (int)premiumType
                                        && h.State == (state ?? "NSW"));

                    if (!includeUnknown)
                    {
                        holidayQuery = holidayQuery.Where(h => h.IsUnknown == false);
                    }
                    if (effectiveDate.HasValue)
                    {
                        holidayQuery = holidayQuery.Where(h => effectiveDate.Value >= h.StartEffectiveDate && effectiveDate.Value <= h.EndEffectiveDate);
                    }

                    result.Value = await (from h in holidayQuery
                                          orderby h.HolidayName descending
                                          select new DTO.DTHolidayForLookup()
                                          {
                                              Id = h.ID,
                                              Name = h.HolidayName
                                          }).ToListAsync(cancelToken).ConfigureAwait(continueOnCapturedContext: false);
                }

                result.HasSucceeded = true;
            }
            catch (Exception ex)
            {
                result.Message = "Error loading Holidays for look up";
                result.ExceptionDetail = ex.ToString();
            }

            return result;
        }
    }
}
