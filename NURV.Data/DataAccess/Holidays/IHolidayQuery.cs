﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace NURV.Data.DataAccess.Holidays
{
    public interface IHolidayQuery
    {
        Task<Common.IResult<IList<DTO.DTHolidayForLookup>>> GetHolidayForLookupAsync(int classOfRiskId, bool includeUnknown, DateTime? effectiveDate, string state, Types.PremiumType premiumType, CancellationToken cancelToken);
    }
}
