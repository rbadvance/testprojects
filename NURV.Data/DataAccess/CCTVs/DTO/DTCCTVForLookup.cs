﻿using System;
using System.Collections.Generic;
using System.Text;

namespace NURV.Data.DataAccess.CCTVs.DTO
{
    public class DTCCTVForLookup
    {
        public int Id { get; set; }
        public string Name { get; set; }
    }
}
