﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace NURV.Data.DataAccess.CCTVs
{
    public interface ICCTVQuery
    {
        Task<Common.IResult<IList<DTO.DTCCTVForLookup>>> GetCCTVForLookupAsync(int classOfRiskId, bool includeUnknown, DateTime? effectiveDate, string state, Types.PremiumType premiumType, CancellationToken cancelToken);
    }
}
