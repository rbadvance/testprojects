﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;

namespace NURV.Data.DataAccess.CCTVs
{
    internal class CCTVQuery : Common.RepositoryBase, ICCTVQuery
    {
        internal CCTVQuery(Common.ContextSettings contextSettings) : base(contextSettings)
        {

        }

        public async Task<Common.IResult<IList<DTO.DTCCTVForLookup>>> GetCCTVForLookupAsync(int classOfRiskId, bool includeUnknown, DateTime? effectiveDate, string state, Types.PremiumType premiumType, CancellationToken cancelToken)
        {
            var result = new Common.Result<IList<DTO.DTCCTVForLookup>>();

            try
            {
                using (Context.INURVDbContext context = Context.NURVDbContext.CreateDbContext(_contextSettings))
                {
                    IQueryable<Model.CCTV>cctvQuery = context.CCTVs
                        .Where(c => c.ClassOfRiskID == classOfRiskId
                                        && c.PremiumTypeID == (int)premiumType
                                        && c.State == (state ?? "NSW"));

                    if (!includeUnknown)
                    {
                        cctvQuery = cctvQuery.Where(c => c.IsUnknown == false);
                    }
                    if (effectiveDate.HasValue)
                    {
                        cctvQuery = cctvQuery.Where(c => effectiveDate.Value >= c.StartEffectiveDate && effectiveDate.Value <= c.EndEffectiveDate);
                    }

                    result.Value = await (from c in cctvQuery
                                          select new DTO.DTCCTVForLookup()
                                          {
                                              Id = c.ID,
                                              Name = c.CCTVName
                                          }).ToListAsync(cancelToken).ConfigureAwait(continueOnCapturedContext: false);
                }

                result.HasSucceeded = true;
            }
            catch (Exception ex)
            {
                result.Message = "Error loading CCTVs for look up";
                result.ExceptionDetail = ex.ToString();
            }

            return result;
        }
    }
}
