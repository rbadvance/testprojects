﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Storage;

namespace NURV.Data.DataAccess.ContentsCovers
{
    internal class ContentsCoverQuery : Common.RepositoryBase, IContentsCoverQuery
    {
        internal ContentsCoverQuery(Common.ContextSettings contextSettings) : base(contextSettings)
        {

        }

        public async Task<Common.IResult<IList<DTO.DTContentsCoverForLookup>>> GetContentsCoverForLookupAsync(CancellationToken cancelToken)
        {
            var result = new Common.Result<IList<DTO.DTContentsCoverForLookup>>();

            try
            {
                using (Context.INURVDbContext context = Context.NURVDbContext.CreateDbContext(_contextSettings))
                {
                    result.Value = await (from c in context.ContentCovers
                                          select new DTO.DTContentsCoverForLookup()
                                          {
                                              Id = c.OptionID,
                                              ContentsSumInsured = c.ContentsSumInsured
                                          }).ToListAsync(cancelToken).ConfigureAwait(continueOnCapturedContext: false);
                }

                result.HasSucceeded = true;
            }
            catch (Exception ex)
            {
                result.Message = "Error loading Contents Cover for look up";
                result.ExceptionDetail = ex.ToString();
            }

            return result;
        }
    }
}
