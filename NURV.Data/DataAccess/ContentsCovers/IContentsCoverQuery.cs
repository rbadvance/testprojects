﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace NURV.Data.DataAccess.ContentsCovers
{
    public interface IContentsCoverQuery
    {
        Task<Common.IResult<IList<DTO.DTContentsCoverForLookup>>> GetContentsCoverForLookupAsync(CancellationToken cancelToken);
    }
}
