﻿using System;
using System.Collections.Generic;
using System.Text;

namespace NURV.Data.DataAccess.ContentsCovers.DTO
{
    public class DTContentsCoverForLookup
    {
        public int Id { get; set; }
        public string ContentsSumInsured { get; set; }
    }
}
