﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Storage;

namespace NURV.Data.DataAccess.ClassesOfRisk
{
    internal class ClassOfRiskQuery : Common.RepositoryBase, IClassOfRiskQuery
    {
        internal ClassOfRiskQuery(Common.ContextSettings contextSettings) : base(contextSettings)
        {

        }

        public async Task<Common.IResult<IList<DTO.DTClassOfRiskForLookup>>> GetClassOfRiskForLookupAsync(CancellationToken cancelToken)
        {
            var result = new Common.Result<IList<DTO.DTClassOfRiskForLookup>>();

            try
            {
                using (Context.INURVDbContext context = Context.NURVDbContext.CreateDbContext(_contextSettings))
                {
                    result.Value = await (from c in context.ClassOfRisks
                                          where c.Displayed
                                          select new DTO.DTClassOfRiskForLookup()
                                          {
                                              Id = c.ID,
                                              Data = c.Data
                                          }).ToListAsync(cancelToken).ConfigureAwait(continueOnCapturedContext: false);
                }

                result.HasSucceeded = true;
            }
            catch (Exception ex)
            {
                result.Message = "Error loading Class of Risk for look up";
                result.ExceptionDetail = ex.ToString();
            }

            return result;
        }
    }
}
