﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace NURV.Data.DataAccess.ClassesOfRisk
{
    public interface IClassOfRiskQuery
    {
        Task<Common.IResult<IList<DTO.DTClassOfRiskForLookup>>> GetClassOfRiskForLookupAsync(CancellationToken cancelToken);
    }
}
