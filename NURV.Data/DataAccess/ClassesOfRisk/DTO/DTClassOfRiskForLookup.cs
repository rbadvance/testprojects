﻿using System;
using System.Collections.Generic;
using System.Text;

namespace NURV.Data.DataAccess.ClassesOfRisk.DTO
{
    public class DTClassOfRiskForLookup
    {
        public short Id { get; set; }
        public string Data { get; set; }
    }
}
