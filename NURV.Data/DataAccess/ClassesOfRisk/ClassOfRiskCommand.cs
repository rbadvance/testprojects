﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Storage;

namespace NURV.Data.DataAccess.ClassesOfRisk
{
    internal class ClassOfRiskCommand : Common.RepositoryBase, IClassOfRiskCommand
    {
        internal ClassOfRiskCommand(Common.ContextSettings contextSettings) : base(contextSettings)
        {

        }
    }
}
