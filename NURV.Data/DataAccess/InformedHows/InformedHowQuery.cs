﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Storage;

namespace NURV.Data.DataAccess.InformedHows
{
    internal class InformedHowQuery : Common.RepositoryBase, IInformedHowQuery
    {
        internal InformedHowQuery(Common.ContextSettings contextSettings) : base(contextSettings)
        {

        }

        public async Task<Common.IResult<IList<DTO.DTInformedHowForLookup>>> GetInformedHowForLookupAsync(CancellationToken cancelToken)
        {
            var result = new Common.Result<IList<DTO.DTInformedHowForLookup>>();

            try
            {
                using (Context.INURVDbContext context = Context.NURVDbContext.CreateDbContext(_contextSettings))
                {
                    result.Value = await (from h in context.InformedHows
                                          select new DTO.DTInformedHowForLookup()
                                          {
                                              Id = h.ID,
                                              Data = h.Data
                                          }).ToListAsync(cancelToken).ConfigureAwait(continueOnCapturedContext: false);
                }

                result.HasSucceeded = true;
            }
            catch (Exception ex)
            {
                result.Message = "Error loading informed How for look up";
                result.ExceptionDetail = ex.ToString();
            }

            return result;
        }
    }
}
