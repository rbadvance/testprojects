﻿using System;
using System.Collections.Generic;
using System.Text;

namespace NURV.Data.DataAccess.InformedHows.DTO
{
    public class DTInformedHowForLookup
    {
        public int Id { get; set; }
        public string Data { get; set; }
    }
}
