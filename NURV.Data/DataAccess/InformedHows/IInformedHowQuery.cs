﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace NURV.Data.DataAccess.InformedHows
{
    public interface IInformedHowQuery
    {
        Task<Common.IResult<IList<DTO.DTInformedHowForLookup>>> GetInformedHowForLookupAsync(CancellationToken cancelToken);
    }
}
