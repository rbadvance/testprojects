﻿using System;
using System.Collections.Generic;
using System.Text;

namespace NURV.Data.DataAccess.OtherClaimsExcesses.DTO
{
    public class DTOtherClaimsExcessForLookup
    {
        public int Id { get; set; }
        public decimal ClaimExcess { get; set; }
    }
}
