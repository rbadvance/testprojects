﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace NURV.Data.DataAccess.OtherClaimsExcesses
{
    public interface IOtherClaimsExcessQuery
    {
        Task<Common.IResult<IList<DTO.DTOtherClaimsExcessForLookup>>> GetOtherClaimsExcessForLookupAsync(int classOfRiskId, DateTime? effectiveDate, string state, Types.PremiumType premiumType, CancellationToken cancelToken);
    }
}
