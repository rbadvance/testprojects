﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;

namespace NURV.Data.DataAccess.OtherClaimsExcesses
{
    internal class OtherClaimsExcessQuery : Common.RepositoryBase, IOtherClaimsExcessQuery
    {
        internal OtherClaimsExcessQuery(Common.ContextSettings contextSettings) : base(contextSettings)
        {

        }

        public async Task<Common.IResult<IList<DTO.DTOtherClaimsExcessForLookup>>> GetOtherClaimsExcessForLookupAsync(int classOfRiskId, DateTime? effectiveDate, string state, Types.PremiumType premiumType, CancellationToken cancelToken)
        {
            var result = new Common.Result<IList<DTO.DTOtherClaimsExcessForLookup>>();

            try
            {
                using (Context.INURVDbContext context = Context.NURVDbContext.CreateDbContext(_contextSettings))
                {
                    IQueryable<Model.OtherClaimsExcess> otherClaimsExcessQuery = context.OtherClaimsExcesses
                        .Where(e => e.ClassOfRiskID == classOfRiskId
                                        && e.PremiumTypeID == (int)premiumType
                                        && e.State == (state ?? "NSW"));

                    if (effectiveDate.HasValue)
                    {
                        otherClaimsExcessQuery = otherClaimsExcessQuery.Where(e => effectiveDate.Value >= e.StartEffectiveDate && effectiveDate.Value <= e.EndEffectiveDate);
                    }

                    result.Value = await (from e in otherClaimsExcessQuery
                                          select new DTO.DTOtherClaimsExcessForLookup()
                                          {
                                              Id = e.OtherClaimsExcessID,
                                              ClaimExcess = e.ClaimExcess
                                          }).ToListAsync(cancelToken).ConfigureAwait(continueOnCapturedContext: false);
                }

                result.HasSucceeded = true;
            }
            catch (Exception ex)
            {
                result.Message = "Error loading Other Claims Excess for look up";
                result.ExceptionDetail = ex.ToString();
            }

            return result;
        }
    }
}
