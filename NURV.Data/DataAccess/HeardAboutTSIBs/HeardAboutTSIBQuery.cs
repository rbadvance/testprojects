﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Storage;

namespace NURV.Data.DataAccess.HeardAboutTSIBs
{
    internal class HeardAboutTSIBQuery : Common.RepositoryBase, IHeardAboutTSIBQuery
    {
        internal HeardAboutTSIBQuery(Common.ContextSettings contextSettings) : base(contextSettings)
        {

        }

        public async Task<Common.IResult<IList<DTO.DTHeardAboutTSIBForLookup>>> GetHeardAboutTSIBForLookupAsync(CancellationToken cancelToken)
        {
            var result = new Common.Result<IList<DTO.DTHeardAboutTSIBForLookup>>();

            try
            {
                using (Context.INURVDbContext context = Context.NURVDbContext.CreateDbContext(_contextSettings))
                {
                    result.Value = await (from h in context.HeardAboutTSIBs
                                          select new DTO.DTHeardAboutTSIBForLookup()
                                          {
                                              Id = h.ID,
                                              Name = h.HeardAboutTSIBName
                                          }).ToListAsync(cancelToken).ConfigureAwait(continueOnCapturedContext: false);
                }

                result.HasSucceeded = true;
            }
            catch (Exception ex)
            {
                result.Message = "Error loading Heard About TSIB for look up";
                result.ExceptionDetail = ex.ToString();
            }

            return result;
        }
    }
}
