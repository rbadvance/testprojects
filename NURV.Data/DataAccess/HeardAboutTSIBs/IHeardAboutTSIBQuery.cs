﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace NURV.Data.DataAccess.HeardAboutTSIBs
{
    public interface IHeardAboutTSIBQuery
    {
        Task<Common.IResult<IList<DTO.DTHeardAboutTSIBForLookup>>> GetHeardAboutTSIBForLookupAsync(CancellationToken cancelToken);
    }
}
