﻿using System;
using System.Collections.Generic;
using System.Text;

namespace NURV.Data.DataAccess.HeardAboutTSIBs.DTO
{
    public class DTHeardAboutTSIBForLookup
    {
        public byte Id { get; set; }
        public string Name { get; set; }
    }
}
