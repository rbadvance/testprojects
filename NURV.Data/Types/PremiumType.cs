﻿using System;
using System.Collections.Generic;
using System.Text;

namespace NURV.Data.Types
{
    public enum PremiumType
    {
        None = 0,
        ExtraPremium = 1,
        ReturnPremium = 2,
        NewBusiness = 3,
        Renewal = 4,
        Transfer = 5,
        Lapse = 6,
        Journal = 7,
        Endorsement = 8,
        NewPremium = 9,
        Reversal = 10,
        Quotation = 11
    }
}
