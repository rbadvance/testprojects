﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace NURV.Data.Model
{
    [Table("tblArrearsClause")]
    internal class ArrearsClause : Common.EntityObjectBase
    {
        [Key]
        [Column("ArrearsClauseID")]
        public int ArrearsClauseId { get; set; }
        [Column("ClassOfRiskID")]
        public int? ClassOfRiskId { get; set; }
        public bool? ArrearsClauseNotified { get; set; }
        [StringLength(20)]
        public string TenantInArrears { get; set; }
        [Column("ArrearsClauseStatusID")]
        public int? ArrearsClauseStatusId { get; set; }
        [Column("ArrearsClauseMessage1ID")]
        [StringLength(50)]
        public string ArrearsClauseMessage1Id { get; set; }
        [Column("ArrearsClauseMessage2ID")]
        [StringLength(50)]
        public string ArrearsClauseMessage2Id { get; set; }
        [Column("ArrearsClauseMessage3ID")]
        [StringLength(50)]
        public string ArrearsClauseMessage3Id { get; set; }
        public bool? ArrearsClauseActive { get; set; }
    }
}
