﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace NURV.Data.Model
{
    [Table("tblRFUnoccupiedExcess")]
    internal class UnoccupiedExcess : Common.EntityObjectBase
    {
        [Column("UnoccupiedExcessID")]
        public int? UnoccupiedExcessId { get; set; }
        [Column("UnoccupiedExcess", TypeName = "money")]
        public decimal? Excess { get; set; }
        [Column("ClassOfRiskID")]
        public int? ClassOfRiskId { get; set; }
        [Column(TypeName = "datetime")]
        public DateTime? StartEffectiveDate { get; set; }
        [Column(TypeName = "datetime")]
        public DateTime? EndEffectiveDate { get; set; }
    }
}
