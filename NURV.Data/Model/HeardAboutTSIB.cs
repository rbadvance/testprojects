﻿using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Runtime.CompilerServices;

[assembly: InternalsVisibleTo("DynamicProxyGenAssembly2")]
namespace NURV.Data.Model
{
    [Table("tblHeardAboutTSIB")]
    internal class HeardAboutTSIB : Common.EntityObjectBase
    {
        [Key, Column(Order = 0)]
        public byte ID { get; set; }

        [MaxLength(100), Column("HeardAboutTSIB"), Required]
        public string HeardAboutTSIBName { get; set; }

        public bool Display { get; set; }
    }
}
