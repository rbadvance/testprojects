﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace NURV.Data.Model
{
    [Table("tblStaff")]
    internal class Staff : Common.EntityObjectBase
    {
        [Key]
        [Column("ID")]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int Id { get; set; }
        [StringLength(100)]
        public string AfterHoursNumber { get; set; }
        [StringLength(100)]
        public string Category { get; set; }
        [StringLength(250)]
        public string Characteristics { get; set; }
        [StringLength(100)]
        public string FirstName { get; set; }
        [StringLength(200)]
        public string ContactCode { get; set; }
        [Column("ContactID")]
        public int? ContactId { get; set; }
        [StringLength(250)]
        public string EmailAddress { get; set; }
        [StringLength(100)]
        public string FaxNumber { get; set; }
        [StringLength(250)]
        public string Address { get; set; }
        [StringLength(100)]
        public string Country { get; set; }
        [StringLength(210)]
        public string Fax { get; set; }
        [StringLength(100)]
        public string Postcode { get; set; }
        [StringLength(100)]
        public string State { get; set; }
        [StringLength(100)]
        public string Suburb { get; set; }
        [StringLength(100)]
        public string MainPhoneNumber { get; set; }
        [StringLength(100)]
        public string MobileNumber { get; set; }
        [StringLength(1000)]
        public string Notes { get; set; }
        [StringLength(100)]
        public string OwnerBranch { get; set; }
        [StringLength(50)]
        public string PersonalCode { get; set; }
        [StringLength(110)]
        public string Position { get; set; }
        [StringLength(100)]
        public string PreferredMailingName { get; set; }
        [StringLength(100)]
        public string Salutation { get; set; }
        [StringLength(100)]
        public string Surname { get; set; }
        [StringLength(100)]
        public string Title { get; set; }
        [StringLength(250)]
        public string ContentsLocationAddress { get; set; }
        [StringLength(100)]
        public string ContentsLocationPostcode { get; set; }
        [StringLength(100)]
        public string ContentsLocationState { get; set; }
        [StringLength(100)]
        public string ContentsLocationSuburb { get; set; }
        [StringLength(100)]
        public string MiddleName { get; set; }
        [StringLength(100)]
        public string DriversLicense { get; set; }
        [StringLength(100)]
        public string CreationUser { get; set; }
        [Column(TypeName = "datetime")]
        public DateTime? CreationDate { get; set; }
        [StringLength(100)]
        public string ModificationUser { get; set; }
        [Column(TypeName = "datetime")]
        public DateTime? ModificationDate { get; set; }
        public int? Deleted { get; set; }
        [Column("time_stamp")]
        public byte[] TimeStamp { get; set; }
        [Column("stf_con_id_old")]
        public int? StfConIdOld { get; set; }
        public bool? HolidayLetting { get; set; }
        [Column(TypeName = "datetime")]
        public DateTime? DateOfBirth { get; set; }
        [Column(TypeName = "datetime")]
        public DateTime? RemovedDate { get; set; }
        [StringLength(100)]
        public string RemovedBy { get; set; }
        [Column("DeliveryPreferenceID")]
        public int? DeliveryPreferenceId { get; set; }
        [Column("ReceivePolicyInfoBySMS")]
        public bool? ReceivePolicyInfoBySms { get; set; }
        public bool? InterpreterOffered { get; set; }
        public bool? InterpreterAccepted { get; set; }
        [StringLength(500)]
        public string InterpreterNote { get; set; }
        public bool? SendDocumentsByEmail { get; set; }
        public bool? SendDocumentsByPost { get; set; }
        [StringLength(250)]
        public string MailingAddress { get; set; }
        [StringLength(50)]
        public string MailingSuburb { get; set; }
        [StringLength(10)]
        public string MailingState { get; set; }
        [StringLength(10)]
        public string MailingPostcode { get; set; }
        [StringLength(2)]
        public string MailingCountryCode { get; set; }
        [StringLength(50)]
        public string ClaimNumber { get; set; }
        public bool? HideMainContactDetails { get; set; }
        [Column("DistributionStatusID")]
        public int? DistributionStatusId { get; set; }
        [Column("OnlinePermissionID")]
        public int? OnlinePermissionId { get; set; }
    }
}
