﻿using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Runtime.CompilerServices;

[assembly: InternalsVisibleTo("DynamicProxyGenAssembly2")]
namespace NURV.Data.Model
{
    /// <remarks>16/09/21 This is currently a view 'viewList_States' but should be converted to a table</remarks>
    [Table("tblPaymentMethod")]
    internal class PaymentMethod : Common.EntityObjectBase
    {
        [Key, Column(Order = 0), DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int ID { get; set; }

        [MaxLength(50)]
        public string Data { get; set; }

        public bool InCancellation { get; set; }
        public bool InHoldCover { get; set; }
        public bool InReceipts { get; set; }
        public bool InOverpayments {get; set;}
        public bool InManualRefunds { get; set; }
        
        [MaxLength(25)]
        public string Type { get; set; }

        [MaxLength(10)]
        public string Method { get; set; }

        public int SortOrder { get; set; }
    }
}
