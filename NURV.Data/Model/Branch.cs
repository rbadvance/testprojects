﻿using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Runtime.CompilerServices;

[assembly: InternalsVisibleTo("DynamicProxyGenAssembly2")]
namespace NURV.Data.Model
{
    [Table("tblBranch")]
    internal class Branch : Common.EntityObjectBase
    {
        [Key, Column(Order = 0), DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int ID { get; set; }

        [MaxLength(50), Required]
        public string Name { get; set; }

        [MaxLength(25), Required]
        public string Code { get; set; }

        [MaxLength(1000), Required]
        public string Address { get; set; }

        [MaxLength(1000), Required]
        public string Suburb { get; set; }

        [MaxLength(1000), Required]
        public string State { get; set; }

        [MaxLength(10), Required]
        public string Postcode { get; set; }

        [MaxLength(1000), Required]
        public string Phone { get; set; }

        [MaxLength(1000), Required]
        public string Fax { get; set; }

        [MaxLength(1000)]
        public string EmailAddress { get; set; }

        [MaxLength(1000)]
        public string WebAddress { get; set; }

        [Column("bra_ContactId")]
        public int? BRAContactID { get; set; }

        public int? NewestTransactionNumber { get; set; }
        public int? TransactionNumberPrefix { get; set; }

        [MaxLength(20), Required]
        public string Type { get; set; }

        public int? NewestLandlordNumber { get; set; }
        public int? LandlordNumberPrefix { get; set; }
        public bool? ConcurrentPolicyDiscount { get; set; }
        public bool? IsHeadOffice { get; set; }
    }
}
