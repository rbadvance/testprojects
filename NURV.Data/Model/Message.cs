﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace NURV.Data.Model
{
    [Table("tblMessage")]
    internal class Message : Common.EntityObjectBase
    {
        [Key]
        [Column("ID")]
        [StringLength(50)]
        public string Id { get; set; }
        [Required]
        [StringLength(4000)]
        [Column("Message")]
        public string MessageText { get; set; }
        [StringLength(100)]
        public string Colour { get; set; }
        public bool? Bold { get; set; }
        public int? Order { get; set; }
        [StringLength(20)]
        public string FontStyle { get; set; }
        public int? FontSize { get; set; }
        [StringLength(10)]
        public string ForeColour { get; set; }
        [StringLength(10)]
        public string BackColour { get; set; }
    }
}
