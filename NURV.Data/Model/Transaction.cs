﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace NURV.Data.Model
{
    [Table("tblTransaction")]
    internal class Transaction : Common.EntityObjectBase
    {
        [Key]
        [Column("ID")]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int Id { get; set; }
        [Column(TypeName = "money")]
        public decimal? AllRisksBase { get; set; }
        [Column(TypeName = "money")]
        public decimal? AllRisksBf { get; set; }
        [Column(TypeName = "money")]
        public decimal? AllRisksPremium { get; set; }
        [Column(TypeName = "money")]
        public decimal? AllRisksSd { get; set; }
        [Column(TypeName = "money")]
        public decimal? Balance { get; set; }
        [Column(TypeName = "money")]
        public decimal? BankingBalance { get; set; }
        [Column(TypeName = "money")]
        public decimal? Base { get; set; }
        [Column(TypeName = "money")]
        public decimal? BasePr { get; set; }
        [Column(TypeName = "money")]
        public decimal? BasePrGst { get; set; }
        public int? BatchNumber { get; set; }
        public int? BatchNumberTemp80 { get; set; }
        public int? BatchNumberTemp87 { get; set; }
        public int? BatchNumberTemp87b { get; set; }
        public int? BatchNumberTemp87c { get; set; }
        public int? BatchNumberTemp87d { get; set; }
        public int? BatchNumberTemp87e { get; set; }
        public int? BatchNumberTemp88 { get; set; }
        [Column(TypeName = "money")]
        public decimal? Bf { get; set; }
        [Column(TypeName = "money")]
        public decimal? BfGst { get; set; }
        [Column(TypeName = "money")]
        public decimal? BfManual { get; set; }
        [Column(TypeName = "money")]
        public decimal? BfPr { get; set; }
        [Column(TypeName = "datetime")]
        public DateTime? BillingEndDate { get; set; }
        [Column(TypeName = "datetime")]
        public DateTime? BillingStartDate { get; set; }
        [StringLength(1000)]
        public string BordereauxNotes { get; set; }
        [StringLength(4000)]
        public string BranchOfficeNotes { get; set; }
        [StringLength(50)]
        public string BuildingType { get; set; }
        [StringLength(250)]
        public string CancelReason { get; set; }
        [Column("ClassOfRiskID")]
        public int? ClassOfRiskId { get; set; }
        [Column(TypeName = "datetime")]
        public DateTime? ClosingDate { get; set; }
        [Column(TypeName = "datetime")]
        public DateTime? CocDate { get; set; }
        public bool? CommissionPaidFlag { get; set; }
        public bool? ConcurrentPolicyDiscount { get; set; }
        [Column(TypeName = "money")]
        public decimal? ConcurrentPolicyDiscountAmount { get; set; }
        [Column("ContactID")]
        public int? ContactId { get; set; }
        [StringLength(50)]
        public string CoverNoteNumber { get; set; }
        [Column(TypeName = "datetime")]
        public DateTime? CreationDate { get; set; }
        [StringLength(50)]
        public string CreationUser { get; set; }
        public int? CreditedTransactionNumber { get; set; }
        [Column(TypeName = "datetime")]
        public DateTime? CriticalCoverDate { get; set; }
        public bool? Deleted { get; set; }
        public bool? DeletionOption { get; set; }
        public bool? DoNotRenew { get; set; }
        [Column(TypeName = "money")]
        public decimal? DwcFee { get; set; }
        [StringLength(1000)]
        public string DwcNotes { get; set; }
        [Column(TypeName = "money")]
        public decimal? DwcPr { get; set; }
        [Column(TypeName = "money")]
        public decimal? DwcSdRate { get; set; }
        [Column(TypeName = "datetime")]
        public DateTime? EffectiveDate { get; set; }
        [StringLength(2000)]
        public string Endorsements { get; set; }
        [StringLength(100)]
        public string Excess { get; set; }
        [Column(TypeName = "money")]
        public decimal? ExcessAmount { get; set; }
        [Column(TypeName = "money")]
        public decimal? ExtraPlbiContents { get; set; }
        public int? ExtraPremiumParentTransaction { get; set; }
        [Column(TypeName = "money")]
        public decimal? Fsl { get; set; }
        [Column(TypeName = "money")]
        public decimal? FslAsh { get; set; }
        [Column(TypeName = "money")]
        public decimal? FslAshPr { get; set; }
        [Column(TypeName = "money")]
        public decimal? FslGst { get; set; }
        [Column(TypeName = "money")]
        public decimal? FslPr { get; set; }
        [Column(TypeName = "money")]
        public decimal? FslPrGst { get; set; }
        [Column(TypeName = "money")]
        public decimal? FslRate { get; set; }
        [Column(TypeName = "datetime")]
        public DateTime? FslRateEndDate { get; set; }
        [Column(TypeName = "money")]
        public decimal? GrossCommission { get; set; }
        [Column(TypeName = "money")]
        public decimal? GrossCommissionGst { get; set; }
        [Column(TypeName = "money")]
        public decimal? GrossCommissionPr { get; set; }
        [Column(TypeName = "money")]
        public decimal? GrossCommissionRate { get; set; }
        [Column(TypeName = "money")]
        public decimal? GstBase { get; set; }
        [Column(TypeName = "money")]
        public decimal? GstBrokerage { get; set; }
        [Column(TypeName = "money")]
        public decimal? GstNewRate { get; set; }
        [Column(TypeName = "datetime")]
        public DateTime? GstNewRateDate { get; set; }
        [Column(TypeName = "money")]
        public decimal? GstPr { get; set; }
        [Column(TypeName = "money")]
        public decimal? GstPrBf { get; set; }
        [Column(TypeName = "money")]
        public decimal? GstPrGc { get; set; }
        [Column(TypeName = "money")]
        public decimal? GstPrMf { get; set; }
        [Column(TypeName = "money")]
        public decimal? GstPrMfPay { get; set; }
        [Column(TypeName = "money")]
        public decimal? GstPrRw { get; set; }
        [Column(TypeName = "money")]
        public decimal? GstPrRwPay { get; set; }
        [Column(TypeName = "money")]
        public decimal? GstPrSa { get; set; }
        [Column(TypeName = "money")]
        public decimal? GstPrTsib { get; set; }
        [Column(TypeName = "money")]
        public decimal? GstPrUw { get; set; }
        [Column(TypeName = "money")]
        public decimal? GstRate { get; set; }
        public bool? GstSaPaidFlag { get; set; }
        public bool? GstToMfPaidFlag { get; set; }
        public bool? GstTsibPaidFlag { get; set; }
        [StringLength(1000)]
        public string HeadOfficeNotes { get; set; }
        [Column(TypeName = "datetime")]
        public DateTime? InceptionDate { get; set; }
        public bool? IncreasedCoverPremium { get; set; }
        [Column(TypeName = "money")]
        public decimal? IncreasedCoverPremiumAmount { get; set; }
        [StringLength(100)]
        public string Insured { get; set; }
        [StringLength(50)]
        public string InsurerCode { get; set; }
        [StringLength(100)]
        public string InterestInsured { get; set; }
        [StringLength(100)]
        public string InterestedParty { get; set; }
        [Column(TypeName = "datetime")]
        public DateTime? InvoicePrinted { get; set; }
        [StringLength(50)]
        public string InvoiceRenewal { get; set; }
        [Column("LandlordID")]
        public int? LandlordId { get; set; }
        [StringLength(250)]
        public string LandlordAddress { get; set; }
        [StringLength(500)]
        public string LandlordName { get; set; }
        [StringLength(10)]
        public string LandlordPostcode { get; set; }
        [StringLength(25)]
        public string LandlordState { get; set; }
        [StringLength(100)]
        public string LandlordSuburb { get; set; }
        public int? LapsingBatch { get; set; }
        [Column(TypeName = "money")]
        public decimal? LegalLiability { get; set; }
        [Column(TypeName = "money")]
        public decimal? LegalLiabilityGst { get; set; }
        [Column(TypeName = "money")]
        public decimal? LegalLiabilityPr { get; set; }
        [Column(TypeName = "money")]
        public decimal? LegalLiabilityPrGst { get; set; }
        public int? LinkedToLppTransaction { get; set; }
        public bool? Locked { get; set; }
        [Column(TypeName = "money")]
        public decimal? Mf { get; set; }
        public bool? MfFlag { get; set; }
        [Column(TypeName = "money")]
        public decimal? MfGst { get; set; }
        [Column(TypeName = "money")]
        public decimal? MfManual { get; set; }
        [Column(TypeName = "datetime")]
        public DateTime? MfPaidDate { get; set; }
        [Column(TypeName = "money")]
        public decimal? MfPr { get; set; }
        [Column(TypeName = "money")]
        public decimal? MfRwGst { get; set; }
        [Column(TypeName = "money")]
        public decimal? MfTsib { get; set; }
        [Column(TypeName = "money")]
        public decimal? ManualAdjustment { get; set; }
        [Column(TypeName = "money")]
        public decimal? MarshCommission { get; set; }
        [Column(TypeName = "money")]
        public decimal? MarshCommissionRate { get; set; }
        [Column(TypeName = "money")]
        public decimal? MarshGst { get; set; }
        public int? MonthsInPremium { get; set; }
        [StringLength(250)]
        public string Mortgagee { get; set; }
        public bool? MultiplePropertyDiscount { get; set; }
        [Column(TypeName = "money")]
        public decimal? MultiplePropertyDiscountAmount { get; set; }
        [Column(TypeName = "money")]
        public decimal? NettBalance { get; set; }
        [Column(TypeName = "money")]
        public decimal? NettToUwPr { get; set; }
        public short? NinetyDayCalculator { get; set; }
        [Column(TypeName = "text")]
        public string Notes { get; set; }
        public bool? OtherSchemesPaid { get; set; }
        [Column(TypeName = "datetime")]
        public DateTime? OtherSchemesPaidDate { get; set; }
        [StringLength(100)]
        public string OwnerBranch { get; set; }
        public bool? PaidFlag { get; set; }
        public bool? PaidUw { get; set; }
        public bool? PayPrMf { get; set; }
        [Column(TypeName = "money")]
        public decimal? PolDocFee { get; set; }
        [Column(TypeName = "money")]
        public decimal? PolDocFeeGst { get; set; }
        [Column(TypeName = "money")]
        public decimal? PolDocFeePr { get; set; }
        [Column(TypeName = "money")]
        public decimal? PolDocFeePrGst { get; set; }
        [StringLength(250)]
        public string PolicyNumber { get; set; }
        [Column(TypeName = "datetime")]
        public DateTime? PolicySentDate { get; set; }
        [StringLength(100)]
        public string PreferredMailingAddress { get; set; }
        [Column(TypeName = "money")]
        public decimal? Premium { get; set; }
        [StringLength(25)]
        public string PremiumAreaCode { get; set; }
        [Column(TypeName = "money")]
        public decimal? PremiumGst { get; set; }
        [StringLength(100)]
        public string PremiumPaidBy { get; set; }
        [Column(TypeName = "money")]
        public decimal? PremiumPr { get; set; }
        [Column(TypeName = "money")]
        public decimal? PremiumPrGst { get; set; }
        [Column(TypeName = "money")]
        public decimal? PremiumTotal { get; set; }
        [Column(TypeName = "money")]
        public decimal? PremiumTotalDaily { get; set; }
        [Column(TypeName = "money")]
        public decimal? PremiumTotalPr { get; set; }
        [Column("PremiumTypeID")]
        public int? PremiumTypeId { get; set; }
        [StringLength(100)]
        public string Property { get; set; }
        [StringLength(250)]
        public string PropertyManager { get; set; }
        [StringLength(100)]
        public string PropertyManagerCode { get; set; }
        [Column("PropertyManagerID")]
        public int? PropertyManagerId { get; set; }
        [StringLength(50)]
        public string PropertyManagerPhone { get; set; }
        [StringLength(2000)]
        public string PropertyNotes { get; set; }
        [StringLength(20)]
        public string RenewalBatch { get; set; }
        [Column(TypeName = "datetime")]
        public DateTime? RenewalDate { get; set; }
        public int? RenewedByTransaction { get; set; }
        public int? RenewedTransaction { get; set; }
        [Column(TypeName = "money")]
        public decimal? RwFee { get; set; }
        [Column(TypeName = "money")]
        public decimal? RwFeePr { get; set; }
        [Column(TypeName = "money")]
        public decimal? RwGst { get; set; }
        [Column(TypeName = "money")]
        public decimal? SaCommission { get; set; }
        [Column(TypeName = "money")]
        public decimal? SaCommissionPr { get; set; }
        [Column(TypeName = "money")]
        public decimal? SaCommissionRate { get; set; }
        [Column(TypeName = "money")]
        public decimal? SaCommissionGst { get; set; }
        [Column(TypeName = "money")]
        public decimal? SaCommissionPrGst { get; set; }
        [Column(TypeName = "datetime")]
        public DateTime? SaPaidDate { get; set; }
        [StringLength(2000)]
        public string SpecialConditions { get; set; }
        [StringLength(250)]
        public string SpecialTransNote { get; set; }
        [Column(TypeName = "money")]
        public decimal? Sd { get; set; }
        [Column(TypeName = "money")]
        public decimal? SdPr { get; set; }
        [Column(TypeName = "decimal(10, 5)")]
        public decimal? SdRate { get; set; }
        [StringLength(50)]
        public string StatusOfTransaction { get; set; }
        [Column(TypeName = "money")]
        public decimal? SumInsured { get; set; }
        [Column(TypeName = "money")]
        public decimal? SumOnCredit { get; set; }
        [Column(TypeName = "money")]
        public decimal? SumWriteOff { get; set; }
        public int? TaxInvoiceNumberMf { get; set; }
        public int? TaxInvoiceNumberRw { get; set; }
        public int? TaxInvoiceNumberSa { get; set; }
        public int? TaxInvoiceNumberUw { get; set; }
        [Column(TypeName = "money")]
        public decimal? ToSa { get; set; }
        [Column(TypeName = "money")]
        public decimal? ToMf { get; set; }
        [Column(TypeName = "money")]
        public decimal? ToRw { get; set; }
        [Column(TypeName = "money")]
        public decimal? ToSelfGross { get; set; }
        [Column(TypeName = "money")]
        public decimal? ToSelfGrossPr { get; set; }
        public int? TransactionNumber { get; set; }
        [StringLength(10)]
        public string TransactionType { get; set; }
        [Column("trn_PremiumPaid", TypeName = "datetime")]
        public DateTime? TrnPremiumPaid { get; set; }
        [Column("trn_ReceivableCreated", TypeName = "datetime")]
        public DateTime? TrnReceivableCreated { get; set; }
        [Column("trn_AccrualCreated", TypeName = "datetime")]
        public DateTime? TrnAccrualCreated { get; set; }
        [Column("trn_ProposalReceived", TypeName = "datetime")]
        public DateTime? TrnProposalReceived { get; set; }
        [Column("trn_CurrentDate", TypeName = "datetime")]
        public DateTime? TrnCurrentDate { get; set; }
        [Column("trn_CancelDate", TypeName = "datetime")]
        public DateTime? TrnCancelDate { get; set; }
        [Column("trn_cr_id")]
        public int? TrnCrId { get; set; }
        [Column("trn_RefundPayee")]
        [StringLength(255)]
        public string TrnRefundPayee { get; set; }
        [Column("trn_RefundAmount", TypeName = "money")]
        public decimal? TrnRefundAmount { get; set; }
        [Column("trn_RefundReference")]
        [StringLength(50)]
        public string TrnRefundReference { get; set; }
        [Column("trn_RefundSelfFee", TypeName = "money")]
        public decimal? TrnRefundSelfFee { get; set; }
        [Column("trn_RefundSelfFeeAccountNo")]
        public int? TrnRefundSelfFeeAccountNo { get; set; }
        public int? Version { get; set; }
        [Column("time_stamp")]
        public byte[] TimeStamp { get; set; }
        [Column("trn_con_id_old")]
        public int? TrnConIdOld { get; set; }
        [StringLength(200)]
        public string ContactCode { get; set; }
        [StringLength(200)]
        public string ClassOfRisk { get; set; }
        [StringLength(200)]
        public string PremiumType { get; set; }
        [StringLength(100)]
        public string InvoicePrintedTxt { get; set; }
        [StringLength(50)]
        public string LlCode { get; set; }
        [StringLength(1000)]
        public string ExtraPremiumReason { get; set; }
        public bool? MigrationError { get; set; }
        [StringLength(100)]
        public string ErrorReason { get; set; }
        [Column("CreditControl_OverdueNoticePrinted", TypeName = "datetime")]
        public DateTime? CreditControlOverdueNoticePrinted { get; set; }
        [Column("CreditControl_FinalReminderPrinted", TypeName = "datetime")]
        public DateTime? CreditControlFinalReminderPrinted { get; set; }
        public int? ParentTransactionNumber { get; set; }
        public bool? ApplicationReceived { get; set; }
        [Column(TypeName = "money")]
        public decimal? RoundingError { get; set; }
        [Column("CreditControl_LapseNoticePrinted", TypeName = "datetime")]
        public DateTime? CreditControlLapseNoticePrinted { get; set; }
        [Column("DoNotRenew_Reason")]
        [StringLength(500)]
        public string DoNotRenewReason { get; set; }
        [Column("Notes_plain", TypeName = "text")]
        public string NotesPlain { get; set; }
        [Column("Notes_done")]
        public bool? NotesDone { get; set; }
        [Column("RPType")]
        [StringLength(50)]
        public string Rptype { get; set; }
        [Column("RPTransactionNumber")]
        public int? RptransactionNumber { get; set; }
        [Column(TypeName = "datetime")]
        public DateTime? SchemePaidDate { get; set; }
        [Column(TypeName = "datetime")]
        public DateTime? ModificationDate { get; set; }
        [StringLength(100)]
        public string ModificationUser { get; set; }
        [Column("RenewalMPD_Ticked")]
        public bool? RenewalMpdTicked { get; set; }
        [Column(TypeName = "datetime")]
        public DateTime? PdsSent { get; set; }
        [Column(TypeName = "datetime")]
        public DateTime? FsgSent { get; set; }
        [StringLength(50)]
        public string PdsVersion { get; set; }
        [StringLength(50)]
        public string FsgVersion { get; set; }
        public bool? BatchLapsed { get; set; }
        [Column("REARequestForCover")]
        public bool? RearequestForCover { get; set; }
        [Column(TypeName = "datetime")]
        public DateTime? CancelPrinted { get; set; }
        [StringLength(100)]
        public string RenewedByHistory { get; set; }
        [StringLength(8000)]
        public string LegacyData { get; set; }
        [Column("trn_CancelledOn", TypeName = "datetime")]
        public DateTime? TrnCancelledOn { get; set; }
        [Column("ExtraReturnPremiumID")]
        public int? ExtraReturnPremiumId { get; set; }
        [Column(TypeName = "money")]
        public decimal? ContentsSumInsured { get; set; }
        [Column(TypeName = "money")]
        public decimal? ContentsCoverBaseAmount { get; set; }
        [Column("ContentsCoverID")]
        public int? ContentsCoverId { get; set; }
        public bool? UnoccupancyPremium { get; set; }
        [Column(TypeName = "money")]
        public decimal? UnoccupancyBaseAmount { get; set; }
        [Column("UWSchemeNumber")]
        [StringLength(20)]
        public string UwschemeNumber { get; set; }
        public bool? WeeklyRentPremium { get; set; }
        [Column("WeeklyRentPremiumID")]
        public int? WeeklyRentPremiumId { get; set; }
        [Column(TypeName = "money")]
        public decimal? WeeklyRentBaseAmount { get; set; }
        [Column("SSSSwivelToLPP")]
        public bool? SssswivelToLpp { get; set; }
        [Column("SSSSwivelToLPPID")]
        public int? SssswivelToLppid { get; set; }
        [Column("SSSSwivelToLPPBase", TypeName = "money")]
        public decimal? SssswivelToLppbase { get; set; }
        [Column("RBILoadingForHolidayLet")]
        public bool? RbiloadingForHolidayLet { get; set; }
        [Column("RBILoadingForHolidayLetID")]
        public int? RbiloadingForHolidayLetId { get; set; }
        [Column("RBILoadingForHolidayLetFactor", TypeName = "money")]
        public decimal? RbiloadingForHolidayLetFactor { get; set; }
        [StringLength(250)]
        public string SpecialTransNotes { get; set; }
        [Column("trn_RefundInfoSentTo")]
        [StringLength(255)]
        public string TrnRefundInfoSentTo { get; set; }
        [Column("trn_RefundCopySentTo")]
        [StringLength(255)]
        public string TrnRefundCopySentTo { get; set; }
        [Column("HowPlacedCoverID")]
        public int? HowPlacedCoverId { get; set; }
        [Column("WhoPlacedCoverID")]
        public int? WhoPlacedCoverId { get; set; }
        [Column("RenewMPD")]
        public bool? RenewMpd { get; set; }
        [Column("bonusID")]
        public int BonusId { get; set; }
        [StringLength(100)]
        public string RenewalPremiumPaidBy { get; set; }
        [Column("RenewCPD")]
        public bool? RenewCpd { get; set; }
        [Column("RenewREAD")]
        public bool? RenewRead { get; set; }
        [Column("READiscount")]
        public bool? Readiscount { get; set; }
        [Column("CreditControl_ExtendedCreditTerms", TypeName = "datetime")]
        public DateTime? CreditControlExtendedCreditTerms { get; set; }
        public bool? PrintReminder { get; set; }
        [Column("reaDiscountBase", TypeName = "money")]
        public decimal? ReaDiscountBase { get; set; }
        [Column("multiplePropertyDiscountBase", TypeName = "money")]
        public decimal? MultiplePropertyDiscountBase { get; set; }
        [Column("concurrentPolicyDiscountBase", TypeName = "money")]
        public decimal? ConcurrentPolicyDiscountBase { get; set; }
        [Column("holidayLetLoadingBase", TypeName = "money")]
        public decimal? HolidayLetLoadingBase { get; set; }
        [Column("extraContentsCoverBase", TypeName = "money")]
        public decimal? ExtraContentsCoverBase { get; set; }
        [Column("weeklyRentBase", TypeName = "money")]
        public decimal? WeeklyRentBase { get; set; }
        [Column("swivelToLppBase", TypeName = "money")]
        public decimal? SwivelToLppBase { get; set; }
        [Column("MPDID")]
        public int? Mpdid { get; set; }
        [Column("RenewalMPDID")]
        public int? RenewalMpdid { get; set; }
        public int? RenewalErrorCode { get; set; }
        public int? WeeklyRentInsured { get; set; }
        [StringLength(4)]
        public string YearBuilt { get; set; }
        public bool? Rewired { get; set; }
        public bool? CommonGrounds { get; set; }
        [Column("sPdsSent", TypeName = "datetime")]
        public DateTime? SPdsSent { get; set; }
        [Column("sPdsVersion")]
        [StringLength(50)]
        public string SPdsVersion { get; set; }
        [Column("trn_RefundID")]
        public int? TrnRefundId { get; set; }
        [Column("sPds2Sent", TypeName = "datetime")]
        public DateTime? SPds2Sent { get; set; }
        [Column("sPds2Version")]
        [StringLength(50)]
        public string SPds2Version { get; set; }
        [Column("fivePercentDiscount")]
        public bool? FivePercentDiscount { get; set; }
        [Column("ExcessID")]
        public int? ExcessId { get; set; }
        [Column("RenewalContentsCoverID")]
        public int? RenewalContentsCoverId { get; set; }
        [Column("RenewalWeeklyRentID")]
        public int? RenewalWeeklyRentId { get; set; }
        [Column("trn_RefundCreatedBy")]
        [StringLength(100)]
        public string TrnRefundCreatedBy { get; set; }
        public bool? Unlapsed { get; set; }
        [Column("fivePercentDiscountBase", TypeName = "money")]
        public decimal? FivePercentDiscountBase { get; set; }
        public bool? StaffDiscount { get; set; }
        [Column(TypeName = "money")]
        public decimal? StaffDiscountBase { get; set; }
        public bool? RenewStaffDiscount { get; set; }
        public bool? HasBeenRecreated { get; set; }
        [Column("InsuredPropertyAddressID")]
        public int? InsuredPropertyAddressId { get; set; }
        public bool? Recreated { get; set; }
        [Column("currentWeeklyRent", TypeName = "money")]
        public decimal? CurrentWeeklyRent { get; set; }
        [Column("reaGroupDiscountID")]
        public int? ReaGroupDiscountId { get; set; }
        [Column("reaGroupDiscountBase", TypeName = "money")]
        public decimal? ReaGroupDiscountBase { get; set; }
        [Column("BaseID")]
        public int? BaseId { get; set; }
        [Column("CCTVID")]
        public int? Cctvid { get; set; }
        [Column("ElectronicAccessID")]
        public int? ElectronicAccessId { get; set; }
        [Column("FurnishedID")]
        public int? FurnishedId { get; set; }
        [Column("ContentsCoverExcessID")]
        public int? ContentsCoverExcessId { get; set; }
        [Column("DwellingTypeID")]
        public int? DwellingTypeId { get; set; }
        [Column("ConstructionTypeID")]
        public int? ConstructionTypeId { get; set; }
        [Column("LandSizeID")]
        public int? LandSizeId { get; set; }
        [Column("PostCodeID")]
        public int? PostCodeId { get; set; }
        [Column("HolidayLetID")]
        public int? HolidayLetId { get; set; }
        [Column("TenantedID")]
        public int? TenantedId { get; set; }
        [Column("SelfManagedID")]
        public int? SelfManagedId { get; set; }
        [Column("RenewCCTVID")]
        public int? RenewCctvid { get; set; }
        [Column("RenewElectronicAccessID")]
        public int? RenewElectronicAccessId { get; set; }
        [Column("RenewFurnishedID")]
        public int? RenewFurnishedId { get; set; }
        [Column("RenewContentsSumInsuredID")]
        public int? RenewContentsSumInsuredId { get; set; }
        [Column("RenewContentsCoverExcessID")]
        public int? RenewContentsCoverExcessId { get; set; }
        [Column("RenewSelfManagedID")]
        public int? RenewSelfManagedId { get; set; }
        public bool? SelfManaged { get; set; }
        [Column("BuildingExcessID")]
        public int? BuildingExcessId { get; set; }
        [Column("15412")]
        public bool _15412 { get; set; }
        public bool? CustomBaseAmount { get; set; }
        [Column("UnoccupiedExcessID")]
        public int? UnoccupiedExcessId { get; set; }
        [Column(TypeName = "money")]
        public decimal? OnlineDiscountReduction { get; set; }
        [Column(TypeName = "money")]
        public decimal? BaseCalculated { get; set; }
        public bool? RenewSpecialPremium { get; set; }
        [Column("RenewBuildingExcessID")]
        public int? RenewBuildingExcessId { get; set; }
        [Column("RenewHolidayLetID")]
        public int? RenewHolidayLetId { get; set; }
        [Column("WeeklyRentSINoInflationAtRenewal")]
        public bool? WeeklyRentSinoInflationAtRenewal { get; set; }
        [Column("BuildingSINoInflationAtRenewal")]
        public bool? BuildingSinoInflationAtRenewal { get; set; }
        [StringLength(20)]
        public string CombinedPolicyDiscountPolicyLink { get; set; }
        public bool? Capped { get; set; }
        public bool? Cupped { get; set; }
        [Column("BaseForCoversPR", TypeName = "money")]
        public decimal? BaseForCoversPr { get; set; }
        [Column("CreditCardTransID")]
        [StringLength(50)]
        public string CreditCardTransId { get; set; }
        [Column("RFContactID")]
        public int? RfcontactId { get; set; }
        [Column("FloodRiskLevelID")]
        public int? FloodRiskLevelId { get; set; }
        [Column("OtherClaimsExcessID")]
        public int? OtherClaimsExcessId { get; set; }
        [Column(TypeName = "datetime")]
        public DateTime? RenewalDeclinedPrinted { get; set; }
        [Column(TypeName = "datetime")]
        public DateTime? CoverSuspendedDate { get; set; }
        public bool? OnHold { get; set; }
        [Column("CreditControl_ExtendCreditTermsWeeks")]
        public short? CreditControlExtendCreditTermsWeeks { get; set; }
        public int? RecreatedFrom { get; set; }
        [Column("NonREAReferrerID")]
        public int? NonReareferrerId { get; set; }
        public int? OriginalTransactionNumber { get; set; }
        public bool? RenewalOnHold { get; set; }
        [StringLength(20)]
        public string PolicyPaymentReference { get; set; }
        [Column("CreditControl_ExtendedLapsingDate", TypeName = "datetime")]
        public DateTime? CreditControlExtendedLapsingDate { get; set; }
        public int? CreditControlReminderBatchNumber { get; set; }
        [Column("UnlapseReasonID")]
        public int? UnlapseReasonId { get; set; }
        [Column("BushFireRiskID")]
        public int? BushFireRiskId { get; set; }
        [Column("BonusOfferID")]
        public int? BonusOfferId { get; set; }
        [Column("HowPayID")]
        public int? HowPayId { get; set; }
        [Column("BPayReceipt")]
        [StringLength(50)]
        public string BpayReceipt { get; set; }
        [Column("TenantDamageExcessID")]
        public int? TenantDamageExcessId { get; set; }
        [Column("PetDamageExcessID")]
        public int? PetDamageExcessId { get; set; }
        [Column("LegalLiabilityStampDutyPR", TypeName = "money")]
        public decimal? LegalLiabilityStampDutyPr { get; set; }
        [Column("LegalLiabilityInBasePR", TypeName = "money")]
        public decimal? LegalLiabilityInBasePr { get; set; }
        [Column(TypeName = "decimal(10, 5)")]
        public decimal? SdRateDisplay { get; set; }
        [Column(TypeName = "money")]
        public decimal? SdDiscount { get; set; }
        [StringLength(200)]
        public string SpecialPremiumReason { get; set; }
        [StringLength(50)]
        public string CoverSuspendedBy { get; set; }
        [Column(TypeName = "datetime")]
        public DateTime? CoverSuspendedFollowUpDate { get; set; }
        [Column("BuildingSumInsuredID")]
        public int? BuildingSumInsuredId { get; set; }
        [Column("YearBuiltID")]
        public int? YearBuiltId { get; set; }
        [Column(TypeName = "decimal(10, 4)")]
        public decimal? MinimumCuppingLoadingFactor { get; set; }
        [Column("CappingCuppingID")]
        public int? CappingCuppingId { get; set; }
        [Column("UnoccupancyID")]
        public int? UnoccupancyId { get; set; }
        public int? MinimumCuppingApplied { get; set; }
        public bool? SendElectronicCommunication { get; set; }
        [Column("PromoCodeID")]
        public short? PromoCodeId { get; set; }
        public bool? TenantReducedHours { get; set; }
        [Column("ArrearsWeekRangeID")]
        public int? ArrearsWeekRangeId { get; set; }
        public bool? BusinessOrTrade { get; set; }
        public bool? AcceptedBusinessUse { get; set; }
        public bool DoNotApplyVoucher { get; set; }
        [Column("DistributorStaffID")]
        public int? DistributorStaffId { get; set; }
    }
}
