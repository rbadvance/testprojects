﻿using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Runtime.CompilerServices;

[assembly: InternalsVisibleTo("DynamicProxyGenAssembly2")]
namespace NURV.Data.Model
{
    [Table("tblRFConstructionType")]
    internal class ConstructionType : Common.EntityObjectBase
    {
        [Key, Column(Order = 0), DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int ConstructionTypeID { get; set; }

        [MaxLength(50), Column("ConstructionType")]
        public string ConstructionTypeName { get; set; }

        [Column(TypeName = "decimal(10, 4)")]
        public decimal? Rate { get; set; }

        public int? ClassOfRiskID { get; set; }
        public int? PremiumTypeID { get; set; }

        [MaxLength(3)]
        public string State { get; set; }
        [Column(TypeName = "datetime")]
        public DateTime? StartEffectiveDate { get; set; }
        [Column(TypeName = "datetime")]
        public DateTime? EndEffectiveDate { get; set; }
        public bool? IsUnknown { get; set; }
    }
}
