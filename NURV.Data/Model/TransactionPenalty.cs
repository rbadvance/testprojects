﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace NURV.Data.Model
{
    [Table("tblTransactionPenalty")]
    internal class TransactionPenalty : Common.EntityObjectBase
    {
        [Key]
        [Column("TransactionPenaltyID")]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int TransactionPenaltyId { get; set; }
        public int TransactionNumber { get; set; }
        [Column("PenaltyID")]
        public int PenaltyId { get; set; }
        [Column(TypeName = "money")]
        public decimal? PenaltyAmount { get; set; }
        public bool? Active { get; set; }
    }
}
