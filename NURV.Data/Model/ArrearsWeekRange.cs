﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace NURV.Data.Model
{
    [Table("tblArrearsWeekRange")]
    internal class ArrearsWeekRange : Common.EntityObjectBase
    {
        [Key, Column("ArrearsWeekRangeID", Order = 0), DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int ArrearsWeekRangeId { get; set; }
        [Required]
        [StringLength(150), Column("ArrearsWeekRange")]
        public string ArrearsWeekRangeName { get; set; }
    }
}
