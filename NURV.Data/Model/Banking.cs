﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace NURV.Data.Model
{
    [Table("tblBanking")]
    internal class Banking : Common.EntityObjectBase
    {
        [Key, Column("ID", Order = 0), DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int Id { get; set; }
        [StringLength(250)]
        public string Bank { get; set; }
        [Column("BankCategoryID")]
        public int? BankCategoryId { get; set; }
        [Column(TypeName = "money")]
        public decimal? BankCredit { get; set; }
        [Column(TypeName = "datetime")]
        public DateTime? BankDate { get; set; }
        [Column(TypeName = "money")]
        public decimal? BankDebit { get; set; }
        [Column("BankSubCategoryID")]
        public int? BankSubCategoryId { get; set; }
        [StringLength(250)]
        public string CategoryForCreditors { get; set; }
        [StringLength(250)]
        public string ChequeNumber { get; set; }
        public bool? Closed { get; set; }
        [Column("ContactForCreditID")]
        public int? ContactForCreditId { get; set; }
        [Column(TypeName = "datetime")]
        public DateTime? CreationDate { get; set; }
        [StringLength(100)]
        public string CreationUser { get; set; }
        [StringLength(2000)]
        public string Description { get; set; }
        [StringLength(1000)]
        public string Drawer { get; set; }
        public int? TransactionNumber { get; set; }
        [Column("TrustID")]
        public int? TrustId { get; set; }
        [Column("time_stamp")]
        public byte[] TimeStamp { get; set; }
        [Column("MigrateID")]
        public int? MigrateId { get; set; }
        [Column(TypeName = "money")]
        public decimal? Writeoff { get; set; }
        [StringLength(200)]
        public string ContactCode { get; set; }
        [StringLength(200)]
        public string BankCategory { get; set; }
        [StringLength(200)]
        public string BankSubCategory { get; set; }
    }
}
