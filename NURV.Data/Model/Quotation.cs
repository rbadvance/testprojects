﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace NURV.Data.Model
{
    [Table("tblQuotation")]
    internal class Quotation : Common.EntityObjectBase
    {
        [Column("ID")]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public long Id { get; set; }
        [StringLength(12)]
        public string QuotationCode { get; set; }
        public int? TransactionNumber { get; set; }
        [Column("WhoPlacedQuotationID")]
        public int? WhoPlacedQuotationId { get; set; }
        [Column("HowPlacedQuotationID")]
        public int? HowPlacedQuotationId { get; set; }
        [Column(TypeName = "datetime")]
        public DateTime? QuotationCreatedDate { get; set; }
        [Column("QuotationCreatedByUserID")]
        [StringLength(10)]
        public string QuotationCreatedByUserId { get; set; }
        [Column(TypeName = "datetime")]
        public DateTime? QuotationPrintedDate { get; set; }
        public bool? QuotationSentByemail { get; set; }
        [Column("QuotationVOIDDate", TypeName = "datetime")]
        public DateTime? QuotationVoiddate { get; set; }
        [Column(TypeName = "datetime")]
        public DateTime? QuotationConvertedDate { get; set; }
        [StringLength(10)]
        public string QuotationConvertedBy { get; set; }
        [Column("QuotationConvertedUserID")]
        [StringLength(10)]
        public string QuotationConvertedUserId { get; set; }
        [Column(TypeName = "datetime")]
        public DateTime? QuotationDeletedDate { get; set; }
        public bool? QuotationLock { get; set; }
        [StringLength(15)]
        public string OnlineQuotationNumber { get; set; }
        [StringLength(500)]
        public string OnlineQuotationEmail { get; set; }
        public bool? OnlineSelfManaged { get; set; }
        [Column("OnlineREACode")]
        [StringLength(50)]
        public string OnlineReacode { get; set; }
        public bool? OnlineShortTermRental { get; set; }
        public int? OnlineCoverOption { get; set; }
        [Column(TypeName = "datetime")]
        public DateTime? OnlineEffectiveDate { get; set; }
        [StringLength(100)]
        public string OnlineSuburb { get; set; }
        [StringLength(10)]
        public string OnlineState { get; set; }
        [StringLength(10)]
        public string OnlinePostcode { get; set; }
        public int? OnlineLandsize { get; set; }
        [Column(TypeName = "money")]
        public decimal? OnlineWeeklyRent { get; set; }
        [Column("OnlineCCTV")]
        public bool? OnlineCctv { get; set; }
        public bool? OnlineFurnished { get; set; }
        public bool? OnlineElectronicAccess { get; set; }
        [Column("OnlineMPD")]
        public bool? OnlineMpd { get; set; }
        [Column("OnlineCPD")]
        public bool? OnlineCpd { get; set; }
        [StringLength(10)]
        public string OnlineYearBuilt { get; set; }
        [Column(TypeName = "money")]
        public decimal? OnlineBuildingSumInsured { get; set; }
        [Column(TypeName = "money")]
        public decimal? OnlineContentsSumInsured { get; set; }
        public bool? OnlineTenanted { get; set; }
        public bool? OnlineTenantArrears { get; set; }
        [Column(TypeName = "money")]
        public decimal? OnlineRentExcess { get; set; }
        [Column(TypeName = "money")]
        public decimal? OnlineBuildingExcess { get; set; }
        public bool? QuotationReminderEmailSent { get; set; }
        [Column("OnlineHowPayID")]
        public int? OnlineHowPayId { get; set; }
        [Column("OnlineHeardAboutTSI")]
        public int? OnlineHeardAboutTsi { get; set; }
        [Column(TypeName = "money")]
        public decimal? OnlineCoverCallCentrePremium { get; set; }
        [Column("OnlineQuotationURLAddress")]
        [StringLength(8000)]
        public string OnlineQuotationUrladdress { get; set; }
        [Column("OnlineArrearsClauseID")]
        public int? OnlineArrearsClauseId { get; set; }
        [Column(TypeName = "datetime")]
        public DateTime? QuotationExpiryDate { get; set; }
    }
}
