﻿using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Runtime.CompilerServices;

[assembly: InternalsVisibleTo("DynamicProxyGenAssembly2")]
namespace NURV.Data.Model
{
    [Table("tblRFLandSize")]
    internal class LandSize : Common.EntityObjectBase
    {
        [Key, Column(Order = 0), DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int LandSizeId { get; set; }

        [MaxLength(100), Column("LandSize")]
        public string LandSizeName { get; set; }

        [Column(TypeName = "decimal(10, 4)")]
        public decimal? Rate { get; set; }

        public int? ClassOfRiskID { get; set; }
        public int? PremiumTypeID { get; set; }
        [Column(TypeName = "datetime")]
        public DateTime? StartEffectiveDate { get; set; }
        [Column(TypeName = "datetime")]
        public DateTime? EndEffectiveDate { get; set; }
        public bool? IsUnknown { get; set; }
    }
}
