﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace NURV.Data.Model
{
    [Table("tblReaGroupDiscount")]
    internal class ReaGroupDiscount : Common.EntityObjectBase
    {
        [Key]
        [Column("id")]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int Id { get; set; }
        [Required]
        [Column("name")]
        [StringLength(50)]
        public string Name { get; set; }
        [Required]
        [Column("description")]
        [StringLength(50)]
        public string Description { get; set; }
        [Column("classOfRiskID")]
        public int ClassOfRiskId { get; set; }
        [Column("premiumTypeID")]
        public int PremiumTypeId { get; set; }
        [Column("effectiveStartDate", TypeName = "datetime")]
        public DateTime EffectiveStartDate { get; set; }
        [Column("effectiveEndDate", TypeName = "datetime")]
        public DateTime EffectiveEndDate { get; set; }
        [Column("percentage", TypeName = "money")]
        public decimal Percentage { get; set; }
    }
}
