﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace NURV.Data.Model
{
    [Table("tblDisclosureQuestions")]
    internal class DisclosureQuestion : Common.EntityObjectBase
    {
        [Key, Column("DisclosureQuestionsID", Order = 0), DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int DisclosureQuestionId { get; set; }
        public int TransactionNumber { get; set; }
        public bool? Declined { get; set; }
        [StringLength(250)]
        public string ReasonForDenial { get; set; }
        [StringLength(250)]
        public string SpecialTerms { get; set; }
        [Column("3ClaimsOrMore")]
        public bool? ThreeclaimsOrMore { get; set; }
        public bool? MoreThan5000 { get; set; }
        public bool? TheftOrFraud { get; set; }
        public bool? UnderwitingApprovalGranted { get; set; }
        public bool? UnderwitingApprovalWaiting { get; set; }
        [StringLength(2000)]
        public string Restrictions { get; set; }
        [StringLength(50)]
        public string ApprovedBy { get; set; }
        [Column(TypeName = "datetime")]
        public DateTime? ApprovedDate { get; set; }
        public bool? ExistingCircumstances { get; set; }
        [StringLength(500)]
        public string ExistingCircumstancesDescription { get; set; }
    }
}
