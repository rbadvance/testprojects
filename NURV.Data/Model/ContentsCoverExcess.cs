﻿using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Runtime.CompilerServices;

[assembly: InternalsVisibleTo("DynamicProxyGenAssembly2")]
namespace NURV.Data.Model
{
    [Table("tblRFContentsCoverExcess")]
    internal class ContentsCoverExcess : Common.EntityObjectBase
    {
        [Key, Column(Order = 0), DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int ContentsCoverExcessID { get; set; }

        [MaxLength(50), Column("ContentsCoverExcess")]
        public string ContentsCoverExcessName { get; set; }

        [MaxLength(10)]
        public string Branch { get; set; }

        public bool? Display { get; set; }
        public bool? Default { get; set; }

        [Column(TypeName = "decimal(10, 4)")]
        public decimal? Rate { get; set; }

        public int? ClassOfRiskID { get; set; }
        [Column(TypeName = "datetime")]
        public DateTime? StartEffectiveDate { get; set; }
        [Column(TypeName = "datetime")]
        public DateTime? EndEffectiveDate { get; set; }

        [MaxLength(3)]
        public string State { get; set; }

        public int? PremiumTypeID { get; set; }
    }
}
