﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

// Code scaffolded by EF Core assumes nullable reference types (NRTs) are not used or disabled.
// If you have enabled NRTs for your project, then un-comment the following line:
// #nullable disable

namespace NURV.Data.Model
{
    [Table("tblBonus_Backup")]
    internal class BonusBackup : Common.EntityObjectBase
    {
        [Key]
        [Column("id")]
        public int Id { get; set; }
        [Required]
        [Column("bonusName")]
        [StringLength(50)]
        public string BonusName { get; set; }
        [Required]
        [Column("bonusInterval")]
        [StringLength(50)]
        public string BonusInterval { get; set; }
        [Column("bonusNumber")]
        public int BonusNumber { get; set; }
        [Column("effectiveFrom", TypeName = "datetime")]
        public DateTime EffectiveFrom { get; set; }
        [Column("effectiveTo", TypeName = "datetime")]
        public DateTime EffectiveTo { get; set; }
        [Column("classOfRiskID")]
        public int ClassOfRiskId { get; set; }
        [Column("perTransaction")]
        public bool PerTransaction { get; set; }
        [Column("premiumTypeID")]
        public int? PremiumTypeId { get; set; }
        [Column("state")]
        [StringLength(25)]
        public string State { get; set; }
        [Column("premiumPaidBy")]
        [StringLength(100)]
        public string PremiumPaidBy { get; set; }
        [Column("howPlacedCoverID")]
        public int? HowPlacedCoverId { get; set; }
    }
}
