﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace NURV.Data.Model
{
    [Table("tblSettings")]
    internal class Settings : Common.EntityObjectBase
    {
        [Key]
        [Column("ID")]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int Id { get; set; }
        [Column("Path_SaReference")]
        [StringLength(1000)]
        public string PathSaReference { get; set; }
        [Column("Path_QldReference")]
        [StringLength(1000)]
        public string PathQldReference { get; set; }
        [Column("Path_WaReference")]
        [StringLength(1000)]
        public string PathWaReference { get; set; }
        [Column("Path_NtReference")]
        [StringLength(1000)]
        public string PathNtReference { get; set; }
        [Column("Path_VicReference")]
        [StringLength(1000)]
        public string PathVicReference { get; set; }
        [Column("Path_NswReference")]
        [StringLength(1000)]
        public string PathNswReference { get; set; }
        [Column("Path_TasReference")]
        [StringLength(1000)]
        public string PathTasReference { get; set; }
        [Column("Path_ActReference")]
        [StringLength(1000)]
        public string PathActReference { get; set; }
        public int? NextTransactionNumber { get; set; }
        public int? NextBatchNumber { get; set; }
        public int? NextRenewalBatchNumber { get; set; }
        public int? NextLapsingBatchNumber { get; set; }
        [Column("NextBatchNumber_TA")]
        public int? NextBatchNumberTa { get; set; }
        [Column("NextBatchNumber_BD")]
        public int? NextBatchNumberBd { get; set; }
        [Column("NextBatchNumber_Aud")]
        public int? NextBatchNumberAud { get; set; }
        [StringLength(20)]
        public string NextChequeNumber { get; set; }
        public int? NextClaimNumber { get; set; }
        [Column("Path_ClaimsExperience")]
        [StringLength(1000)]
        public string PathClaimsExperience { get; set; }
        [StringLength(250)]
        public string EmailServer { get; set; }
        [Column(TypeName = "money")]
        public decimal? ToSelfCancelFee { get; set; }
        [Column(TypeName = "money")]
        public decimal? MinimumRefundAmount { get; set; }
        [Column("time_stamp")]
        public byte[] TimeStamp { get; set; }
        public float? SumInsuredIncrease { get; set; }
        public int? NextLandlordNumber { get; set; }
        [Column("NextBPayReference")]
        public int? NextBpayReference { get; set; }
        public int? NextPolicyNumber { get; set; }
        [StringLength(1000)]
        public string CanUseContactEditForm { get; set; }
        [Column("NextBatchNumber_MailMerge")]
        public int? NextBatchNumberMailMerge { get; set; }
        public int? NextLppPolicyNumber { get; set; }
        public int? NextSssPolicyNumber { get; set; }
        [Column(TypeName = "money")]
        public decimal? MinimumImposedExcess { get; set; }
        public int? MaxCreditTermsExtension { get; set; }
        [StringLength(255)]
        public string TransactionsBlankStatusEmailRecipients { get; set; }
        [Column("EArchiveFileLocation")]
        [StringLength(100)]
        public string EarchiveFileLocation { get; set; }
        [Column("NextRefundID")]
        public int? NextRefundId { get; set; }
        public int? NextCommercialBatchNumber { get; set; }
        [Column("NextBatchNumber_CL")]
        public int? NextBatchNumberCl { get; set; }
    }
}
