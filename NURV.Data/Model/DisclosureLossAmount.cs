﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace NURV.Data.Model
{
    [Table("tblDisclosureLossAmount")]
    internal class DisclosureLossAmount : Common.EntityObjectBase
    {
        [Key]
        [Column("DisclosureLossAmountID")]
        public int DisclosureLossAmountId { get; set; }
        [Required]
        [StringLength(50)]
        [Column("DisclosureLossAmount")]
        public string DisclosureLossAmountName { get; set; }
        public bool? Display { get; set; }
        public int? SortOrder { get; set; }
    }
}
