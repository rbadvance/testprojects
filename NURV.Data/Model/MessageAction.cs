﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace NURV.Data.Model
{
    [Table("tblMessageAction")]
    internal class MessageAction : Common.EntityObjectBase
    {
        [Required]
        [Column("MessageID")]
        [StringLength(50)]
        public string MessageId { get; set; }
        [Required]
        [StringLength(100)]
        public string Action { get; set; }
    }
}
