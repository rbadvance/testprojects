﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace NURV.Data.Model
{
    [Table("tblStatusType")]
    internal class StatusType : Common.EntityObjectBase
    {
        [Key]
        [Column("StatusTypeID")]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int StatusTypeId { get; set; }
        [StringLength(200)]
        [Column("StatusType")]
        public string Type { get; set; }
    }
}
