﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace NURV.Data.Model
{
    [Table("tblBonusTime")]
    internal class BonusTime : Common.EntityObjectBase
    {
        [Key, Column("BonusTimeID", Order = 0), DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int BonusTimeId { get; set; }
        [StringLength(50)]
        public string Description { get; set; }
        public int? BonusMonths { get; set; }
        public int? BonusDays { get; set; }
        public bool? Display { get; set; }
        public bool? Removed { get; set; }
        [Column(TypeName = "datetime")]
        public DateTime? CreatedDate { get; set; }
        [StringLength(100)]
        public string CreatedBy { get; set; }
    }
}
