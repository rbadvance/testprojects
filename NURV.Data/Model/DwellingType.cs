﻿using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Runtime.CompilerServices;

[assembly: InternalsVisibleTo("DynamicProxyGenAssembly2")]
namespace NURV.Data.Model
{
    [Table("tblRFDwellingType")]
    internal class DwellingType : Common.EntityObjectBase
    {
        [Key, Column(Order = 0), DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int DwellingTypeID { get; set; }

        [MaxLength(75), Column("DwellingType")]
        public string DwellingTypeName { get; set; }

        public bool? Display { get; set; }
        public int? ClassOfRiskID { get; set; }
        public bool? QuoteRequired { get; set; }

        [Column(TypeName = "decimal(10, 4)")]
        public decimal? DwellingTypePricingFactor { get; set; }
        [Column(TypeName = "datetime")]
        public DateTime? StartEffectiveDate { get; set; }
        [Column(TypeName = "datetime")]
        public DateTime? EndEffectiveDate { get; set; }
        public bool? IsUnknown { get; set; }
    }
}
