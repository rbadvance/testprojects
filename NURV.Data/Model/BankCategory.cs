﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace NURV.Data.Model
{
    [Table("tblBankCategory")]
    internal class BankCategory : Common.EntityObjectBase
    {
        [Key, Column("ID",Order = 0), DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int Id { get; set; }
        [Column("bc_id")]
        [StringLength(3)]
        public string BcId { get; set; }
        [Required]
        [StringLength(100)]
        public string Description { get; set; }
        [Column("time_stamp")]
        public byte[] TimeStamp { get; set; }
    }
}
