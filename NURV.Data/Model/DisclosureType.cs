﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace NURV.Data.Model
{
    [Table("tblDisclosureType")]
    internal class DisclosureType : Common.EntityObjectBase
    {
        [Key]
        [Column("DisclosureTypeID")]
        public int DisclosureTypeId { get; set; }
        [Required]
        [StringLength(50)]
        [Column("DisclosureType")]
        public string DisclosureTypeName { get; set; }
    }
}
