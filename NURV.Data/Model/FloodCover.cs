﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace NURV.Data.Model
{
    [Table("tblRFFloodCover")]
    internal class FloodCover : Common.EntityObjectBase
    {
        [Key]
        [Column("FloodCoverID")]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int FloodCoverId { get; set; }
        [Column("FloodRiskLevelID")]
        public int? FloodRiskLevelId { get; set; }
        [Column("ClassOfRiskID")]
        public int? ClassOfRiskId { get; set; }
        [Column("PremiumTypeID")]
        public int? PremiumTypeId { get; set; }
        [Column(TypeName = "decimal(18, 14)")]
        public decimal? FloodRiskLevelFactor { get; set; }
        [Column(TypeName = "money")]
        public decimal? FloodRiskLevelBasePremium { get; set; }
        [Column(TypeName = "datetime")]
        public DateTime? StartEffectiveDate { get; set; }
        [Column(TypeName = "datetime")]
        public DateTime? EndEffectiveDate { get; set; }
    }
}
