﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace NURV.Data.Model
{
    [Table("tblRFLegalLiability")]
    internal class LegalLiability : Common.EntityObjectBase
    {
        [Key]
        [Column("LegalLiabilityID")]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int LegalLiabilityId { get; set; }
        [StringLength(10)]
        public string State { get; set; }
        [Column("ClassOfRiskID")]
        public int? ClassOfRiskId { get; set; }
        [Column("PremiumTypeID")]
        public int? PremiumTypeId { get; set; }
        [Column("LegalLiability", TypeName = "decimal(10, 4)")]
        public decimal? Amount { get; set; }
        [Column(TypeName = "datetime")]
        public DateTime? StartEffectiveDate { get; set; }
        [Column(TypeName = "datetime")]
        public DateTime? EndEffectiveDate { get; set; }
        [Column(TypeName = "datetime")]
        public DateTime? StartCreationDate { get; set; }
        [Column(TypeName = "datetime")]
        public DateTime? EndCreationDate { get; set; }
    }
}
