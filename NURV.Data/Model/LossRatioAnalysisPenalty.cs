﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace NURV.Data.Model
{
    [Table("tblLossRatioAnalysisPenalty")]
    internal class LossRatioAnalysisPenalty : Common.EntityObjectBase
    {
        [Column("PenaltyID")]
        public int? PenaltyId { get; set; }
        [Column("PenaltyGroupID")]
        public int? PenaltyGroupId { get; set; }
        [Column("RuleID")]
        public int? RuleId { get; set; }
        [Column("ClassOfRiskID")]
        public int? ClassOfRiskId { get; set; }
        [StringLength(500)]
        public string PenaltyDescription { get; set; }
        [StringLength(50)]
        public string PenaltyShortDescription { get; set; }
        [Column(TypeName = "money")]
        public decimal? CurrentExcess { get; set; }
        [Column(TypeName = "money")]
        public decimal? ImposedExcess { get; set; }
        [Column(TypeName = "decimal(7, 3)")]
        public decimal? ExcessIncreaseFactor { get; set; }
        [Column(TypeName = "money")]
        public decimal? ExcessIncreaseAmount { get; set; }
        public bool? Default { get; set; }
        [Column("TypeOfLossID")]
        public int? TypeOfLossId { get; set; }
        [Column("SubTypeOfLossID")]
        public int? SubTypeOfLossId { get; set; }
        public int? PenaltyValidityYears { get; set; }
        public bool? ApplyPenaltyInHoldCover { get; set; }
        public bool? ApplyPenaltyInRenewal { get; set; }
        public bool? DeclineLandlord { get; set; }
        public bool? Active { get; set; }
        [Column("SpecialConditionsMessageID")]
        [StringLength(50)]
        public string SpecialConditionsMessageId { get; set; }
        public bool? Display { get; set; }
    }
}
