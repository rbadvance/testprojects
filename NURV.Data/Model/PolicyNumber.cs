﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace NURV.Data.Model
{
    [Table("tblPolicyNumber")]
    internal class PolicyNumber : Common.EntityObjectBase
    {
        [Required]
        [StringLength(10)]
        public string State { get; set; }
        public int Month { get; set; }
        [StringLength(50)]
        [Column("PolicyNumber")]
        public string Number { get; set; }
        [Column("time_stamp")]
        public byte[] TimeStamp { get; set; }
        [StringLength(50)]
        public string InsurerCode { get; set; }
    }
}
