﻿using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Runtime.CompilerServices;

[assembly: InternalsVisibleTo("DynamicProxyGenAssembly2")]
namespace NURV.Data.Model
{
    [Table("tblClassOfRisk")]
    internal class ClassOfRisk : Common.EntityObjectBase
    {
        [Key, Column(Order = 0)] //, DatabaseGenerated(DatabaseGeneratedOption.Identity)] currently not an identity for some reason?
        public byte ID { get; set; }

        [MaxLength(100), Required]
        public string Data { get; set; }

        [MaxLength(100), Required]
        public string Code { get; set; }

        public bool Displayed { get; set; }

        [MaxLength(100)]
        public string CustomerData { get; set; }

        [MaxLength(100)]
        public string ProductName { get; set; }

        [MaxLength(5)]
        public string PaymentReferencePrefix { get; set; }

        public int? InvoiceSortOrder { get; set; }

        [MaxLength(300)]
        public string Description { get; set; }

        public bool? AllowNoTouchClaim { get; set; }

    }
}
