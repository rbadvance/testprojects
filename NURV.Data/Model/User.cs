﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace NURV.Data.Model
{
    [Table("tblUser")]
    internal class User : Common.EntityObjectBase
    {
        [Key]
        [Column("usr_LogonName")]
        [StringLength(50)]
        public string UsrLogonName { get; set; }
        [Required]
        [Column("usr_FullName")]
        [StringLength(50)]
        public string UsrFullName { get; set; }
        [Required]
        [StringLength(50)]
        public string EmailAddress { get; set; }
        [Required]
        [Column("SigningID")]
        [StringLength(10)]
        public string SigningId { get; set; }
        [Required]
        [StringLength(25)]
        public string OwnerBranch { get; set; }
        [Column("time_stamp")]
        public byte[] TimeStamp { get; set; }
        [StringLength(100)]
        public string RecentTransactions { get; set; }
        [StringLength(100)]
        public string RecentContacts { get; set; }
        [StringLength(100)]
        public string RecentClaims { get; set; }
        [StringLength(1000)]
        public string LastEmailSearch { get; set; }
        [StringLength(100)]
        public string CompanyPosition { get; set; }
        [StringLength(100)]
        public string SigningCredentials { get; set; }
        [StringLength(100)]
        public string Phone { get; set; }
        [StringLength(100)]
        public string Fax { get; set; }
        public bool? Deleted { get; set; }
        [Column(TypeName = "money")]
        public decimal ClaimsAuthLimit { get; set; }
        [Column("suncorpID")]
        [StringLength(50)]
        public string SuncorpId { get; set; }
        [Column("RecentClaims_New")]
        [StringLength(200)]
        public string RecentClaimsNew { get; set; }
        [Column("NURVX_SkinName")]
        [StringLength(100)]
        public string NurvxSkinName { get; set; }
        [Column("FormSize_Claims_Width")]
        public int? FormSizeClaimsWidth { get; set; }
        [Column("FormSize_Claims_Height")]
        public int? FormSizeClaimsHeight { get; set; }
        [Column("FormSize_Claims_Maximized")]
        public bool? FormSizeClaimsMaximized { get; set; }
        [Column("FormSize_ElectronicWorklist_Width")]
        public int? FormSizeElectronicWorklistWidth { get; set; }
        [Column("FormSize_ElectronicWorklist_Height")]
        public int? FormSizeElectronicWorklistHeight { get; set; }
        [Column("FormSize_ElectronicWorklist_Maximized")]
        public bool? FormSizeElectronicWorklistMaximized { get; set; }
        [Column("ElectronicWorklistLayout_New")]
        public string ElectronicWorklistLayoutNew { get; set; }
        [Column("pdfFilePath")]
        [StringLength(1000)]
        public string PdfFilePath { get; set; }
        [Column("UserGroupID")]
        public int? UserGroupId { get; set; }
        public string ScheduleOfLossLayout { get; set; }
        public string ScheduleOfLossEventsLayout { get; set; }
        public string ScheduleOfLossDetailsLayout { get; set; }
        [Column("REALossRatioBatchListLayout")]
        public string RealossRatioBatchListLayout { get; set; }
        [Column("SplitterLocation_SOL")]
        public int? SplitterLocationSol { get; set; }
        [Column("ClaimsAuthLimit_PerClaim", TypeName = "money")]
        public decimal? ClaimsAuthLimitPerClaim { get; set; }
        [Column("ClaimsAuthLimit_ExGratia", TypeName = "money")]
        public decimal? ClaimsAuthLimitExGratia { get; set; }
        public string PaymentEnquiryLayout { get; set; }
        public string ChangeLogLayout { get; set; }
        [Column("FormSize_Main_Width")]
        public int? FormSizeMainWidth { get; set; }
        [Column("FormSize_Main_Height")]
        public int? FormSizeMainHeight { get; set; }
        [Column("FormSize_Main_Maximized")]
        public bool? FormSizeMainMaximized { get; set; }
        [Column("FormSize_Main_Top")]
        public int? FormSizeMainTop { get; set; }
        [Column("FormSize_Main_Left")]
        public int? FormSizeMainLeft { get; set; }
        [Column("Ribbon_Minimized")]
        public bool? RibbonMinimized { get; set; }
        [Column("Ribbon_QATBelow")]
        public bool? RibbonQatbelow { get; set; }
        [Column("Ribbon_QATLayout")]
        public string RibbonQatlayout { get; set; }
        [Column("ClaimTeamID")]
        public int? ClaimTeamId { get; set; }
        [Column("Ribbon_Simplified")]
        public bool? RibbonSimplified { get; set; }
        [StringLength(100)]
        public string RecentCommercialTransactions { get; set; }
        [Column("VulnerabilityUserAccessLevelID")]
        public int? VulnerabilityUserAccessLevelId { get; set; }
        public string ContactSearchResultsLayout { get; set; }
        public bool? AllowNoTouchClaim { get; set; }
        public string ClaimsSearchResultsLayout { get; set; }
        public string TransactionSearchResultsLayout { get; set; }
    }
}
