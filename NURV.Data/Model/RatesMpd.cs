﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace NURV.Data.Model
{
    [Table("tblRatesMPD")]
    internal class RatesMpd : Common.EntityObjectBase
    {
        [Column("ID")]
        public int Id { get; set; }
        [Required]
        [StringLength(100)]
        public string PropertiesRange { get; set; }
        public bool Display { get; set; }
        [Column("ClassOfRiskID")]
        public int? ClassOfRiskId { get; set; }
        [Column("PremiumTypeID")]
        public int? PremiumTypeId { get; set; }
        [Column("MPDRPOptionID")]
        public int? MpdrpoptionId { get; set; }
        public int? Discount { get; set; }
        public int? MinProperties { get; set; }
        public int? MaxProperties { get; set; }
        [Column(TypeName = "decimal(10, 4)")]
        public decimal? Rate { get; set; }
        [Column(TypeName = "datetime")]
        public DateTime? StartEffectiveDate { get; set; }
        [Column(TypeName = "datetime")]
        public DateTime? EndEffectiveDate { get; set; }
        [StringLength(3)]
        public string State { get; set; }
    }
}
