﻿using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Runtime.CompilerServices;

[assembly: InternalsVisibleTo("DynamicProxyGenAssembly2")]
namespace NURV.Data.Model
{
    [Table("tblInformedByWho")]
    internal class InformedByWho : Common.EntityObjectBase
    {
        [Key, Column(Order = 0), DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int ID { get; set; }

        [MaxLength(50)]
        public string Data { get; set; }

        [MaxLength(250)]
        public string FullText { get; set; }

        public int? InCancellation { get; set; }
        public int? InHoldCover { get; set; }
        public int? InHoldCoverReferrer { get; set; }
        public int? InRefunds { get; set; }
        public int? InOverpayments { get; set; }
        public int? InExtraReturnPremium { get; set; }
        public int? InEndorsement { get; set; }
        public int? InEndorsementReferrer { get; set; }
        public bool? InClaimLodgement { get; set; }
        public bool? IsREA { get; set; }
        public bool? IsLandlord { get; set; }
        public int? ClaimSortOrder { get; set; }
        public bool? IsOnsite { get; set; }
        public bool? InEPRP { get; set; }

        [MaxLength(200)]
        public string DisplayText { get; set; }

        public bool? IsArrangedBy { get; set; }
    }
}
