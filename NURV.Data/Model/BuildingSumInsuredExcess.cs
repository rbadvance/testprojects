﻿using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Runtime.CompilerServices;

[assembly: InternalsVisibleTo("DynamicProxyGenAssembly2")]
namespace NURV.Data.Model
{
    [Table("tblRFBuildingSumInsuredExcess")]
    internal class BuildingSumInsuredExcess : Common.EntityObjectBase
    {
        [Key, Column(Order = 0), DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int BuildingSumInsuredID { get; set; }

        [MaxLength(50), Column("BuildingSumInsuredExcess")]
        public string BuildingSumInsuredExcessName { get; set; }

        public bool? Display { get; set; }

        [Column(TypeName = "decimal(10, 4)")]
        public decimal? Rate { get; set; }

        public int? ClassOfRiskID { get; set; }
        [Column(TypeName = "datetime")]
        public DateTime? StartEffectiveDate { get; set; }
        [Column(TypeName = "datetime")]
        public DateTime? EndEffectiveDate { get; set; }

        [MaxLength(3)]
        public string State { get; set; }

        public int? PremiumTypeID { get; set; }
        [Column(TypeName = "datetime")]
        public DateTime? StartCreationDate { get; set; }
        [Column(TypeName = "datetime")]
        public DateTime? EndCreationDate { get; set; }
        public bool? Default { get; set; }
    }
}
