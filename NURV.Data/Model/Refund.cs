﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace NURV.Data.Model
{
    [Table("tblRefund")]
    internal class Refund : Common.EntityObjectBase
    {
        [Key]
        [Column("ID")]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int Id { get; set; }
        public int? TransactionNumber { get; set; }
        [Column("Bank_tf_id")]
        public int? BankTfId { get; set; }
        [StringLength(20)]
        public string Type { get; set; }
        [StringLength(10)]
        public string PayMethod { get; set; }
        [StringLength(500)]
        public string PayeeName { get; set; }
        [StringLength(250)]
        public string PayeeAddress { get; set; }
        [StringLength(50)]
        public string PayeeSuburb { get; set; }
        [StringLength(10)]
        public string PayeeState { get; set; }
        [StringLength(10)]
        public string PayeePostCode { get; set; }
        [StringLength(500)]
        public string SendToName { get; set; }
        [StringLength(250)]
        public string SendToAddress { get; set; }
        [StringLength(50)]
        public string SendToSuburb { get; set; }
        [StringLength(10)]
        public string SendToState { get; set; }
        [StringLength(10)]
        public string SendToPostCode { get; set; }
        [StringLength(500)]
        public string SendCopyToName { get; set; }
        [StringLength(250)]
        public string SendCopyToAddress { get; set; }
        [StringLength(50)]
        public string SendCopyToSuburb { get; set; }
        [StringLength(10)]
        public string SendCopyToState { get; set; }
        [StringLength(10)]
        public string SendCopyToPostCode { get; set; }
        [Column(TypeName = "money")]
        public decimal? RefundAmount { get; set; }
        [Column("CancelReasonID")]
        public int? CancelReasonId { get; set; }
        [Column("BSB")]
        [StringLength(10)]
        public string Bsb { get; set; }
        [StringLength(50)]
        public string Account { get; set; }
        [Column(TypeName = "datetime")]
        public DateTime? CreatedDate { get; set; }
        [StringLength(50)]
        public string CreatedBy { get; set; }
        [Column(TypeName = "datetime")]
        public DateTime? ApprovedDate { get; set; }
        [StringLength(50)]
        public string ApprovedBy { get; set; }
        [Column(TypeName = "datetime")]
        public DateTime? PrintedDate { get; set; }
        [StringLength(50)]
        public string PrintedBy { get; set; }
        [Column("Offset_tf_id")]
        public int? OffsetTfId { get; set; }
        public int? DebitAccount { get; set; }
        [Column(TypeName = "datetime")]
        public DateTime? CancelledFrom { get; set; }
        [Column("RPTypeID")]
        public int? RptypeId { get; set; }
        [Column("WhoInformedID")]
        public int? WhoInformedId { get; set; }
        [StringLength(100)]
        public string MemberWhoInformed { get; set; }
        [Column("HowInformedID")]
        public int? HowInformedId { get; set; }
        [Column("EvidenceID")]
        public int? EvidenceId { get; set; }
        public bool? ForceNoCopy { get; set; }
        [StringLength(250)]
        public string PayeeCountry { get; set; }
        [StringLength(250)]
        public string SendCopyToCountry { get; set; }
        [StringLength(250)]
        public string SendToCountry { get; set; }
        [Column("Transfer_tf_id")]
        public int? TransferTfId { get; set; }
        [Column("Transfer_TransactionNumber1")]
        public int? TransferTransactionNumber1 { get; set; }
        [Column("Transfer_TransactionNumber2")]
        public int? TransferTransactionNumber2 { get; set; }
        [Column("Transfer_TransactionNumber3")]
        public int? TransferTransactionNumber3 { get; set; }
        [Column("Transfer_Amount1", TypeName = "money")]
        public decimal? TransferAmount1 { get; set; }
        [Column("Transfer_Amount2", TypeName = "money")]
        public decimal? TransferAmount2 { get; set; }
        [Column("Transfer_Amount3", TypeName = "money")]
        public decimal? TransferAmount3 { get; set; }
        [StringLength(200)]
        public string PayeeEmailAddress { get; set; }
        [StringLength(200)]
        public string SendCopyToEmailAddress { get; set; }
    }
}
