﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace NURV.Data.Model
{
    [Table("tblBushFireRiskLevel")]
    internal class BushFireRiskLevel : Common.EntityObjectBase
    {
        [Column("BushFireRiskLevelID")]
        public int BushFireRiskLevelId { get; set; }
        [StringLength(50)]
        public string BushFireCategory { get; set; }
        public bool? AllowNewCover { get; set; }
        public bool? AllowRenewal { get; set; }
        public bool? AllowCapping { get; set; }
        public int? DisplayColourR { get; set; }
        public int? DisplayColourG { get; set; }
        public int? DisplayColourB { get; set; }
        [Column(TypeName = "datetime")]
        public DateTime? StartEffectiveDate { get; set; }
        [Column(TypeName = "datetime")]
        public DateTime? EndEffectiveDate { get; set; }
    }
}
