﻿using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Runtime.CompilerServices;

[assembly: InternalsVisibleTo("DynamicProxyGenAssembly2")]
namespace NURV.Data.Model
{
    [Table("tblRFTenanted")]
    internal class Tenanted : Common.EntityObjectBase
    {
        [Key, Column(Order = 0), DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int TenantedID { get; set; }

        [MaxLength(10), Column("Tenanted")]
        public string TenantedName { get; set; }

        [Column(TypeName = "decimal(10, 4)")]
        public decimal? Rate { get; set; }

        public int? ClassOfRiskID { get; set; }
        //public int? PremiumTypeID { get; set; }
        [Column(TypeName = "datetime")]
        public DateTime? StartEffectiveDate { get; set; }
        [Column(TypeName = "datetime")]
        public DateTime? EndEffectiveDate { get; set; }

        //[MaxLength(3)]
        //public string State { get; set; }

        public bool? IsUnknown { get; set; }
    }
}
