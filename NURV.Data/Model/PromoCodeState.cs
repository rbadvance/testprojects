﻿using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Runtime.CompilerServices;

[assembly: InternalsVisibleTo("DynamicProxyGenAssembly2")]
namespace NURV.Data.Model
{
    [Table("tblPromoCodeState")]
    internal class PromoCodeState : Common.EntityObjectBase
    {
        [Key, Column(Order = 0), DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public short PromoCodeStateID { get; set; }

        public short PromoCodeID { get; set; }

        [MaxLength(3)]
        public string State { get; set; }
    }
}
