﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace NURV.Data.Model
{
    [Table("tblFloodRiskLevel")]
    internal class FloodRiskLevel : Common.EntityObjectBase
    {
        [Column("FloodRiskLevelID")]
        public int FloodRiskLevelId { get; set; }
        [StringLength(50)]
        public string FloodRiskCategory { get; set; }
        public bool? AllowNewCover { get; set; }
        public bool? AllowRenewal { get; set; }
        public bool? AllowCapping { get; set; }
        public int? DisplayColourR { get; set; }
        public int? DisplayColourG { get; set; }
        public int? DisplayColourB { get; set; }
        [Column(TypeName = "datetime")]
        public DateTime? StartEffectiveDate { get; set; }
        [Column(TypeName = "datetime")]
        public DateTime? EndEffectiveDate { get; set; }
    }
}
