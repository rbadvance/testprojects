﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace NURV.Data.Model
{
    [Table("tblEmbargo")]
    internal class Embargo : Common.EntityObjectBase
    {
        [Key, Column("ID", Order = 0), DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int Id { get; set; }
        [Column(TypeName = "datetime")]
        public DateTime EffectiveDate { get; set; }
        [Required]
        [StringLength(4)]
        public string PostCode { get; set; }
        [Column("ReasonID")]
        public int ReasonId { get; set; }
        [Column(TypeName = "datetime")]
        public DateTime? EmbargoLifted { get; set; }
        [StringLength(50)]
        public string AppliedBy { get; set; }
        [StringLength(50)]
        public string RemovedBy { get; set; }
        [StringLength(200)]
        public string Description { get; set; }
        [StringLength(100)]
        public string Suburb { get; set; }
        public bool? IsPermanent { get; set; }
    }
}
