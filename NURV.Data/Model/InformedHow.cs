﻿using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Runtime.CompilerServices;

[assembly: InternalsVisibleTo("DynamicProxyGenAssembly2")]
namespace NURV.Data.Model
{
    [Table("tblInformedHow")]
    internal class InformedHow : Common.EntityObjectBase
    {
        [Key, Column(Order = 0), DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int ID { get; set;  }

        [MaxLength(40)]
        public string Data { get; set; }

        public bool? InCancellation { get; set; }
        public bool? InHoldCoverLL { get; set; }
        public bool? InHoldCoverContact { get; set; }
        
        [MaxLength(40)]
        public string Display { get; set; }

        public bool? InQuotation { get; set; }
        public bool? InClaimLodgement { get; set; }
        public bool? InClaimCommunications { get; set; }
        public bool? InClaimScheduleOfLoss { get; set; }
        public bool? DisplayBonusOffer { get; set; }
        public bool? IsInvoice { get; set; }
        public bool? ClaimsOnlineOnly { get; set; }
        public bool? AllowNoTouchClaim { get; set; }
        public bool? IsQuote { get; set; }
    }
}
