﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace NURV.Data.Model
{
    [Table("tblDisclosureClaims")]
    internal class DisclosureClaim : Common.EntityObjectBase
    {
        [Key, Column("DisclosureClaimID", Order = 0), DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int DisclosureClaimsId { get; set; }
        [Column("DisclosureQuestionID")]
        public int DisclosureQuestionsId { get; set; }
        [Column("DisclosureClassID")]
        public int? DisclosureClassId { get; set; }
        [Column("DisclosureTypeID")]
        public int? DisclosureTypeId { get; set; }
        [StringLength(20)]
        public string DateOfLoss { get; set; }
        [Column("LossAmountID")]
        public int? LossAmountId { get; set; }
        [StringLength(250)]
        public string Insurer { get; set; }
        [StringLength(250)]
        public string RiskManagement { get; set; }
        [StringLength(1000)]
        public string AddressOfLoss { get; set; }
    }
}
