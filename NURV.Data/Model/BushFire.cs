﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace NURV.Data.Model
{
    [Table("tblRFBushFire")]
    internal class BushFire : Common.EntityObjectBase
    {
        [Key]
        [Column("BushFireID")]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int BushFireId { get; set; }
        [StringLength(100)]
        public string Suburb { get; set; }
        [StringLength(10)]
        public string Postcode { get; set; }
        [Column("BushFireRiskLevelID")]
        public int? BushFireRiskLevelId { get; set; }
        [Column("ClassOfRiskID")]
        public int? ClassOfRiskId { get; set; }
        [Column("PremiumTypeID")]
        public int? PremiumTypeId { get; set; }
        [StringLength(10)]
        public string BushFireZone { get; set; }
        [Column(TypeName = "decimal(18, 14)")]
        public decimal? BushFireRiskRatingFactor { get; set; }
        [Column(TypeName = "datetime")]
        public DateTime? StartEffectiveDate { get; set; }
        [Column(TypeName = "datetime")]
        public DateTime? EndEffectiveDate { get; set; }
    }
}
