﻿using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Runtime.CompilerServices;

[assembly: InternalsVisibleTo("DynamicProxyGenAssembly2")]
namespace NURV.Data.Model
{
    [Table("tblRFContentsCover")]
    internal class ContentsCover : Common.EntityObjectBase
    {
        [Key, Column(Order = 0)]
        public int OptionID { get; set; }

        [MaxLength(100)]
        public string ContentsSumInsured { get; set; }

        public int? ClassOfRiskID { get; set; }

        [Column("eprpID")]
        public int? EPRPID { get; set; }

        public bool? QuoteRequired { get; set; }
        [Column(TypeName = "datetime")]
        public DateTime? StartEffectiveDate { get; set; }
        [Column(TypeName = "datetime")]
        public DateTime? EndEffectiveDate { get; set; }

        [Column("premiumTypeID")]
        public int? PremiumTypeID { get; set; }

        public int? RenewOptionID { get; set; }
        public bool? Default { get; set; }

        [Column(TypeName = "decimal(19, 4)")]
        public decimal? MinContentsCover { get; set; }

        [Column(TypeName = "decimal(19, 4)")]
        public decimal? TopContentsCover { get; set; }

        [Column(TypeName = "decimal(24, 20)")]
        public decimal? Rate { get; set; }

        [MaxLength(3)]
        public string RFRate { get; set; }

        [Column(TypeName = "decimal(19, 4)")]
        public decimal? BaseAmount { get; set; }

        [Column(TypeName = "decimal(24, 20)")]
        public decimal? CUM { get; set; }
    }
}
