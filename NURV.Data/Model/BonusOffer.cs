﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace NURV.Data.Model
{
    [Table("tblBonusOffer")]
    internal class BonusOffer : Common.EntityObjectBase
    {
        [Key, Column("BonusOfferID", Order = 0), DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int BonusOfferId { get; set; }
        [StringLength(200)]
        public string Code { get; set; }
        [Column("BonusTimeID")]
        public int? BonusTimeId { get; set; }
        [Column("BonusUsageID")]
        public int? BonusUsageId { get; set; }
        [Column("BonusGrantID")]
        public int? BonusGrantId { get; set; }
        [Column("ContactID")]
        public int? ContactId { get; set; }
        [Column("LandlordID")]
        public int? LandlordId { get; set; }
        [Column("ArrangedByID")]
        public int? ArrangedById { get; set; }
        [Column(TypeName = "datetime")]
        public DateTime? CreatedDate { get; set; }
        [StringLength(50)]
        public string CreatedBy { get; set; }
        [Column(TypeName = "datetime")]
        public DateTime? EffectiveDateFrom { get; set; }
        [Column(TypeName = "datetime")]
        public DateTime? EffectiveDateTo { get; set; }
        [Column(TypeName = "datetime")]
        public DateTime? CreatedDateFrom { get; set; }
        [Column(TypeName = "datetime")]
        public DateTime? CreatedDateTo { get; set; }
        public bool? Inactive { get; set; }
        [Column(TypeName = "datetime")]
        public DateTime? LastUpdatedOn { get; set; }
        [StringLength(50)]
        public string LastUpdatedBy { get; set; }
    }
}
