﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace NURV.Data.Model
{
    [Table("tblPostcode")]
    internal class Postcode : Common.EntityObjectBase
    {
        [Key]
        [Column("ID")]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int Id { get; set; }
        [Required]
        [StringLength(10)]
        [Column("Postcode")]
        public string Code { get; set; }
        public bool? AllowTenants { get; set; }
        [StringLength(10)]
        public string AreaCode { get; set; }
        [StringLength(10)]
        public string AreaOther { get; set; }
        [Column("BSPNumber")]
        [StringLength(100)]
        public string Bspnumber { get; set; }
        [Column("BSPName")]
        [StringLength(100)]
        public string Bspname { get; set; }
        [StringLength(100)]
        public string DeliveryOffice { get; set; }
        [StringLength(100)]
        public string ParcelZone { get; set; }
        [StringLength(100)]
        public string PreSortIndicator { get; set; }
        [StringLength(100)]
        public string Suburb { get; set; }
        [StringLength(100)]
        public string State { get; set; }
        [StringLength(1000)]
        public string TenantsMessage { get; set; }
        [Column("time_stamp")]
        public byte[] TimeStamp { get; set; }
        public bool? Obsolete { get; set; }
        [StringLength(100)]
        public string Comments { get; set; }
        [StringLength(100)]
        public string Category { get; set; }
    }
}
