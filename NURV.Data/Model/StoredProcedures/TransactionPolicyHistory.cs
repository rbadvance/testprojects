﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace NURV.Data.Model.StoredProcedures
{
    [NotMapped]
    internal class TransactionPolicyHistory
    {
        public int KeyID { get; set; }
        public int? ParentKeyID { get; set; }
        public int TransactionNumber { get; set; }
        public int? OriginalTransactionNumber { get; set; }
        public int? ParentTransaction { get; set; }
        public int TxnLevel { get; set; }
        public int? PremiumTypeID { get; set; }

        [Required, MaxLength(10)]
        public string Code { get; set; }

        [MaxLength(255)]
        public string Payee { get; set; }

        [MaxLength(1000)]
        public string Reason { get; set; }

        [MaxLength(50)]
        public string StatusOfTransaction { get; set; }

        public DateTime? EffectiveDate { get; set; }
        public DateTime? RenewalDate { get; set; }
        public DateTime? CreationDate { get; set; }

        [Column(TypeName = "decimal(19, 4)")]
        public decimal? PremiumTotal { get; set; }

        [Column(TypeName = "decimal(19, 4)")]
        public decimal? Balance { get; set; }

        [Column(TypeName = "decimal(19, 4)")]
        public decimal? CurrentWeeklyRent { get; set; }

        [Column(TypeName = "decimal(19, 4)")]
        public decimal? ContentsSumInsured { get; set; }
    }
}
