﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace NURV.Data.Model
{
    [Table("tblEndorsement")]
    internal class Endorsement : Common.EntityObjectBase
    {
        [Column("ID"), DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int Id { get; set; }
        [Column(TypeName = "datetime")]
        public DateTime? EndorsementDate { get; set; }
        [StringLength(2000)]
        public string EndorsementNotes { get; set; }
        public int? TransactionNumber { get; set; }
        public int? Version { get; set; }
        public bool? ApplicationReceived { get; set; }
        [StringLength(50)]
        public string BuildingType { get; set; }
        [Column("ContactID")]
        public int? ContactId { get; set; }
        public bool? DoNotRenew { get; set; }
        [Column("DoNotRenew_Reason")]
        [StringLength(500)]
        public string DoNotRenewReason { get; set; }
        [StringLength(250)]
        public string LandlordAddress { get; set; }
        [Column("LandlordID")]
        public int? LandlordId { get; set; }
        [StringLength(500)]
        public string LandlordName { get; set; }
        [StringLength(25)]
        public string LandlordState { get; set; }
        [StringLength(100)]
        public string LandlordSuburb { get; set; }
        public int? LinkedToLppTransaction { get; set; }
        [StringLength(250)]
        public string Mortgagee { get; set; }
        public bool? OtherSchemesPaid { get; set; }
        [Column(TypeName = "datetime")]
        public DateTime? OtherSchemesPaidDate { get; set; }
        [StringLength(100)]
        public string PreferredMailingAddress { get; set; }
        [StringLength(100)]
        public string PremiumPaidBy { get; set; }
        [Column(TypeName = "datetime")]
        public DateTime? RenewalDate { get; set; }
        [Column("time_stamp")]
        public byte[] TimeStamp { get; set; }
        [Column("end_con_id_old")]
        public int? EndConIdOld { get; set; }
        [Column("end_Printed", TypeName = "datetime")]
        public DateTime? EndPrinted { get; set; }
        [Column(TypeName = "datetime")]
        public DateTime? SchemePaidDate { get; set; }
        [StringLength(50)]
        public string Username { get; set; }
        [Column("MultipleEndorsementID")]
        public int? MultipleEndorsementId { get; set; }
        [StringLength(10)]
        public string LandlordPostcode { get; set; }
        [StringLength(20)]
        public string CombinedPolicyDiscountPolicyLink { get; set; }
        public bool? SendElectronicCommunication { get; set; }
    }
}
