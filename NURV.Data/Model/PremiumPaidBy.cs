﻿using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Runtime.CompilerServices;

[assembly: InternalsVisibleTo("DynamicProxyGenAssembly2")]
namespace NURV.Data.Model
{
    [Table("tblPremiumPaidBy")]
    internal class PremiumPaidBy : Common.EntityObjectBase
    {
        [Key, Column(Order = 0), DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int PremiumPaidByID { get; set; }

        [MaxLength(20), Column("PremiumPaidBy")]
        public string PremiumPaidByName { get; set; }

        [MaxLength(20)]
        public string ToPay { get; set; }

        public int? PremiumTypeID { get; set; }
        public bool? NonREAReferrer { get; set; }
        public bool? DisplayInManaged { get; set; }
        public bool? DisplayInSelfManaged { get; set; }
        public bool? CopyAtRenewal { get; set; }
        public bool? IsLandlord { get; set; }
        public bool? IsArangedBy { get; set; }
    }
}
