﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Runtime.CompilerServices;
using System.ComponentModel.DataAnnotations.Schema;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel;

namespace NURV.Data.Model.Common
{
    internal abstract class EntityObjectWithModifiedBase : EntityObjectWithCreatedBase
    {
        public DateTimeOffset? ModifiedDate { get; set; }

        [MaxLength(50)]
        public string ModifiedBy { get; set; }
    }
}
