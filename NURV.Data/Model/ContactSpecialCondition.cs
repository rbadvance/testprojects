﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace NURV.Data.Model
{
    [Table("tblContactSpecialCondition")]
    internal class ContactSpecialCondition : Common.EntityObjectBase
    {
        [Key]
        [Column("SpecialConditionID")]
        public int SpecialConditionId { get; set; }
        [Column("ContactID")]
        public int ContactId { get; set; }
        [Key]
        [Column("PremiumTypeID")]
        public int PremiumTypeId { get; set; }
        [Key]
        [Column(TypeName = "datetime")]
        public DateTime CreatedDate { get; set; }
        [Required]
        [StringLength(100)]
        public string CreatedBy { get; set; }
        [Column(TypeName = "datetime")]
        public DateTime? EndDate { get; set; }
        [StringLength(100)]
        public string RemovedBy { get; set; }
    }
}
