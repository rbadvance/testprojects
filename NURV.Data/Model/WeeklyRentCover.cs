﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace NURV.Data.Model
{
    [Table("tblRFWeeklyRentCover")]
    internal class WeeklyRentCover : Common.EntityObjectBase
    {
        [Column("ID")]
        public int Id { get; set; }
        [StringLength(50)]
        public string RentRange { get; set; }
        public int? TopWeeklyRental { get; set; }
        public bool? Displayed { get; set; }
        [Column("ClassOfRiskID")]
        public int? ClassOfRiskId { get; set; }
        [Column("eprpID")]
        public int? EprpId { get; set; }
        public bool? QuoteRequired { get; set; }
        [Column(TypeName = "datetime")]
        public DateTime? StartEffectiveDate { get; set; }
        [Column(TypeName = "datetime")]
        public DateTime? EndEffectiveDate { get; set; }
        [Column("premiumTypeID")]
        public int? PremiumTypeId { get; set; }
        [Column("RenewOptionID")]
        public int? RenewOptionId { get; set; }
        public bool? Default { get; set; }
        [Column("URange")]
        public int? Urange { get; set; }
        [Column("LRange")]
        public int? Lrange { get; set; }
        [Column(TypeName = "decimal(24, 20)")]
        public decimal? Rate { get; set; }
        [Column("RFState")]
        [StringLength(3)]
        public string Rfstate { get; set; }
        [Column(TypeName = "money")]
        public decimal? BaseAmount { get; set; }
        [Column("CUM", TypeName = "decimal(24, 20)")]
        public decimal? Cum { get; set; }
    }
}
