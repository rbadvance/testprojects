﻿using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Runtime.CompilerServices;

[assembly: InternalsVisibleTo("DynamicProxyGenAssembly2")]
namespace NURV.Data.Model
{
    [Table("tblPromoCode")]
    internal class PromoCode : Common.EntityObjectBase
    {
        [Key, Column(Order = 0), DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public short PromoCodeID { get; set; }

        [MaxLength(200), Required, Column("PromoCode")]
        public string PromoCodeName { get; set; }
        [Column(TypeName = "datetime")]
        public DateTime CreatedDate { get; set; }

        [MaxLength(50), Required]
        public string CreatedBy { get; set; }
        [Column(TypeName = "datetime")]
        public DateTime EffectiveDateFrom { get; set; }
        [Column(TypeName = "datetime")]
        public DateTime? EffectiveDateTo { get; set; }
        public bool? Inactive { get; set; }
        [Column(TypeName = "datetime")]
        public DateTime? LastupdatedOn { get; set; }

        [MaxLength(50)]
        public string LastUpdatedBy { get; set; }
    }
}
