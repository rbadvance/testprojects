﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace NURV.Data.Model
{
    [Table("tblClaim")]
    internal class Claim : Common.EntityObjectBase
    {
        [Key, Column("ID", Order = 0), DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int Id { get; set; }
        [StringLength(250)]
        public string Action { get; set; }
        [StringLength(1000)]
        public string AddressCombo { get; set; }
        [StringLength(100)]
        public string AgentFax { get; set; }
        [StringLength(100)]
        public string AgentPhone { get; set; }
        public bool? BailFeePaid { get; set; }
        [Column(TypeName = "money")]
        public decimal? BondAmount { get; set; }
        [Column("Delete_Jun08_ClaimDate", TypeName = "datetime")]
        public DateTime? DeleteJun08ClaimDate { get; set; }
        [StringLength(50)]
        public string ClaimNumber { get; set; }
        public bool? ClaimsFeeFlag { get; set; }
        [Column("ClassOfRiskID")]
        public int? ClassOfRiskId { get; set; }
        [Column("Delete_Jun08_ClientPaid", TypeName = "money")]
        public decimal? DeleteJun08ClientPaid { get; set; }
        [Column(TypeName = "datetime")]
        public DateTime? ClosingDate { get; set; }
        [Column(TypeName = "text")]
        public string Comment { get; set; }
        [StringLength(250)]
        public string CommRecipient { get; set; }
        [Column("ContactID")]
        public int? ContactId { get; set; }
        [Column(TypeName = "datetime")]
        public DateTime? CreationDate { get; set; }
        [StringLength(100)]
        public string CreationUser { get; set; }
        [Column(TypeName = "datetime")]
        public DateTime? DateOfLoss { get; set; }
        public bool? Declined { get; set; }
        [Column(TypeName = "text")]
        public string Details { get; set; }
        [Column(TypeName = "money")]
        public decimal? Excess { get; set; }
        [Column(TypeName = "datetime")]
        public DateTime? FinalisedDate { get; set; }
        public bool? FinalisedFlag { get; set; }
        [Column(TypeName = "datetime")]
        public DateTime? InceptionDate { get; set; }
        [Column("Landlord_Delete")]
        [StringLength(100)]
        public string LandlordDelete { get; set; }
        [StringLength(500)]
        public string LandlordAddress { get; set; }
        [StringLength(100)]
        public string Location { get; set; }
        [Column("Pre1091_LossOfRent", TypeName = "money")]
        public decimal? Pre1091LossOfRent { get; set; }
        [Column(TypeName = "money")]
        public decimal? ManualAdjustment { get; set; }
        [Column(TypeName = "datetime")]
        public DateTime? ModificationDate { get; set; }
        public bool? OutstandingClaimFlag { get; set; }
        [StringLength(50)]
        public string OwnerBranch { get; set; }
        [StringLength(50)]
        public string PolicyNumber { get; set; }
        [StringLength(250)]
        public string PropertyManager { get; set; }
        [Column("QAssessorCode")]
        [StringLength(100)]
        public string QassessorCode { get; set; }
        [Column("QCountry")]
        [StringLength(100)]
        public string Qcountry { get; set; }
        [Column("QItemInvolved")]
        [StringLength(100)]
        public string QitemInvolved { get; set; }
        [Column("QLossOccurred")]
        [StringLength(100)]
        public string QlossOccurred { get; set; }
        [Column("QPolicyType")]
        [StringLength(100)]
        public string QpolicyType { get; set; }
        [Column("QPremiumClass")]
        [StringLength(100)]
        public string QpremiumClass { get; set; }
        [Column("QRiskNumber")]
        [StringLength(100)]
        public string QriskNumber { get; set; }
        [Column("QTypeOfClaim")]
        [StringLength(100)]
        public string QtypeOfClaim { get; set; }
        [Column(TypeName = "datetime")]
        public DateTime? ReceivedHeadOfficeDate { get; set; }
        [Column(TypeName = "money")]
        public decimal? RelettingExpenses { get; set; }
        [Column(TypeName = "datetime")]
        public DateTime? ReviewDate { get; set; }
        [Column(TypeName = "datetime")]
        public DateTime? SentToHeadOfficeDate { get; set; }
        [StringLength(100)]
        public string StaffName { get; set; }
        [StringLength(50)]
        public string SubAgentCode { get; set; }
        [Column(TypeName = "money")]
        public decimal? TotalClaimAmountAdjustment { get; set; }
        public int? TransactionNumber { get; set; }
        [StringLength(100)]
        public string UwClaimNumber { get; set; }
        [StringLength(50)]
        public string UwCode { get; set; }
        [StringLength(1000)]
        public string UwMemo { get; set; }
        [Column(TypeName = "datetime")]
        public DateTime? VacancyDate { get; set; }
        public short? YearOfClaim { get; set; }
        public int? Deleted { get; set; }
        [Column("time_stamp")]
        public byte[] TimeStamp { get; set; }
        [Column("clm_con_id_old")]
        public int? ClmConIdOld { get; set; }
        [StringLength(200)]
        public string ClassOfRisk { get; set; }
        [StringLength(200)]
        public string ContactCode { get; set; }
        [Column("Delete_Jun08_ChequeNumber")]
        [StringLength(1000)]
        public string DeleteJun08ChequeNumber { get; set; }
        [StringLength(8000)]
        public string Recovery { get; set; }
        [StringLength(50)]
        public string TypeOfLossOfRent { get; set; }
        [Column(TypeName = "datetime")]
        public DateTime? AssessorAppointedDate { get; set; }
        [StringLength(1000)]
        public string AssessorContactDetails { get; set; }
        [Column("BoldUWMemo")]
        public bool? BoldUwmemo { get; set; }
        [Column("ClaimStatusID")]
        public int? ClaimStatusId { get; set; }
        [Column(TypeName = "money")]
        public decimal? WeeklyRentAmount { get; set; }
        public int? RentLossDays { get; set; }
        public int? RentPaidDays { get; set; }
        public bool? BondCreditApplies { get; set; }
        [Column(TypeName = "datetime")]
        public DateTime? ReopenedDate { get; set; }
        [StringLength(20)]
        public string ArchiveBoxNumber { get; set; }
        [Column("Delete_Jun08_AssessorFee", TypeName = "money")]
        public decimal? DeleteJun08AssessorFee { get; set; }
        [Column(TypeName = "datetime")]
        public DateTime? DateStatusChanged { get; set; }
        [Column(TypeName = "datetime")]
        public DateTime? FollowUpDate { get; set; }
        [Column("ClaimDeclineReasonID")]
        public int? ClaimDeclineReasonId { get; set; }
        public bool? IsAssessedClaim { get; set; }
        [Column("LandlordID")]
        public int? LandlordId { get; set; }
        [Column(TypeName = "money")]
        public decimal? EstimatedAssessorFee { get; set; }
        [Column("REAhandlingClaim")]
        [StringLength(50)]
        public string ReahandlingClaim { get; set; }
        [Column("REAisContact")]
        public bool? ReaisContact { get; set; }
        [StringLength(20)]
        public string EventCode { get; set; }
        [Column("LPPLossOfRent")]
        public bool? LpplossOfRent { get; set; }
        [Column("LodgedHowID")]
        public int? LodgedHowId { get; set; }
        [Column(TypeName = "datetime")]
        public DateTime? LodgementDate { get; set; }
        [Column("LodgedByID")]
        public int? LodgedById { get; set; }
        [Column("LodgedWhoContactID")]
        public int? LodgedWhoContactId { get; set; }
        [Column("LodgedByStaffID")]
        public int? LodgedByStaffId { get; set; }
        [Column("ClaimContactID")]
        public int? ClaimContactId { get; set; }
        [Column("REAHandlingClaimContactID")]
        public int? ReahandlingClaimContactId { get; set; }
        [StringLength(2000)]
        public string BriefDescription { get; set; }
        [Column("ClaimTypeID")]
        public int? ClaimTypeId { get; set; }
        [Column("ClaimAssignedToID")]
        public int? ClaimAssignedToId { get; set; }
        [Column("PropertyManagerID")]
        public int? PropertyManagerId { get; set; }
        [Column("InsuredStaffID")]
        public int? InsuredStaffId { get; set; }
        [Column(TypeName = "datetime")]
        public DateTime? LastImportDate { get; set; }
        [Column(TypeName = "datetime")]
        public DateTime? LastExportDate { get; set; }
        [StringLength(300)]
        public string ClaimPath { get; set; }
        public int? ClaimVersion { get; set; }
        [Column("ClaimPriorityID")]
        public int? ClaimPriorityId { get; set; }
        public int? NextLossOfRentRowNumber { get; set; }
        public int? NextUntenantableRowNumber { get; set; }
        public int? NextAdditionalBenefitRowNumber { get; set; }
        public int? NextScheduleOfLossRowNumber { get; set; }
        [Column("StatusOfClaimID")]
        public int? StatusOfClaimId { get; set; }
        [Column("TaxStatusID")]
        public int? TaxStatusId { get; set; }
        [Column("TaxStatusABN")]
        [StringLength(20)]
        public string TaxStatusAbn { get; set; }
        [Column(TypeName = "decimal(10, 2)")]
        public decimal? TaxStatusPercentage { get; set; }
        [Column("ITCClaimPercentage", TypeName = "decimal(10, 2)")]
        public decimal? ItcclaimPercentage { get; set; }
        [Column("ITCPremiumPercentage", TypeName = "decimal(10, 2)")]
        public decimal? ItcpremiumPercentage { get; set; }
        [StringLength(50)]
        public string AssignedToUser { get; set; }
        public bool? TenantsStillInProperty { get; set; }
        public bool? ArrearsClauseApplies { get; set; }
        [Column(TypeName = "datetime")]
        public DateTime? ArrearsClauseCheckedDate { get; set; }
        [Column("LossOfRentDeclinedReasonID")]
        public int? LossOfRentDeclinedReasonId { get; set; }
        [Column("RecoveryStatusID")]
        public int? RecoveryStatusId { get; set; }
        public bool? TenantApplicationReceived { get; set; }
        public bool? RecoveryApplicationReceived { get; set; }
        public bool? RecoveryTenantInsured { get; set; }
        [StringLength(500)]
        public string RecoveryInsuranceDetails { get; set; }
        public bool? LossAdjusterDoNotAskAgain { get; set; }
        [Column("DisputeOutcomeID")]
        public int? DisputeOutcomeId { get; set; }
        [StringLength(100)]
        public string LockedBy { get; set; }
        [Column(TypeName = "datetime")]
        public DateTime? LockedAt { get; set; }
        [StringLength(500)]
        public string Landlord { get; set; }
        public int? NextInvoiceRowNumber { get; set; }
        public bool? DrugLabRelated { get; set; }
        [Column("InvestigationDetectionMethodID")]
        public int? InvestigationDetectionMethodId { get; set; }
        [Column("InvestigationOutcomeID")]
        public int? InvestigationOutcomeId { get; set; }
        [StringLength(100)]
        public string InvestigationReviewOfficer { get; set; }
        public bool? NaturalHazard { get; set; }
        public bool? Untenantable { get; set; }
        [Column("ClaimClosedReasonID")]
        public int? ClaimClosedReasonId { get; set; }
        [Column("ClaimWithdrawnReasonID")]
        public int? ClaimWithdrawnReasonId { get; set; }
        [StringLength(160)]
        public string ReminderReason { get; set; }
        public int? NextBreachNoticeRowNumber { get; set; }
        [Column("COVID19Related")]
        public bool? Covid19related { get; set; }
        [Column(TypeName = "money")]
        public decimal? ClaimantTotalEstimation { get; set; }
        public bool? ClaimOnlineReviewed { get; set; }
        [Column(TypeName = "datetime")]
        public DateTime? ClaimOnlineReviewedDate { get; set; }
        [StringLength(50)]
        public string ClaimOnlineReviewedUser { get; set; }
        public bool? OnlineReview { get; set; }
        [Column("NTCReviewed")]
        public bool? Ntcreviewed { get; set; }
        public bool? OnlineClaimProcessComplete { get; set; }
        [Column("NTCUntenantableRepairsNotCompleted")]
        public bool? NtcuntenantableRepairsNotCompleted { get; set; }
        [Column("NTCUntenantableOverlap")]
        public bool? NtcuntenantableOverlap { get; set; }
        [Column("NTCLORReletInFuture")]
        public bool? NtclorreletInFuture { get; set; }
        [Column("ContactMethodID")]
        public int? ContactMethodId { get; set; }
        [StringLength(150)]
        public string ContactTime { get; set; }
        [Column("InsuredContactMethodID")]
        public int? InsuredContactMethodId { get; set; }
        [StringLength(150)]
        public string InsuredContactTime { get; set; }
        public bool? FinancialHardship { get; set; }
        [Column(TypeName = "decimal(16, 2)")]
        public decimal? ExcessClaimAmt { get; set; }
        [Column(TypeName = "decimal(16, 2)")]
        public decimal? ExcessAgreedAmt { get; set; }
        [Column(TypeName = "decimal(16, 2)")]
        public decimal? ExcessWaivedAmt { get; set; }
        [Column(TypeName = "decimal(16, 2)")]
        public decimal? ExcessPaidAmt { get; set; }
        [Column(TypeName = "decimal(16, 2)")]
        public decimal? ExcessOwedAmt { get; set; }
        public int? NextDocumentRowNumber { get; set; }
        public int? NextCommunicationRowNumber { get; set; }
        public bool? IsContactByPhone { get; set; }
        public bool? IsInsuredContactByPhone { get; set; }
        [Column("IsCopyAllREAEmailToLandlord")]
        public bool? IsCopyAllReaemailToLandlord { get; set; }
        [StringLength(200)]
        public string AdditionalEmailRecipientAddress { get; set; }
        [StringLength(100)]
        public string LodgedByPhone { get; set; }
        [StringLength(200)]
        public string LodgedByEmail { get; set; }
        [Column("DecisionOutcomeID")]
        public int? DecisionOutcomeId { get; set; }
        [StringLength(20)]
        public string ClaimPaymentReference { get; set; }
        public int? NextQuoteRowNumber { get; set; }
    }
}
