﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace NURV.Data.Model
{
    [Table("tblRFTenantDamageExcess")]
    internal class TenantDamageExcess : Common.EntityObjectBase
    {
        [Key]
        [Column("TenantDamageExcessID")]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int TenantDamageExcessId { get; set; }
        [Column("TenantDamageExcess", TypeName = "money")]
        public decimal? Excess { get; set; }
        [Column("ClassOfRiskID")]
        public int? ClassOfRiskId { get; set; }
        [Column("PremiumTypeID")]
        public int? PremiumTypeId { get; set; }
        [StringLength(10)]
        public string State { get; set; }
        public bool? Display { get; set; }
        public bool? Default { get; set; }
        [Column(TypeName = "decimal(18, 4)")]
        public decimal? Rate { get; set; }
        [Column(TypeName = "datetime")]
        public DateTime? StartEffectiveDate { get; set; }
        [Column(TypeName = "datetime")]
        public DateTime? EndEffectiveDate { get; set; }
    }
}
