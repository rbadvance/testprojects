﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace NURV.Data.Model
{
    [Table("tblDisclosureClass")]
    internal class DisclosureClass : Common.EntityObjectBase
    {
        [Key]
        [Column("DisclosureClassID")]
        public int DisclosureClassId { get; set; }
        [Required]
        [StringLength(50), Column("DisclosureClass")]
        public string DisclosureClassName { get; set; }
    }
}
