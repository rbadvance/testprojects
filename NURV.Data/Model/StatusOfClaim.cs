﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace NURV.Data.Model
{
    [Table("tblStatusOfClaim")]
    internal class StatusOfClaim : Common.EntityObjectBase
    {
        [Key]
        [Column("StatusOfClaimID")]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int StatusOfClaimId { get; set; }
        [StringLength(50)]
        [Column("StatusOfClaim")]
        public string Status { get; set; }
        [StringLength(500)]
        public string Description { get; set; }
        public int? SortOrder { get; set; }
        [Column("StatusTypeID")]
        public int? StatusTypeId { get; set; }
        public bool? AllowPayments { get; set; }
        [StringLength(50)]
        public string ClaimsOnlineStatus { get; set; }
        [StringLength(250)]
        public string ClaimsOnlineStatusDescription { get; set; }
        public bool? WhatsNextMessageRequired { get; set; }
        [StringLength(250)]
        public string WhatsNextMessage { get; set; }
        public bool? AllowNoTouchClaim { get; set; }
    }
}
