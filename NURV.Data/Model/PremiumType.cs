﻿using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Runtime.CompilerServices;

[assembly: InternalsVisibleTo("DynamicProxyGenAssembly2")]
namespace NURV.Data.Model
{
    [Table("tblPremiumType")]
    internal class PremiumType : Common.EntityObjectBase
    {
        [Key, Column(Order = 0)]
        public byte ID { get; set; }

        [MaxLength(100), Required]
        public string Data { get; set; }

        [MaxLength(10), Required]
        public string Code { get; set; }

        public bool Display { get; set; }
        public bool? DisplayBonusOffer { get; set; }
        public bool? AllowNoTouchClaim { get; set; }
    }
}
