﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace NURV.Data.Model
{
    [Table("tblTransactionPDSandFSG")]
    internal class TransactionPDSandFSG : Common.EntityObjectBase
    {
        public int? TransactionNumber { get; set; }
        [Column("PDSandFSGID")]
        public int? PdsandFsgid { get; set; }
        [Column(TypeName = "datetime")]
        public DateTime? DateSent { get; set; }
        [Column("docVersion")]
        [StringLength(100)]
        public string DocVersion { get; set; }
        public bool? SentToLandlord { get; set; }
        public bool? SentByEmail { get; set; }
    }
}
