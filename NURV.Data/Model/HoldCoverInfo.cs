﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace NURV.Data.Model
{
    [Table("tblHoldCoverInfo")]
    internal class HoldCoverInfo : Common.EntityObjectBase
    {
        public int? TransactionNumber { get; set; }
        public int? WhoRequested { get; set; }
        public int? HowRequested { get; set; }
        public bool? WelcomeLetter { get; set; }
        [Column(TypeName = "datetime")]
        public DateTime? WelcomePrinted { get; set; }
        [Column("AttachPDS")]
        public bool? AttachPds { get; set; }
        [Column("AttachFSG")]
        public bool? AttachFsg { get; set; }
        [Column("AttachRBIProposal")]
        public bool? AttachRbiproposal { get; set; }
        [Column("HowPayID")]
        public int? HowPayId { get; set; }
        public bool? Recreated { get; set; }
        public int? ReferToTrans { get; set; }
        [Column("WLJoiningID")]
        public int? WljoiningId { get; set; }
        [Column("ArrearsClauseID")]
        public int? ArrearsClauseId { get; set; }
        [Column("HeardAboutTSI")]
        public int? HeardAboutTsi { get; set; }
        [Column("BPayReceipt")]
        [StringLength(50)]
        public string BpayReceipt { get; set; }
    }
}
