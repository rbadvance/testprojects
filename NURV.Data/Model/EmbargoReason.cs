﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace NURV.Data.Model
{
    [Table("tblEmbargoReasons")]
    internal class EmbargoReason : Common.EntityObjectBase
    {
        [Key, Column("ID", Order = 0), DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int Id { get; set; }
        [Required]
        [StringLength(50)]
        public string Reason { get; set; }
        [StringLength(50)]
        public string ReasonDescription { get; set; }
        public int SortOrder { get; set; }
        public bool? Display { get; set; }
    }
}
