﻿using System.Drawing;
using System.Reflection;
using DevExpress.XtraEditors.Repository;
using DevExpress.XtraEditors.Registrator;
using DevExpress.XtraEditors.Drawing;
using DevExpress.XtraEditors.ViewInfo;
using DevExpress.Accessibility;

namespace NURV.Controls
{
    [UserRepositoryItem("RegisterFancyCheckEdit")]
    internal class RepositoryItemFancyCheckEdit : RepositoryItemCheckEdit
    {
        //The static constructor that calls the registration method
        static RepositoryItemFancyCheckEdit() { RegisterFancyCheckEdit(); }

        //Initialize new properties
        public RepositoryItemFancyCheckEdit()
        {
            _useDefaultMode = true;
        }

        //The unique name for the custom editor
        public const string CUSTOM_EDIT_NAME = "FancyCheckEdit";

        //Return the unique name
        public override string EditorTypeName { get { return CUSTOM_EDIT_NAME; } }

        //Register the editor
        public static void RegisterFancyCheckEdit()
        {
            //Icon representing the editor within a container editor's Designer
            Image img = null;
            try
            {
                img = (Bitmap)Bitmap.FromStream(Assembly.GetExecutingAssembly().
                  GetManifestResourceStream("DevExpress.CustomEditors.CustomEdit.bmp"));
            }
            catch
            {
            }
            EditorRegistrationInfo.Default.Editors.Add(new EditorClassInfo(CUSTOM_EDIT_NAME,
              typeof(FancyCheckEdit), typeof(RepositoryItemFancyCheckEdit),
              typeof(CheckEditViewInfo), new CheckEditPainter(),true, img, typeof(CheckEditAccessible)));
        }

        //A custom property
        private bool _useDefaultMode;

        public bool UseDefaultMode
        {
            get { return _useDefaultMode; }
            set
            {
                if (_useDefaultMode != value)
                {
                    _useDefaultMode = value;
                    OnPropertiesChanged();
                }
            }
        }

        //Override the Assign method
        public override void Assign(RepositoryItem item)
        {
            BeginUpdate();
            try
            {
                base.Assign(item);
                if (!(item is RepositoryItemFancyCheckEdit source))
                {
                    return;
                }
                _useDefaultMode = source.UseDefaultMode;
            }
            finally
            {
                EndUpdate();
            }
        }
    }
}
