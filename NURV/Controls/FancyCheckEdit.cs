﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using DevExpress.XtraEditors;
using System.Windows.Forms;
using System.Drawing;
using System.Drawing.Drawing2D;
using System.ComponentModel;
using DevExpress.XtraEditors.Controls;
using DevExpress.Utils.Drawing;
using DevExpress.XtraEditors.ViewInfo;

namespace NURV.Controls
{
    [ToolboxItem(true)]
    internal class FancyCheckEdit : CheckEdit, IAutoHeightControl
    {
        //Fields
        private Color _offBackColor = Color.FromArgb(222,82,70);
        private Color _offToggleColor = Color.WhiteSmoke;
        private Color _indeterminateBackColor = Color.Silver;
        private Color _indeterminateToggleColor = Color.Gainsboro;
        private Color _onBackColor = Color.FromArgb(26, 162, 96);
        private Color _onToggleColor = Color.WhiteSmoke;
        private bool _solidStyle = true;

        //Constructor
        public FancyCheckEdit() : base()
        {
            MinimumSize = new Size(20, 21);
            MaximumSize = new Size(0, 21);
            CheckState = CheckState.Indeterminate;
        }

        //The static constructor that calls the registration method
        static FancyCheckEdit() { RepositoryItemFancyCheckEdit.RegisterFancyCheckEdit(); }

        //Return the unique name
        public override string EditorTypeName { get => RepositoryItemFancyCheckEdit.CUSTOM_EDIT_NAME; }

        //Override the Properties property
        //Simply type-cast the object to the custom repository item type
        [DesignerSerializationVisibility(DesignerSerializationVisibility.Content)] //, Browsable(false)]
        public new RepositoryItemFancyCheckEdit Properties => base.Properties as RepositoryItemFancyCheckEdit;

        #region IAutoHeightControl implement
        bool IAutoHeightControl.SupportsAutoHeight { get { return true; } }

        public event EventHandler HeightChanged;
        event EventHandler IAutoHeightControl.HeightChanged
        {
            add { HeightChanged += value; }
            remove { HeightChanged -= value; }
        }
        protected void RaiseHeightChanged()
        {
            if (HeightChanged != null)
                HeightChanged(this, EventArgs.Empty);
        }

        int IAutoHeightControl.CalcHeight(GraphicsCache cache)
        {
            if (ViewInfo.IsReady)
            {
                IHeightAdaptable ih = ViewInfo as IHeightAdaptable;
                if (ih != null)
                    return ih.CalcHeight(cache, Width);
            }
            return Height;

        }
        #endregion

        protected override void OnPropertiesChanged()
        {
            base.OnPropertiesChanged();
            this.RaiseHeightChanged();
        }

        [Category("Advance")]
        public Color OffBackColor
        {
            get => _offBackColor;

            set
            {
                _offBackColor = value;
                Invalidate();
            }
        }

        [Category("Advance")]
        public Color OffToggleColor
        {
            get => _offToggleColor;

            set
            {
                _offToggleColor = value;
                Invalidate();
            }
        }

        [Category("Advance")]
        public Color IndeterminateBackColor
        {
            get => _indeterminateBackColor;

            set
            {
                _indeterminateBackColor = value;
                Invalidate();
            }
        }

        [Category("Advance")]
        public Color IndeterminateToggleColor
        {
            get => _indeterminateToggleColor;

            set
            {
                _indeterminateToggleColor = value;
                Invalidate();
            }
        }

        //Properties
        [Category("Advance")]
        public Color OnBackColor
        {
            get => _onBackColor;

            set
            {
                _onBackColor = value;
                Invalidate();
            }
        }

        [Category("Advance")]
        public Color OnToggleColor
        {
            get => _onToggleColor;

            set
            {
                _onToggleColor = value;
                Invalidate();
            }
        }

        [Category("Advance")]
        [DefaultValue(true)]
        public bool SolidStyle
        {
            get => _solidStyle;

            set
            {
                _solidStyle = value;
                Invalidate();
            }
        }

        [Category("Advance")]
        [DefaultValue(true)]
        public bool AllowTriState
        {
            get => Properties.AllowGrayed;

            set
            {
                Properties.AllowGrayed = value;
                if (Properties.AllowGrayed)
                {
                    base.CheckState = CheckState.Indeterminate;
                }
                Invalidate();
            }
        }

        [Category("Advance")]
        public CheckState DefaultValue
        {
            get => base.CheckState;

            set
            {
                base.CheckState = value;
                Invalidate();
            }
        }

        [Browsable(false)]
        public override CheckState CheckState
        {
            get => base.CheckState;
            set => base.CheckState = value;
        }

        [Browsable(false)]
        public override bool Checked
        {
            get => base.Checked;
            set => base.Checked = value;
        }

        [Browsable(false)]
        public override string Text
        {
            get => base.Text;

            set
            {

            }
        }

        public bool DisplayFocusCues { get; set; } = true;

        protected override bool ShowFocusCues => DisplayFocusCues;

        protected override void OnPaint(PaintEventArgs e)
        {
            int toggleSize = Height - 5;
            e.Graphics.SmoothingMode = SmoothingMode.AntiAlias;
            e.Graphics.Clear(Parent.BackColor);
        
            int togglePositionX;
            int togglePositionY = 2;
            Color toggleColor;

            if (Checked) //ON
            {
                if (CheckState == CheckState.Indeterminate)
                {
                    //Draw the control surface
                    if (_solidStyle)
                    {
                        e.Graphics.FillPath(new SolidBrush(_indeterminateBackColor), GetFigurePath());
                    }
                    else
                    {
                        e.Graphics.DrawPath(new Pen(_indeterminateBackColor, 2), GetFigurePath());
                    }

                    //set toggle
                    toggleColor = _indeterminateToggleColor;
                    togglePositionX = (int)(Width * 0.5) - (int)(Height * 0.5) + 1;
                }
                else
                {
                    //Draw the control surface
                    if (_solidStyle)
                    {
                        e.Graphics.FillPath(new SolidBrush(_onBackColor), GetFigurePath());
                    }
                    else
                    {
                        e.Graphics.DrawPath(new Pen(_onBackColor, 2), GetFigurePath());
                    }

                    //set toggle
                    toggleColor = _onToggleColor;
                    togglePositionX = Width - Height + 1;

                }
            }
            else //OFF
            {
                //Draw the control surface
                if (_solidStyle)
                {
                    e.Graphics.FillPath(new SolidBrush(_offBackColor), GetFigurePath());
                }
                else
                {
                    e.Graphics.DrawPath(new Pen(_offBackColor, 2), GetFigurePath());
                }

                //set toggle
                toggleColor = _offToggleColor;
                togglePositionX = 2;
            }

            //Draw the toggle
            e.Graphics.FillEllipse(new SolidBrush(toggleColor),
                new Rectangle(togglePositionX, togglePositionY, toggleSize, toggleSize));

            if (ViewInfo.ShowErrorIcon && ErrorIcon != null)
            {
                e.Graphics.DrawImage(ErrorIcon, new Rectangle(3, 3, 15, 15));
            }
            else if (ShowFocusCues && Focused)
            {
                // Draw Focus Cues if needed
                e.Graphics.DrawPath(new Pen(Color.Black, 1.2F) { DashStyle = DashStyle.Dot }, GetFigurePath());

                //Draw the toggle focus cue
                e.Graphics.FillEllipse(new SolidBrush(Color.Black),
                    new Rectangle(togglePositionX + 5, togglePositionY + 5, 6, 6));
            }
        }

        //Methods
        private GraphicsPath GetFigurePath()
        {
            int arcSize = Height - 1;
            var leftArc = new Rectangle(0, 0, arcSize, arcSize);
            var rightArc = new Rectangle(Width - arcSize - 2, 0, arcSize, arcSize);

            var path = new GraphicsPath();
            path.StartFigure();
            path.AddArc(leftArc, 90, 180);
            path.AddArc(rightArc, 270, 180);
            path.CloseFigure();

            return path;
        }
    }
}
