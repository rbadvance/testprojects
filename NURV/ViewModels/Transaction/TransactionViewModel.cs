﻿using System;
using System.Threading;
using System.Threading.Tasks;
using System.Runtime.CompilerServices;
using System.Windows.Forms;
using System.Linq;
using System.Collections.Generic;
using System.Collections;
using System.Reactive.Linq;

using ReactiveUI;
using Splat;
using DevExpress.XtraEditors.DXErrorProvider;
using NURV.Data;
using NURV.Data.DataAccess;
using NURV.Data.DataAccess.Common;

[assembly: InternalsVisibleTo("NURV.UnitTest")]
namespace NURV.ViewModels.Transaction
{
    internal class TransactionViewModel : Common.ViewModelBase
    {
        #region Local Variables

        // Policy Tab
        private int _classOfRisk;
        private int _premiumType;
        private int _selfManaged;
        private bool _onHold;
        private string _policyNumber;
        private string _reaCode;
        private string _newBusinessArrangedByCode;
        private int _dwellingType;
        private int _constructionType;
        private int _yearBuilt;
        private bool _rewiredReplumbed;
        private bool _freeStanding;
        private bool _commonGrounds;
        private bool _businessOrTrade;
        private bool _acceptedBusinessUse;
        private int _holidayLet;
        private int _furnished;
        private int _cctv;
        private int _electronicAccess;

        // Disclosure Tab
        private bool _disclosure5Yes;
        private bool _disclosure5No;
        private bool _disclosure5AwaitingApproval;

        // lookup collections
        public IList<Data.DataAccess.LandSizes.DTO.DTLandSizeForLookup> LandSizesForLookup { get; private set; }
        public IList<Data.DataAccess.Holidays.DTO.DTHolidayForLookup> HolidaysForLookup { get; private set; }
        public IList<Data.DataAccess.ConstructionTypes.DTO.DTConstructionTypeForLookup> ConstructionTypesForLookup { get; private set; }
        public IList<Data.DataAccess.SelfManaged.DTO.DTSelfManagedForLookup> SelfManagedForLookup { get; private set; }
        public IList<Data.DataAccess.CCTVs.DTO.DTCCTVForLookup> CCTVsForLookup { get; private set; }
        public IList<Data.DataAccess.Furnished.DTO.DTFurnishedForLookup> FurnishedForLookup { get; private set; }
        public IList<Data.DataAccess.ElectronicAccesses.DTO.DTElectronicAccessForLookup> ElectronicAccessesForLookup { get; private set; }
        public IList<Data.DataAccess.OtherClaimsExcesses.DTO.DTOtherClaimsExcessForLookup> OtherClaimsExcessesForLookup { get; private set; }
        public IList<Data.DataAccess.ClassesOfRisk.DTO.DTClassOfRiskForLookup> ClassOfRisksForLookup { get; private set; }

        private Types.ScreenMode _currentScreenMode = Types.ScreenMode.ReadOnly;

        #endregion

        #region Fields

        #region Policy Tab

        #region Policy Detail

        public int ClassOfRisk { get => _classOfRisk; set => this.RaiseAndSetIfChanged(ref _classOfRisk, value); }
        public int PremiumType { get => _premiumType; set => this.RaiseAndSetIfChanged(ref _premiumType, value); }
        public int SelfManaged { get => _selfManaged; set => this.RaiseAndSetIfChanged(ref _selfManaged, value); }
        public bool OnHold { get => _onHold; set => this.RaiseAndSetIfChanged(ref _onHold, value); }
        public string PolicyNumber { get => _policyNumber; set => this.RaiseAndSetIfChanged(ref _policyNumber, value); }
        public string REACode { get => _reaCode; set => this.RaiseAndSetIfChanged(ref _reaCode, value); }
        public string NewBusinessArrangedByCode { get => _newBusinessArrangedByCode; set => this.RaiseAndSetIfChanged(ref _newBusinessArrangedByCode, value); }
        public int DwellingType { get => _dwellingType; set => this.RaiseAndSetIfChanged(ref _dwellingType, value); }
        public int ConstructionType { get => _constructionType; set => this.RaiseAndSetIfChanged(ref _constructionType, value); }
        public int YearBuilt { get => _yearBuilt; set => this.RaiseAndSetIfChanged(ref _yearBuilt, value); }
        public bool RewiredReplumbed { get => _rewiredReplumbed; set => this.RaiseAndSetIfChanged(ref _rewiredReplumbed, value); }
        public bool FreeStanding { get => _freeStanding; set => this.RaiseAndSetIfChanged(ref _freeStanding, value); }
        public bool CommonGrounds { get => _commonGrounds; set => this.RaiseAndSetIfChanged(ref _commonGrounds, value); }
        public bool BusinessOrTrade { get => _businessOrTrade; set => this.RaiseAndSetIfChanged(ref _businessOrTrade, value); }
        public bool AcceptedBusinessUse { get => _acceptedBusinessUse; set => this.RaiseAndSetIfChanged(ref _acceptedBusinessUse, value); }
        public int HolidayLet { get => _holidayLet; set => this.RaiseAndSetIfChanged(ref _holidayLet, value); }
        public int Furnished { get => _furnished; set => this.RaiseAndSetIfChanged(ref _furnished, value); }
        public int CCTV { get => _cctv; set => this.RaiseAndSetIfChanged(ref _cctv, value); }
        public int ElectronicAccess { get => _electronicAccess; set => this.RaiseAndSetIfChanged(ref _electronicAccess, value); }

        #endregion

        #region Other Details

        public string LandlordCode { get; set; }
        public string LandlordName { get; set; }
        public int BranchId { get; set; }
        public string FloodRisk { get; set; }
        public string Bushfire { get; set; }
        public decimal WeeklyRentSumInsured { get; set; }
        public decimal ContentsSumInsured { get; set; }
        public decimal BuildingSumInsured { get; set; }
        public decimal BuildingSIExcess { get; set; }
        public decimal UnderwriterExcess { get; set; }
        public int LandSize { get; set; }
        public bool Tenanted { get; set; }
        public bool JobReducedDue { get; set; }
        public bool Arrears { get; set; }
        public int WeeksinArrears { get; set; }

        #endregion

        #region Dates

        public string TransactionNumber { get; set; }
        public DateTimeOffset CreatedOn { get; set; }
        public DateTimeOffset InceptionFrom { get; set; }
        public DateTimeOffset ClosingOn { get; set; }
        public DateTimeOffset COISentOn { get; set; }
        public DateTimeOffset PolicySentOn { get; set; }
        public decimal Balance { get; set; }
        public DateTimeOffset EffectiveFrom { get; set; }
        public DateTimeOffset BilledFrom { get; set; }
        public DateTimeOffset BilledTo { get; set; }
        public DateTimeOffset RenewalOn { get; set; }
        public DateTimeOffset InvoicePrintedOn { get; set; }

        #endregion

        #region Discounts

        public bool DiscountStaff { get; set; }
        public bool DiscountREA { get; set; }
        public bool DiscountMultipleProperty { get; set; }
        public bool Discount5Percent { get; set; }
        public bool BonusOffer15For12 { get; set; }

        #endregion

        #endregion

        #region Disclosure Tab

        #region Questionnaire

        public bool Disclosure5Yes
        {
            get => _disclosure5Yes;
            set
            {
                this.RaiseAndSetIfChanged(ref _disclosure5Yes, value);
                UpdateDisclosure5Selection(value);
            }
        }

        public bool Disclosure5No
        {
            get => _disclosure5No;
            set
            {
                this.RaiseAndSetIfChanged(ref _disclosure5No, value);
                UpdateDisclosure5Selection(value);
            }
        }

        public bool Disclosure5AwaitingApproval
        {
            get => _disclosure5AwaitingApproval;
            set
            {
                this.RaiseAndSetIfChanged(ref _disclosure5AwaitingApproval, value);
                UpdateDisclosure5Selection(value);
            }
        }

        #endregion

        #region Approval



        #endregion

        #endregion

        #endregion

        public TransactionViewModel()
        {
            // Configure Commands (...done in the constructor so it's on the UI thread).
            ConfigureCommands();
        }

        public async Task InitAsync()
        {
            var tasks = new List<Task>();

            // Load drop-downs
            tasks.Add(LoadDropDownsAsync());

            // TODO: temporary here for now for testing...
            tasks.Add(LoadAdditionalDropDownsAsync(11,false,DateTime.Now,"ACT", Data.Types.PremiumType.NewBusiness));

            // await all tasks
            await Task.WhenAll(tasks).ConfigureAwait(false);

            BindingSource = new BindingSource
            {
                this
            };

            // TODO: temp logic
            //SetScreenMode(Types.ScreenMode.New);
        }

        private async Task LoadDropDownsAsync()
        {
            using (var context = new NURV.Data.Context.NURVContext(trackingOn:true))
            {
                var tasks = new List<Task>();

                //10        cboLandlordState.RowSource = "viewList_States"
                //20        cboTransactionType.RowSource = "viewList_TransactionTypes"
                //30        cboBuildingType.RowSource = "viewList_BuildingTypes"
                //40        cboPremiumType.RowSource = "viewList_PremiumType"
                //50        cboClassOfRisk.RowSource = "Select ID, Data From viewList_ClassOfRisk where Displayed = 1"
                //60        cboExtraReturnPremiumType.RowSource = "Select ID, ShortDescription From viewList_ExtraReturnPremiumType"
                //70        cboWhoPlaced.RowSource = "Select ID, Data From viewList_InformedByWho Where InHoldCover = 1 " & ExcludeArrangedBy & " Order By Data"
                //80        cboHowPlaced.RowSource = "Select ID, Data From viewList_InformedHow Where InHoldCoverLL = 1 Order By Data"
                //90        cboPaymentMethod.RowSource = "Select ID, Data From viewList_PaymentMethod Where InHoldCover = 1 Order By Data"
                //100       UpdateDoNotRenewReasons


                // get class of risk data
                Task<IResult<IList<Data.DataAccess.ClassesOfRisk.DTO.DTClassOfRiskForLookup>>> classOfRiskTask = context.ClassOfRiskQuery.Value.GetClassOfRiskForLookupAsync(CancellationToken.None);
                tasks.Add(classOfRiskTask);

                // get extra rreturn premium type data

                // get who placed data

                // get how placed data

                // get payment methods data

                // await all dropdown query tasks
                await Task.WhenAll(tasks).ConfigureAwait(false);

                var errors = new List<string>();

                // TODO: temporary code to test result of Stored Procedure (can't test SP in unit test).
                //var x = await context.TransactionPolicyHistoryQuery.GetTransactionPolicyHistoryAsync(508055845, CancellationToken.None).ConfigureAwait(false);

                if (classOfRiskTask.Result.HasSucceeded)
                {
                    ClassOfRisksForLookup = classOfRiskTask.Result.Value;
                }
                else
                {
                    XtraMessageBox_Show(null, classOfRiskTask.Result.Message);
                }

                // show any errors (if any)
                if (errors.Any())
                {
                    XtraMessageBox_Show(null, $"Error(s) loading dropdown data:{Environment.NewLine}{string.Join(Environment.NewLine, errors)}");
                }
            }
        }

        private async Task LoadAdditionalDropDownsAsync(int classOfRiskId, bool includeUnknown, DateTime? effectiveDate, string state, Data.Types.PremiumType premiumType)
        {
            using (var context = new NURV.Data.Context.NURVContext(trackingOn: true))
            {
                var tasks = new List<Task>();

                // get land size data
                Task<IResult<IList<Data.DataAccess.LandSizes.DTO.DTLandSizeForLookup>>> landSizeTask
                    = context.LandSizeQuery.Value.GetLandSizeForLookupAsync(classOfRiskId, includeUnknown, effectiveDate, CancellationToken.None);
                tasks.Add(landSizeTask);

                // get holiday data
                Task<IResult<IList<Data.DataAccess.Holidays.DTO.DTHolidayForLookup>>> holidayTask 
                    = context.HolidayQuery.Value.GetHolidayForLookupAsync(classOfRiskId, includeUnknown, effectiveDate, state, premiumType, CancellationToken.None);
                tasks.Add(holidayTask);

                // get construction Type data
                Task<IResult<IList<Data.DataAccess.ConstructionTypes.DTO.DTConstructionTypeForLookup>>> constructionTypeTask 
                    = context.ConstructionTypeQuery.Value.GetConstructionTypeForLookupAsync(classOfRiskId, includeUnknown, effectiveDate, CancellationToken.None);
                tasks.Add(constructionTypeTask);

                // get self managed data
                Task<IResult<IList<Data.DataAccess.SelfManaged.DTO.DTSelfManagedForLookup>>> selfManagedTask 
                    = context.SelfManagedQuery.Value.GetSelfManagedForLookupAsync(classOfRiskId, effectiveDate, state, premiumType, CancellationToken.None);
                tasks.Add(selfManagedTask);

                // get cctv data
                Task<IResult<IList<Data.DataAccess.CCTVs.DTO.DTCCTVForLookup>>> cctvTask 
                    = context.CCTVQuery.Value.GetCCTVForLookupAsync(classOfRiskId, includeUnknown, effectiveDate, state, premiumType, CancellationToken.None);
                tasks.Add(cctvTask);

                // get furnished data
                Task<IResult<IList<Data.DataAccess.Furnished.DTO.DTFurnishedForLookup>>> furnishedTask 
                    = context.FurnishedQuery.Value.GetFurnishedForLookupAsync(classOfRiskId, includeUnknown, effectiveDate, state, premiumType, CancellationToken.None);
                tasks.Add(furnishedTask);

                // get electronic access data
                Task<IResult<IList<Data.DataAccess.ElectronicAccesses.DTO.DTElectronicAccessForLookup>>> electronicAccessTask 
                    = context.ElectronicAccessQuery.Value.GetElectronicAccessForLookupAsync(classOfRiskId, includeUnknown, effectiveDate, state, premiumType, CancellationToken.None);
                tasks.Add(electronicAccessTask);

                // await all dropdown query tasks
                await Task.WhenAll(tasks).ConfigureAwait(false);

                var errors = new List<string>();

                if (landSizeTask.Result.HasSucceeded)
                {
                    LandSizesForLookup = landSizeTask.Result.Value;
                }
                else
                {
                    errors.Add(landSizeTask.Result.Message);
                }

                if (holidayTask.Result.HasSucceeded)
                {
                    HolidaysForLookup = holidayTask.Result.Value;
                }
                else
                {
                    errors.Add(holidayTask.Result.Message);
                }

                if (constructionTypeTask.Result.HasSucceeded)
                {
                    ConstructionTypesForLookup = constructionTypeTask.Result.Value;
                }
                else
                {
                    errors.Add(constructionTypeTask.Result.Message);
                }

                if (selfManagedTask.Result.HasSucceeded)
                {
                    SelfManagedForLookup = selfManagedTask.Result.Value;
                }
                else
                {
                    errors.Add(selfManagedTask.Result.Message);
                }

                if (cctvTask.Result.HasSucceeded)
                {
                    CCTVsForLookup = cctvTask.Result.Value;
                }
                else
                {
                    errors.Add(cctvTask.Result.Message);
                }

                if (furnishedTask.Result.HasSucceeded)
                {
                    FurnishedForLookup = furnishedTask.Result.Value;
                }
                else
                {
                    errors.Add(furnishedTask.Result.Message);
                }

                if (electronicAccessTask.Result.HasSucceeded)
                {
                    ElectronicAccessesForLookup = electronicAccessTask.Result.Value;
                }
                else
                {
                    errors.Add(electronicAccessTask.Result.Message);
                }

                // show any errors (if any)
                if (errors.Any())
                {
                    XtraMessageBox_Show(null, $"Error(s) loading additional dropdown data:{Environment.NewLine}{string.Join(Environment.NewLine, errors)}");
                }
            }
        }

        #region Commands

        private void ConfigureCommands()
        {
            RefreshCommand = ReactiveCommand.Create(() => { RefreshClicked(); });
            ExtendCreditTermsCommand = ReactiveCommand.Create(() => { ExtendCreditTermsClicked(); _ = CommandCanExecute(nameof(ExtendCreditTermsCommand)); });
            RecalculateBalanceCommand = ReactiveCommand.Create(() => { RecalculateBalanceClicked(); _ = CommandCanExecute(nameof(RecalculateBalanceCommand)); });
            CreateClaimCommand = ReactiveCommand.Create(() => { CreateClaimClicked(); _ = CommandCanExecute(nameof(CreateClaimCommand)); });
            SendPolicyDocumentCommand = ReactiveCommand.Create(() => { SendPolicyDocumentClicked(); _ = CommandCanExecute(nameof(SendPolicyDocumentCommand)); });
            PremiumCommand = ReactiveCommand.Create(() => { SendPolicyDocumentClicked(); _ = CommandCanExecute(nameof(PremiumCommand)); });
            NotesAndLogsCommand = ReactiveCommand.Create(() => { NotesAndLogsClicked(); _ = CommandCanExecute(nameof(NotesAndLogsCommand)); });
            EndorsementCommand = ReactiveCommand.Create(() => { EndorsementClicked(); _ = CommandCanExecute(nameof(EndorsementCommand)); });
            EditPaymentCommand = ReactiveCommand.Create(() => { EditPaymentClicked(); _ = CommandCanExecute(nameof(EditPaymentCommand)); });
            EditCommand = ReactiveCommand.Create(() => { EditClicked(); _ = CommandCanExecute(nameof(EditCommand)); });
            SaveCommand = ReactiveCommand.Create(() => { SaveClickedAsync(); _ = CommandCanExecute(nameof(SaveCommand)); });
            CancelCommand = ReactiveCommand.Create(() => { CancelClicked(); _ = CommandCanExecute(nameof(CancelCommand)); });
        }

        public ReactiveCommand<System.Reactive.Unit, System.Reactive.Unit> RefreshCommand { get; private set; }
        public ReactiveCommand<System.Reactive.Unit, System.Reactive.Unit> ExtendCreditTermsCommand { get; private set; }
        public ReactiveCommand<System.Reactive.Unit, System.Reactive.Unit> RecalculateBalanceCommand { get; private set; }
        public ReactiveCommand<System.Reactive.Unit, System.Reactive.Unit> CreateClaimCommand { get; private set; }
        public ReactiveCommand<System.Reactive.Unit, System.Reactive.Unit> SendPolicyDocumentCommand { get; private set; }
        public ReactiveCommand<System.Reactive.Unit, System.Reactive.Unit> PremiumCommand { get; private set; }
        public ReactiveCommand<System.Reactive.Unit, System.Reactive.Unit> NotesAndLogsCommand { get; private set; }
        public ReactiveCommand<System.Reactive.Unit, System.Reactive.Unit> EndorsementCommand { get; private set; }
        public ReactiveCommand<System.Reactive.Unit, System.Reactive.Unit> EditPaymentCommand { get; private set; }
        public ReactiveCommand<System.Reactive.Unit, System.Reactive.Unit> EditCommand { get; private set; }
        public ReactiveCommand<System.Reactive.Unit, System.Reactive.Unit> SaveCommand { get; private set; }
        public ReactiveCommand<System.Reactive.Unit, System.Reactive.Unit> CancelCommand { get; private set; }

        #region Command Actions

        private void RefreshClicked()
        {
            //TODO: Implement Refresh logic
        }

        private void ExtendCreditTermsClicked()
        {
            //TODO: Implement Extend Credit Terms logic
        }

        private void RecalculateBalanceClicked()
        {
            //TODO: Implement Recalc Balance logic
        }

        private void CreateClaimClicked()
        {
            //TODO: Implement Create Claim logic
        }

        private void SendPolicyDocumentClicked()
        {
            //TODO: Implement Send Policy Document logic
            //var sendForm = new SendTransactionCommunicationForm();

            //sendForm.ShowDialogLocal(this, null);
        }

        private void NotesAndLogsClicked()
        {
            //TODO: Implement Notes/Logs logic
        }

        private void EndorsementClicked()
        {
            //TODO: Implement Endorsement logic
            XtraMessageBox_Show(EndorsementDialogResult, "If Txn Status = Expired, 'Unlock Only Do Not Renew'\r\nOtherwise, unlock fields that Do not affect the Policy $");
        }

        private void EditPaymentClicked()
        {
            //TODO: Implement Edit Payment logic
            SetScreenMode(Types.ScreenMode.Edit);
        }

        private void EditClicked()
        {
            //TODO: Implement Edit logic
            SetScreenMode(Types.ScreenMode.Edit);
        }

        private async void SaveClickedAsync()
        {
            if (ValidateForm())
            {
                XtraMessageBox_Show(null, $"Fix the following error(s):{Environment.NewLine}{string.Join(Environment.NewLine, _errors.Select(e => e.Value?.ErrorText))}");
            }
            else
            {
                /// call centralised save logic
                Core.Services.Transaction.ITransactionService transactionService = Locator.Current.GetService<Core.Services.Transaction.ITransactionService>();
                Core.Services.Common.IResult<bool> saveResult = await transactionService.SaveTransactionAsync(null).ConfigureAwait(continueOnCapturedContext:false);
                if (!saveResult.HasSucceeded)
                {
                    XtraMessageBox_Show(null, $"Error saving: {saveResult.Message}");
                }
                else
                {
                    //TODO: do something here
                }
            }
        }

        private void CancelClicked()
        {
            //TODO: Implement Cancel logic
        }

        private System.IObservable<bool> CommandCanExecute(string commandName)
        {
            return this.WhenAny(vm => vm._currentScreenMode, s => s.Value != Types.ScreenMode.ReadOnly);
        }

        #endregion

        #endregion

        #region Actions
        
        private void SetScreenMode(Types.ScreenMode newScreenMode) { SetScreenMode(newScreenMode, false); }

        private void SetScreenMode(Types.ScreenMode newScreenMode, bool setVisibility)
        {
            // this method should be kept simple, its basically just setting the screen mode and calling EnableControls()
            // dont let this get overblown.

            // this is also the only place where we should set or access currentScreenMode. 
            // we should use the IsCurrentScreenModexxx properties to test what mode we're in.
            _currentScreenMode = newScreenMode;

            //EnableControls(setVisibility);

        }

        public void EndorsementDialogResult(DialogResult result)
        {
            if (result == DialogResult.Yes)
            {
                // Do something
            }
        }

        internal Task DisplayTransactionAsync(string transactionNumber)
        {
            OnBeginUpdate();

            try
            {
                throw new NotImplementedException();
            }
            finally
            {
                OnEndUpdate();
            }
        }

        private async Task OpenTransactionAsync(IWin32Window owner, string transactionNumber)
        {
            Views.Transaction.ITransactionForm policyForm = Locator.Current.GetService<Views.Transaction.ITransactionForm>();
            await policyForm.DisplayTransactionAsync(transactionNumber);
            _ = policyForm.ShowDialog(owner);
        }

        /// <summary>
        /// Links 3 different bool properties for Disclosure 5, so only one is TRUE at any given time
        /// </summary>
        private void UpdateDisclosure5Selection(bool value, [CallerMemberName] string memberName = null)
        {
            if (value)
            {
                switch (memberName)
                {
                    case nameof(Disclosure5Yes):
                        Disclosure5No = false;
                        Disclosure5AwaitingApproval = false;
                        break;
                    case nameof(Disclosure5No):
                        Disclosure5Yes = false;
                        Disclosure5AwaitingApproval = false;
                        break;
                    case nameof(Disclosure5AwaitingApproval):
                        Disclosure5No = false;
                        Disclosure5Yes = false;
                        break;
                }
            }
        }

        #endregion

        #region Validation

        public override void GetPropertyError(string propertyName, ErrorInfo info)
        {
            if (_propertyBeingEdited == propertyName || ValidateAllControls || _namesOfControlsEdited.Contains(propertyName))
            {
                info.ErrorType = ErrorType.None;

                switch (propertyName)
                {
                    case nameof(OnHold): // validate on hold
                        if (OnHold == false)
                        {
                            info.ErrorType = ErrorType.Critical;
                            info.ErrorText = "On Hold required";
                        }
                        break;

                    case nameof(PolicyNumber): // validate policy number
                        if (string.IsNullOrWhiteSpace(PolicyNumber))
                        {
                            info.ErrorType = ErrorType.Critical;
                            info.ErrorText = "Policy Number is required";
                        }
                        break;
                }

                if (info.ErrorType > ErrorType.None)
                {
                    if (!_errors.ContainsKey(propertyName))
                    {
                        _errors[propertyName] = new ErrorInfo(info.ErrorText, info.ErrorType);
                    }
                }
                else
                {
                    _errors.Remove(propertyName);
                }
            }
        }

        #endregion
    }
}
