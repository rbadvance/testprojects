﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using DevExpress.XtraEditors;

namespace NURV.ViewModels.Common
{
    public class MvvmMessageBoxEventArgs : EventArgs
    {
        public MvvmMessageBoxEventArgs(Action<DialogResult> resultAction, string messageBoxText, string caption = "", MessageBoxButtons buttons = MessageBoxButtons.OK, MessageBoxIcon icon = MessageBoxIcon.None, 
                                                                                                                                            MessageBoxDefaultButton defaultButton = MessageBoxDefaultButton.Button1)
        {
            _resultAction = resultAction;
            _messageBoxText = messageBoxText;
            _caption = caption;
            _buttons = buttons;
            _icon = icon;
            _defaultButton = defaultButton;
        }

        private Action<DialogResult> _resultAction;
        private string _messageBoxText;
        private string _caption;
        private MessageBoxButtons _buttons;
        private MessageBoxIcon _icon;
        private MessageBoxDefaultButton _defaultButton;

        public void Show(IWin32Window owner)
        {
            DialogResult messageBoxResult = XtraMessageBox.Show(owner:owner, text:_messageBoxText, caption:_caption, buttons:_buttons, icon:_icon, defaultButton:_defaultButton);
            _resultAction?.Invoke(messageBoxResult);
        }

        public void Show()
        {
            DialogResult messageBoxResult = XtraMessageBox.Show(text:_messageBoxText, caption:_caption, buttons:_buttons, icon:_icon, defaultButton: _defaultButton);
            _resultAction?.Invoke(messageBoxResult);
        }
    }
}
