﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.ComponentModel;
using System.Runtime.CompilerServices;
using System.Windows.Forms;
using DevExpress.XtraEditors;
using DevExpress.XtraEditors.DXErrorProvider;

using ReactiveUI;

namespace NURV.ViewModels.Common
{
    internal abstract class ViewModelBase : ReactiveObject, IDXDataErrorInfo
    {
        protected private string _propertyBeingEdited;

        /// <summary>
        /// List of control names that have been edited.
        /// Using ResetForm() will reset this list.
        /// </summary>
        protected private HashSet<string> _namesOfControlsEdited = new HashSet<string>();

        public BindingSource BindingSource { get; private protected set; }

        protected private Dictionary<string, ErrorInfo> _errors = new Dictionary<string, ErrorInfo>();

        public ViewModelBase()
        {
            PropertyChanged += ViewModelBase_PropertyChanged;
        }

        public event EventHandler BeginUpdate;
        public event EventHandler EndUpdate;
        public event EventHandler<Common.MvvmMessageBoxEventArgs> MessageBoxRequest;
        public bool ValidateAllControls { get; set; }
        public bool HasErrors => _errors.Any();

        public void GetError(ErrorInfo info)
        {
            if (HasErrors)
            {
                // just select the first error found
                ErrorInfo firstError = _errors.Values.First();
                info.ErrorText = firstError.ErrorText;
                info.ErrorType = firstError.ErrorType;
            }
            else
            {
                info.ErrorType = ErrorType.None;
            }
        }

        public abstract void GetPropertyError(string propertyName, ErrorInfo info);

        protected virtual void OnBeginUpdate() => BeginUpdate?.Invoke(this, EventArgs.Empty);

        protected virtual void OnEndUpdate() => EndUpdate?.Invoke(this, EventArgs.Empty);

        protected virtual void XtraMessageBox_Show(Action<DialogResult> resultAction, string messageBoxText, string caption = "", MessageBoxButtons buttons = MessageBoxButtons.OK, MessageBoxIcon icon = MessageBoxIcon.None,
                            MessageBoxDefaultButton defaultButton = MessageBoxDefaultButton.Button1) => MessageBoxRequest?.Invoke(this, new Common.MvvmMessageBoxEventArgs(resultAction, messageBoxText, caption, buttons, icon, defaultButton));

        private void ViewModelBase_PropertyChanged(object sender, PropertyChangedEventArgs e)
        {
            _propertyBeingEdited = e.PropertyName;

            // add the control to the edited list, if not already
            if (!_namesOfControlsEdited.Contains(_propertyBeingEdited))
            {
                _namesOfControlsEdited.Add(_propertyBeingEdited);
            }
        }

        private protected void ResetForm()
        {
            _namesOfControlsEdited.Clear();
        }

        public bool ValidateForm()
        {
            try
            {
                ValidateAllControls = true;
                BindingSource.ResetBindings(false);
            }
            finally
            {
                ValidateAllControls = false;
            }

            return HasErrors;
        }
    }
}
