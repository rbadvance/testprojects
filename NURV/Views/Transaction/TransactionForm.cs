﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using DevExpress.XtraEditors;
using DevExpress.XtraEditors.DXErrorProvider;

using ReactiveUI;
using ReactiveUI.Validation;
using ReactiveUI.Validation.Abstractions;
using ReactiveUI.Validation.Extensions;
using NURV.ViewModels.Transaction;

namespace NURV.Views.Transaction
{
    internal partial class TransactionForm : XtraForm, IViewFor<TransactionViewModel>, ITransactionForm
    {
        private TransactionViewModel _viewModel { get; set; }

        public TransactionForm()
        {
            InitializeComponent();
            this.Load += TransactionForm_LoadAsync;
        }

        private async void TransactionForm_LoadAsync(object sender, EventArgs e)
        {
            _viewModel = new TransactionViewModel();
            await _viewModel.InitAsync();
            _viewModel.BeginUpdate += ViewModel_BeginUpdate;
            _viewModel.EndUpdate += ViewModel_EndUpdate;
            _viewModel.MessageBoxRequest += ViewModel_MessageBoxRequest;

            // Configure class of risk lookup
            ClassOfRiskField.Properties.DisplayMember = nameof(Data.DataAccess.ClassesOfRisk.DTO.DTClassOfRiskForLookup.Data);
            ClassOfRiskField.Properties.ValueMember = nameof(Data.DataAccess.ClassesOfRisk.DTO.DTClassOfRiskForLookup.Id);
            ClassOfRiskField.Properties.Columns.Add(new DevExpress.XtraEditors.Controls.LookUpColumnInfo(nameof(Data.DataAccess.ClassesOfRisk.DTO.DTClassOfRiskForLookup.Data)));
            ClassOfRiskField.Properties.ShowHeader = false;

            // Configure self managed lookup
            SelfManagedField.Properties.DisplayMember = nameof(Data.DataAccess.ClassesOfRisk.DTO.DTClassOfRiskForLookup.Data);
            SelfManagedField.Properties.ValueMember = nameof(Data.DataAccess.ClassesOfRisk.DTO.DTClassOfRiskForLookup.Id);
            SelfManagedField.Properties.Columns.Add(new DevExpress.XtraEditors.Controls.LookUpColumnInfo(nameof(Data.DataAccess.ClassesOfRisk.DTO.DTClassOfRiskForLookup.Data)));
            SelfManagedField.Properties.ShowHeader = false;

            // Configure land size lookup
            LandSizeField.Properties.DisplayMember = nameof(Data.DataAccess.LandSizes.DTO.DTLandSizeForLookup.Name);
            LandSizeField.Properties.ValueMember = nameof(Data.DataAccess.LandSizes.DTO.DTLandSizeForLookup.Id);
            LandSizeField.Properties.Columns.Add(new DevExpress.XtraEditors.Controls.LookUpColumnInfo(nameof(Data.DataAccess.LandSizes.DTO.DTLandSizeForLookup.Name)));
            LandSizeField.Properties.ShowHeader = false;

            // Configure construction type lookup
            ConstructionTypeField.Properties.DisplayMember = nameof(Data.DataAccess.ConstructionTypes.DTO.DTConstructionTypeForLookup.Name);
            ConstructionTypeField.Properties.ValueMember = nameof(Data.DataAccess.ConstructionTypes.DTO.DTConstructionTypeForLookup.Id);
            ConstructionTypeField.Properties.Columns.Add(new DevExpress.XtraEditors.Controls.LookUpColumnInfo(nameof(Data.DataAccess.ConstructionTypes.DTO.DTConstructionTypeForLookup.Name)));
            ConstructionTypeField.Properties.ShowHeader = false;

            // Configure holiday let lookup
            HolidayLetField.Properties.DisplayMember = nameof(Data.DataAccess.Holidays.DTO.DTHolidayForLookup.Name);
            HolidayLetField.Properties.ValueMember = nameof(Data.DataAccess.Holidays.DTO.DTHolidayForLookup.Id);
            HolidayLetField.Properties.Columns.Add(new DevExpress.XtraEditors.Controls.LookUpColumnInfo(nameof(Data.DataAccess.Holidays.DTO.DTHolidayForLookup.Name)));
            HolidayLetField.Properties.ShowHeader = false;

            // Configure furnished lookup
            IsFurnishedField.Properties.DisplayMember = nameof(Data.DataAccess.Furnished.DTO.DTFurnishedForLookup.Name);
            IsFurnishedField.Properties.ValueMember = nameof(Data.DataAccess.Furnished.DTO.DTFurnishedForLookup.Id);
            IsFurnishedField.Properties.Columns.Add(new DevExpress.XtraEditors.Controls.LookUpColumnInfo(nameof(Data.DataAccess.Furnished.DTO.DTFurnishedForLookup.Name)));
            IsFurnishedField.Properties.ShowHeader = false;

            // Configure cctv lookup
            IsCCTVField.Properties.DisplayMember = nameof(Data.DataAccess.CCTVs.DTO.DTCCTVForLookup.Name);
            IsCCTVField.Properties.ValueMember = nameof(Data.DataAccess.CCTVs.DTO.DTCCTVForLookup.Id);
            IsCCTVField.Properties.Columns.Add(new DevExpress.XtraEditors.Controls.LookUpColumnInfo(nameof(Data.DataAccess.CCTVs.DTO.DTCCTVForLookup.Name)));
            IsCCTVField.Properties.ShowHeader = false;

            // Configure electronic access lookup
            IsElectronicAccessField.Properties.DisplayMember = nameof(Data.DataAccess.ElectronicAccesses.DTO.DTElectronicAccessForLookup.Name);
            IsElectronicAccessField.Properties.ValueMember = nameof(Data.DataAccess.ElectronicAccesses.DTO.DTElectronicAccessForLookup.Id);
            IsElectronicAccessField.Properties.Columns.Add(new DevExpress.XtraEditors.Controls.LookUpColumnInfo(nameof(Data.DataAccess.ElectronicAccesses.DTO.DTElectronicAccessForLookup.Name)));
            IsElectronicAccessField.Properties.ShowHeader = false;

            // bind view to viewmodel
            _ = PolicyNumberField.DataBindings.Add(nameof(TextEdit.EditValue), _viewModel.BindingSource, nameof(_viewModel.PolicyNumber), true, DataSourceUpdateMode.OnPropertyChanged);
            _ = REACodeField.DataBindings.Add(nameof(ButtonEdit.EditValue), _viewModel.BindingSource, nameof(_viewModel.REACode), true, DataSourceUpdateMode.OnPropertyChanged);
            _ = OnHoldField.DataBindings.Add(nameof(NURV.Controls.FancyCheckEdit.EditValue), _viewModel.BindingSource, nameof(_viewModel.OnHold), true, DataSourceUpdateMode.OnPropertyChanged);
            
            _ = Disclosure5YesField.DataBindings.Add(nameof(CheckEdit.EditValue), _viewModel.BindingSource, nameof(_viewModel.Disclosure5Yes), true, DataSourceUpdateMode.OnPropertyChanged);
            _ = Disclosure5NoField.DataBindings.Add(nameof(CheckEdit.EditValue), _viewModel.BindingSource, nameof(_viewModel.Disclosure5No), true, DataSourceUpdateMode.OnPropertyChanged);
            _ = Disclosure5AwaitingApprovalField.DataBindings.Add(nameof(CheckEdit.EditValue), _viewModel.BindingSource, nameof(_viewModel.Disclosure5AwaitingApproval), true, DataSourceUpdateMode.OnPropertyChanged);
            
            _ = this.Bind(_viewModel, vm => vm.LandSizesForLookup, view => view.LandSizeField.Properties.DataSource);
            _ = LandSizeField.DataBindings.Add(nameof(LookUpEdit.EditValue), _viewModel.BindingSource, nameof(_viewModel.LandSize), true, DataSourceUpdateMode.OnPropertyChanged);

            _ = this.Bind(_viewModel, vm => vm.ConstructionTypesForLookup, view => view.ConstructionTypeField.Properties.DataSource);
            _ = ConstructionTypeField.DataBindings.Add(nameof(LookUpEdit.EditValue), _viewModel.BindingSource, nameof(_viewModel.ConstructionType), true, DataSourceUpdateMode.OnPropertyChanged);
       
            _ = this.Bind(_viewModel, vm => vm.ClassOfRisksForLookup, view => view.ClassOfRiskField.Properties.DataSource);
            _ = ClassOfRiskField.DataBindings.Add(nameof(LookUpEdit.EditValue), _viewModel.BindingSource, nameof(_viewModel.ClassOfRisk), true, DataSourceUpdateMode.OnPropertyChanged);

            _ = this.Bind(_viewModel, vm => vm.SelfManagedForLookup, view => view.SelfManagedField.Properties.DataSource);
            _ = SelfManagedField.DataBindings.Add(nameof(LookUpEdit.EditValue), _viewModel.BindingSource, nameof(_viewModel.SelfManaged), true, DataSourceUpdateMode.OnPropertyChanged);

            _ = this.Bind(_viewModel, vm => vm.HolidaysForLookup, view => view.HolidayLetField.Properties.DataSource);
            _ = HolidayLetField.DataBindings.Add(nameof(LookUpEdit.EditValue), _viewModel.BindingSource, nameof(_viewModel.HolidayLet), true, DataSourceUpdateMode.OnPropertyChanged);

            _ = this.Bind(_viewModel, vm => vm.FurnishedForLookup, view => view.IsFurnishedField.Properties.DataSource);
            _ = IsFurnishedField.DataBindings.Add(nameof(LookUpEdit.EditValue), _viewModel.BindingSource, nameof(_viewModel.Furnished), true, DataSourceUpdateMode.OnPropertyChanged);

            _ = this.Bind(_viewModel, vm => vm.CCTVsForLookup, view => view.IsCCTVField.Properties.DataSource);
            _ = IsCCTVField.DataBindings.Add(nameof(LookUpEdit.EditValue), _viewModel.BindingSource, nameof(_viewModel.CCTV), true, DataSourceUpdateMode.OnPropertyChanged);

            _ = this.Bind(_viewModel, vm => vm.ElectronicAccessesForLookup, view => view.IsElectronicAccessField.Properties.DataSource);
            _ = IsElectronicAccessField.DataBindings.Add(nameof(LookUpEdit.EditValue), _viewModel.BindingSource, nameof(_viewModel.ElectronicAccess), true, DataSourceUpdateMode.OnPropertyChanged);

            _ = PremiumTypeField.DataBindings.Add(nameof(LookUpEdit.EditValue), _viewModel.BindingSource, nameof(_viewModel.PremiumType), true, DataSourceUpdateMode.OnPropertyChanged);          

            // bind commands
            _ = this.BindCommand(_viewModel, vm => vm.RefreshCommand, view => view.RefreshButton, nameof(DevExpress.XtraBars.BarButtonItem.ItemClick));
            _ = this.BindCommand(_viewModel, vm => vm.ExtendCreditTermsCommand, view => view.ExtendCreditTermsButton, nameof(DevExpress.XtraBars.BarButtonItem.ItemClick));
            _ = this.BindCommand(_viewModel, vm => vm.RecalculateBalanceCommand, view => view.RecalcBalanceButton, nameof(DevExpress.XtraBars.BarButtonItem.ItemClick));
            _ = this.BindCommand(_viewModel, vm => vm.CreateClaimCommand, view => view.CreateClaimButton, nameof(DevExpress.XtraBars.BarButtonItem.ItemClick));
            _ = this.BindCommand(_viewModel, vm => vm.SendPolicyDocumentCommand, view => view.SendPolicyDocumentButton, nameof(DevExpress.XtraBars.BarButtonItem.ItemClick));
            _ = this.BindCommand(_viewModel, vm => vm.PremiumCommand, view => view.PremiumButton, nameof(DevExpress.XtraBars.BarButtonItem.ItemClick));
            _ = this.BindCommand(_viewModel, vm => vm.NotesAndLogsCommand, view => view.NotesAndLogsButton, nameof(DevExpress.XtraBars.BarButtonItem.ItemClick));
            _ = this.BindCommand(_viewModel, vm => vm.EndorsementCommand, view => view.EndorsementButton, nameof(DevExpress.XtraBars.BarButtonItem.ItemClick));
            _ = this.BindCommand(_viewModel, vm => vm.EditPaymentCommand, view => view.EditPaymentButton, nameof(DevExpress.XtraBars.BarButtonItem.ItemClick));
            _ = this.BindCommand(_viewModel, vm => vm.EditCommand, view => view.EditButton, nameof(DevExpress.XtraBars.BarButtonItem.ItemClick));
            _ = this.BindCommand(_viewModel, vm => vm.SaveCommand, view => view.SaveButton, nameof(DevExpress.XtraBars.BarButtonItem.ItemClick));
            _ = this.BindCommand(_viewModel, vm => vm.CancelCommand, view => view.CancelButton, nameof(DevExpress.XtraBars.BarButtonItem.ItemClick));

            dxErrorProvider1.DataSource = _viewModel.BindingSource;
            dxErrorProvider1.ContainerControl = this;
            dxErrorProvider1.ClearErrors();
        }

        private void ViewModel_EndUpdate(object sender, EventArgs e) => MainLayoutControl.BeginUpdate();

        private void ViewModel_BeginUpdate(object sender, EventArgs e) => MainLayoutControl.EndUpdate();

        public void ViewModel_MessageBoxRequest(object sender, ViewModels.Common.MvvmMessageBoxEventArgs e) => e.Show(this);

        public Task DisplayTransactionAsync(string transactionNumber) => _viewModel.DisplayTransactionAsync(transactionNumber);

        object IViewFor.ViewModel { get => _viewModel; set => _viewModel = (TransactionViewModel)value; }

        TransactionViewModel IViewFor<TransactionViewModel>.ViewModel { get => _viewModel; set => _viewModel = value; }

    }
}
