﻿
namespace NURV.Views.Transaction
{
    partial class TransactionForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            DevExpress.XtraGrid.GridLevelNode gridLevelNode1 = new DevExpress.XtraGrid.GridLevelNode();
            DevExpress.XtraGrid.GridFormatRule gridFormatRule1 = new DevExpress.XtraGrid.GridFormatRule();
            DevExpress.XtraEditors.FormatConditionRuleValue formatConditionRuleValue1 = new DevExpress.XtraEditors.FormatConditionRuleValue();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(TransactionForm));
            DevExpress.XtraEditors.Controls.EditorButtonImageOptions editorButtonImageOptions1 = new DevExpress.XtraEditors.Controls.EditorButtonImageOptions();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject1 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject2 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject3 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject4 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.XtraEditors.Controls.EditorButtonImageOptions editorButtonImageOptions2 = new DevExpress.XtraEditors.Controls.EditorButtonImageOptions();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject5 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject6 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject7 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject8 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.XtraEditors.Controls.EditorButtonImageOptions editorButtonImageOptions3 = new DevExpress.XtraEditors.Controls.EditorButtonImageOptions();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject9 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject10 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject11 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject12 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.XtraEditors.Controls.EditorButtonImageOptions editorButtonImageOptions4 = new DevExpress.XtraEditors.Controls.EditorButtonImageOptions();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject13 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject14 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject15 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject16 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.XtraEditors.Controls.EditorButtonImageOptions editorButtonImageOptions5 = new DevExpress.XtraEditors.Controls.EditorButtonImageOptions();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject17 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject18 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject19 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject20 = new DevExpress.Utils.SerializableAppearanceObject();
            this.HistoryDetailGridView = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.gridColumn59 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn60 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn61 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn62 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn63 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn64 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn65 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn66 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn67 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn68 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn69 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn70 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.HistoryGrid = new DevExpress.XtraGrid.GridControl();
            this.HistoryGridView = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.gridColumn47 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn48 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn49 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn50 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn51 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn52 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn53 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn54 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn55 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn56 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn57 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn58 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.barManager1 = new DevExpress.XtraBars.BarManager(this.components);
            this.PolicyMenuBar = new DevExpress.XtraBars.Bar();
            this.RefreshButton = new DevExpress.XtraBars.BarButtonItem();
            this.ExtendCreditTermsButton = new DevExpress.XtraBars.BarButtonItem();
            this.RecalcBalanceButton = new DevExpress.XtraBars.BarButtonItem();
            this.CreateClaimButton = new DevExpress.XtraBars.BarButtonItem();
            this.SendPolicyDocumentButton = new DevExpress.XtraBars.BarButtonItem();
            this.PremiumButton = new DevExpress.XtraBars.BarButtonItem();
            this.NotesAndLogsButton = new DevExpress.XtraBars.BarButtonItem();
            this.EndorsementButton = new DevExpress.XtraBars.BarButtonItem();
            this.EditPaymentButton = new DevExpress.XtraBars.BarButtonItem();
            this.EditButton = new DevExpress.XtraBars.BarButtonItem();
            this.SaveButton = new DevExpress.XtraBars.BarButtonItem();
            this.CancelButton = new DevExpress.XtraBars.BarButtonItem();
            this.barDockControlTop = new DevExpress.XtraBars.BarDockControl();
            this.barDockControlBottom = new DevExpress.XtraBars.BarDockControl();
            this.barDockControlLeft = new DevExpress.XtraBars.BarDockControl();
            this.barDockControlRight = new DevExpress.XtraBars.BarDockControl();
            this.MainLayoutControl = new DevExpress.XtraLayout.LayoutControl();
            this.Disclosure5AwaitingApprovalField = new DevExpress.XtraEditors.CheckEdit();
            this.Disclosure5NoField = new DevExpress.XtraEditors.CheckEdit();
            this.PDSandFSGGrid = new DevExpress.XtraGrid.GridControl();
            this.PDSandFSGGridView = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.gridColumn71 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn72 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn73 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.LogGrid = new DevExpress.XtraGrid.GridControl();
            this.LogGridView = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.gridColumn28 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn29 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn30 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.repositoryItemRichTextEdit1 = new DevExpress.XtraEditors.Repository.RepositoryItemRichTextEdit();
            this.gridColumn31 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.LedgerGrid = new DevExpress.XtraGrid.GridControl();
            this.LedgerGridView = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.gridColumn27 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn13 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn14 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn15 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn16 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn17 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn18 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn19 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn20 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn21 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn22 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn23 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn24 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn25 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn26 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.PremiumDetailsGrid = new DevExpress.XtraGrid.GridControl();
            this.PremiumDetailsGridView = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.gridColumn7 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn8 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn9 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn10 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn11 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn12 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.PremiumBaseRatesGrid = new DevExpress.XtraGrid.GridControl();
            this.PremiumBaseRatesGridView = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.gridColumn6 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn1 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn2 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn3 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn4 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn5 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.DisclosureClaimsGrid = new DevExpress.XtraGrid.GridControl();
            this.DisclosureClaimsGridView = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.gridColumn32 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn33 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn34 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn35 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn36 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn37 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn38 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.EasyDocsGrid = new DevExpress.XtraGrid.GridControl();
            this.EasyDocsGridView = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.gridColumn39 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn40 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn41 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn42 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn43 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn44 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn45 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn46 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.NotesField = new DevExpress.XtraRichEdit.RichEditControl();
            this.PremiumTypeField = new DevExpress.XtraEditors.LookUpEdit();
            this.PolicyNumberField = new DevExpress.XtraEditors.TextEdit();
            this.REACodeField = new DevExpress.XtraEditors.ButtonEdit();
            this.NewBusinessArrangedByCodeField = new DevExpress.XtraEditors.ButtonEdit();
            this.DwellingTypeField = new DevExpress.XtraEditors.LookUpEdit();
            this.ConstructionTypeField = new DevExpress.XtraEditors.LookUpEdit();
            this.YearBuiltField = new DevExpress.XtraEditors.LookUpEdit();
            this.LandlordCodeField = new DevExpress.XtraEditors.ButtonEdit();
            this.LandlordNameField = new DevExpress.XtraEditors.TextEdit();
            this.BranchField = new DevExpress.XtraEditors.LookUpEdit();
            this.BushfireRatingField = new DevExpress.XtraEditors.TextEdit();
            this.ContentsSumInsuredField = new DevExpress.XtraEditors.LookUpEdit();
            this.BuildingSumInsuredAmountField = new DevExpress.XtraEditors.TextEdit();
            this.WeeksInArrearsField = new DevExpress.XtraEditors.LookUpEdit();
            this.FloodRiskRatingField = new DevExpress.XtraEditors.TextEdit();
            this.WeeklyRentSumInsuredAmountField = new DevExpress.XtraEditors.TextEdit();
            this.UnderwriterExcessAmountField = new DevExpress.XtraEditors.TextEdit();
            this.BuildingSumInsuredExcessAmountField = new DevExpress.XtraEditors.TextEdit();
            this.LandSizeField = new DevExpress.XtraEditors.LookUpEdit();
            this.TransactionNumberField = new DevExpress.XtraEditors.TextEdit();
            this.CreatedOnField = new DevExpress.XtraEditors.DateEdit();
            this.InceptionFromField = new DevExpress.XtraEditors.DateEdit();
            this.BalanceOutstandingAmountField = new DevExpress.XtraEditors.TextEdit();
            this.EffectiveFromField = new DevExpress.XtraEditors.DateEdit();
            this.BilledFromField = new DevExpress.XtraEditors.DateEdit();
            this.ClosingOnField = new DevExpress.XtraEditors.DateEdit();
            this.COISentOnField = new DevExpress.XtraEditors.DateEdit();
            this.BilledToField = new DevExpress.XtraEditors.DateEdit();
            this.RenewalOnField = new DevExpress.XtraEditors.DateEdit();
            this.PolicySentOnField = new DevExpress.XtraEditors.DateEdit();
            this.InvoicePrintedOnField = new DevExpress.XtraEditors.DateEdit();
            this.ContactCodeHeaderField = new DevExpress.XtraEditors.TextEdit();
            this.ContactNameHeaderField = new DevExpress.XtraEditors.ButtonEdit();
            this.LandlordCodeHeaderField = new DevExpress.XtraEditors.TextEdit();
            this.LandlordNameHeaderField = new DevExpress.XtraEditors.ButtonEdit();
            this.TransactionNumberHeaderField = new DevExpress.XtraEditors.TextEdit();
            this.TransactionStatusHeaderField = new DevExpress.XtraEditors.TextEdit();
            this.TransactionRenewalHeaderField = new DevExpress.XtraEditors.TextEdit();
            this.PremiumAmountHeaderField = new DevExpress.XtraEditors.TextEdit();
            this.OutstandingAmountHeaderField = new DevExpress.XtraEditors.TextEdit();
            this.ClaimCountHeaderField = new DevExpress.XtraEditors.ButtonEdit();
            this.OnHoldField = new NURV.Controls.FancyCheckEdit();
            this.RewiredReplumbedField = new NURV.Controls.FancyCheckEdit();
            this.FreeStandingField = new NURV.Controls.FancyCheckEdit();
            this.CommonGroundsField = new NURV.Controls.FancyCheckEdit();
            this.BusinessOrTradeField = new NURV.Controls.FancyCheckEdit();
            this.AcceptedBusinessUseField = new NURV.Controls.FancyCheckEdit();
            this.IsJobReducedDuePandemicField = new NURV.Controls.FancyCheckEdit();
            this.IsArrearsField = new NURV.Controls.FancyCheckEdit();
            this.TenantedField = new NURV.Controls.FancyCheckEdit();
            this.StaffDiscountField = new NURV.Controls.FancyCheckEdit();
            this.IsREADiscountField = new NURV.Controls.FancyCheckEdit();
            this.IsMultiplePropertyDiscountField = new NURV.Controls.FancyCheckEdit();
            this.IsFivePercentDiscountField = new NURV.Controls.FancyCheckEdit();
            this.Is15For12BonusOfferField = new NURV.Controls.FancyCheckEdit();
            this.PremiumPaidByField = new DevExpress.XtraEditors.LookUpEdit();
            this.PreferredMailingAddressTypeField = new DevExpress.XtraEditors.LookUpEdit();
            this.InsurerCodeField = new DevExpress.XtraEditors.TextEdit();
            this.OnlineQuoteNumberField = new DevExpress.XtraEditors.ButtonEdit();
            this.QuoteNumberField = new DevExpress.XtraEditors.TextEdit();
            this.AskedForQuoteWhoField = new DevExpress.XtraEditors.LookUpEdit();
            this.HowAskedForQuoteField = new DevExpress.XtraEditors.LookUpEdit();
            this.QuotedByField = new DevExpress.XtraEditors.TextEdit();
            this.QuotedOnField = new DevExpress.XtraEditors.TextEdit();
            this.ConvertedToCoverByField = new DevExpress.XtraEditors.TextEdit();
            this.ConvertedToCoverOnField = new DevExpress.XtraEditors.TextEdit();
            this.QuoteExpiresOnField = new DevExpress.XtraEditors.TextEdit();
            this.OnlineQuoteEmailAddressField = new DevExpress.XtraEditors.TextEdit();
            this.WhoPlacedCoverField = new DevExpress.XtraEditors.LookUpEdit();
            this.HowPlacedCoverField = new DevExpress.XtraEditors.LookUpEdit();
            this.HeardAboutTSIviaField = new DevExpress.XtraEditors.LookUpEdit();
            this.MortgageeField = new DevExpress.XtraEditors.MemoEdit();
            this.PaymentMethodField = new DevExpress.XtraEditors.LookUpEdit();
            this.PaymentReferenceField = new DevExpress.XtraEditors.TextEdit();
            this.BPayReceiptField = new DevExpress.XtraEditors.TextEdit();
            this.OnlineReferenceField = new DevExpress.XtraEditors.TextEdit();
            this.CCTransactionIDField = new DevExpress.XtraEditors.TextEdit();
            this.VoucherTypeField = new DevExpress.XtraEditors.LookUpEdit();
            this.PromoCodeField = new DevExpress.XtraEditors.LookUpEdit();
            this.LinkCreditTransactionNumberField = new DevExpress.XtraEditors.TextEdit();
            this.LinkParentTransactionNumberField = new DevExpress.XtraEditors.TextEdit();
            this.LinkReferToTransactionNumberField = new DevExpress.XtraEditors.TextEdit();
            this.LinkWelcomeLetterTransactionNumberField = new DevExpress.XtraEditors.TextEdit();
            this.RecreatedField = new NURV.Controls.FancyCheckEdit();
            this.PaymentReceivedField = new NURV.Controls.FancyCheckEdit();
            this.ApprovedByField = new DevExpress.XtraEditors.TextEdit();
            this.ApprovedOnField = new DevExpress.XtraEditors.TextEdit();
            this.RenewingTransactionsField = new DevExpress.XtraEditors.TextEdit();
            this.RenewalReasonField = new DevExpress.XtraEditors.LookUpEdit();
            this.RenewalPremiumPaidByField = new DevExpress.XtraEditors.LookUpEdit();
            this.textEdit51 = new DevExpress.XtraEditors.TextEdit();
            this.RenewalDiscountAmountField = new DevExpress.XtraEditors.LookUpEdit();
            this.RenewedTransactionField = new DevExpress.XtraEditors.ButtonEdit();
            this.RenewedByTransactionField = new DevExpress.XtraEditors.ButtonEdit();
            this.LastRenewalErrorField = new DevExpress.XtraEditors.MemoEdit();
            this.RenewalBatchField = new DevExpress.XtraEditors.TextEdit();
            this.LedgerHoldCoverField = new DevExpress.XtraEditors.DateEdit();
            this.LedgerPremiumPaidField = new DevExpress.XtraEditors.DateEdit();
            this.LedgerCurrentOnField = new DevExpress.XtraEditors.DateEdit();
            this.LedgerDistributeFundsField = new DevExpress.XtraEditors.DateEdit();
            this.LedgerInvoiceOverdueOnField = new DevExpress.XtraEditors.DateEdit();
            this.LedgerExtendedOverdueOnField = new DevExpress.XtraEditors.DateEdit();
            this.LedgerOverdueNoticeSentField = new DevExpress.XtraEditors.DateEdit();
            this.LedgerExtendedLapsingOnField = new DevExpress.XtraEditors.DateEdit();
            this.LedgerFinalReminderSentField = new DevExpress.XtraEditors.DateEdit();
            this.LedgerCancelledLapsedOnField = new DevExpress.XtraEditors.DateEdit();
            this.NotesLastModifiedOnField = new DevExpress.XtraEditors.TextEdit();
            this.NotesLastModifiedByField = new DevExpress.XtraEditors.TextEdit();
            this.NotesCreatedOnField = new DevExpress.XtraEditors.TextEdit();
            this.NotesCreatedByField = new DevExpress.XtraEditors.TextEdit();
            this.SchemePaidField = new DevExpress.XtraEditors.DateEdit();
            this.EasyDocsEmailAddressField = new DevExpress.XtraEditors.TextEdit();
            this.EasyDocsPrimaryAssociateField = new DevExpress.XtraEditors.ButtonEdit();
            this.EasyDocsMobilePhoneField = new DevExpress.XtraEditors.TextEdit();
            this.LLReceivesElectronicCommunicationsField = new NURV.Controls.FancyCheckEdit();
            this.REAReceivesElectronicCommunicationsField = new NURV.Controls.FancyCheckEdit();
            this.DenialReasonField = new DevExpress.XtraEditors.MemoEdit();
            this.SpecialTermsField = new DevExpress.XtraEditors.MemoEdit();
            this.DisclosureItem1Field = new NURV.Controls.FancyCheckEdit();
            this.DisclosureItem2Field = new NURV.Controls.FancyCheckEdit();
            this.CircumstancesField = new DevExpress.XtraEditors.MemoEdit();
            this.RestrictionsField = new DevExpress.XtraEditors.MemoEdit();
            this.Disclosure3Field = new NURV.Controls.FancyCheckEdit();
            this.Disclosure4Field = new NURV.Controls.FancyCheckEdit();
            this.RenewalMultiPropertyDiscountField = new NURV.Controls.FancyCheckEdit();
            this.RenewalsOnHoldField = new NURV.Controls.FancyCheckEdit();
            this.DoNotRenewField = new NURV.Controls.FancyCheckEdit();
            this.RenewalImposedExcessField = new NURV.Controls.FancyCheckEdit();
            this.RealEstateDiscountField = new NURV.Controls.FancyCheckEdit();
            this.RenewalStaffDiscountField = new NURV.Controls.FancyCheckEdit();
            this.ApplyToRenewingPremiumField = new NURV.Controls.FancyCheckEdit();
            this.Disclosure5YesField = new DevExpress.XtraEditors.CheckEdit();
            this.ClassOfRiskField = new DevExpress.XtraEditors.LookUpEdit();
            this.SelfManagedField = new DevExpress.XtraEditors.LookUpEdit();
            this.IsFurnishedField = new DevExpress.XtraEditors.LookUpEdit();
            this.IsCCTVField = new DevExpress.XtraEditors.LookUpEdit();
            this.IsElectronicAccessField = new DevExpress.XtraEditors.LookUpEdit();
            this.HolidayLetField = new DevExpress.XtraEditors.LookUpEdit();
            this.Root = new DevExpress.XtraLayout.LayoutControlGroup();
            this.tabbedControlGroup1 = new DevExpress.XtraLayout.TabbedControlGroup();
            this.PolicyTabGroup = new DevExpress.XtraLayout.LayoutControlGroup();
            this.PolicyDetailsGroup = new DevExpress.XtraLayout.LayoutControlGroup();
            this.PolicyClassLayoutControlItem = new DevExpress.XtraLayout.LayoutControlItem();
            this.PremiumTypeLayoutControlItem = new DevExpress.XtraLayout.LayoutControlItem();
            this.PolicyNumberLayoutControlItem = new DevExpress.XtraLayout.LayoutControlItem();
            this.emptySpaceItem24 = new DevExpress.XtraLayout.EmptySpaceItem();
            this.REACodeLayoutControlItem = new DevExpress.XtraLayout.LayoutControlItem();
            this.NewBusinessArrangedByLayoutControlItem = new DevExpress.XtraLayout.LayoutControlItem();
            this.DwellingTypeLayoutControlItem = new DevExpress.XtraLayout.LayoutControlItem();
            this.ConstructionTypeLayoutControlItem = new DevExpress.XtraLayout.LayoutControlItem();
            this.YearBuiltLayoutControlItem = new DevExpress.XtraLayout.LayoutControlItem();
            this.CommonGroundsLayoutControlItem = new DevExpress.XtraLayout.LayoutControlItem();
            this.emptySpaceItem34 = new DevExpress.XtraLayout.EmptySpaceItem();
            this.CCTVLayoutControlItem = new DevExpress.XtraLayout.LayoutControlItem();
            this.ElectronicAccessLayoutControlItem = new DevExpress.XtraLayout.LayoutControlItem();
            this.BusinessOrTradeLayoutControlItem = new DevExpress.XtraLayout.LayoutControlItem();
            this.FreeStandingLayoutControlItem = new DevExpress.XtraLayout.LayoutControlItem();
            this.AcceptedBusinessUseLayoutControlItem = new DevExpress.XtraLayout.LayoutControlItem();
            this.OnHoldLayoutControlItem = new DevExpress.XtraLayout.LayoutControlItem();
            this.RewiredReplumbedLayoutControlItem = new DevExpress.XtraLayout.LayoutControlItem();
            this.emptySpaceItem40 = new DevExpress.XtraLayout.EmptySpaceItem();
            this.emptySpaceItem41 = new DevExpress.XtraLayout.EmptySpaceItem();
            this.FurnishedLayoutControlItem = new DevExpress.XtraLayout.LayoutControlItem();
            this.SelfManagedlayoutControlItem = new DevExpress.XtraLayout.LayoutControlItem();
            this.HolidayLetLayoutControlItem = new DevExpress.XtraLayout.LayoutControlItem();
            this.PropertyGroup = new DevExpress.XtraLayout.LayoutControlGroup();
            this.LandlordCodeLayoutControlItem = new DevExpress.XtraLayout.LayoutControlItem();
            this.LandlordNameLayoutControlItem = new DevExpress.XtraLayout.LayoutControlItem();
            this.InsuredPropertyAddressLayoutControlItem = new DevExpress.XtraLayout.LayoutControlItem();
            this.BranchLayoutControlItem = new DevExpress.XtraLayout.LayoutControlItem();
            this.BushfireLayoutControlItem = new DevExpress.XtraLayout.LayoutControlItem();
            this.ContentsSumInsuredLayoutControlItem = new DevExpress.XtraLayout.LayoutControlItem();
            this.BuildingSumInsuredLayoutControlItem = new DevExpress.XtraLayout.LayoutControlItem();
            this.TenantedLayoutControlItem = new DevExpress.XtraLayout.LayoutControlItem();
            this.emptySpaceItem3 = new DevExpress.XtraLayout.EmptySpaceItem();
            this.emptySpaceItem4 = new DevExpress.XtraLayout.EmptySpaceItem();
            this.FloodRiskLayoutControlItem = new DevExpress.XtraLayout.LayoutControlItem();
            this.WeeklyRentSumInsuredLayoutControlItem = new DevExpress.XtraLayout.LayoutControlItem();
            this.UnderwriterExcessLayoutControlItem = new DevExpress.XtraLayout.LayoutControlItem();
            this.BuildingSumInsuredExcessLayoutControlItem = new DevExpress.XtraLayout.LayoutControlItem();
            this.LandSizeLayoutControlItem = new DevExpress.XtraLayout.LayoutControlItem();
            this.emptySpaceItem17 = new DevExpress.XtraLayout.EmptySpaceItem();
            this.JobReducedCoronaVirusLayoutControlItem = new DevExpress.XtraLayout.LayoutControlItem();
            this.ArrearsLayoutControlItem = new DevExpress.XtraLayout.LayoutControlItem();
            this.WeeksInArrearsLayoutControlItem = new DevExpress.XtraLayout.LayoutControlItem();
            this.emptySpaceItem1 = new DevExpress.XtraLayout.EmptySpaceItem();
            this.DatesGroup = new DevExpress.XtraLayout.LayoutControlGroup();
            this.TransactionNumberLayoutControlItem = new DevExpress.XtraLayout.LayoutControlItem();
            this.CreatedOnLayoutControlItem = new DevExpress.XtraLayout.LayoutControlItem();
            this.InceptionFromLayoutControlItem = new DevExpress.XtraLayout.LayoutControlItem();
            this.BalanceLayoutControlItem = new DevExpress.XtraLayout.LayoutControlItem();
            this.EffectiveFromLayoutControlItem = new DevExpress.XtraLayout.LayoutControlItem();
            this.BilledFromLayoutControlItem = new DevExpress.XtraLayout.LayoutControlItem();
            this.ClosingOnLayoutControlItem = new DevExpress.XtraLayout.LayoutControlItem();
            this.COISentOnLayoutControlItem = new DevExpress.XtraLayout.LayoutControlItem();
            this.BilledToLayoutControlItem = new DevExpress.XtraLayout.LayoutControlItem();
            this.RenewalOnLayoutControlItem = new DevExpress.XtraLayout.LayoutControlItem();
            this.PolicySentOnLayoutControlItem = new DevExpress.XtraLayout.LayoutControlItem();
            this.InvoicePrintedOnLayoutControlItem = new DevExpress.XtraLayout.LayoutControlItem();
            this.DiscountsGroup = new DevExpress.XtraLayout.LayoutControlGroup();
            this.FivePercentDiscountLayoutControlItem = new DevExpress.XtraLayout.LayoutControlItem();
            this.MultiplePropertyDiscountLayoutControlItem = new DevExpress.XtraLayout.LayoutControlItem();
            this.READiscountLayoutControlItem = new DevExpress.XtraLayout.LayoutControlItem();
            this.StaffDiscountLayoutControlItem = new DevExpress.XtraLayout.LayoutControlItem();
            this.emptySpaceItem21 = new DevExpress.XtraLayout.EmptySpaceItem();
            this.emptySpaceItem12 = new DevExpress.XtraLayout.EmptySpaceItem();
            this.FifteenForTwelveBonusOfferLayoutControlItem = new DevExpress.XtraLayout.LayoutControlItem();
            this.emptySpaceItem35 = new DevExpress.XtraLayout.EmptySpaceItem();
            this.OtherDetailsTabGroup = new DevExpress.XtraLayout.LayoutControlGroup();
            this.PremiumDetailsGroup = new DevExpress.XtraLayout.LayoutControlGroup();
            this.PremiumPaidByLayoutControlItem = new DevExpress.XtraLayout.LayoutControlItem();
            this.PreferredMailingAddressLayoutControlItem = new DevExpress.XtraLayout.LayoutControlItem();
            this.InsurerCodeLayoutControlItem = new DevExpress.XtraLayout.LayoutControlItem();
            this.QuoteDetailsGroup = new DevExpress.XtraLayout.LayoutControlGroup();
            this.OnlineQuoteNumberLayoutControlItem = new DevExpress.XtraLayout.LayoutControlItem();
            this.QuoteNumberLayoutControlItem = new DevExpress.XtraLayout.LayoutControlItem();
            this.AskedForQuoteLayoutControlItem = new DevExpress.XtraLayout.LayoutControlItem();
            this.HowAskedLayoutControlItem = new DevExpress.XtraLayout.LayoutControlItem();
            this.QuotedByLayoutControlItem = new DevExpress.XtraLayout.LayoutControlItem();
            this.QuotedOnLayoutControlItem = new DevExpress.XtraLayout.LayoutControlItem();
            this.ConvertedToCoverByLayoutControlItem = new DevExpress.XtraLayout.LayoutControlItem();
            this.ConvertedToCoverOnLayoutControlItem = new DevExpress.XtraLayout.LayoutControlItem();
            this.QuoteExpiresOnLayoutControlItem = new DevExpress.XtraLayout.LayoutControlItem();
            this.OnlineQuoteEmailLayoutControlItem = new DevExpress.XtraLayout.LayoutControlItem();
            this.emptySpaceItem7 = new DevExpress.XtraLayout.EmptySpaceItem();
            this.CoverDetailsGroup = new DevExpress.XtraLayout.LayoutControlGroup();
            this.RecreatedLayoutControlItem = new DevExpress.XtraLayout.LayoutControlItem();
            this.WhoPlacedCoverLayoutControlItem = new DevExpress.XtraLayout.LayoutControlItem();
            this.HowPlacedCoverLayoutControlItem = new DevExpress.XtraLayout.LayoutControlItem();
            this.emptySpaceItem2 = new DevExpress.XtraLayout.EmptySpaceItem();
            this.HeardAboutTSIViaLayoutControlItem = new DevExpress.XtraLayout.LayoutControlItem();
            this.MortgageeLayoutControlItem = new DevExpress.XtraLayout.LayoutControlItem();
            this.PaymentDetailsGroup = new DevExpress.XtraLayout.LayoutControlGroup();
            this.PaymentReceivedLayoutControlItem = new DevExpress.XtraLayout.LayoutControlItem();
            this.PaymentMethodLayoutControlItem = new DevExpress.XtraLayout.LayoutControlItem();
            this.PaymentReferenceLayoutControlItem = new DevExpress.XtraLayout.LayoutControlItem();
            this.BPayReceiptLayoutControlItem = new DevExpress.XtraLayout.LayoutControlItem();
            this.OnlineReferenceLayoutControlItem = new DevExpress.XtraLayout.LayoutControlItem();
            this.CCTransactionIDLayoutControlItem = new DevExpress.XtraLayout.LayoutControlItem();
            this.VoucherLayoutControlItem = new DevExpress.XtraLayout.LayoutControlItem();
            this.PromoCodeLayoutControlItem = new DevExpress.XtraLayout.LayoutControlItem();
            this.emptySpaceItem25 = new DevExpress.XtraLayout.EmptySpaceItem();
            this.LinkedTransactionsGroup = new DevExpress.XtraLayout.LayoutControlGroup();
            this.CreditTransactionLayoutControlItem = new DevExpress.XtraLayout.LayoutControlItem();
            this.ParentTransactionLayoutControlItem = new DevExpress.XtraLayout.LayoutControlItem();
            this.ReferToTransactionLayoutControlItem = new DevExpress.XtraLayout.LayoutControlItem();
            this.WelcomeLetterJoinedLayoutControlItem = new DevExpress.XtraLayout.LayoutControlItem();
            this.emptySpaceItem15 = new DevExpress.XtraLayout.EmptySpaceItem();
            this.emptySpaceItem32 = new DevExpress.XtraLayout.EmptySpaceItem();
            this.emptySpaceItem5 = new DevExpress.XtraLayout.EmptySpaceItem();
            this.DisclosureTabGroup = new DevExpress.XtraLayout.LayoutControlGroup();
            this.ApprovalGroup = new DevExpress.XtraLayout.LayoutControlGroup();
            this.ApprovedByLayoutControlItem = new DevExpress.XtraLayout.LayoutControlItem();
            this.ApprovedOnLayoutControlItem = new DevExpress.XtraLayout.LayoutControlItem();
            this.emptySpaceItem14 = new DevExpress.XtraLayout.EmptySpaceItem();
            this.QuesstionnaireGroup = new DevExpress.XtraLayout.LayoutControlGroup();
            this.DisclosureItem1LayoutControlItem = new DevExpress.XtraLayout.LayoutControlItem();
            this.DenialReasonLayoutControlItem = new DevExpress.XtraLayout.LayoutControlItem();
            this.SpecialTermsLayoutControlItem = new DevExpress.XtraLayout.LayoutControlItem();
            this.DisclosureItem2LayoutControlItem = new DevExpress.XtraLayout.LayoutControlItem();
            this.emptySpaceItem10 = new DevExpress.XtraLayout.EmptySpaceItem();
            this.emptySpaceItem13 = new DevExpress.XtraLayout.EmptySpaceItem();
            this.layoutControlItem5 = new DevExpress.XtraLayout.LayoutControlItem();
            this.DisclosureItem3LayoutControlItem = new DevExpress.XtraLayout.LayoutControlItem();
            this.DisclosureItem4LayoutControlItem = new DevExpress.XtraLayout.LayoutControlItem();
            this.CircumstancesLayoutControlItem = new DevExpress.XtraLayout.LayoutControlItem();
            this.DisclosureItem5LayoutControlItem = new DevExpress.XtraLayout.LayoutControlItem();
            this.RestrictionsLayoutControlItem = new DevExpress.XtraLayout.LayoutControlItem();
            this.emptySpaceItem18 = new DevExpress.XtraLayout.EmptySpaceItem();
            this.emptySpaceItem39 = new DevExpress.XtraLayout.EmptySpaceItem();
            this.Disclosure5NoLayoutItem = new DevExpress.XtraLayout.LayoutControlItem();
            this.Disclosure5AwaitingApprovalLayoutItem = new DevExpress.XtraLayout.LayoutControlItem();
            this.emptySpaceItem20 = new DevExpress.XtraLayout.EmptySpaceItem();
            this.PremiumAndRenewalTabGroup = new DevExpress.XtraLayout.LayoutControlGroup();
            this.ExistingPremiumDetailsGroup = new DevExpress.XtraLayout.LayoutControlGroup();
            this.layoutControlItem6 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem7 = new DevExpress.XtraLayout.LayoutControlItem();
            this.emptySpaceItem37 = new DevExpress.XtraLayout.EmptySpaceItem();
            this.RenewingTransactionSettingsGroup = new DevExpress.XtraLayout.LayoutControlGroup();
            this.RenewingTransactionsLayoutControlItem = new DevExpress.XtraLayout.LayoutControlItem();
            this.RenewalReasonLayoutControlItem = new DevExpress.XtraLayout.LayoutControlItem();
            this.RenewalPremiumPaidByLayoutControlItem = new DevExpress.XtraLayout.LayoutControlItem();
            this.RenewalImposedExcessLayoutControlItem = new DevExpress.XtraLayout.LayoutControlItem();
            this.RenewExcessAmountLayoutControlItem = new DevExpress.XtraLayout.LayoutControlItem();
            this.DoNotRenewLayoutControlItem = new DevExpress.XtraLayout.LayoutControlItem();
            this.emptySpaceItem28 = new DevExpress.XtraLayout.EmptySpaceItem();
            this.emptySpaceItem29 = new DevExpress.XtraLayout.EmptySpaceItem();
            this.emptySpaceItem27 = new DevExpress.XtraLayout.EmptySpaceItem();
            this.RenewalsOnHoldLayoutControlItem = new DevExpress.XtraLayout.LayoutControlItem();
            this.RenewingDiscountsGroup = new DevExpress.XtraLayout.LayoutControlGroup();
            this.RenewalMultiplePropertyDiscountLayoutControlItem = new DevExpress.XtraLayout.LayoutControlItem();
            this.ApplyToRenewingPremiumLayoutControlItem = new DevExpress.XtraLayout.LayoutControlItem();
            this.RenewalStaffDiscountLayoutControlItem = new DevExpress.XtraLayout.LayoutControlItem();
            this.RealEstateDiscountLayoutControlItem = new DevExpress.XtraLayout.LayoutControlItem();
            this.emptySpaceItem31 = new DevExpress.XtraLayout.EmptySpaceItem();
            this.RenewalDiscountAmountLayoutControlItem = new DevExpress.XtraLayout.LayoutControlItem();
            this.emptySpaceItem11 = new DevExpress.XtraLayout.EmptySpaceItem();
            this.emptySpaceItem36 = new DevExpress.XtraLayout.EmptySpaceItem();
            this.RenewalProcessingGroup = new DevExpress.XtraLayout.LayoutControlGroup();
            this.RenewedTransactionLayoutControlItem = new DevExpress.XtraLayout.LayoutControlItem();
            this.RenewedByTransactionLayoutControlItem = new DevExpress.XtraLayout.LayoutControlItem();
            this.LastRenewalErrorLayoutControlItem = new DevExpress.XtraLayout.LayoutControlItem();
            this.RenewalBatchLayoutControlItem = new DevExpress.XtraLayout.LayoutControlItem();
            this.emptySpaceItem19 = new DevExpress.XtraLayout.EmptySpaceItem();
            this.emptySpaceItem22 = new DevExpress.XtraLayout.EmptySpaceItem();
            this.emptySpaceItem23 = new DevExpress.XtraLayout.EmptySpaceItem();
            this.emptySpaceItem30 = new DevExpress.XtraLayout.EmptySpaceItem();
            this.LedgerTabGroup = new DevExpress.XtraLayout.LayoutControlGroup();
            this.LedgerHoldCoverLayoutControlItem = new DevExpress.XtraLayout.LayoutControlItem();
            this.LedgerCurrentOnLayoutControlItem = new DevExpress.XtraLayout.LayoutControlItem();
            this.LedgerInvoiceOverdueOnLayoutControlItem = new DevExpress.XtraLayout.LayoutControlItem();
            this.LedgerOverdueNoticeSentLayoutControlItem = new DevExpress.XtraLayout.LayoutControlItem();
            this.LedgerExtendedLapsingOnLayoutControlItem = new DevExpress.XtraLayout.LayoutControlItem();
            this.LedgerPremiumPaidLayoutControlItem = new DevExpress.XtraLayout.LayoutControlItem();
            this.LedgerDistributeFundsLayoutControlItem = new DevExpress.XtraLayout.LayoutControlItem();
            this.LedgerExtendedOverdueOnLayoutControlItem = new DevExpress.XtraLayout.LayoutControlItem();
            this.LedgerFinalReminderSentLayoutControlItem = new DevExpress.XtraLayout.LayoutControlItem();
            this.LedgerCancelledLapsingOnLayoutControlItem = new DevExpress.XtraLayout.LayoutControlItem();
            this.emptySpaceItem8 = new DevExpress.XtraLayout.EmptySpaceItem();
            this.layoutControlItem8 = new DevExpress.XtraLayout.LayoutControlItem();
            this.NotesTabGroup = new DevExpress.XtraLayout.LayoutControlGroup();
            this.LogGroup = new DevExpress.XtraLayout.LayoutControlGroup();
            this.NotesAndSpecialConditionsGroup = new DevExpress.XtraLayout.LayoutControlGroup();
            this.layoutControlItem1 = new DevExpress.XtraLayout.LayoutControlItem();
            this.SchemePaidLayoutControlItem = new DevExpress.XtraLayout.LayoutControlItem();
            this.emptySpaceItem9 = new DevExpress.XtraLayout.EmptySpaceItem();
            this.NotesCreatedByLayoutControlItem = new DevExpress.XtraLayout.LayoutControlItem();
            this.NotesCreatedOnLayoutControlItem = new DevExpress.XtraLayout.LayoutControlItem();
            this.NotesLastModifiedByLayoutControlItem = new DevExpress.XtraLayout.LayoutControlItem();
            this.NotesLastModifiedOnLayoutControlItem = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem2 = new DevExpress.XtraLayout.LayoutControlItem();
            this.EasyDocTabGroup = new DevExpress.XtraLayout.LayoutControlGroup();
            this.LLReceivesElectronicCommunicationLayoutControlItem = new DevExpress.XtraLayout.LayoutControlItem();
            this.REAReceivesElectronicCommunicationLayoutControlItem = new DevExpress.XtraLayout.LayoutControlItem();
            this.EasyDocsEmailAddressLayoutControlItem = new DevExpress.XtraLayout.LayoutControlItem();
            this.EasyDocsPrimaryAssociateLayoutControlItem = new DevExpress.XtraLayout.LayoutControlItem();
            this.emptySpaceItem16 = new DevExpress.XtraLayout.EmptySpaceItem();
            this.EasyDocsMobileNumberLayoutControlItem = new DevExpress.XtraLayout.LayoutControlItem();
            this.emptySpaceItem26 = new DevExpress.XtraLayout.EmptySpaceItem();
            this.layoutControlItem4 = new DevExpress.XtraLayout.LayoutControlItem();
            this.emptySpaceItem33 = new DevExpress.XtraLayout.EmptySpaceItem();
            this.HistoryTabGroup = new DevExpress.XtraLayout.LayoutControlGroup();
            this.layoutControlItem10 = new DevExpress.XtraLayout.LayoutControlItem();
            this.PDSAndFSGTabGroup = new DevExpress.XtraLayout.LayoutControlGroup();
            this.PDSAndFSGGroup = new DevExpress.XtraLayout.LayoutControlGroup();
            this.layoutControlItem11 = new DevExpress.XtraLayout.LayoutControlItem();
            this.CommunicationsGroup = new DevExpress.XtraLayout.LayoutControlGroup();
            this.emptySpaceItem38 = new DevExpress.XtraLayout.EmptySpaceItem();
            this.emptySpaceItem6 = new DevExpress.XtraLayout.EmptySpaceItem();
            this.ContactCodeHeaderLayoutControlItem = new DevExpress.XtraLayout.LayoutControlItem();
            this.ContactNameHeaderLayoutControlItem = new DevExpress.XtraLayout.LayoutControlItem();
            this.LandlordCodeHeaderLayoutControlItem = new DevExpress.XtraLayout.LayoutControlItem();
            this.LandlordNameHeaderLayoutControlItem = new DevExpress.XtraLayout.LayoutControlItem();
            this.TransactionNumberHeaderLayoutControlItem = new DevExpress.XtraLayout.LayoutControlItem();
            this.TransactionStatusHeaderLayoutControlItem = new DevExpress.XtraLayout.LayoutControlItem();
            this.TransactionRenewalHeaderLayoutControlItem = new DevExpress.XtraLayout.LayoutControlItem();
            this.PremiumAmountHeaderLayoutControlItem = new DevExpress.XtraLayout.LayoutControlItem();
            this.OutstandingAmountHeaderLayoutControlItem = new DevExpress.XtraLayout.LayoutControlItem();
            this.ClaimCountHeaderLayoutControlItem = new DevExpress.XtraLayout.LayoutControlItem();
            this.LookupEditToolTipController = new DevExpress.Utils.ToolTipController(this.components);
            this.popupMenu1 = new DevExpress.XtraBars.PopupMenu(this.components);
            this.dxErrorProvider1 = new DevExpress.XtraEditors.DXErrorProvider.DXErrorProvider(this.components);
            ((System.ComponentModel.ISupportInitialize)(this.HistoryDetailGridView)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.HistoryGrid)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.HistoryGridView)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.barManager1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.MainLayoutControl)).BeginInit();
            this.MainLayoutControl.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.Disclosure5AwaitingApprovalField.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Disclosure5NoField.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.PDSandFSGGrid)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.PDSandFSGGridView)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.LogGrid)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.LogGridView)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemRichTextEdit1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.LedgerGrid)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.LedgerGridView)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.PremiumDetailsGrid)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.PremiumDetailsGridView)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.PremiumBaseRatesGrid)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.PremiumBaseRatesGridView)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.DisclosureClaimsGrid)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.DisclosureClaimsGridView)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.EasyDocsGrid)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.EasyDocsGridView)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.PremiumTypeField.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.PolicyNumberField.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.REACodeField.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.NewBusinessArrangedByCodeField.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.DwellingTypeField.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ConstructionTypeField.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.YearBuiltField.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.LandlordCodeField.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.LandlordNameField.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.BranchField.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.BushfireRatingField.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ContentsSumInsuredField.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.BuildingSumInsuredAmountField.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.WeeksInArrearsField.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.FloodRiskRatingField.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.WeeklyRentSumInsuredAmountField.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.UnderwriterExcessAmountField.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.BuildingSumInsuredExcessAmountField.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.LandSizeField.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TransactionNumberField.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.CreatedOnField.Properties.CalendarTimeProperties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.CreatedOnField.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.InceptionFromField.Properties.CalendarTimeProperties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.InceptionFromField.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.BalanceOutstandingAmountField.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.EffectiveFromField.Properties.CalendarTimeProperties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.EffectiveFromField.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.BilledFromField.Properties.CalendarTimeProperties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.BilledFromField.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ClosingOnField.Properties.CalendarTimeProperties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ClosingOnField.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.COISentOnField.Properties.CalendarTimeProperties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.COISentOnField.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.BilledToField.Properties.CalendarTimeProperties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.BilledToField.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.RenewalOnField.Properties.CalendarTimeProperties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.RenewalOnField.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.PolicySentOnField.Properties.CalendarTimeProperties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.PolicySentOnField.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.InvoicePrintedOnField.Properties.CalendarTimeProperties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.InvoicePrintedOnField.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ContactCodeHeaderField.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ContactNameHeaderField.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.LandlordCodeHeaderField.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.LandlordNameHeaderField.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TransactionNumberHeaderField.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TransactionStatusHeaderField.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TransactionRenewalHeaderField.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.PremiumAmountHeaderField.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.OutstandingAmountHeaderField.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ClaimCountHeaderField.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.OnHoldField.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.RewiredReplumbedField.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.FreeStandingField.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.CommonGroundsField.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.BusinessOrTradeField.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.AcceptedBusinessUseField.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.IsJobReducedDuePandemicField.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.IsArrearsField.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TenantedField.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.StaffDiscountField.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.IsREADiscountField.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.IsMultiplePropertyDiscountField.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.IsFivePercentDiscountField.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Is15For12BonusOfferField.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.PremiumPaidByField.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.PreferredMailingAddressTypeField.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.InsurerCodeField.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.OnlineQuoteNumberField.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.QuoteNumberField.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.AskedForQuoteWhoField.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.HowAskedForQuoteField.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.QuotedByField.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.QuotedOnField.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ConvertedToCoverByField.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ConvertedToCoverOnField.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.QuoteExpiresOnField.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.OnlineQuoteEmailAddressField.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.WhoPlacedCoverField.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.HowPlacedCoverField.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.HeardAboutTSIviaField.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.MortgageeField.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.PaymentMethodField.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.PaymentReferenceField.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.BPayReceiptField.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.OnlineReferenceField.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.CCTransactionIDField.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.VoucherTypeField.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.PromoCodeField.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.LinkCreditTransactionNumberField.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.LinkParentTransactionNumberField.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.LinkReferToTransactionNumberField.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.LinkWelcomeLetterTransactionNumberField.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.RecreatedField.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.PaymentReceivedField.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ApprovedByField.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ApprovedOnField.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.RenewingTransactionsField.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.RenewalReasonField.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.RenewalPremiumPaidByField.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEdit51.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.RenewalDiscountAmountField.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.RenewedTransactionField.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.RenewedByTransactionField.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.LastRenewalErrorField.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.RenewalBatchField.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.LedgerHoldCoverField.Properties.CalendarTimeProperties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.LedgerHoldCoverField.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.LedgerPremiumPaidField.Properties.CalendarTimeProperties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.LedgerPremiumPaidField.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.LedgerCurrentOnField.Properties.CalendarTimeProperties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.LedgerCurrentOnField.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.LedgerDistributeFundsField.Properties.CalendarTimeProperties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.LedgerDistributeFundsField.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.LedgerInvoiceOverdueOnField.Properties.CalendarTimeProperties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.LedgerInvoiceOverdueOnField.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.LedgerExtendedOverdueOnField.Properties.CalendarTimeProperties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.LedgerExtendedOverdueOnField.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.LedgerOverdueNoticeSentField.Properties.CalendarTimeProperties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.LedgerOverdueNoticeSentField.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.LedgerExtendedLapsingOnField.Properties.CalendarTimeProperties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.LedgerExtendedLapsingOnField.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.LedgerFinalReminderSentField.Properties.CalendarTimeProperties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.LedgerFinalReminderSentField.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.LedgerCancelledLapsedOnField.Properties.CalendarTimeProperties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.LedgerCancelledLapsedOnField.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.NotesLastModifiedOnField.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.NotesLastModifiedByField.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.NotesCreatedOnField.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.NotesCreatedByField.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.SchemePaidField.Properties.CalendarTimeProperties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.SchemePaidField.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.EasyDocsEmailAddressField.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.EasyDocsPrimaryAssociateField.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.EasyDocsMobilePhoneField.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.LLReceivesElectronicCommunicationsField.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.REAReceivesElectronicCommunicationsField.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.DenialReasonField.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.SpecialTermsField.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.DisclosureItem1Field.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.DisclosureItem2Field.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.CircumstancesField.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.RestrictionsField.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Disclosure3Field.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Disclosure4Field.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.RenewalMultiPropertyDiscountField.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.RenewalsOnHoldField.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.DoNotRenewField.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.RenewalImposedExcessField.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.RealEstateDiscountField.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.RenewalStaffDiscountField.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ApplyToRenewingPremiumField.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Disclosure5YesField.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ClassOfRiskField.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.SelfManagedField.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.IsFurnishedField.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.IsCCTVField.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.IsElectronicAccessField.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.HolidayLetField.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Root)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.tabbedControlGroup1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.PolicyTabGroup)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.PolicyDetailsGroup)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.PolicyClassLayoutControlItem)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.PremiumTypeLayoutControlItem)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.PolicyNumberLayoutControlItem)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem24)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.REACodeLayoutControlItem)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.NewBusinessArrangedByLayoutControlItem)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.DwellingTypeLayoutControlItem)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ConstructionTypeLayoutControlItem)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.YearBuiltLayoutControlItem)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.CommonGroundsLayoutControlItem)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem34)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.CCTVLayoutControlItem)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ElectronicAccessLayoutControlItem)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.BusinessOrTradeLayoutControlItem)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.FreeStandingLayoutControlItem)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.AcceptedBusinessUseLayoutControlItem)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.OnHoldLayoutControlItem)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.RewiredReplumbedLayoutControlItem)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem40)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem41)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.FurnishedLayoutControlItem)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.SelfManagedlayoutControlItem)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.HolidayLetLayoutControlItem)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.PropertyGroup)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.LandlordCodeLayoutControlItem)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.LandlordNameLayoutControlItem)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.InsuredPropertyAddressLayoutControlItem)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.BranchLayoutControlItem)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.BushfireLayoutControlItem)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ContentsSumInsuredLayoutControlItem)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.BuildingSumInsuredLayoutControlItem)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TenantedLayoutControlItem)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem4)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.FloodRiskLayoutControlItem)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.WeeklyRentSumInsuredLayoutControlItem)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.UnderwriterExcessLayoutControlItem)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.BuildingSumInsuredExcessLayoutControlItem)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.LandSizeLayoutControlItem)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem17)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.JobReducedCoronaVirusLayoutControlItem)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ArrearsLayoutControlItem)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.WeeksInArrearsLayoutControlItem)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.DatesGroup)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TransactionNumberLayoutControlItem)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.CreatedOnLayoutControlItem)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.InceptionFromLayoutControlItem)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.BalanceLayoutControlItem)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.EffectiveFromLayoutControlItem)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.BilledFromLayoutControlItem)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ClosingOnLayoutControlItem)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.COISentOnLayoutControlItem)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.BilledToLayoutControlItem)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.RenewalOnLayoutControlItem)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.PolicySentOnLayoutControlItem)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.InvoicePrintedOnLayoutControlItem)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.DiscountsGroup)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.FivePercentDiscountLayoutControlItem)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.MultiplePropertyDiscountLayoutControlItem)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.READiscountLayoutControlItem)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.StaffDiscountLayoutControlItem)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem21)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem12)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.FifteenForTwelveBonusOfferLayoutControlItem)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem35)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.OtherDetailsTabGroup)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.PremiumDetailsGroup)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.PremiumPaidByLayoutControlItem)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.PreferredMailingAddressLayoutControlItem)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.InsurerCodeLayoutControlItem)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.QuoteDetailsGroup)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.OnlineQuoteNumberLayoutControlItem)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.QuoteNumberLayoutControlItem)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.AskedForQuoteLayoutControlItem)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.HowAskedLayoutControlItem)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.QuotedByLayoutControlItem)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.QuotedOnLayoutControlItem)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ConvertedToCoverByLayoutControlItem)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ConvertedToCoverOnLayoutControlItem)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.QuoteExpiresOnLayoutControlItem)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.OnlineQuoteEmailLayoutControlItem)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem7)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.CoverDetailsGroup)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.RecreatedLayoutControlItem)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.WhoPlacedCoverLayoutControlItem)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.HowPlacedCoverLayoutControlItem)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.HeardAboutTSIViaLayoutControlItem)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.MortgageeLayoutControlItem)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.PaymentDetailsGroup)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.PaymentReceivedLayoutControlItem)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.PaymentMethodLayoutControlItem)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.PaymentReferenceLayoutControlItem)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.BPayReceiptLayoutControlItem)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.OnlineReferenceLayoutControlItem)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.CCTransactionIDLayoutControlItem)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.VoucherLayoutControlItem)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.PromoCodeLayoutControlItem)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem25)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.LinkedTransactionsGroup)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.CreditTransactionLayoutControlItem)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ParentTransactionLayoutControlItem)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ReferToTransactionLayoutControlItem)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.WelcomeLetterJoinedLayoutControlItem)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem15)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem32)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem5)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.DisclosureTabGroup)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ApprovalGroup)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ApprovedByLayoutControlItem)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ApprovedOnLayoutControlItem)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem14)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.QuesstionnaireGroup)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.DisclosureItem1LayoutControlItem)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.DenialReasonLayoutControlItem)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.SpecialTermsLayoutControlItem)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.DisclosureItem2LayoutControlItem)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem10)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem13)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem5)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.DisclosureItem3LayoutControlItem)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.DisclosureItem4LayoutControlItem)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.CircumstancesLayoutControlItem)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.DisclosureItem5LayoutControlItem)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.RestrictionsLayoutControlItem)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem18)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem39)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Disclosure5NoLayoutItem)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Disclosure5AwaitingApprovalLayoutItem)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem20)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.PremiumAndRenewalTabGroup)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ExistingPremiumDetailsGroup)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem6)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem7)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem37)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.RenewingTransactionSettingsGroup)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.RenewingTransactionsLayoutControlItem)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.RenewalReasonLayoutControlItem)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.RenewalPremiumPaidByLayoutControlItem)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.RenewalImposedExcessLayoutControlItem)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.RenewExcessAmountLayoutControlItem)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.DoNotRenewLayoutControlItem)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem28)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem29)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem27)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.RenewalsOnHoldLayoutControlItem)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.RenewingDiscountsGroup)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.RenewalMultiplePropertyDiscountLayoutControlItem)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ApplyToRenewingPremiumLayoutControlItem)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.RenewalStaffDiscountLayoutControlItem)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.RealEstateDiscountLayoutControlItem)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem31)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.RenewalDiscountAmountLayoutControlItem)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem11)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem36)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.RenewalProcessingGroup)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.RenewedTransactionLayoutControlItem)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.RenewedByTransactionLayoutControlItem)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.LastRenewalErrorLayoutControlItem)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.RenewalBatchLayoutControlItem)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem19)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem22)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem23)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem30)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.LedgerTabGroup)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.LedgerHoldCoverLayoutControlItem)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.LedgerCurrentOnLayoutControlItem)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.LedgerInvoiceOverdueOnLayoutControlItem)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.LedgerOverdueNoticeSentLayoutControlItem)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.LedgerExtendedLapsingOnLayoutControlItem)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.LedgerPremiumPaidLayoutControlItem)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.LedgerDistributeFundsLayoutControlItem)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.LedgerExtendedOverdueOnLayoutControlItem)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.LedgerFinalReminderSentLayoutControlItem)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.LedgerCancelledLapsingOnLayoutControlItem)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem8)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem8)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.NotesTabGroup)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.LogGroup)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.NotesAndSpecialConditionsGroup)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.SchemePaidLayoutControlItem)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem9)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.NotesCreatedByLayoutControlItem)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.NotesCreatedOnLayoutControlItem)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.NotesLastModifiedByLayoutControlItem)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.NotesLastModifiedOnLayoutControlItem)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.EasyDocTabGroup)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.LLReceivesElectronicCommunicationLayoutControlItem)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.REAReceivesElectronicCommunicationLayoutControlItem)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.EasyDocsEmailAddressLayoutControlItem)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.EasyDocsPrimaryAssociateLayoutControlItem)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem16)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.EasyDocsMobileNumberLayoutControlItem)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem26)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem4)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem33)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.HistoryTabGroup)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem10)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.PDSAndFSGTabGroup)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.PDSAndFSGGroup)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem11)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.CommunicationsGroup)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem38)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem6)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ContactCodeHeaderLayoutControlItem)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ContactNameHeaderLayoutControlItem)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.LandlordCodeHeaderLayoutControlItem)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.LandlordNameHeaderLayoutControlItem)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TransactionNumberHeaderLayoutControlItem)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TransactionStatusHeaderLayoutControlItem)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TransactionRenewalHeaderLayoutControlItem)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.PremiumAmountHeaderLayoutControlItem)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.OutstandingAmountHeaderLayoutControlItem)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ClaimCountHeaderLayoutControlItem)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.popupMenu1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dxErrorProvider1)).BeginInit();
            this.SuspendLayout();
            // 
            // HistoryDetailGridView
            // 
            this.HistoryDetailGridView.Appearance.HeaderPanel.Options.UseTextOptions = true;
            this.HistoryDetailGridView.Appearance.HeaderPanel.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.HistoryDetailGridView.Columns.AddRange(new DevExpress.XtraGrid.Columns.GridColumn[] {
            this.gridColumn59,
            this.gridColumn60,
            this.gridColumn61,
            this.gridColumn62,
            this.gridColumn63,
            this.gridColumn64,
            this.gridColumn65,
            this.gridColumn66,
            this.gridColumn67,
            this.gridColumn68,
            this.gridColumn69,
            this.gridColumn70});
            this.HistoryDetailGridView.GridControl = this.HistoryGrid;
            this.HistoryDetailGridView.Name = "HistoryDetailGridView";
            this.HistoryDetailGridView.OptionsBehavior.Editable = false;
            this.HistoryDetailGridView.OptionsDetail.AllowZoomDetail = false;
            this.HistoryDetailGridView.OptionsDetail.ShowDetailTabs = false;
            this.HistoryDetailGridView.OptionsView.ShowGroupPanel = false;
            // 
            // gridColumn59
            // 
            this.gridColumn59.Caption = "Transaction Number";
            this.gridColumn59.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            this.gridColumn59.FieldName = "TransactionNumber";
            this.gridColumn59.Name = "gridColumn59";
            this.gridColumn59.Visible = true;
            this.gridColumn59.VisibleIndex = 0;
            this.gridColumn59.Width = 110;
            // 
            // gridColumn60
            // 
            this.gridColumn60.Caption = "Code";
            this.gridColumn60.FieldName = "Code";
            this.gridColumn60.Name = "gridColumn60";
            this.gridColumn60.Visible = true;
            this.gridColumn60.VisibleIndex = 1;
            this.gridColumn60.Width = 92;
            // 
            // gridColumn61
            // 
            this.gridColumn61.Caption = "Status";
            this.gridColumn61.FieldName = "StatusOfTransaction";
            this.gridColumn61.Name = "gridColumn61";
            this.gridColumn61.Visible = true;
            this.gridColumn61.VisibleIndex = 2;
            this.gridColumn61.Width = 92;
            // 
            // gridColumn62
            // 
            this.gridColumn62.Caption = "Effective On";
            this.gridColumn62.DisplayFormat.FormatString = "d";
            this.gridColumn62.DisplayFormat.FormatType = DevExpress.Utils.FormatType.DateTime;
            this.gridColumn62.FieldName = "EffectiveDate";
            this.gridColumn62.Name = "gridColumn62";
            this.gridColumn62.Visible = true;
            this.gridColumn62.VisibleIndex = 3;
            this.gridColumn62.Width = 92;
            // 
            // gridColumn63
            // 
            this.gridColumn63.Caption = "Renewal On";
            this.gridColumn63.DisplayFormat.FormatString = "d";
            this.gridColumn63.DisplayFormat.FormatType = DevExpress.Utils.FormatType.DateTime;
            this.gridColumn63.FieldName = "RenewalDate";
            this.gridColumn63.Name = "gridColumn63";
            this.gridColumn63.Visible = true;
            this.gridColumn63.VisibleIndex = 4;
            this.gridColumn63.Width = 92;
            // 
            // gridColumn64
            // 
            this.gridColumn64.Caption = "Created On";
            this.gridColumn64.DisplayFormat.FormatString = "d";
            this.gridColumn64.DisplayFormat.FormatType = DevExpress.Utils.FormatType.DateTime;
            this.gridColumn64.FieldName = "CreationDate";
            this.gridColumn64.Name = "gridColumn64";
            this.gridColumn64.Visible = true;
            this.gridColumn64.VisibleIndex = 5;
            this.gridColumn64.Width = 92;
            // 
            // gridColumn65
            // 
            this.gridColumn65.Caption = "Premium Amt";
            this.gridColumn65.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Custom;
            this.gridColumn65.FieldName = "PremiumTotalPr";
            this.gridColumn65.Name = "gridColumn65";
            this.gridColumn65.Visible = true;
            this.gridColumn65.VisibleIndex = 6;
            this.gridColumn65.Width = 92;
            // 
            // gridColumn66
            // 
            this.gridColumn66.Caption = "Outstanding Amt";
            this.gridColumn66.DisplayFormat.FormatString = "C2";
            this.gridColumn66.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            this.gridColumn66.FieldName = "Balance";
            this.gridColumn66.Name = "gridColumn66";
            this.gridColumn66.Visible = true;
            this.gridColumn66.VisibleIndex = 7;
            this.gridColumn66.Width = 92;
            // 
            // gridColumn67
            // 
            this.gridColumn67.Caption = "Weekly Rent SI";
            this.gridColumn67.DisplayFormat.FormatString = "C2";
            this.gridColumn67.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            this.gridColumn67.FieldName = "currentWeeklyRent";
            this.gridColumn67.Name = "gridColumn67";
            this.gridColumn67.Visible = true;
            this.gridColumn67.VisibleIndex = 8;
            this.gridColumn67.Width = 92;
            // 
            // gridColumn68
            // 
            this.gridColumn68.Caption = "Contents SI";
            this.gridColumn68.DisplayFormat.FormatString = "C2";
            this.gridColumn68.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            this.gridColumn68.FieldName = "ContentsSumInsured";
            this.gridColumn68.Name = "gridColumn68";
            this.gridColumn68.Visible = true;
            this.gridColumn68.VisibleIndex = 9;
            this.gridColumn68.Width = 92;
            // 
            // gridColumn69
            // 
            this.gridColumn69.Caption = "Payee";
            this.gridColumn69.FieldName = "Payee";
            this.gridColumn69.Name = "gridColumn69";
            this.gridColumn69.Visible = true;
            this.gridColumn69.VisibleIndex = 10;
            this.gridColumn69.Width = 92;
            // 
            // gridColumn70
            // 
            this.gridColumn70.Caption = "Reason";
            this.gridColumn70.FieldName = "Reason";
            this.gridColumn70.Name = "gridColumn70";
            this.gridColumn70.Visible = true;
            this.gridColumn70.VisibleIndex = 11;
            this.gridColumn70.Width = 108;
            // 
            // HistoryGrid
            // 
            gridLevelNode1.LevelTemplate = this.HistoryDetailGridView;
            gridLevelNode1.RelationName = "ChildTransactions";
            this.HistoryGrid.LevelTree.Nodes.AddRange(new DevExpress.XtraGrid.GridLevelNode[] {
            gridLevelNode1});
            this.HistoryGrid.Location = new System.Drawing.Point(24, 91);
            this.HistoryGrid.MainView = this.HistoryGridView;
            this.HistoryGrid.MenuManager = this.barManager1;
            this.HistoryGrid.Name = "HistoryGrid";
            this.HistoryGrid.ShowOnlyPredefinedDetails = true;
            this.HistoryGrid.Size = new System.Drawing.Size(1193, 577);
            this.HistoryGrid.TabIndex = 200;
            this.HistoryGrid.ViewCollection.AddRange(new DevExpress.XtraGrid.Views.Base.BaseView[] {
            this.HistoryGridView,
            this.HistoryDetailGridView});
            // 
            // HistoryGridView
            // 
            this.HistoryGridView.Appearance.HeaderPanel.Options.UseTextOptions = true;
            this.HistoryGridView.Appearance.HeaderPanel.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.HistoryGridView.ChildGridLevelName = "ChildTransactions";
            this.HistoryGridView.Columns.AddRange(new DevExpress.XtraGrid.Columns.GridColumn[] {
            this.gridColumn47,
            this.gridColumn48,
            this.gridColumn49,
            this.gridColumn50,
            this.gridColumn51,
            this.gridColumn52,
            this.gridColumn53,
            this.gridColumn54,
            this.gridColumn55,
            this.gridColumn56,
            this.gridColumn57,
            this.gridColumn58});
            gridFormatRule1.ApplyToRow = true;
            gridFormatRule1.Column = this.gridColumn47;
            gridFormatRule1.Name = "Format0";
            formatConditionRuleValue1.Appearance.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(192)))), ((int)(((byte)(192)))));
            formatConditionRuleValue1.Appearance.Options.UseBackColor = true;
            formatConditionRuleValue1.Value1 = 604020008;
            gridFormatRule1.Rule = formatConditionRuleValue1;
            gridFormatRule1.StopIfTrue = true;
            this.HistoryGridView.FormatRules.Add(gridFormatRule1);
            this.HistoryGridView.GridControl = this.HistoryGrid;
            this.HistoryGridView.Name = "HistoryGridView";
            this.HistoryGridView.OptionsBehavior.Editable = false;
            this.HistoryGridView.OptionsDetail.AllowZoomDetail = false;
            this.HistoryGridView.OptionsDetail.ShowDetailTabs = false;
            this.HistoryGridView.OptionsView.ShowChildrenInGroupPanel = true;
            this.HistoryGridView.SortInfo.AddRange(new DevExpress.XtraGrid.Columns.GridColumnSortInfo[] {
            new DevExpress.XtraGrid.Columns.GridColumnSortInfo(this.gridColumn47, DevExpress.Data.ColumnSortOrder.Ascending)});
            // 
            // gridColumn47
            // 
            this.gridColumn47.AppearanceHeader.Options.UseTextOptions = true;
            this.gridColumn47.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far;
            this.gridColumn47.Caption = "Transaction Number";
            this.gridColumn47.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            this.gridColumn47.FieldName = "TransactionNumber";
            this.gridColumn47.Name = "gridColumn47";
            this.gridColumn47.Visible = true;
            this.gridColumn47.VisibleIndex = 0;
            this.gridColumn47.Width = 127;
            // 
            // gridColumn48
            // 
            this.gridColumn48.Caption = "Code";
            this.gridColumn48.FieldName = "Code";
            this.gridColumn48.Name = "gridColumn48";
            this.gridColumn48.OptionsColumn.FixedWidth = true;
            this.gridColumn48.Visible = true;
            this.gridColumn48.VisibleIndex = 1;
            this.gridColumn48.Width = 45;
            // 
            // gridColumn49
            // 
            this.gridColumn49.AppearanceCell.Options.UseTextOptions = true;
            this.gridColumn49.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far;
            this.gridColumn49.AppearanceHeader.Options.UseTextOptions = true;
            this.gridColumn49.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far;
            this.gridColumn49.Caption = "Effective On";
            this.gridColumn49.DisplayFormat.FormatString = "d";
            this.gridColumn49.DisplayFormat.FormatType = DevExpress.Utils.FormatType.DateTime;
            this.gridColumn49.FieldName = "EffectiveDate";
            this.gridColumn49.Name = "gridColumn49";
            this.gridColumn49.Visible = true;
            this.gridColumn49.VisibleIndex = 3;
            this.gridColumn49.Width = 93;
            // 
            // gridColumn50
            // 
            this.gridColumn50.AppearanceCell.Options.UseTextOptions = true;
            this.gridColumn50.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far;
            this.gridColumn50.AppearanceHeader.Options.UseTextOptions = true;
            this.gridColumn50.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far;
            this.gridColumn50.Caption = "Renewal On";
            this.gridColumn50.DisplayFormat.FormatString = "d";
            this.gridColumn50.DisplayFormat.FormatType = DevExpress.Utils.FormatType.DateTime;
            this.gridColumn50.FieldName = "RenewalDate";
            this.gridColumn50.Name = "gridColumn50";
            this.gridColumn50.Visible = true;
            this.gridColumn50.VisibleIndex = 4;
            this.gridColumn50.Width = 93;
            // 
            // gridColumn51
            // 
            this.gridColumn51.AppearanceCell.Options.UseTextOptions = true;
            this.gridColumn51.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far;
            this.gridColumn51.AppearanceHeader.Options.UseTextOptions = true;
            this.gridColumn51.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far;
            this.gridColumn51.Caption = "Created On";
            this.gridColumn51.DisplayFormat.FormatString = "d";
            this.gridColumn51.DisplayFormat.FormatType = DevExpress.Utils.FormatType.DateTime;
            this.gridColumn51.FieldName = "CreationDate";
            this.gridColumn51.Name = "gridColumn51";
            this.gridColumn51.Visible = true;
            this.gridColumn51.VisibleIndex = 5;
            this.gridColumn51.Width = 93;
            // 
            // gridColumn52
            // 
            this.gridColumn52.AppearanceHeader.Options.UseTextOptions = true;
            this.gridColumn52.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far;
            this.gridColumn52.Caption = "Premium Amt";
            this.gridColumn52.DisplayFormat.FormatString = "C2";
            this.gridColumn52.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            this.gridColumn52.FieldName = "PremiumTotal";
            this.gridColumn52.Name = "gridColumn52";
            this.gridColumn52.Visible = true;
            this.gridColumn52.VisibleIndex = 6;
            this.gridColumn52.Width = 93;
            // 
            // gridColumn53
            // 
            this.gridColumn53.AppearanceHeader.Options.UseTextOptions = true;
            this.gridColumn53.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far;
            this.gridColumn53.Caption = "Outstanding Amt";
            this.gridColumn53.DisplayFormat.FormatString = "C2";
            this.gridColumn53.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            this.gridColumn53.FieldName = "Balance";
            this.gridColumn53.Name = "gridColumn53";
            this.gridColumn53.Visible = true;
            this.gridColumn53.VisibleIndex = 7;
            this.gridColumn53.Width = 93;
            // 
            // gridColumn54
            // 
            this.gridColumn54.Caption = "Status";
            this.gridColumn54.FieldName = "StatusOfTransaction";
            this.gridColumn54.Name = "gridColumn54";
            this.gridColumn54.Visible = true;
            this.gridColumn54.VisibleIndex = 2;
            this.gridColumn54.Width = 93;
            // 
            // gridColumn55
            // 
            this.gridColumn55.AppearanceHeader.Options.UseTextOptions = true;
            this.gridColumn55.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far;
            this.gridColumn55.Caption = "Weekly Rent SI";
            this.gridColumn55.DisplayFormat.FormatString = "C2";
            this.gridColumn55.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            this.gridColumn55.FieldName = "currentWeeklyRent";
            this.gridColumn55.Name = "gridColumn55";
            this.gridColumn55.Visible = true;
            this.gridColumn55.VisibleIndex = 8;
            this.gridColumn55.Width = 93;
            // 
            // gridColumn56
            // 
            this.gridColumn56.AppearanceHeader.Options.UseTextOptions = true;
            this.gridColumn56.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far;
            this.gridColumn56.Caption = "Contents SI";
            this.gridColumn56.DisplayFormat.FormatString = "C2";
            this.gridColumn56.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            this.gridColumn56.FieldName = "ContentsSumInsured";
            this.gridColumn56.Name = "gridColumn56";
            this.gridColumn56.Visible = true;
            this.gridColumn56.VisibleIndex = 9;
            this.gridColumn56.Width = 93;
            // 
            // gridColumn57
            // 
            this.gridColumn57.Caption = "Payee";
            this.gridColumn57.FieldName = "Payee";
            this.gridColumn57.Name = "gridColumn57";
            this.gridColumn57.Visible = true;
            this.gridColumn57.VisibleIndex = 10;
            this.gridColumn57.Width = 93;
            // 
            // gridColumn58
            // 
            this.gridColumn58.Caption = "Reason";
            this.gridColumn58.Name = "gridColumn58";
            this.gridColumn58.Visible = true;
            this.gridColumn58.VisibleIndex = 11;
            this.gridColumn58.Width = 129;
            // 
            // barManager1
            // 
            this.barManager1.Bars.AddRange(new DevExpress.XtraBars.Bar[] {
            this.PolicyMenuBar});
            this.barManager1.DockControls.Add(this.barDockControlTop);
            this.barManager1.DockControls.Add(this.barDockControlBottom);
            this.barManager1.DockControls.Add(this.barDockControlLeft);
            this.barManager1.DockControls.Add(this.barDockControlRight);
            this.barManager1.Form = this;
            this.barManager1.Items.AddRange(new DevExpress.XtraBars.BarItem[] {
            this.RefreshButton,
            this.ExtendCreditTermsButton,
            this.RecalcBalanceButton,
            this.CreateClaimButton,
            this.SendPolicyDocumentButton,
            this.PremiumButton,
            this.NotesAndLogsButton,
            this.EndorsementButton,
            this.EditPaymentButton,
            this.EditButton,
            this.SaveButton,
            this.CancelButton});
            this.barManager1.MainMenu = this.PolicyMenuBar;
            this.barManager1.MaxItemId = 12;
            // 
            // PolicyMenuBar
            // 
            this.PolicyMenuBar.BarName = "Main menu";
            this.PolicyMenuBar.DockCol = 0;
            this.PolicyMenuBar.DockRow = 0;
            this.PolicyMenuBar.DockStyle = DevExpress.XtraBars.BarDockStyle.Top;
            this.PolicyMenuBar.LinksPersistInfo.AddRange(new DevExpress.XtraBars.LinkPersistInfo[] {
            new DevExpress.XtraBars.LinkPersistInfo(this.RefreshButton),
            new DevExpress.XtraBars.LinkPersistInfo(this.ExtendCreditTermsButton),
            new DevExpress.XtraBars.LinkPersistInfo(this.RecalcBalanceButton),
            new DevExpress.XtraBars.LinkPersistInfo(this.CreateClaimButton),
            new DevExpress.XtraBars.LinkPersistInfo(this.SendPolicyDocumentButton),
            new DevExpress.XtraBars.LinkPersistInfo(this.PremiumButton),
            new DevExpress.XtraBars.LinkPersistInfo(this.NotesAndLogsButton),
            new DevExpress.XtraBars.LinkPersistInfo(this.EndorsementButton, true),
            new DevExpress.XtraBars.LinkPersistInfo(this.EditPaymentButton),
            new DevExpress.XtraBars.LinkPersistInfo(this.EditButton),
            new DevExpress.XtraBars.LinkPersistInfo(this.SaveButton),
            new DevExpress.XtraBars.LinkPersistInfo(this.CancelButton)});
            this.PolicyMenuBar.OptionsBar.AllowQuickCustomization = false;
            this.PolicyMenuBar.OptionsBar.DisableCustomization = true;
            this.PolicyMenuBar.OptionsBar.DrawDragBorder = false;
            this.PolicyMenuBar.OptionsBar.MultiLine = true;
            this.PolicyMenuBar.OptionsBar.UseWholeRow = true;
            this.PolicyMenuBar.Text = "Main menu";
            // 
            // RefreshButton
            // 
            this.RefreshButton.Caption = "&Refresh";
            this.RefreshButton.Id = 0;
            this.RefreshButton.ImageOptions.SvgImage = ((DevExpress.Utils.Svg.SvgImage)(resources.GetObject("RefreshButton.ImageOptions.SvgImage")));
            this.RefreshButton.Name = "RefreshButton";
            this.RefreshButton.PaintStyle = DevExpress.XtraBars.BarItemPaintStyle.CaptionGlyph;
            // 
            // ExtendCreditTermsButton
            // 
            this.ExtendCreditTermsButton.Caption = "Extend Credit Terms";
            this.ExtendCreditTermsButton.Id = 1;
            this.ExtendCreditTermsButton.ImageOptions.SvgImage = ((DevExpress.Utils.Svg.SvgImage)(resources.GetObject("ExtendCreditTermsButton.ImageOptions.SvgImage")));
            this.ExtendCreditTermsButton.Name = "ExtendCreditTermsButton";
            this.ExtendCreditTermsButton.PaintStyle = DevExpress.XtraBars.BarItemPaintStyle.CaptionGlyph;
            // 
            // RecalcBalanceButton
            // 
            this.RecalcBalanceButton.Caption = "Recalc &Balance";
            this.RecalcBalanceButton.Id = 2;
            this.RecalcBalanceButton.ImageOptions.SvgImage = ((DevExpress.Utils.Svg.SvgImage)(resources.GetObject("RecalcBalanceButton.ImageOptions.SvgImage")));
            this.RecalcBalanceButton.Name = "RecalcBalanceButton";
            this.RecalcBalanceButton.PaintStyle = DevExpress.XtraBars.BarItemPaintStyle.CaptionGlyph;
            // 
            // CreateClaimButton
            // 
            this.CreateClaimButton.Caption = "Create C&laim";
            this.CreateClaimButton.Id = 3;
            this.CreateClaimButton.ImageOptions.SvgImage = ((DevExpress.Utils.Svg.SvgImage)(resources.GetObject("CreateClaimButton.ImageOptions.SvgImage")));
            this.CreateClaimButton.Name = "CreateClaimButton";
            this.CreateClaimButton.PaintStyle = DevExpress.XtraBars.BarItemPaintStyle.CaptionGlyph;
            // 
            // SendPolicyDocumentButton
            // 
            this.SendPolicyDocumentButton.Caption = "Send Policy Document";
            this.SendPolicyDocumentButton.Id = 4;
            this.SendPolicyDocumentButton.ImageOptions.SvgImage = ((DevExpress.Utils.Svg.SvgImage)(resources.GetObject("SendPolicyDocumentButton.ImageOptions.SvgImage")));
            this.SendPolicyDocumentButton.Name = "SendPolicyDocumentButton";
            this.SendPolicyDocumentButton.PaintStyle = DevExpress.XtraBars.BarItemPaintStyle.CaptionGlyph;
            // 
            // PremiumButton
            // 
            this.PremiumButton.Caption = "&Premium";
            this.PremiumButton.Id = 5;
            this.PremiumButton.Name = "PremiumButton";
            // 
            // NotesAndLogsButton
            // 
            this.NotesAndLogsButton.Caption = "N&otes/Logs";
            this.NotesAndLogsButton.Id = 6;
            this.NotesAndLogsButton.Name = "NotesAndLogsButton";
            // 
            // EndorsementButton
            // 
            this.EndorsementButton.Alignment = DevExpress.XtraBars.BarItemLinkAlignment.Right;
            this.EndorsementButton.Caption = "E&ndorsement";
            this.EndorsementButton.Id = 7;
            this.EndorsementButton.ImageOptions.SvgImage = ((DevExpress.Utils.Svg.SvgImage)(resources.GetObject("EndorsementButton.ImageOptions.SvgImage")));
            this.EndorsementButton.Name = "EndorsementButton";
            this.EndorsementButton.PaintStyle = DevExpress.XtraBars.BarItemPaintStyle.CaptionGlyph;
            // 
            // EditPaymentButton
            // 
            this.EditPaymentButton.Alignment = DevExpress.XtraBars.BarItemLinkAlignment.Right;
            this.EditPaymentButton.Caption = "Edit Payment";
            this.EditPaymentButton.Id = 8;
            this.EditPaymentButton.ImageOptions.SvgImage = ((DevExpress.Utils.Svg.SvgImage)(resources.GetObject("EditPaymentButton.ImageOptions.SvgImage")));
            this.EditPaymentButton.Name = "EditPaymentButton";
            this.EditPaymentButton.PaintStyle = DevExpress.XtraBars.BarItemPaintStyle.CaptionGlyph;
            // 
            // EditButton
            // 
            this.EditButton.Alignment = DevExpress.XtraBars.BarItemLinkAlignment.Right;
            this.EditButton.Caption = "&Edit";
            this.EditButton.Id = 9;
            this.EditButton.ImageOptions.SvgImage = ((DevExpress.Utils.Svg.SvgImage)(resources.GetObject("EditButton.ImageOptions.SvgImage")));
            this.EditButton.Name = "EditButton";
            this.EditButton.PaintStyle = DevExpress.XtraBars.BarItemPaintStyle.CaptionGlyph;
            // 
            // SaveButton
            // 
            this.SaveButton.Alignment = DevExpress.XtraBars.BarItemLinkAlignment.Right;
            this.SaveButton.Caption = "&Save";
            this.SaveButton.Id = 10;
            this.SaveButton.ImageOptions.SvgImage = ((DevExpress.Utils.Svg.SvgImage)(resources.GetObject("SaveButton.ImageOptions.SvgImage")));
            this.SaveButton.Name = "SaveButton";
            this.SaveButton.PaintStyle = DevExpress.XtraBars.BarItemPaintStyle.CaptionGlyph;
            // 
            // CancelButton
            // 
            this.CancelButton.Alignment = DevExpress.XtraBars.BarItemLinkAlignment.Right;
            this.CancelButton.Caption = "&Cancel";
            this.CancelButton.Id = 11;
            this.CancelButton.ImageOptions.SvgImage = ((DevExpress.Utils.Svg.SvgImage)(resources.GetObject("CancelButton.ImageOptions.SvgImage")));
            this.CancelButton.Name = "CancelButton";
            this.CancelButton.PaintStyle = DevExpress.XtraBars.BarItemPaintStyle.CaptionGlyph;
            // 
            // barDockControlTop
            // 
            this.barDockControlTop.CausesValidation = false;
            this.barDockControlTop.Dock = System.Windows.Forms.DockStyle.Top;
            this.barDockControlTop.Location = new System.Drawing.Point(0, 0);
            this.barDockControlTop.Manager = this.barManager1;
            this.barDockControlTop.Size = new System.Drawing.Size(1241, 24);
            // 
            // barDockControlBottom
            // 
            this.barDockControlBottom.CausesValidation = false;
            this.barDockControlBottom.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.barDockControlBottom.Location = new System.Drawing.Point(0, 716);
            this.barDockControlBottom.Manager = this.barManager1;
            this.barDockControlBottom.Size = new System.Drawing.Size(1241, 0);
            // 
            // barDockControlLeft
            // 
            this.barDockControlLeft.CausesValidation = false;
            this.barDockControlLeft.Dock = System.Windows.Forms.DockStyle.Left;
            this.barDockControlLeft.Location = new System.Drawing.Point(0, 24);
            this.barDockControlLeft.Manager = this.barManager1;
            this.barDockControlLeft.Size = new System.Drawing.Size(0, 692);
            // 
            // barDockControlRight
            // 
            this.barDockControlRight.CausesValidation = false;
            this.barDockControlRight.Dock = System.Windows.Forms.DockStyle.Right;
            this.barDockControlRight.Location = new System.Drawing.Point(1241, 24);
            this.barDockControlRight.Manager = this.barManager1;
            this.barDockControlRight.Size = new System.Drawing.Size(0, 692);
            // 
            // MainLayoutControl
            // 
            this.MainLayoutControl.Controls.Add(this.Disclosure5AwaitingApprovalField);
            this.MainLayoutControl.Controls.Add(this.Disclosure5NoField);
            this.MainLayoutControl.Controls.Add(this.PDSandFSGGrid);
            this.MainLayoutControl.Controls.Add(this.HistoryGrid);
            this.MainLayoutControl.Controls.Add(this.LogGrid);
            this.MainLayoutControl.Controls.Add(this.LedgerGrid);
            this.MainLayoutControl.Controls.Add(this.PremiumDetailsGrid);
            this.MainLayoutControl.Controls.Add(this.PremiumBaseRatesGrid);
            this.MainLayoutControl.Controls.Add(this.DisclosureClaimsGrid);
            this.MainLayoutControl.Controls.Add(this.EasyDocsGrid);
            this.MainLayoutControl.Controls.Add(this.NotesField);
            this.MainLayoutControl.Controls.Add(this.PremiumTypeField);
            this.MainLayoutControl.Controls.Add(this.PolicyNumberField);
            this.MainLayoutControl.Controls.Add(this.REACodeField);
            this.MainLayoutControl.Controls.Add(this.NewBusinessArrangedByCodeField);
            this.MainLayoutControl.Controls.Add(this.DwellingTypeField);
            this.MainLayoutControl.Controls.Add(this.ConstructionTypeField);
            this.MainLayoutControl.Controls.Add(this.YearBuiltField);
            this.MainLayoutControl.Controls.Add(this.LandlordCodeField);
            this.MainLayoutControl.Controls.Add(this.LandlordNameField);
            this.MainLayoutControl.Controls.Add(this.BranchField);
            this.MainLayoutControl.Controls.Add(this.BushfireRatingField);
            this.MainLayoutControl.Controls.Add(this.ContentsSumInsuredField);
            this.MainLayoutControl.Controls.Add(this.BuildingSumInsuredAmountField);
            this.MainLayoutControl.Controls.Add(this.WeeksInArrearsField);
            this.MainLayoutControl.Controls.Add(this.FloodRiskRatingField);
            this.MainLayoutControl.Controls.Add(this.WeeklyRentSumInsuredAmountField);
            this.MainLayoutControl.Controls.Add(this.UnderwriterExcessAmountField);
            this.MainLayoutControl.Controls.Add(this.BuildingSumInsuredExcessAmountField);
            this.MainLayoutControl.Controls.Add(this.LandSizeField);
            this.MainLayoutControl.Controls.Add(this.TransactionNumberField);
            this.MainLayoutControl.Controls.Add(this.CreatedOnField);
            this.MainLayoutControl.Controls.Add(this.InceptionFromField);
            this.MainLayoutControl.Controls.Add(this.BalanceOutstandingAmountField);
            this.MainLayoutControl.Controls.Add(this.EffectiveFromField);
            this.MainLayoutControl.Controls.Add(this.BilledFromField);
            this.MainLayoutControl.Controls.Add(this.ClosingOnField);
            this.MainLayoutControl.Controls.Add(this.COISentOnField);
            this.MainLayoutControl.Controls.Add(this.BilledToField);
            this.MainLayoutControl.Controls.Add(this.RenewalOnField);
            this.MainLayoutControl.Controls.Add(this.PolicySentOnField);
            this.MainLayoutControl.Controls.Add(this.InvoicePrintedOnField);
            this.MainLayoutControl.Controls.Add(this.ContactCodeHeaderField);
            this.MainLayoutControl.Controls.Add(this.ContactNameHeaderField);
            this.MainLayoutControl.Controls.Add(this.LandlordCodeHeaderField);
            this.MainLayoutControl.Controls.Add(this.LandlordNameHeaderField);
            this.MainLayoutControl.Controls.Add(this.TransactionNumberHeaderField);
            this.MainLayoutControl.Controls.Add(this.TransactionStatusHeaderField);
            this.MainLayoutControl.Controls.Add(this.TransactionRenewalHeaderField);
            this.MainLayoutControl.Controls.Add(this.PremiumAmountHeaderField);
            this.MainLayoutControl.Controls.Add(this.OutstandingAmountHeaderField);
            this.MainLayoutControl.Controls.Add(this.ClaimCountHeaderField);
            this.MainLayoutControl.Controls.Add(this.OnHoldField);
            this.MainLayoutControl.Controls.Add(this.RewiredReplumbedField);
            this.MainLayoutControl.Controls.Add(this.FreeStandingField);
            this.MainLayoutControl.Controls.Add(this.CommonGroundsField);
            this.MainLayoutControl.Controls.Add(this.BusinessOrTradeField);
            this.MainLayoutControl.Controls.Add(this.AcceptedBusinessUseField);
            this.MainLayoutControl.Controls.Add(this.IsJobReducedDuePandemicField);
            this.MainLayoutControl.Controls.Add(this.IsArrearsField);
            this.MainLayoutControl.Controls.Add(this.TenantedField);
            this.MainLayoutControl.Controls.Add(this.StaffDiscountField);
            this.MainLayoutControl.Controls.Add(this.IsREADiscountField);
            this.MainLayoutControl.Controls.Add(this.IsMultiplePropertyDiscountField);
            this.MainLayoutControl.Controls.Add(this.IsFivePercentDiscountField);
            this.MainLayoutControl.Controls.Add(this.Is15For12BonusOfferField);
            this.MainLayoutControl.Controls.Add(this.PremiumPaidByField);
            this.MainLayoutControl.Controls.Add(this.PreferredMailingAddressTypeField);
            this.MainLayoutControl.Controls.Add(this.InsurerCodeField);
            this.MainLayoutControl.Controls.Add(this.OnlineQuoteNumberField);
            this.MainLayoutControl.Controls.Add(this.QuoteNumberField);
            this.MainLayoutControl.Controls.Add(this.AskedForQuoteWhoField);
            this.MainLayoutControl.Controls.Add(this.HowAskedForQuoteField);
            this.MainLayoutControl.Controls.Add(this.QuotedByField);
            this.MainLayoutControl.Controls.Add(this.QuotedOnField);
            this.MainLayoutControl.Controls.Add(this.ConvertedToCoverByField);
            this.MainLayoutControl.Controls.Add(this.ConvertedToCoverOnField);
            this.MainLayoutControl.Controls.Add(this.QuoteExpiresOnField);
            this.MainLayoutControl.Controls.Add(this.OnlineQuoteEmailAddressField);
            this.MainLayoutControl.Controls.Add(this.WhoPlacedCoverField);
            this.MainLayoutControl.Controls.Add(this.HowPlacedCoverField);
            this.MainLayoutControl.Controls.Add(this.HeardAboutTSIviaField);
            this.MainLayoutControl.Controls.Add(this.MortgageeField);
            this.MainLayoutControl.Controls.Add(this.PaymentMethodField);
            this.MainLayoutControl.Controls.Add(this.PaymentReferenceField);
            this.MainLayoutControl.Controls.Add(this.BPayReceiptField);
            this.MainLayoutControl.Controls.Add(this.OnlineReferenceField);
            this.MainLayoutControl.Controls.Add(this.CCTransactionIDField);
            this.MainLayoutControl.Controls.Add(this.VoucherTypeField);
            this.MainLayoutControl.Controls.Add(this.PromoCodeField);
            this.MainLayoutControl.Controls.Add(this.LinkCreditTransactionNumberField);
            this.MainLayoutControl.Controls.Add(this.LinkParentTransactionNumberField);
            this.MainLayoutControl.Controls.Add(this.LinkReferToTransactionNumberField);
            this.MainLayoutControl.Controls.Add(this.LinkWelcomeLetterTransactionNumberField);
            this.MainLayoutControl.Controls.Add(this.RecreatedField);
            this.MainLayoutControl.Controls.Add(this.PaymentReceivedField);
            this.MainLayoutControl.Controls.Add(this.ApprovedByField);
            this.MainLayoutControl.Controls.Add(this.ApprovedOnField);
            this.MainLayoutControl.Controls.Add(this.RenewingTransactionsField);
            this.MainLayoutControl.Controls.Add(this.RenewalReasonField);
            this.MainLayoutControl.Controls.Add(this.RenewalPremiumPaidByField);
            this.MainLayoutControl.Controls.Add(this.textEdit51);
            this.MainLayoutControl.Controls.Add(this.RenewalDiscountAmountField);
            this.MainLayoutControl.Controls.Add(this.RenewedTransactionField);
            this.MainLayoutControl.Controls.Add(this.RenewedByTransactionField);
            this.MainLayoutControl.Controls.Add(this.LastRenewalErrorField);
            this.MainLayoutControl.Controls.Add(this.RenewalBatchField);
            this.MainLayoutControl.Controls.Add(this.LedgerHoldCoverField);
            this.MainLayoutControl.Controls.Add(this.LedgerPremiumPaidField);
            this.MainLayoutControl.Controls.Add(this.LedgerCurrentOnField);
            this.MainLayoutControl.Controls.Add(this.LedgerDistributeFundsField);
            this.MainLayoutControl.Controls.Add(this.LedgerInvoiceOverdueOnField);
            this.MainLayoutControl.Controls.Add(this.LedgerExtendedOverdueOnField);
            this.MainLayoutControl.Controls.Add(this.LedgerOverdueNoticeSentField);
            this.MainLayoutControl.Controls.Add(this.LedgerExtendedLapsingOnField);
            this.MainLayoutControl.Controls.Add(this.LedgerFinalReminderSentField);
            this.MainLayoutControl.Controls.Add(this.LedgerCancelledLapsedOnField);
            this.MainLayoutControl.Controls.Add(this.NotesLastModifiedOnField);
            this.MainLayoutControl.Controls.Add(this.NotesLastModifiedByField);
            this.MainLayoutControl.Controls.Add(this.NotesCreatedOnField);
            this.MainLayoutControl.Controls.Add(this.NotesCreatedByField);
            this.MainLayoutControl.Controls.Add(this.SchemePaidField);
            this.MainLayoutControl.Controls.Add(this.EasyDocsEmailAddressField);
            this.MainLayoutControl.Controls.Add(this.EasyDocsPrimaryAssociateField);
            this.MainLayoutControl.Controls.Add(this.EasyDocsMobilePhoneField);
            this.MainLayoutControl.Controls.Add(this.LLReceivesElectronicCommunicationsField);
            this.MainLayoutControl.Controls.Add(this.REAReceivesElectronicCommunicationsField);
            this.MainLayoutControl.Controls.Add(this.DenialReasonField);
            this.MainLayoutControl.Controls.Add(this.SpecialTermsField);
            this.MainLayoutControl.Controls.Add(this.DisclosureItem1Field);
            this.MainLayoutControl.Controls.Add(this.DisclosureItem2Field);
            this.MainLayoutControl.Controls.Add(this.CircumstancesField);
            this.MainLayoutControl.Controls.Add(this.RestrictionsField);
            this.MainLayoutControl.Controls.Add(this.Disclosure3Field);
            this.MainLayoutControl.Controls.Add(this.Disclosure4Field);
            this.MainLayoutControl.Controls.Add(this.RenewalMultiPropertyDiscountField);
            this.MainLayoutControl.Controls.Add(this.RenewalsOnHoldField);
            this.MainLayoutControl.Controls.Add(this.DoNotRenewField);
            this.MainLayoutControl.Controls.Add(this.RenewalImposedExcessField);
            this.MainLayoutControl.Controls.Add(this.RealEstateDiscountField);
            this.MainLayoutControl.Controls.Add(this.RenewalStaffDiscountField);
            this.MainLayoutControl.Controls.Add(this.ApplyToRenewingPremiumField);
            this.MainLayoutControl.Controls.Add(this.Disclosure5YesField);
            this.MainLayoutControl.Controls.Add(this.ClassOfRiskField);
            this.MainLayoutControl.Controls.Add(this.SelfManagedField);
            this.MainLayoutControl.Controls.Add(this.IsFurnishedField);
            this.MainLayoutControl.Controls.Add(this.IsCCTVField);
            this.MainLayoutControl.Controls.Add(this.IsElectronicAccessField);
            this.MainLayoutControl.Controls.Add(this.HolidayLetField);
            this.MainLayoutControl.Dock = System.Windows.Forms.DockStyle.Fill;
            this.MainLayoutControl.Location = new System.Drawing.Point(0, 24);
            this.MainLayoutControl.Name = "MainLayoutControl";
            this.MainLayoutControl.OptionsCustomizationForm.DesignTimeCustomizationFormPositionAndSize = new System.Drawing.Rectangle(3188, 451, 650, 400);
            this.MainLayoutControl.Root = this.Root;
            this.MainLayoutControl.Size = new System.Drawing.Size(1241, 692);
            this.MainLayoutControl.TabIndex = 0;
            this.MainLayoutControl.Text = "layoutControl1";
            // 
            // Disclosure5AwaitingApprovalField
            // 
            this.Disclosure5AwaitingApprovalField.Location = new System.Drawing.Point(978, 409);
            this.Disclosure5AwaitingApprovalField.MenuManager = this.barManager1;
            this.Disclosure5AwaitingApprovalField.Name = "Disclosure5AwaitingApprovalField";
            this.Disclosure5AwaitingApprovalField.Properties.Caption = "Awaiting Approval";
            this.Disclosure5AwaitingApprovalField.Properties.CheckBoxOptions.Style = DevExpress.XtraEditors.Controls.CheckBoxStyle.Radio;
            this.Disclosure5AwaitingApprovalField.Size = new System.Drawing.Size(149, 19);
            this.Disclosure5AwaitingApprovalField.StyleController = this.MainLayoutControl;
            this.Disclosure5AwaitingApprovalField.TabIndex = 203;
            // 
            // Disclosure5NoField
            // 
            this.Disclosure5NoField.Location = new System.Drawing.Point(922, 409);
            this.Disclosure5NoField.MenuManager = this.barManager1;
            this.Disclosure5NoField.Name = "Disclosure5NoField";
            this.Disclosure5NoField.Properties.Caption = "No";
            this.Disclosure5NoField.Properties.CheckBoxOptions.Style = DevExpress.XtraEditors.Controls.CheckBoxStyle.Radio;
            this.Disclosure5NoField.Size = new System.Drawing.Size(52, 19);
            this.Disclosure5NoField.StyleController = this.MainLayoutControl;
            this.Disclosure5NoField.TabIndex = 202;
            // 
            // PDSandFSGGrid
            // 
            this.PDSandFSGGrid.Location = new System.Drawing.Point(36, 121);
            this.PDSandFSGGrid.MainView = this.PDSandFSGGridView;
            this.PDSandFSGGrid.MenuManager = this.barManager1;
            this.PDSandFSGGrid.Name = "PDSandFSGGrid";
            this.PDSandFSGGrid.Size = new System.Drawing.Size(484, 255);
            this.PDSandFSGGrid.TabIndex = 201;
            this.PDSandFSGGrid.ViewCollection.AddRange(new DevExpress.XtraGrid.Views.Base.BaseView[] {
            this.PDSandFSGGridView});
            // 
            // PDSandFSGGridView
            // 
            this.PDSandFSGGridView.Appearance.HeaderPanel.Options.UseTextOptions = true;
            this.PDSandFSGGridView.Appearance.HeaderPanel.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.PDSandFSGGridView.Columns.AddRange(new DevExpress.XtraGrid.Columns.GridColumn[] {
            this.gridColumn71,
            this.gridColumn72,
            this.gridColumn73});
            this.PDSandFSGGridView.GridControl = this.PDSandFSGGrid;
            this.PDSandFSGGridView.Name = "PDSandFSGGridView";
            this.PDSandFSGGridView.OptionsView.ShowGroupPanel = false;
            // 
            // gridColumn71
            // 
            this.gridColumn71.Caption = "Document";
            this.gridColumn71.Name = "gridColumn71";
            this.gridColumn71.Visible = true;
            this.gridColumn71.VisibleIndex = 0;
            // 
            // gridColumn72
            // 
            this.gridColumn72.Caption = "Version";
            this.gridColumn72.Name = "gridColumn72";
            this.gridColumn72.Visible = true;
            this.gridColumn72.VisibleIndex = 1;
            // 
            // gridColumn73
            // 
            this.gridColumn73.Caption = "Date Sent";
            this.gridColumn73.Name = "gridColumn73";
            this.gridColumn73.Visible = true;
            this.gridColumn73.VisibleIndex = 2;
            // 
            // LogGrid
            // 
            this.LogGrid.Location = new System.Drawing.Point(36, 121);
            this.LogGrid.MainView = this.LogGridView;
            this.LogGrid.MenuManager = this.barManager1;
            this.LogGrid.Name = "LogGrid";
            this.LogGrid.RepositoryItems.AddRange(new DevExpress.XtraEditors.Repository.RepositoryItem[] {
            this.repositoryItemRichTextEdit1});
            this.LogGrid.ShowOnlyPredefinedDetails = true;
            this.LogGrid.Size = new System.Drawing.Size(1169, 321);
            this.LogGrid.TabIndex = 199;
            this.LogGrid.ViewCollection.AddRange(new DevExpress.XtraGrid.Views.Base.BaseView[] {
            this.LogGridView});
            // 
            // LogGridView
            // 
            this.LogGridView.Appearance.HeaderPanel.Options.UseTextOptions = true;
            this.LogGridView.Appearance.HeaderPanel.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.LogGridView.Columns.AddRange(new DevExpress.XtraGrid.Columns.GridColumn[] {
            this.gridColumn28,
            this.gridColumn29,
            this.gridColumn30,
            this.gridColumn31});
            this.LogGridView.GridControl = this.LogGrid;
            this.LogGridView.Name = "LogGridView";
            this.LogGridView.OptionsBehavior.AllowPixelScrolling = DevExpress.Utils.DefaultBoolean.True;
            this.LogGridView.OptionsBehavior.ReadOnly = true;
            this.LogGridView.OptionsDetail.EnableMasterViewMode = false;
            this.LogGridView.OptionsView.RowAutoHeight = true;
            this.LogGridView.SortInfo.AddRange(new DevExpress.XtraGrid.Columns.GridColumnSortInfo[] {
            new DevExpress.XtraGrid.Columns.GridColumnSortInfo(this.gridColumn28, DevExpress.Data.ColumnSortOrder.Descending)});
            // 
            // gridColumn28
            // 
            this.gridColumn28.AppearanceCell.Options.UseTextOptions = true;
            this.gridColumn28.AppearanceCell.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Top;
            this.gridColumn28.Caption = "Date";
            this.gridColumn28.DisplayFormat.FormatString = "g";
            this.gridColumn28.DisplayFormat.FormatType = DevExpress.Utils.FormatType.DateTime;
            this.gridColumn28.FieldName = "Date";
            this.gridColumn28.GroupFormat.FormatString = "d";
            this.gridColumn28.GroupFormat.FormatType = DevExpress.Utils.FormatType.DateTime;
            this.gridColumn28.Name = "gridColumn28";
            this.gridColumn28.OptionsColumn.AllowEdit = false;
            this.gridColumn28.OptionsColumn.AllowFocus = false;
            this.gridColumn28.Visible = true;
            this.gridColumn28.VisibleIndex = 0;
            this.gridColumn28.Width = 115;
            // 
            // gridColumn29
            // 
            this.gridColumn29.AppearanceCell.Options.UseTextOptions = true;
            this.gridColumn29.AppearanceCell.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Top;
            this.gridColumn29.Caption = "User";
            this.gridColumn29.FieldName = "UserFullName";
            this.gridColumn29.Name = "gridColumn29";
            this.gridColumn29.OptionsColumn.AllowEdit = false;
            this.gridColumn29.OptionsColumn.AllowFocus = false;
            this.gridColumn29.Visible = true;
            this.gridColumn29.VisibleIndex = 1;
            this.gridColumn29.Width = 128;
            // 
            // gridColumn30
            // 
            this.gridColumn30.Caption = "Note";
            this.gridColumn30.ColumnEdit = this.repositoryItemRichTextEdit1;
            this.gridColumn30.FieldName = "Note";
            this.gridColumn30.Name = "gridColumn30";
            this.gridColumn30.OptionsColumn.AllowGroup = DevExpress.Utils.DefaultBoolean.True;
            this.gridColumn30.OptionsColumn.AllowSort = DevExpress.Utils.DefaultBoolean.True;
            this.gridColumn30.Visible = true;
            this.gridColumn30.VisibleIndex = 2;
            this.gridColumn30.Width = 725;
            // 
            // repositoryItemRichTextEdit1
            // 
            this.repositoryItemRichTextEdit1.Appearance.Options.UseFont = true;
            this.repositoryItemRichTextEdit1.DocumentFormat = DevExpress.XtraRichEdit.DocumentFormat.Html;
            this.repositoryItemRichTextEdit1.MaxHeight = 500;
            this.repositoryItemRichTextEdit1.Name = "repositoryItemRichTextEdit1";
            this.repositoryItemRichTextEdit1.OptionsHorizontalScrollbar.Visibility = DevExpress.XtraRichEdit.RichEditScrollbarVisibility.Auto;
            this.repositoryItemRichTextEdit1.ReadOnly = true;
            this.repositoryItemRichTextEdit1.ShowCaretInReadOnly = false;
            // 
            // gridColumn31
            // 
            this.gridColumn31.AppearanceCell.Options.UseTextOptions = true;
            this.gridColumn31.AppearanceCell.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Top;
            this.gridColumn31.Caption = "Auto";
            this.gridColumn31.FieldName = "IsAudit";
            this.gridColumn31.Name = "gridColumn31";
            this.gridColumn31.OptionsColumn.AllowEdit = false;
            this.gridColumn31.OptionsColumn.AllowFocus = false;
            this.gridColumn31.Visible = true;
            this.gridColumn31.VisibleIndex = 3;
            this.gridColumn31.Width = 86;
            // 
            // LedgerGrid
            // 
            this.LedgerGrid.Location = new System.Drawing.Point(24, 139);
            this.LedgerGrid.MainView = this.LedgerGridView;
            this.LedgerGrid.MenuManager = this.barManager1;
            this.LedgerGrid.Name = "LedgerGrid";
            this.LedgerGrid.Size = new System.Drawing.Size(1193, 529);
            this.LedgerGrid.TabIndex = 198;
            this.LedgerGrid.ViewCollection.AddRange(new DevExpress.XtraGrid.Views.Base.BaseView[] {
            this.LedgerGridView});
            // 
            // LedgerGridView
            // 
            this.LedgerGridView.Appearance.HeaderPanel.Options.UseTextOptions = true;
            this.LedgerGridView.Appearance.HeaderPanel.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.LedgerGridView.Columns.AddRange(new DevExpress.XtraGrid.Columns.GridColumn[] {
            this.gridColumn27,
            this.gridColumn13,
            this.gridColumn14,
            this.gridColumn15,
            this.gridColumn16,
            this.gridColumn17,
            this.gridColumn18,
            this.gridColumn19,
            this.gridColumn20,
            this.gridColumn21,
            this.gridColumn22,
            this.gridColumn23,
            this.gridColumn24,
            this.gridColumn25,
            this.gridColumn26});
            this.LedgerGridView.GridControl = this.LedgerGrid;
            this.LedgerGridView.Name = "LedgerGridView";
            // 
            // gridColumn27
            // 
            this.gridColumn27.Caption = "Batch";
            this.gridColumn27.Name = "gridColumn27";
            this.gridColumn27.Visible = true;
            this.gridColumn27.VisibleIndex = 0;
            // 
            // gridColumn13
            // 
            this.gridColumn13.Caption = "Trans Date";
            this.gridColumn13.Name = "gridColumn13";
            this.gridColumn13.Visible = true;
            this.gridColumn13.VisibleIndex = 1;
            // 
            // gridColumn14
            // 
            this.gridColumn14.Caption = "Banked";
            this.gridColumn14.Name = "gridColumn14";
            this.gridColumn14.Visible = true;
            this.gridColumn14.VisibleIndex = 2;
            // 
            // gridColumn15
            // 
            this.gridColumn15.Caption = "Closed";
            this.gridColumn15.Name = "gridColumn15";
            this.gridColumn15.Visible = true;
            this.gridColumn15.VisibleIndex = 3;
            // 
            // gridColumn16
            // 
            this.gridColumn16.Caption = "Office";
            this.gridColumn16.Name = "gridColumn16";
            this.gridColumn16.Visible = true;
            this.gridColumn16.VisibleIndex = 4;
            // 
            // gridColumn17
            // 
            this.gridColumn17.Caption = "Type";
            this.gridColumn17.Name = "gridColumn17";
            this.gridColumn17.Visible = true;
            this.gridColumn17.VisibleIndex = 5;
            // 
            // gridColumn18
            // 
            this.gridColumn18.Caption = "Method";
            this.gridColumn18.Name = "gridColumn18";
            this.gridColumn18.Visible = true;
            this.gridColumn18.VisibleIndex = 6;
            // 
            // gridColumn19
            // 
            this.gridColumn19.Caption = "Debit";
            this.gridColumn19.Name = "gridColumn19";
            this.gridColumn19.Visible = true;
            this.gridColumn19.VisibleIndex = 7;
            // 
            // gridColumn20
            // 
            this.gridColumn20.Caption = "Credit";
            this.gridColumn20.Name = "gridColumn20";
            this.gridColumn20.Visible = true;
            this.gridColumn20.VisibleIndex = 8;
            // 
            // gridColumn21
            // 
            this.gridColumn21.Caption = "Description";
            this.gridColumn21.Name = "gridColumn21";
            this.gridColumn21.Visible = true;
            this.gridColumn21.VisibleIndex = 9;
            // 
            // gridColumn22
            // 
            this.gridColumn22.Caption = "Payment Ref";
            this.gridColumn22.Name = "gridColumn22";
            this.gridColumn22.Visible = true;
            this.gridColumn22.VisibleIndex = 10;
            // 
            // gridColumn23
            // 
            this.gridColumn23.Caption = "Payee";
            this.gridColumn23.Name = "gridColumn23";
            this.gridColumn23.Visible = true;
            this.gridColumn23.VisibleIndex = 11;
            // 
            // gridColumn24
            // 
            this.gridColumn24.Caption = "Bank";
            this.gridColumn24.Name = "gridColumn24";
            this.gridColumn24.Visible = true;
            this.gridColumn24.VisibleIndex = 12;
            // 
            // gridColumn25
            // 
            this.gridColumn25.Caption = "Branch";
            this.gridColumn25.Name = "gridColumn25";
            this.gridColumn25.Visible = true;
            this.gridColumn25.VisibleIndex = 13;
            // 
            // gridColumn26
            // 
            this.gridColumn26.Caption = "Comment";
            this.gridColumn26.Name = "gridColumn26";
            this.gridColumn26.Visible = true;
            this.gridColumn26.VisibleIndex = 14;
            // 
            // PremiumDetailsGrid
            // 
            this.PremiumDetailsGrid.Location = new System.Drawing.Point(36, 167);
            this.PremiumDetailsGrid.MainView = this.PremiumDetailsGridView;
            this.PremiumDetailsGrid.MenuManager = this.barManager1;
            this.PremiumDetailsGrid.Name = "PremiumDetailsGrid";
            this.PremiumDetailsGrid.Size = new System.Drawing.Size(628, 165);
            this.PremiumDetailsGrid.TabIndex = 197;
            this.PremiumDetailsGrid.ViewCollection.AddRange(new DevExpress.XtraGrid.Views.Base.BaseView[] {
            this.PremiumDetailsGridView});
            // 
            // PremiumDetailsGridView
            // 
            this.PremiumDetailsGridView.Appearance.HeaderPanel.Options.UseTextOptions = true;
            this.PremiumDetailsGridView.Appearance.HeaderPanel.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.PremiumDetailsGridView.Columns.AddRange(new DevExpress.XtraGrid.Columns.GridColumn[] {
            this.gridColumn7,
            this.gridColumn8,
            this.gridColumn9,
            this.gridColumn10,
            this.gridColumn11,
            this.gridColumn12});
            this.PremiumDetailsGridView.GridControl = this.PremiumDetailsGrid;
            this.PremiumDetailsGridView.Name = "PremiumDetailsGridView";
            this.PremiumDetailsGridView.OptionsBehavior.Editable = false;
            this.PremiumDetailsGridView.OptionsBehavior.ReadOnly = true;
            this.PremiumDetailsGridView.OptionsView.ShowGroupPanel = false;
            this.PremiumDetailsGridView.VertScrollVisibility = DevExpress.XtraGrid.Views.Base.ScrollVisibility.Never;
            // 
            // gridColumn7
            // 
            this.gridColumn7.Caption = "Description";
            this.gridColumn7.FieldName = "TypeDesc";
            this.gridColumn7.Name = "gridColumn7";
            this.gridColumn7.Visible = true;
            this.gridColumn7.VisibleIndex = 0;
            this.gridColumn7.Width = 211;
            // 
            // gridColumn8
            // 
            this.gridColumn8.Caption = "Base";
            this.gridColumn8.DisplayFormat.FormatString = "C2";
            this.gridColumn8.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            this.gridColumn8.FieldName = "Base";
            this.gridColumn8.Name = "gridColumn8";
            this.gridColumn8.Visible = true;
            this.gridColumn8.VisibleIndex = 1;
            this.gridColumn8.Width = 110;
            // 
            // gridColumn9
            // 
            this.gridColumn9.Caption = "Emergency Services Levy";
            this.gridColumn9.DisplayFormat.FormatString = "C2";
            this.gridColumn9.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            this.gridColumn9.FieldName = "EmergencyServicesLevy";
            this.gridColumn9.Name = "gridColumn9";
            this.gridColumn9.Visible = true;
            this.gridColumn9.VisibleIndex = 2;
            this.gridColumn9.Width = 110;
            // 
            // gridColumn10
            // 
            this.gridColumn10.Caption = "Stamp Duty";
            this.gridColumn10.DisplayFormat.FormatString = "C2";
            this.gridColumn10.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            this.gridColumn10.FieldName = "StampDuty";
            this.gridColumn10.Name = "gridColumn10";
            this.gridColumn10.Visible = true;
            this.gridColumn10.VisibleIndex = 3;
            this.gridColumn10.Width = 110;
            // 
            // gridColumn11
            // 
            this.gridColumn11.Caption = "Premium";
            this.gridColumn11.DisplayFormat.FormatString = "C2";
            this.gridColumn11.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            this.gridColumn11.FieldName = "Premium";
            this.gridColumn11.Name = "gridColumn11";
            this.gridColumn11.Visible = true;
            this.gridColumn11.VisibleIndex = 4;
            this.gridColumn11.Width = 110;
            // 
            // gridColumn12
            // 
            this.gridColumn12.Caption = "Gross Commission";
            this.gridColumn12.DisplayFormat.FormatString = "C2";
            this.gridColumn12.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            this.gridColumn12.FieldName = "GrossCommission";
            this.gridColumn12.Name = "gridColumn12";
            this.gridColumn12.Visible = true;
            this.gridColumn12.VisibleIndex = 5;
            this.gridColumn12.Width = 112;
            // 
            // PremiumBaseRatesGrid
            // 
            this.PremiumBaseRatesGrid.Location = new System.Drawing.Point(36, 121);
            this.PremiumBaseRatesGrid.MainView = this.PremiumBaseRatesGridView;
            this.PremiumBaseRatesGrid.MenuManager = this.barManager1;
            this.PremiumBaseRatesGrid.Name = "PremiumBaseRatesGrid";
            this.PremiumBaseRatesGrid.Size = new System.Drawing.Size(628, 42);
            this.PremiumBaseRatesGrid.TabIndex = 196;
            this.PremiumBaseRatesGrid.ViewCollection.AddRange(new DevExpress.XtraGrid.Views.Base.BaseView[] {
            this.PremiumBaseRatesGridView});
            // 
            // PremiumBaseRatesGridView
            // 
            this.PremiumBaseRatesGridView.Appearance.HeaderPanel.Options.UseTextOptions = true;
            this.PremiumBaseRatesGridView.Appearance.HeaderPanel.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.PremiumBaseRatesGridView.Columns.AddRange(new DevExpress.XtraGrid.Columns.GridColumn[] {
            this.gridColumn6,
            this.gridColumn1,
            this.gridColumn2,
            this.gridColumn3,
            this.gridColumn4,
            this.gridColumn5});
            this.PremiumBaseRatesGridView.GridControl = this.PremiumBaseRatesGrid;
            this.PremiumBaseRatesGridView.Name = "PremiumBaseRatesGridView";
            this.PremiumBaseRatesGridView.OptionsBehavior.Editable = false;
            this.PremiumBaseRatesGridView.OptionsBehavior.ReadOnly = true;
            this.PremiumBaseRatesGridView.OptionsView.ShowGroupPanel = false;
            this.PremiumBaseRatesGridView.VertScrollVisibility = DevExpress.XtraGrid.Views.Base.ScrollVisibility.Never;
            // 
            // gridColumn6
            // 
            this.gridColumn6.Caption = "Description";
            this.gridColumn6.FieldName = "TypeDesc";
            this.gridColumn6.Name = "gridColumn6";
            this.gridColumn6.Visible = true;
            this.gridColumn6.VisibleIndex = 0;
            this.gridColumn6.Width = 211;
            // 
            // gridColumn1
            // 
            this.gridColumn1.Caption = "GST";
            this.gridColumn1.DisplayFormat.FormatString = "P2";
            this.gridColumn1.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            this.gridColumn1.FieldName = "GSTRate";
            this.gridColumn1.Name = "gridColumn1";
            this.gridColumn1.Visible = true;
            this.gridColumn1.VisibleIndex = 1;
            this.gridColumn1.Width = 109;
            // 
            // gridColumn2
            // 
            this.gridColumn2.Caption = "Emergency Services Levy";
            this.gridColumn2.DisplayFormat.FormatString = "P2";
            this.gridColumn2.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            this.gridColumn2.FieldName = "EmergencyServicesLevyRate";
            this.gridColumn2.Name = "gridColumn2";
            this.gridColumn2.Visible = true;
            this.gridColumn2.VisibleIndex = 2;
            this.gridColumn2.Width = 109;
            // 
            // gridColumn3
            // 
            this.gridColumn3.Caption = "Stamp Duty";
            this.gridColumn3.DisplayFormat.FormatString = "P2";
            this.gridColumn3.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            this.gridColumn3.FieldName = "StampDutyRate";
            this.gridColumn3.Name = "gridColumn3";
            this.gridColumn3.Visible = true;
            this.gridColumn3.VisibleIndex = 3;
            this.gridColumn3.Width = 109;
            // 
            // gridColumn4
            // 
            this.gridColumn4.Name = "gridColumn4";
            this.gridColumn4.Visible = true;
            this.gridColumn4.VisibleIndex = 4;
            this.gridColumn4.Width = 109;
            // 
            // gridColumn5
            // 
            this.gridColumn5.Caption = "Gross Commission";
            this.gridColumn5.DisplayFormat.FormatString = "P2";
            this.gridColumn5.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            this.gridColumn5.FieldName = "GrossCommissionRate";
            this.gridColumn5.Name = "gridColumn5";
            this.gridColumn5.Visible = true;
            this.gridColumn5.VisibleIndex = 5;
            this.gridColumn5.Width = 116;
            // 
            // DisclosureClaimsGrid
            // 
            this.DisclosureClaimsGrid.Location = new System.Drawing.Point(36, 212);
            this.DisclosureClaimsGrid.MainView = this.DisclosureClaimsGridView;
            this.DisclosureClaimsGrid.MenuManager = this.barManager1;
            this.DisclosureClaimsGrid.Name = "DisclosureClaimsGrid";
            this.DisclosureClaimsGrid.Size = new System.Drawing.Size(1169, 85);
            this.DisclosureClaimsGrid.TabIndex = 195;
            this.DisclosureClaimsGrid.ViewCollection.AddRange(new DevExpress.XtraGrid.Views.Base.BaseView[] {
            this.DisclosureClaimsGridView});
            // 
            // DisclosureClaimsGridView
            // 
            this.DisclosureClaimsGridView.Appearance.HeaderPanel.Options.UseTextOptions = true;
            this.DisclosureClaimsGridView.Appearance.HeaderPanel.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.DisclosureClaimsGridView.Columns.AddRange(new DevExpress.XtraGrid.Columns.GridColumn[] {
            this.gridColumn32,
            this.gridColumn33,
            this.gridColumn34,
            this.gridColumn35,
            this.gridColumn36,
            this.gridColumn37,
            this.gridColumn38});
            this.DisclosureClaimsGridView.GridControl = this.DisclosureClaimsGrid;
            this.DisclosureClaimsGridView.Name = "DisclosureClaimsGridView";
            // 
            // gridColumn32
            // 
            this.gridColumn32.Caption = "Class";
            this.gridColumn32.Name = "gridColumn32";
            this.gridColumn32.Visible = true;
            this.gridColumn32.VisibleIndex = 0;
            // 
            // gridColumn33
            // 
            this.gridColumn33.Caption = "Type Of Claim";
            this.gridColumn33.Name = "gridColumn33";
            this.gridColumn33.Visible = true;
            this.gridColumn33.VisibleIndex = 1;
            // 
            // gridColumn34
            // 
            this.gridColumn34.Caption = "Date Of Loss";
            this.gridColumn34.Name = "gridColumn34";
            this.gridColumn34.Visible = true;
            this.gridColumn34.VisibleIndex = 2;
            // 
            // gridColumn35
            // 
            this.gridColumn35.Caption = "Loss Amount";
            this.gridColumn35.Name = "gridColumn35";
            this.gridColumn35.Visible = true;
            this.gridColumn35.VisibleIndex = 3;
            // 
            // gridColumn36
            // 
            this.gridColumn36.Caption = "Insurer";
            this.gridColumn36.Name = "gridColumn36";
            this.gridColumn36.Visible = true;
            this.gridColumn36.VisibleIndex = 4;
            // 
            // gridColumn37
            // 
            this.gridColumn37.Caption = "Risk Management";
            this.gridColumn37.Name = "gridColumn37";
            this.gridColumn37.Visible = true;
            this.gridColumn37.VisibleIndex = 5;
            // 
            // gridColumn38
            // 
            this.gridColumn38.Caption = "Address";
            this.gridColumn38.Name = "gridColumn38";
            this.gridColumn38.Visible = true;
            this.gridColumn38.VisibleIndex = 6;
            // 
            // EasyDocsGrid
            // 
            this.EasyDocsGrid.Location = new System.Drawing.Point(24, 169);
            this.EasyDocsGrid.MainView = this.EasyDocsGridView;
            this.EasyDocsGrid.MenuManager = this.barManager1;
            this.EasyDocsGrid.Name = "EasyDocsGrid";
            this.EasyDocsGrid.Size = new System.Drawing.Size(1193, 499);
            this.EasyDocsGrid.TabIndex = 194;
            this.EasyDocsGrid.ViewCollection.AddRange(new DevExpress.XtraGrid.Views.Base.BaseView[] {
            this.EasyDocsGridView});
            // 
            // EasyDocsGridView
            // 
            this.EasyDocsGridView.Appearance.HeaderPanel.Options.UseTextOptions = true;
            this.EasyDocsGridView.Appearance.HeaderPanel.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.EasyDocsGridView.Columns.AddRange(new DevExpress.XtraGrid.Columns.GridColumn[] {
            this.gridColumn39,
            this.gridColumn40,
            this.gridColumn41,
            this.gridColumn42,
            this.gridColumn43,
            this.gridColumn44,
            this.gridColumn45,
            this.gridColumn46});
            this.EasyDocsGridView.GridControl = this.EasyDocsGrid;
            this.EasyDocsGridView.Name = "EasyDocsGridView";
            // 
            // gridColumn39
            // 
            this.gridColumn39.Caption = "Document";
            this.gridColumn39.Name = "gridColumn39";
            this.gridColumn39.Visible = true;
            this.gridColumn39.VisibleIndex = 0;
            this.gridColumn39.Width = 142;
            // 
            // gridColumn40
            // 
            this.gridColumn40.Caption = "Delivery By";
            this.gridColumn40.Name = "gridColumn40";
            this.gridColumn40.Visible = true;
            this.gridColumn40.VisibleIndex = 1;
            this.gridColumn40.Width = 77;
            // 
            // gridColumn41
            // 
            this.gridColumn41.Caption = "Requested By";
            this.gridColumn41.Name = "gridColumn41";
            this.gridColumn41.Visible = true;
            this.gridColumn41.VisibleIndex = 2;
            this.gridColumn41.Width = 119;
            // 
            // gridColumn42
            // 
            this.gridColumn42.Caption = "Requested Date";
            this.gridColumn42.Name = "gridColumn42";
            this.gridColumn42.Visible = true;
            this.gridColumn42.VisibleIndex = 3;
            this.gridColumn42.Width = 158;
            // 
            // gridColumn43
            // 
            this.gridColumn43.Caption = "Addressee";
            this.gridColumn43.Name = "gridColumn43";
            this.gridColumn43.Visible = true;
            this.gridColumn43.VisibleIndex = 4;
            this.gridColumn43.Width = 158;
            // 
            // gridColumn44
            // 
            this.gridColumn44.Caption = "Email Address";
            this.gridColumn44.Name = "gridColumn44";
            this.gridColumn44.Visible = true;
            this.gridColumn44.VisibleIndex = 5;
            this.gridColumn44.Width = 158;
            // 
            // gridColumn45
            // 
            this.gridColumn45.Caption = "Application Reference";
            this.gridColumn45.Name = "gridColumn45";
            this.gridColumn45.Visible = true;
            this.gridColumn45.VisibleIndex = 6;
            this.gridColumn45.Width = 158;
            // 
            // gridColumn46
            // 
            this.gridColumn46.Caption = "Task Reference";
            this.gridColumn46.Name = "gridColumn46";
            this.gridColumn46.Visible = true;
            this.gridColumn46.VisibleIndex = 7;
            this.gridColumn46.Width = 168;
            // 
            // NotesField
            // 
            this.NotesField.ActiveViewType = DevExpress.XtraRichEdit.RichEditViewType.Simple;
            this.NotesField.LayoutUnit = DevExpress.XtraRichEdit.DocumentLayoutUnit.Pixel;
            this.NotesField.Location = new System.Drawing.Point(48, 476);
            this.NotesField.MenuManager = this.barManager1;
            this.NotesField.Name = "NotesField";
            this.NotesField.ReadOnly = true;
            this.NotesField.Size = new System.Drawing.Size(844, 168);
            this.NotesField.TabIndex = 192;
            this.NotesField.Unit = DevExpress.Office.DocumentUnit.Millimeter;
            // 
            // PremiumTypeField
            // 
            this.PremiumTypeField.Location = new System.Drawing.Point(169, 146);
            this.PremiumTypeField.Name = "PremiumTypeField";
            this.PremiumTypeField.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.PremiumTypeField.Properties.NullText = "";
            this.PremiumTypeField.Size = new System.Drawing.Size(151, 20);
            this.PremiumTypeField.StyleController = this.MainLayoutControl;
            this.PremiumTypeField.TabIndex = 78;
            // 
            // PolicyNumberField
            // 
            this.PolicyNumberField.Location = new System.Drawing.Point(169, 220);
            this.PolicyNumberField.Name = "PolicyNumberField";
            this.PolicyNumberField.Size = new System.Drawing.Size(151, 20);
            this.PolicyNumberField.StyleController = this.MainLayoutControl;
            this.PolicyNumberField.TabIndex = 77;
            // 
            // REACodeField
            // 
            this.REACodeField.Location = new System.Drawing.Point(169, 245);
            this.REACodeField.Name = "REACodeField";
            editorButtonImageOptions1.SvgImageSize = new System.Drawing.Size(16, 16);
            this.REACodeField.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Glyph, "", -1, true, true, false, editorButtonImageOptions1, new DevExpress.Utils.KeyShortcut(System.Windows.Forms.Keys.None), serializableAppearanceObject1, serializableAppearanceObject2, serializableAppearanceObject3, serializableAppearanceObject4, "", null, null, DevExpress.Utils.ToolTipAnchor.Default)});
            this.REACodeField.Size = new System.Drawing.Size(151, 20);
            this.REACodeField.StyleController = this.MainLayoutControl;
            this.REACodeField.TabIndex = 171;
            // 
            // NewBusinessArrangedByCodeField
            // 
            this.NewBusinessArrangedByCodeField.Location = new System.Drawing.Point(169, 272);
            this.NewBusinessArrangedByCodeField.Name = "NewBusinessArrangedByCodeField";
            editorButtonImageOptions2.SvgImage = ((DevExpress.Utils.Svg.SvgImage)(resources.GetObject("editorButtonImageOptions2.SvgImage")));
            editorButtonImageOptions2.SvgImageSize = new System.Drawing.Size(16, 16);
            this.NewBusinessArrangedByCodeField.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Glyph, "", -1, true, true, false, editorButtonImageOptions2, new DevExpress.Utils.KeyShortcut(System.Windows.Forms.Keys.None), serializableAppearanceObject5, serializableAppearanceObject6, serializableAppearanceObject7, serializableAppearanceObject8, "", null, null, DevExpress.Utils.ToolTipAnchor.Default)});
            this.NewBusinessArrangedByCodeField.Size = new System.Drawing.Size(151, 22);
            this.NewBusinessArrangedByCodeField.StyleController = this.MainLayoutControl;
            this.NewBusinessArrangedByCodeField.TabIndex = 172;
            // 
            // DwellingTypeField
            // 
            this.DwellingTypeField.Location = new System.Drawing.Point(169, 299);
            this.DwellingTypeField.Name = "DwellingTypeField";
            this.DwellingTypeField.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.DwellingTypeField.Properties.NullText = "";
            this.DwellingTypeField.Size = new System.Drawing.Size(151, 20);
            this.DwellingTypeField.StyleController = this.MainLayoutControl;
            this.DwellingTypeField.TabIndex = 174;
            // 
            // ConstructionTypeField
            // 
            this.ConstructionTypeField.Location = new System.Drawing.Point(169, 323);
            this.ConstructionTypeField.Name = "ConstructionTypeField";
            this.ConstructionTypeField.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.ConstructionTypeField.Properties.NullText = "";
            this.ConstructionTypeField.Size = new System.Drawing.Size(151, 20);
            this.ConstructionTypeField.StyleController = this.MainLayoutControl;
            this.ConstructionTypeField.TabIndex = 175;
            // 
            // YearBuiltField
            // 
            this.YearBuiltField.Location = new System.Drawing.Point(169, 347);
            this.YearBuiltField.Name = "YearBuiltField";
            this.YearBuiltField.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.YearBuiltField.Properties.DisplayFormat.FormatType = DevExpress.Utils.FormatType.DateTime;
            this.YearBuiltField.Properties.EditFormat.FormatType = DevExpress.Utils.FormatType.DateTime;
            this.YearBuiltField.Properties.NullText = "";
            this.YearBuiltField.Size = new System.Drawing.Size(151, 20);
            this.YearBuiltField.StyleController = this.MainLayoutControl;
            this.YearBuiltField.TabIndex = 176;
            // 
            // LandlordCodeField
            // 
            this.LandlordCodeField.Location = new System.Drawing.Point(481, 121);
            this.LandlordCodeField.Name = "LandlordCodeField";
            this.LandlordCodeField.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Glyph),
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Glyph)});
            this.LandlordCodeField.Size = new System.Drawing.Size(220, 20);
            this.LandlordCodeField.StyleController = this.MainLayoutControl;
            this.LandlordCodeField.TabIndex = 83;
            // 
            // LandlordNameField
            // 
            this.LandlordNameField.Location = new System.Drawing.Point(481, 145);
            this.LandlordNameField.Name = "LandlordNameField";
            this.LandlordNameField.Properties.ReadOnly = true;
            this.LandlordNameField.Size = new System.Drawing.Size(220, 20);
            this.LandlordNameField.StyleController = this.MainLayoutControl;
            this.LandlordNameField.TabIndex = 84;
            // 
            // BranchField
            // 
            this.BranchField.Location = new System.Drawing.Point(481, 196);
            this.BranchField.Name = "BranchField";
            this.BranchField.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.BranchField.Properties.NullText = "";
            this.BranchField.Size = new System.Drawing.Size(130, 20);
            this.BranchField.StyleController = this.MainLayoutControl;
            this.BranchField.TabIndex = 85;
            // 
            // BushfireRatingField
            // 
            this.BushfireRatingField.Location = new System.Drawing.Point(481, 244);
            this.BushfireRatingField.Name = "BushfireRatingField";
            this.BushfireRatingField.Properties.ReadOnly = true;
            this.BushfireRatingField.Size = new System.Drawing.Size(130, 20);
            this.BushfireRatingField.StyleController = this.MainLayoutControl;
            this.BushfireRatingField.TabIndex = 87;
            // 
            // ContentsSumInsuredField
            // 
            this.ContentsSumInsuredField.Location = new System.Drawing.Point(481, 292);
            this.ContentsSumInsuredField.Name = "ContentsSumInsuredField";
            this.ContentsSumInsuredField.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.ContentsSumInsuredField.Properties.NullText = "";
            this.ContentsSumInsuredField.Size = new System.Drawing.Size(130, 20);
            this.ContentsSumInsuredField.StyleController = this.MainLayoutControl;
            this.ContentsSumInsuredField.TabIndex = 88;
            // 
            // BuildingSumInsuredAmountField
            // 
            this.BuildingSumInsuredAmountField.Location = new System.Drawing.Point(481, 316);
            this.BuildingSumInsuredAmountField.Name = "BuildingSumInsuredAmountField";
            this.BuildingSumInsuredAmountField.Size = new System.Drawing.Size(130, 20);
            this.BuildingSumInsuredAmountField.StyleController = this.MainLayoutControl;
            this.BuildingSumInsuredAmountField.TabIndex = 89;
            // 
            // WeeksInArrearsField
            // 
            this.WeeksInArrearsField.Location = new System.Drawing.Point(481, 487);
            this.WeeksInArrearsField.Name = "WeeksInArrearsField";
            this.WeeksInArrearsField.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.WeeksInArrearsField.Properties.NullText = "";
            this.WeeksInArrearsField.Size = new System.Drawing.Size(89, 20);
            this.WeeksInArrearsField.StyleController = this.MainLayoutControl;
            this.WeeksInArrearsField.TabIndex = 94;
            // 
            // FloodRiskRatingField
            // 
            this.FloodRiskRatingField.Location = new System.Drawing.Point(481, 220);
            this.FloodRiskRatingField.Name = "FloodRiskRatingField";
            this.FloodRiskRatingField.Properties.ReadOnly = true;
            this.FloodRiskRatingField.Size = new System.Drawing.Size(130, 20);
            this.FloodRiskRatingField.StyleController = this.MainLayoutControl;
            this.FloodRiskRatingField.TabIndex = 173;
            // 
            // WeeklyRentSumInsuredAmountField
            // 
            this.WeeklyRentSumInsuredAmountField.Location = new System.Drawing.Point(481, 268);
            this.WeeklyRentSumInsuredAmountField.Name = "WeeklyRentSumInsuredAmountField";
            this.WeeklyRentSumInsuredAmountField.Size = new System.Drawing.Size(130, 20);
            this.WeeklyRentSumInsuredAmountField.StyleController = this.MainLayoutControl;
            this.WeeklyRentSumInsuredAmountField.TabIndex = 184;
            // 
            // UnderwriterExcessAmountField
            // 
            this.UnderwriterExcessAmountField.Location = new System.Drawing.Point(481, 364);
            this.UnderwriterExcessAmountField.Name = "UnderwriterExcessAmountField";
            this.UnderwriterExcessAmountField.Size = new System.Drawing.Size(130, 20);
            this.UnderwriterExcessAmountField.StyleController = this.MainLayoutControl;
            this.UnderwriterExcessAmountField.TabIndex = 185;
            // 
            // BuildingSumInsuredExcessAmountField
            // 
            this.BuildingSumInsuredExcessAmountField.Location = new System.Drawing.Point(481, 340);
            this.BuildingSumInsuredExcessAmountField.Name = "BuildingSumInsuredExcessAmountField";
            this.BuildingSumInsuredExcessAmountField.Size = new System.Drawing.Size(130, 20);
            this.BuildingSumInsuredExcessAmountField.StyleController = this.MainLayoutControl;
            this.BuildingSumInsuredExcessAmountField.TabIndex = 186;
            // 
            // LandSizeField
            // 
            this.LandSizeField.Location = new System.Drawing.Point(481, 388);
            this.LandSizeField.Name = "LandSizeField";
            this.LandSizeField.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.LandSizeField.Properties.NullText = "";
            this.LandSizeField.Size = new System.Drawing.Size(130, 20);
            this.LandSizeField.StyleController = this.MainLayoutControl;
            this.LandSizeField.TabIndex = 90;
            // 
            // TransactionNumberField
            // 
            this.TransactionNumberField.Location = new System.Drawing.Point(862, 121);
            this.TransactionNumberField.Name = "TransactionNumberField";
            this.TransactionNumberField.Size = new System.Drawing.Size(96, 20);
            this.TransactionNumberField.StyleController = this.MainLayoutControl;
            this.TransactionNumberField.TabIndex = 103;
            // 
            // CreatedOnField
            // 
            this.CreatedOnField.EditValue = null;
            this.CreatedOnField.Location = new System.Drawing.Point(862, 145);
            this.CreatedOnField.Name = "CreatedOnField";
            this.CreatedOnField.Properties.AllowDropDownWhenReadOnly = DevExpress.Utils.DefaultBoolean.False;
            this.CreatedOnField.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.CreatedOnField.Properties.CalendarTimeProperties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.CreatedOnField.Size = new System.Drawing.Size(96, 20);
            this.CreatedOnField.StyleController = this.MainLayoutControl;
            this.CreatedOnField.TabIndex = 106;
            // 
            // InceptionFromField
            // 
            this.InceptionFromField.EditValue = null;
            this.InceptionFromField.Location = new System.Drawing.Point(862, 169);
            this.InceptionFromField.Name = "InceptionFromField";
            this.InceptionFromField.Properties.AllowDropDownWhenReadOnly = DevExpress.Utils.DefaultBoolean.False;
            this.InceptionFromField.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.InceptionFromField.Properties.CalendarTimeProperties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.InceptionFromField.Size = new System.Drawing.Size(96, 20);
            this.InceptionFromField.StyleController = this.MainLayoutControl;
            this.InceptionFromField.TabIndex = 108;
            // 
            // BalanceOutstandingAmountField
            // 
            this.BalanceOutstandingAmountField.Location = new System.Drawing.Point(1095, 121);
            this.BalanceOutstandingAmountField.Name = "BalanceOutstandingAmountField";
            this.BalanceOutstandingAmountField.Size = new System.Drawing.Size(110, 20);
            this.BalanceOutstandingAmountField.StyleController = this.MainLayoutControl;
            this.BalanceOutstandingAmountField.TabIndex = 104;
            // 
            // EffectiveFromField
            // 
            this.EffectiveFromField.EditValue = null;
            this.EffectiveFromField.Location = new System.Drawing.Point(1095, 145);
            this.EffectiveFromField.Name = "EffectiveFromField";
            this.EffectiveFromField.Properties.AllowDropDownWhenReadOnly = DevExpress.Utils.DefaultBoolean.False;
            this.EffectiveFromField.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.EffectiveFromField.Properties.CalendarTimeProperties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.EffectiveFromField.Size = new System.Drawing.Size(110, 20);
            this.EffectiveFromField.StyleController = this.MainLayoutControl;
            this.EffectiveFromField.TabIndex = 107;
            // 
            // BilledFromField
            // 
            this.BilledFromField.EditValue = null;
            this.BilledFromField.Location = new System.Drawing.Point(1095, 169);
            this.BilledFromField.Name = "BilledFromField";
            this.BilledFromField.Properties.AllowDropDownWhenReadOnly = DevExpress.Utils.DefaultBoolean.False;
            this.BilledFromField.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.BilledFromField.Properties.CalendarTimeProperties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.BilledFromField.Size = new System.Drawing.Size(110, 20);
            this.BilledFromField.StyleController = this.MainLayoutControl;
            this.BilledFromField.TabIndex = 109;
            // 
            // ClosingOnField
            // 
            this.ClosingOnField.EditValue = null;
            this.ClosingOnField.Location = new System.Drawing.Point(862, 193);
            this.ClosingOnField.Name = "ClosingOnField";
            this.ClosingOnField.Properties.AllowDropDownWhenReadOnly = DevExpress.Utils.DefaultBoolean.False;
            this.ClosingOnField.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.ClosingOnField.Properties.CalendarTimeProperties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.ClosingOnField.Size = new System.Drawing.Size(96, 20);
            this.ClosingOnField.StyleController = this.MainLayoutControl;
            this.ClosingOnField.TabIndex = 110;
            // 
            // COISentOnField
            // 
            this.COISentOnField.EditValue = null;
            this.COISentOnField.Location = new System.Drawing.Point(862, 217);
            this.COISentOnField.Name = "COISentOnField";
            this.COISentOnField.Properties.AllowDropDownWhenReadOnly = DevExpress.Utils.DefaultBoolean.False;
            this.COISentOnField.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.COISentOnField.Properties.CalendarTimeProperties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.COISentOnField.Size = new System.Drawing.Size(96, 20);
            this.COISentOnField.StyleController = this.MainLayoutControl;
            this.COISentOnField.TabIndex = 112;
            // 
            // BilledToField
            // 
            this.BilledToField.EditValue = null;
            this.BilledToField.Location = new System.Drawing.Point(1095, 193);
            this.BilledToField.Name = "BilledToField";
            this.BilledToField.Properties.AllowDropDownWhenReadOnly = DevExpress.Utils.DefaultBoolean.False;
            this.BilledToField.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.BilledToField.Properties.CalendarTimeProperties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.BilledToField.Size = new System.Drawing.Size(110, 20);
            this.BilledToField.StyleController = this.MainLayoutControl;
            this.BilledToField.TabIndex = 111;
            // 
            // RenewalOnField
            // 
            this.RenewalOnField.EditValue = null;
            this.RenewalOnField.Location = new System.Drawing.Point(1095, 217);
            this.RenewalOnField.Name = "RenewalOnField";
            this.RenewalOnField.Properties.AllowDropDownWhenReadOnly = DevExpress.Utils.DefaultBoolean.False;
            this.RenewalOnField.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.RenewalOnField.Properties.CalendarTimeProperties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.RenewalOnField.Size = new System.Drawing.Size(110, 20);
            this.RenewalOnField.StyleController = this.MainLayoutControl;
            this.RenewalOnField.TabIndex = 113;
            // 
            // PolicySentOnField
            // 
            this.PolicySentOnField.EditValue = null;
            this.PolicySentOnField.Location = new System.Drawing.Point(862, 241);
            this.PolicySentOnField.Name = "PolicySentOnField";
            this.PolicySentOnField.Properties.AllowDropDownWhenReadOnly = DevExpress.Utils.DefaultBoolean.False;
            this.PolicySentOnField.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.PolicySentOnField.Properties.CalendarTimeProperties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.PolicySentOnField.Size = new System.Drawing.Size(96, 20);
            this.PolicySentOnField.StyleController = this.MainLayoutControl;
            this.PolicySentOnField.TabIndex = 114;
            // 
            // InvoicePrintedOnField
            // 
            this.InvoicePrintedOnField.EditValue = null;
            this.InvoicePrintedOnField.Location = new System.Drawing.Point(1095, 241);
            this.InvoicePrintedOnField.Name = "InvoicePrintedOnField";
            this.InvoicePrintedOnField.Properties.AllowDropDownWhenReadOnly = DevExpress.Utils.DefaultBoolean.False;
            this.InvoicePrintedOnField.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.InvoicePrintedOnField.Properties.CalendarTimeProperties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.InvoicePrintedOnField.Size = new System.Drawing.Size(110, 20);
            this.InvoicePrintedOnField.StyleController = this.MainLayoutControl;
            this.InvoicePrintedOnField.TabIndex = 115;
            // 
            // ContactCodeHeaderField
            // 
            this.ContactCodeHeaderField.Location = new System.Drawing.Point(12, 28);
            this.ContactCodeHeaderField.Margin = new System.Windows.Forms.Padding(3, 3, 3, 0);
            this.ContactCodeHeaderField.Name = "ContactCodeHeaderField";
            this.ContactCodeHeaderField.Properties.ReadOnly = true;
            this.ContactCodeHeaderField.Size = new System.Drawing.Size(113, 20);
            this.ContactCodeHeaderField.StyleController = this.MainLayoutControl;
            this.ContactCodeHeaderField.TabIndex = 4;
            // 
            // ContactNameHeaderField
            // 
            this.ContactNameHeaderField.Location = new System.Drawing.Point(129, 28);
            this.ContactNameHeaderField.Name = "ContactNameHeaderField";
            editorButtonImageOptions3.SvgImage = ((DevExpress.Utils.Svg.SvgImage)(resources.GetObject("editorButtonImageOptions3.SvgImage")));
            editorButtonImageOptions3.SvgImageSize = new System.Drawing.Size(16, 16);
            this.ContactNameHeaderField.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Glyph, "", -1, true, true, false, editorButtonImageOptions3, new DevExpress.Utils.KeyShortcut(System.Windows.Forms.Keys.None), serializableAppearanceObject9, serializableAppearanceObject10, serializableAppearanceObject11, serializableAppearanceObject12, "", null, null, DevExpress.Utils.ToolTipAnchor.Default)});
            this.ContactNameHeaderField.Properties.ReadOnly = true;
            this.ContactNameHeaderField.Size = new System.Drawing.Size(113, 22);
            this.ContactNameHeaderField.StyleController = this.MainLayoutControl;
            this.ContactNameHeaderField.TabIndex = 5;
            // 
            // LandlordCodeHeaderField
            // 
            this.LandlordCodeHeaderField.Location = new System.Drawing.Point(246, 28);
            this.LandlordCodeHeaderField.Name = "LandlordCodeHeaderField";
            this.LandlordCodeHeaderField.Properties.ReadOnly = true;
            this.LandlordCodeHeaderField.Size = new System.Drawing.Size(113, 20);
            this.LandlordCodeHeaderField.StyleController = this.MainLayoutControl;
            this.LandlordCodeHeaderField.TabIndex = 6;
            // 
            // LandlordNameHeaderField
            // 
            this.LandlordNameHeaderField.Location = new System.Drawing.Point(363, 28);
            this.LandlordNameHeaderField.Name = "LandlordNameHeaderField";
            editorButtonImageOptions4.SvgImage = ((DevExpress.Utils.Svg.SvgImage)(resources.GetObject("editorButtonImageOptions4.SvgImage")));
            editorButtonImageOptions4.SvgImageSize = new System.Drawing.Size(16, 16);
            this.LandlordNameHeaderField.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Glyph, "", -1, true, true, false, editorButtonImageOptions4, new DevExpress.Utils.KeyShortcut(System.Windows.Forms.Keys.None), serializableAppearanceObject13, serializableAppearanceObject14, serializableAppearanceObject15, serializableAppearanceObject16, "", null, null, DevExpress.Utils.ToolTipAnchor.Default)});
            this.LandlordNameHeaderField.Properties.ReadOnly = true;
            this.LandlordNameHeaderField.Size = new System.Drawing.Size(113, 22);
            this.LandlordNameHeaderField.StyleController = this.MainLayoutControl;
            this.LandlordNameHeaderField.TabIndex = 7;
            // 
            // TransactionNumberHeaderField
            // 
            this.TransactionNumberHeaderField.Location = new System.Drawing.Point(480, 28);
            this.TransactionNumberHeaderField.Name = "TransactionNumberHeaderField";
            this.TransactionNumberHeaderField.Size = new System.Drawing.Size(113, 20);
            this.TransactionNumberHeaderField.StyleController = this.MainLayoutControl;
            this.TransactionNumberHeaderField.TabIndex = 8;
            // 
            // TransactionStatusHeaderField
            // 
            this.TransactionStatusHeaderField.Location = new System.Drawing.Point(597, 28);
            this.TransactionStatusHeaderField.Name = "TransactionStatusHeaderField";
            this.TransactionStatusHeaderField.Properties.ReadOnly = true;
            this.TransactionStatusHeaderField.Size = new System.Drawing.Size(113, 20);
            this.TransactionStatusHeaderField.StyleController = this.MainLayoutControl;
            this.TransactionStatusHeaderField.TabIndex = 9;
            // 
            // TransactionRenewalHeaderField
            // 
            this.TransactionRenewalHeaderField.Location = new System.Drawing.Point(714, 28);
            this.TransactionRenewalHeaderField.Name = "TransactionRenewalHeaderField";
            this.TransactionRenewalHeaderField.Properties.ReadOnly = true;
            this.TransactionRenewalHeaderField.Size = new System.Drawing.Size(113, 20);
            this.TransactionRenewalHeaderField.StyleController = this.MainLayoutControl;
            this.TransactionRenewalHeaderField.TabIndex = 10;
            // 
            // PremiumAmountHeaderField
            // 
            this.PremiumAmountHeaderField.Location = new System.Drawing.Point(831, 28);
            this.PremiumAmountHeaderField.Name = "PremiumAmountHeaderField";
            this.PremiumAmountHeaderField.Properties.ReadOnly = true;
            this.PremiumAmountHeaderField.Size = new System.Drawing.Size(112, 20);
            this.PremiumAmountHeaderField.StyleController = this.MainLayoutControl;
            this.PremiumAmountHeaderField.TabIndex = 11;
            // 
            // OutstandingAmountHeaderField
            // 
            this.OutstandingAmountHeaderField.Location = new System.Drawing.Point(947, 28);
            this.OutstandingAmountHeaderField.Name = "OutstandingAmountHeaderField";
            this.OutstandingAmountHeaderField.Properties.ReadOnly = true;
            this.OutstandingAmountHeaderField.Size = new System.Drawing.Size(113, 20);
            this.OutstandingAmountHeaderField.StyleController = this.MainLayoutControl;
            this.OutstandingAmountHeaderField.TabIndex = 12;
            // 
            // ClaimCountHeaderField
            // 
            this.ClaimCountHeaderField.Location = new System.Drawing.Point(1064, 28);
            this.ClaimCountHeaderField.Name = "ClaimCountHeaderField";
            editorButtonImageOptions5.SvgImage = ((DevExpress.Utils.Svg.SvgImage)(resources.GetObject("editorButtonImageOptions5.SvgImage")));
            editorButtonImageOptions5.SvgImageSize = new System.Drawing.Size(16, 16);
            this.ClaimCountHeaderField.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Glyph, "", -1, true, true, false, editorButtonImageOptions5, new DevExpress.Utils.KeyShortcut(System.Windows.Forms.Keys.None), serializableAppearanceObject17, serializableAppearanceObject18, serializableAppearanceObject19, serializableAppearanceObject20, "", null, null, DevExpress.Utils.ToolTipAnchor.Default)});
            this.ClaimCountHeaderField.Properties.ReadOnly = true;
            this.ClaimCountHeaderField.Size = new System.Drawing.Size(165, 22);
            this.ClaimCountHeaderField.StyleController = this.MainLayoutControl;
            this.ClaimCountHeaderField.TabIndex = 102;
            // 
            // OnHoldField
            // 
            this.OnHoldField.AllowTriState = false;
            this.OnHoldField.DefaultValue = System.Windows.Forms.CheckState.Unchecked;
            this.OnHoldField.DisplayFocusCues = true;
            this.OnHoldField.EditValue = null;
            this.OnHoldField.IndeterminateBackColor = System.Drawing.Color.Silver;
            this.OnHoldField.IndeterminateToggleColor = System.Drawing.Color.Gainsboro;
            this.OnHoldField.Location = new System.Drawing.Point(169, 195);
            this.OnHoldField.MaximumSize = new System.Drawing.Size(0, 21);
            this.OnHoldField.MinimumSize = new System.Drawing.Size(30, 21);
            this.OnHoldField.Name = "OnHoldField";
            this.OnHoldField.OffBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(222)))), ((int)(((byte)(82)))), ((int)(((byte)(70)))));
            this.OnHoldField.OffToggleColor = System.Drawing.Color.WhiteSmoke;
            this.OnHoldField.OnBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(26)))), ((int)(((byte)(162)))), ((int)(((byte)(96)))));
            this.OnHoldField.OnToggleColor = System.Drawing.Color.WhiteSmoke;
            this.OnHoldField.Properties.Caption = "FancyCheckEdit";
            this.OnHoldField.Properties.UseDefaultMode = true;
            this.OnHoldField.Size = new System.Drawing.Size(48, 21);
            this.OnHoldField.StyleController = this.MainLayoutControl;
            this.OnHoldField.TabIndex = 190;
            // 
            // RewiredReplumbedField
            // 
            this.RewiredReplumbedField.AllowTriState = false;
            this.RewiredReplumbedField.DefaultValue = System.Windows.Forms.CheckState.Unchecked;
            this.RewiredReplumbedField.DisplayFocusCues = true;
            this.RewiredReplumbedField.EditValue = null;
            this.RewiredReplumbedField.IndeterminateBackColor = System.Drawing.Color.Silver;
            this.RewiredReplumbedField.IndeterminateToggleColor = System.Drawing.Color.Gainsboro;
            this.RewiredReplumbedField.Location = new System.Drawing.Point(169, 372);
            this.RewiredReplumbedField.MaximumSize = new System.Drawing.Size(0, 21);
            this.RewiredReplumbedField.MinimumSize = new System.Drawing.Size(30, 21);
            this.RewiredReplumbedField.Name = "RewiredReplumbedField";
            this.RewiredReplumbedField.OffBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(222)))), ((int)(((byte)(82)))), ((int)(((byte)(70)))));
            this.RewiredReplumbedField.OffToggleColor = System.Drawing.Color.WhiteSmoke;
            this.RewiredReplumbedField.OnBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(26)))), ((int)(((byte)(162)))), ((int)(((byte)(96)))));
            this.RewiredReplumbedField.OnToggleColor = System.Drawing.Color.WhiteSmoke;
            this.RewiredReplumbedField.Properties.Caption = "FancyCheckEdit";
            this.RewiredReplumbedField.Properties.UseDefaultMode = true;
            this.RewiredReplumbedField.Size = new System.Drawing.Size(48, 21);
            this.RewiredReplumbedField.StyleController = this.MainLayoutControl;
            this.RewiredReplumbedField.TabIndex = 191;
            // 
            // FreeStandingField
            // 
            this.FreeStandingField.AllowTriState = false;
            this.FreeStandingField.DefaultValue = System.Windows.Forms.CheckState.Unchecked;
            this.FreeStandingField.DisplayFocusCues = true;
            this.FreeStandingField.EditValue = null;
            this.FreeStandingField.IndeterminateBackColor = System.Drawing.Color.Silver;
            this.FreeStandingField.IndeterminateToggleColor = System.Drawing.Color.Gainsboro;
            this.FreeStandingField.Location = new System.Drawing.Point(169, 397);
            this.FreeStandingField.MaximumSize = new System.Drawing.Size(0, 21);
            this.FreeStandingField.MinimumSize = new System.Drawing.Size(30, 21);
            this.FreeStandingField.Name = "FreeStandingField";
            this.FreeStandingField.OffBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(222)))), ((int)(((byte)(82)))), ((int)(((byte)(70)))));
            this.FreeStandingField.OffToggleColor = System.Drawing.Color.WhiteSmoke;
            this.FreeStandingField.OnBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(26)))), ((int)(((byte)(162)))), ((int)(((byte)(96)))));
            this.FreeStandingField.OnToggleColor = System.Drawing.Color.WhiteSmoke;
            this.FreeStandingField.Properties.AutoHeight = false;
            this.FreeStandingField.Properties.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.Default;
            this.FreeStandingField.Properties.Caption = "FancyCheckEdit";
            this.FreeStandingField.Properties.UseDefaultMode = true;
            this.FreeStandingField.Size = new System.Drawing.Size(48, 21);
            this.FreeStandingField.StyleController = this.MainLayoutControl;
            this.FreeStandingField.TabIndex = 177;
            // 
            // CommonGroundsField
            // 
            this.CommonGroundsField.AllowTriState = false;
            this.CommonGroundsField.DefaultValue = System.Windows.Forms.CheckState.Unchecked;
            this.CommonGroundsField.DisplayFocusCues = true;
            this.CommonGroundsField.EditValue = null;
            this.CommonGroundsField.IndeterminateBackColor = System.Drawing.Color.Silver;
            this.CommonGroundsField.IndeterminateToggleColor = System.Drawing.Color.Gainsboro;
            this.CommonGroundsField.Location = new System.Drawing.Point(169, 422);
            this.CommonGroundsField.MaximumSize = new System.Drawing.Size(0, 21);
            this.CommonGroundsField.MinimumSize = new System.Drawing.Size(30, 21);
            this.CommonGroundsField.Name = "CommonGroundsField";
            this.CommonGroundsField.OffBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(222)))), ((int)(((byte)(82)))), ((int)(((byte)(70)))));
            this.CommonGroundsField.OffToggleColor = System.Drawing.Color.WhiteSmoke;
            this.CommonGroundsField.OnBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(26)))), ((int)(((byte)(162)))), ((int)(((byte)(96)))));
            this.CommonGroundsField.OnToggleColor = System.Drawing.Color.WhiteSmoke;
            this.CommonGroundsField.Properties.AutoHeight = false;
            this.CommonGroundsField.Properties.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.Default;
            this.CommonGroundsField.Properties.Caption = "FancyCheckEdit";
            this.CommonGroundsField.Properties.UseDefaultMode = true;
            this.CommonGroundsField.Size = new System.Drawing.Size(48, 21);
            this.CommonGroundsField.StyleController = this.MainLayoutControl;
            this.CommonGroundsField.TabIndex = 179;
            // 
            // BusinessOrTradeField
            // 
            this.BusinessOrTradeField.DefaultValue = System.Windows.Forms.CheckState.Indeterminate;
            this.BusinessOrTradeField.DisplayFocusCues = true;
            this.BusinessOrTradeField.EditValue = null;
            this.BusinessOrTradeField.IndeterminateBackColor = System.Drawing.Color.Silver;
            this.BusinessOrTradeField.IndeterminateToggleColor = System.Drawing.Color.Gainsboro;
            this.BusinessOrTradeField.Location = new System.Drawing.Point(169, 447);
            this.BusinessOrTradeField.MaximumSize = new System.Drawing.Size(0, 21);
            this.BusinessOrTradeField.MinimumSize = new System.Drawing.Size(30, 21);
            this.BusinessOrTradeField.Name = "BusinessOrTradeField";
            this.BusinessOrTradeField.OffBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(222)))), ((int)(((byte)(82)))), ((int)(((byte)(70)))));
            this.BusinessOrTradeField.OffToggleColor = System.Drawing.Color.WhiteSmoke;
            this.BusinessOrTradeField.OnBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(26)))), ((int)(((byte)(162)))), ((int)(((byte)(96)))));
            this.BusinessOrTradeField.OnToggleColor = System.Drawing.Color.WhiteSmoke;
            this.BusinessOrTradeField.Properties.AllowGrayed = true;
            this.BusinessOrTradeField.Properties.AutoHeight = false;
            this.BusinessOrTradeField.Properties.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.Default;
            this.BusinessOrTradeField.Properties.Caption = "checkEdit1";
            this.BusinessOrTradeField.Properties.UseDefaultMode = true;
            this.BusinessOrTradeField.Size = new System.Drawing.Size(48, 21);
            this.BusinessOrTradeField.StyleController = this.MainLayoutControl;
            this.BusinessOrTradeField.TabIndex = 187;
            // 
            // AcceptedBusinessUseField
            // 
            this.AcceptedBusinessUseField.AllowTriState = false;
            this.AcceptedBusinessUseField.DefaultValue = System.Windows.Forms.CheckState.Unchecked;
            this.AcceptedBusinessUseField.DisplayFocusCues = true;
            this.AcceptedBusinessUseField.EditValue = null;
            this.AcceptedBusinessUseField.IndeterminateBackColor = System.Drawing.Color.Silver;
            this.AcceptedBusinessUseField.IndeterminateToggleColor = System.Drawing.Color.Gainsboro;
            this.AcceptedBusinessUseField.Location = new System.Drawing.Point(169, 472);
            this.AcceptedBusinessUseField.MaximumSize = new System.Drawing.Size(0, 21);
            this.AcceptedBusinessUseField.MinimumSize = new System.Drawing.Size(30, 21);
            this.AcceptedBusinessUseField.Name = "AcceptedBusinessUseField";
            this.AcceptedBusinessUseField.OffBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(222)))), ((int)(((byte)(82)))), ((int)(((byte)(70)))));
            this.AcceptedBusinessUseField.OffToggleColor = System.Drawing.Color.WhiteSmoke;
            this.AcceptedBusinessUseField.OnBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(26)))), ((int)(((byte)(162)))), ((int)(((byte)(96)))));
            this.AcceptedBusinessUseField.OnToggleColor = System.Drawing.Color.WhiteSmoke;
            this.AcceptedBusinessUseField.Properties.AutoHeight = false;
            this.AcceptedBusinessUseField.Properties.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.Default;
            this.AcceptedBusinessUseField.Properties.Caption = "FancyCheckEdit";
            this.AcceptedBusinessUseField.Properties.UseDefaultMode = true;
            this.AcceptedBusinessUseField.Size = new System.Drawing.Size(48, 21);
            this.AcceptedBusinessUseField.StyleController = this.MainLayoutControl;
            this.AcceptedBusinessUseField.TabIndex = 188;
            // 
            // IsJobReducedDuePandemicField
            // 
            this.IsJobReducedDuePandemicField.DefaultValue = System.Windows.Forms.CheckState.Indeterminate;
            this.IsJobReducedDuePandemicField.DisplayFocusCues = true;
            this.IsJobReducedDuePandemicField.EditValue = null;
            this.IsJobReducedDuePandemicField.IndeterminateBackColor = System.Drawing.Color.Silver;
            this.IsJobReducedDuePandemicField.IndeterminateToggleColor = System.Drawing.Color.Gainsboro;
            this.IsJobReducedDuePandemicField.Location = new System.Drawing.Point(481, 437);
            this.IsJobReducedDuePandemicField.MaximumSize = new System.Drawing.Size(0, 21);
            this.IsJobReducedDuePandemicField.MinimumSize = new System.Drawing.Size(30, 21);
            this.IsJobReducedDuePandemicField.Name = "IsJobReducedDuePandemicField";
            this.IsJobReducedDuePandemicField.OffBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(222)))), ((int)(((byte)(82)))), ((int)(((byte)(70)))));
            this.IsJobReducedDuePandemicField.OffToggleColor = System.Drawing.Color.WhiteSmoke;
            this.IsJobReducedDuePandemicField.OnBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(26)))), ((int)(((byte)(162)))), ((int)(((byte)(96)))));
            this.IsJobReducedDuePandemicField.OnToggleColor = System.Drawing.Color.WhiteSmoke;
            this.IsJobReducedDuePandemicField.Properties.AllowGrayed = true;
            this.IsJobReducedDuePandemicField.Properties.AutoHeight = false;
            this.IsJobReducedDuePandemicField.Properties.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.Default;
            this.IsJobReducedDuePandemicField.Properties.Caption = "FancyCheckEdit";
            this.IsJobReducedDuePandemicField.Properties.UseDefaultMode = true;
            this.IsJobReducedDuePandemicField.Size = new System.Drawing.Size(48, 21);
            this.IsJobReducedDuePandemicField.StyleController = this.MainLayoutControl;
            this.IsJobReducedDuePandemicField.TabIndex = 92;
            // 
            // IsArrearsField
            // 
            this.IsArrearsField.DefaultValue = System.Windows.Forms.CheckState.Indeterminate;
            this.IsArrearsField.DisplayFocusCues = true;
            this.IsArrearsField.EditValue = null;
            this.IsArrearsField.IndeterminateBackColor = System.Drawing.Color.Silver;
            this.IsArrearsField.IndeterminateToggleColor = System.Drawing.Color.Gainsboro;
            this.IsArrearsField.Location = new System.Drawing.Point(481, 462);
            this.IsArrearsField.MaximumSize = new System.Drawing.Size(0, 21);
            this.IsArrearsField.MinimumSize = new System.Drawing.Size(30, 21);
            this.IsArrearsField.Name = "IsArrearsField";
            this.IsArrearsField.OffBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(222)))), ((int)(((byte)(82)))), ((int)(((byte)(70)))));
            this.IsArrearsField.OffToggleColor = System.Drawing.Color.WhiteSmoke;
            this.IsArrearsField.OnBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(26)))), ((int)(((byte)(162)))), ((int)(((byte)(96)))));
            this.IsArrearsField.OnToggleColor = System.Drawing.Color.WhiteSmoke;
            this.IsArrearsField.Properties.AllowGrayed = true;
            this.IsArrearsField.Properties.AutoHeight = false;
            this.IsArrearsField.Properties.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.Default;
            this.IsArrearsField.Properties.Caption = "FancyCheckEdit";
            this.IsArrearsField.Properties.UseDefaultMode = true;
            this.IsArrearsField.Size = new System.Drawing.Size(48, 21);
            this.IsArrearsField.StyleController = this.MainLayoutControl;
            this.IsArrearsField.TabIndex = 93;
            // 
            // TenantedField
            // 
            this.TenantedField.DefaultValue = System.Windows.Forms.CheckState.Indeterminate;
            this.TenantedField.DisplayFocusCues = true;
            this.TenantedField.EditValue = null;
            this.TenantedField.IndeterminateBackColor = System.Drawing.Color.Silver;
            this.TenantedField.IndeterminateToggleColor = System.Drawing.Color.Gainsboro;
            this.TenantedField.Location = new System.Drawing.Point(481, 412);
            this.TenantedField.MaximumSize = new System.Drawing.Size(0, 21);
            this.TenantedField.MinimumSize = new System.Drawing.Size(30, 21);
            this.TenantedField.Name = "TenantedField";
            this.TenantedField.OffBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(222)))), ((int)(((byte)(82)))), ((int)(((byte)(70)))));
            this.TenantedField.OffToggleColor = System.Drawing.Color.WhiteSmoke;
            this.TenantedField.OnBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(26)))), ((int)(((byte)(162)))), ((int)(((byte)(96)))));
            this.TenantedField.OnToggleColor = System.Drawing.Color.WhiteSmoke;
            this.TenantedField.Properties.AllowGrayed = true;
            this.TenantedField.Properties.AutoHeight = false;
            this.TenantedField.Properties.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.Default;
            this.TenantedField.Properties.Caption = "FancyCheckEdit";
            this.TenantedField.Properties.UseDefaultMode = true;
            this.TenantedField.Size = new System.Drawing.Size(48, 21);
            this.TenantedField.StyleController = this.MainLayoutControl;
            this.TenantedField.TabIndex = 91;
            // 
            // StaffDiscountField
            // 
            this.StaffDiscountField.AllowTriState = false;
            this.StaffDiscountField.DefaultValue = System.Windows.Forms.CheckState.Unchecked;
            this.StaffDiscountField.DisplayFocusCues = true;
            this.StaffDiscountField.EditValue = null;
            this.StaffDiscountField.IndeterminateBackColor = System.Drawing.Color.Silver;
            this.StaffDiscountField.IndeterminateToggleColor = System.Drawing.Color.Gainsboro;
            this.StaffDiscountField.Location = new System.Drawing.Point(862, 307);
            this.StaffDiscountField.MaximumSize = new System.Drawing.Size(0, 95);
            this.StaffDiscountField.MinimumSize = new System.Drawing.Size(30, 21);
            this.StaffDiscountField.Name = "StaffDiscountField";
            this.StaffDiscountField.OffBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(222)))), ((int)(((byte)(82)))), ((int)(((byte)(70)))));
            this.StaffDiscountField.OffToggleColor = System.Drawing.Color.WhiteSmoke;
            this.StaffDiscountField.OnBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(26)))), ((int)(((byte)(162)))), ((int)(((byte)(96)))));
            this.StaffDiscountField.OnToggleColor = System.Drawing.Color.WhiteSmoke;
            this.StaffDiscountField.Properties.AutoHeight = false;
            this.StaffDiscountField.Properties.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.Default;
            this.StaffDiscountField.Properties.Caption = "FancyCheckEdit";
            this.StaffDiscountField.Properties.UseDefaultMode = true;
            this.StaffDiscountField.Size = new System.Drawing.Size(48, 21);
            this.StaffDiscountField.StyleController = this.MainLayoutControl;
            this.StaffDiscountField.TabIndex = 95;
            // 
            // IsREADiscountField
            // 
            this.IsREADiscountField.AllowTriState = false;
            this.IsREADiscountField.DefaultValue = System.Windows.Forms.CheckState.Unchecked;
            this.IsREADiscountField.DisplayFocusCues = true;
            this.IsREADiscountField.EditValue = null;
            this.IsREADiscountField.IndeterminateBackColor = System.Drawing.Color.Silver;
            this.IsREADiscountField.IndeterminateToggleColor = System.Drawing.Color.Gainsboro;
            this.IsREADiscountField.Location = new System.Drawing.Point(862, 332);
            this.IsREADiscountField.MaximumSize = new System.Drawing.Size(0, 95);
            this.IsREADiscountField.MinimumSize = new System.Drawing.Size(30, 21);
            this.IsREADiscountField.Name = "IsREADiscountField";
            this.IsREADiscountField.OffBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(222)))), ((int)(((byte)(82)))), ((int)(((byte)(70)))));
            this.IsREADiscountField.OffToggleColor = System.Drawing.Color.WhiteSmoke;
            this.IsREADiscountField.OnBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(26)))), ((int)(((byte)(162)))), ((int)(((byte)(96)))));
            this.IsREADiscountField.OnToggleColor = System.Drawing.Color.WhiteSmoke;
            this.IsREADiscountField.Properties.AutoHeight = false;
            this.IsREADiscountField.Properties.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.Default;
            this.IsREADiscountField.Properties.Caption = "FancyCheckEdit";
            this.IsREADiscountField.Properties.UseDefaultMode = true;
            this.IsREADiscountField.Size = new System.Drawing.Size(48, 21);
            this.IsREADiscountField.StyleController = this.MainLayoutControl;
            this.IsREADiscountField.TabIndex = 98;
            // 
            // IsMultiplePropertyDiscountField
            // 
            this.IsMultiplePropertyDiscountField.AllowTriState = false;
            this.IsMultiplePropertyDiscountField.DefaultValue = System.Windows.Forms.CheckState.Unchecked;
            this.IsMultiplePropertyDiscountField.DisplayFocusCues = true;
            this.IsMultiplePropertyDiscountField.EditValue = null;
            this.IsMultiplePropertyDiscountField.IndeterminateBackColor = System.Drawing.Color.Silver;
            this.IsMultiplePropertyDiscountField.IndeterminateToggleColor = System.Drawing.Color.Gainsboro;
            this.IsMultiplePropertyDiscountField.Location = new System.Drawing.Point(862, 357);
            this.IsMultiplePropertyDiscountField.MaximumSize = new System.Drawing.Size(0, 95);
            this.IsMultiplePropertyDiscountField.MinimumSize = new System.Drawing.Size(30, 21);
            this.IsMultiplePropertyDiscountField.Name = "IsMultiplePropertyDiscountField";
            this.IsMultiplePropertyDiscountField.OffBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(222)))), ((int)(((byte)(82)))), ((int)(((byte)(70)))));
            this.IsMultiplePropertyDiscountField.OffToggleColor = System.Drawing.Color.WhiteSmoke;
            this.IsMultiplePropertyDiscountField.OnBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(26)))), ((int)(((byte)(162)))), ((int)(((byte)(96)))));
            this.IsMultiplePropertyDiscountField.OnToggleColor = System.Drawing.Color.WhiteSmoke;
            this.IsMultiplePropertyDiscountField.Properties.AutoHeight = false;
            this.IsMultiplePropertyDiscountField.Properties.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.Default;
            this.IsMultiplePropertyDiscountField.Properties.Caption = "FancyCheckEdit";
            this.IsMultiplePropertyDiscountField.Properties.UseDefaultMode = true;
            this.IsMultiplePropertyDiscountField.Size = new System.Drawing.Size(48, 21);
            this.IsMultiplePropertyDiscountField.StyleController = this.MainLayoutControl;
            this.IsMultiplePropertyDiscountField.TabIndex = 96;
            // 
            // IsFivePercentDiscountField
            // 
            this.IsFivePercentDiscountField.AllowTriState = false;
            this.IsFivePercentDiscountField.AutoSizeInLayoutControl = true;
            this.IsFivePercentDiscountField.DefaultValue = System.Windows.Forms.CheckState.Unchecked;
            this.IsFivePercentDiscountField.DisplayFocusCues = true;
            this.IsFivePercentDiscountField.EditValue = null;
            this.IsFivePercentDiscountField.IndeterminateBackColor = System.Drawing.Color.Silver;
            this.IsFivePercentDiscountField.IndeterminateToggleColor = System.Drawing.Color.Gainsboro;
            this.IsFivePercentDiscountField.Location = new System.Drawing.Point(862, 382);
            this.IsFivePercentDiscountField.MaximumSize = new System.Drawing.Size(0, 95);
            this.IsFivePercentDiscountField.MinimumSize = new System.Drawing.Size(30, 21);
            this.IsFivePercentDiscountField.Name = "IsFivePercentDiscountField";
            this.IsFivePercentDiscountField.OffBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(222)))), ((int)(((byte)(82)))), ((int)(((byte)(70)))));
            this.IsFivePercentDiscountField.OffToggleColor = System.Drawing.Color.WhiteSmoke;
            this.IsFivePercentDiscountField.OnBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(26)))), ((int)(((byte)(162)))), ((int)(((byte)(96)))));
            this.IsFivePercentDiscountField.OnToggleColor = System.Drawing.Color.WhiteSmoke;
            this.IsFivePercentDiscountField.Properties.AutoHeight = false;
            this.IsFivePercentDiscountField.Properties.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.Default;
            this.IsFivePercentDiscountField.Properties.Caption = "FancyCheckEdit";
            this.IsFivePercentDiscountField.Properties.UseDefaultMode = true;
            this.IsFivePercentDiscountField.Size = new System.Drawing.Size(48, 21);
            this.IsFivePercentDiscountField.StyleController = this.MainLayoutControl;
            this.IsFivePercentDiscountField.TabIndex = 97;
            // 
            // Is15For12BonusOfferField
            // 
            this.Is15For12BonusOfferField.AllowTriState = false;
            this.Is15For12BonusOfferField.DefaultValue = System.Windows.Forms.CheckState.Unchecked;
            this.Is15For12BonusOfferField.DisplayFocusCues = true;
            this.Is15For12BonusOfferField.EditValue = null;
            this.Is15For12BonusOfferField.IndeterminateBackColor = System.Drawing.Color.Silver;
            this.Is15For12BonusOfferField.IndeterminateToggleColor = System.Drawing.Color.Gainsboro;
            this.Is15For12BonusOfferField.Location = new System.Drawing.Point(862, 407);
            this.Is15For12BonusOfferField.MaximumSize = new System.Drawing.Size(0, 95);
            this.Is15For12BonusOfferField.MinimumSize = new System.Drawing.Size(30, 21);
            this.Is15For12BonusOfferField.Name = "Is15For12BonusOfferField";
            this.Is15For12BonusOfferField.OffBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(222)))), ((int)(((byte)(82)))), ((int)(((byte)(70)))));
            this.Is15For12BonusOfferField.OffToggleColor = System.Drawing.Color.WhiteSmoke;
            this.Is15For12BonusOfferField.OnBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(26)))), ((int)(((byte)(162)))), ((int)(((byte)(96)))));
            this.Is15For12BonusOfferField.OnToggleColor = System.Drawing.Color.WhiteSmoke;
            this.Is15For12BonusOfferField.Properties.AutoHeight = false;
            this.Is15For12BonusOfferField.Properties.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.Default;
            this.Is15For12BonusOfferField.Properties.Caption = "FancyCheckEdit";
            this.Is15For12BonusOfferField.Properties.UseDefaultMode = true;
            this.Is15For12BonusOfferField.Size = new System.Drawing.Size(48, 21);
            this.Is15For12BonusOfferField.StyleController = this.MainLayoutControl;
            this.Is15For12BonusOfferField.TabIndex = 160;
            // 
            // PremiumPaidByField
            // 
            this.PremiumPaidByField.Location = new System.Drawing.Point(169, 121);
            this.PremiumPaidByField.Name = "PremiumPaidByField";
            this.PremiumPaidByField.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.PremiumPaidByField.Properties.NullText = "";
            this.PremiumPaidByField.Size = new System.Drawing.Size(159, 20);
            this.PremiumPaidByField.StyleController = this.MainLayoutControl;
            this.PremiumPaidByField.TabIndex = 15;
            // 
            // PreferredMailingAddressTypeField
            // 
            this.PreferredMailingAddressTypeField.Location = new System.Drawing.Point(169, 145);
            this.PreferredMailingAddressTypeField.Name = "PreferredMailingAddressTypeField";
            this.PreferredMailingAddressTypeField.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.PreferredMailingAddressTypeField.Properties.NullText = "";
            this.PreferredMailingAddressTypeField.Size = new System.Drawing.Size(159, 20);
            this.PreferredMailingAddressTypeField.StyleController = this.MainLayoutControl;
            this.PreferredMailingAddressTypeField.TabIndex = 116;
            // 
            // InsurerCodeField
            // 
            this.InsurerCodeField.Location = new System.Drawing.Point(169, 169);
            this.InsurerCodeField.Name = "InsurerCodeField";
            this.InsurerCodeField.Size = new System.Drawing.Size(159, 20);
            this.InsurerCodeField.StyleController = this.MainLayoutControl;
            this.InsurerCodeField.TabIndex = 117;
            // 
            // OnlineQuoteNumberField
            // 
            this.OnlineQuoteNumberField.Location = new System.Drawing.Point(169, 235);
            this.OnlineQuoteNumberField.Name = "OnlineQuoteNumberField";
            this.OnlineQuoteNumberField.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton()});
            this.OnlineQuoteNumberField.Size = new System.Drawing.Size(159, 20);
            this.OnlineQuoteNumberField.StyleController = this.MainLayoutControl;
            this.OnlineQuoteNumberField.TabIndex = 119;
            // 
            // QuoteNumberField
            // 
            this.QuoteNumberField.Location = new System.Drawing.Point(169, 259);
            this.QuoteNumberField.Name = "QuoteNumberField";
            this.QuoteNumberField.Size = new System.Drawing.Size(159, 20);
            this.QuoteNumberField.StyleController = this.MainLayoutControl;
            this.QuoteNumberField.TabIndex = 118;
            // 
            // AskedForQuoteWhoField
            // 
            this.AskedForQuoteWhoField.Location = new System.Drawing.Point(169, 283);
            this.AskedForQuoteWhoField.Name = "AskedForQuoteWhoField";
            this.AskedForQuoteWhoField.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.AskedForQuoteWhoField.Properties.NullText = "";
            this.AskedForQuoteWhoField.Size = new System.Drawing.Size(159, 20);
            this.AskedForQuoteWhoField.StyleController = this.MainLayoutControl;
            this.AskedForQuoteWhoField.TabIndex = 123;
            // 
            // HowAskedForQuoteField
            // 
            this.HowAskedForQuoteField.Location = new System.Drawing.Point(169, 307);
            this.HowAskedForQuoteField.Name = "HowAskedForQuoteField";
            this.HowAskedForQuoteField.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.HowAskedForQuoteField.Properties.NullText = "";
            this.HowAskedForQuoteField.Size = new System.Drawing.Size(159, 20);
            this.HowAskedForQuoteField.StyleController = this.MainLayoutControl;
            this.HowAskedForQuoteField.TabIndex = 122;
            // 
            // QuotedByField
            // 
            this.QuotedByField.Location = new System.Drawing.Point(169, 331);
            this.QuotedByField.Name = "QuotedByField";
            this.QuotedByField.Size = new System.Drawing.Size(159, 20);
            this.QuotedByField.StyleController = this.MainLayoutControl;
            this.QuotedByField.TabIndex = 121;
            // 
            // QuotedOnField
            // 
            this.QuotedOnField.Location = new System.Drawing.Point(169, 355);
            this.QuotedOnField.Name = "QuotedOnField";
            this.QuotedOnField.Size = new System.Drawing.Size(159, 20);
            this.QuotedOnField.StyleController = this.MainLayoutControl;
            this.QuotedOnField.TabIndex = 125;
            // 
            // ConvertedToCoverByField
            // 
            this.ConvertedToCoverByField.Location = new System.Drawing.Point(169, 379);
            this.ConvertedToCoverByField.Name = "ConvertedToCoverByField";
            this.ConvertedToCoverByField.Size = new System.Drawing.Size(159, 20);
            this.ConvertedToCoverByField.StyleController = this.MainLayoutControl;
            this.ConvertedToCoverByField.TabIndex = 124;
            // 
            // ConvertedToCoverOnField
            // 
            this.ConvertedToCoverOnField.Location = new System.Drawing.Point(169, 403);
            this.ConvertedToCoverOnField.Name = "ConvertedToCoverOnField";
            this.ConvertedToCoverOnField.Size = new System.Drawing.Size(159, 20);
            this.ConvertedToCoverOnField.StyleController = this.MainLayoutControl;
            this.ConvertedToCoverOnField.TabIndex = 126;
            // 
            // QuoteExpiresOnField
            // 
            this.QuoteExpiresOnField.Location = new System.Drawing.Point(169, 427);
            this.QuoteExpiresOnField.Name = "QuoteExpiresOnField";
            this.QuoteExpiresOnField.Size = new System.Drawing.Size(159, 20);
            this.QuoteExpiresOnField.StyleController = this.MainLayoutControl;
            this.QuoteExpiresOnField.TabIndex = 128;
            // 
            // OnlineQuoteEmailAddressField
            // 
            this.OnlineQuoteEmailAddressField.Location = new System.Drawing.Point(169, 451);
            this.OnlineQuoteEmailAddressField.Name = "OnlineQuoteEmailAddressField";
            this.OnlineQuoteEmailAddressField.Size = new System.Drawing.Size(159, 20);
            this.OnlineQuoteEmailAddressField.StyleController = this.MainLayoutControl;
            this.OnlineQuoteEmailAddressField.TabIndex = 127;
            // 
            // WhoPlacedCoverField
            // 
            this.WhoPlacedCoverField.Location = new System.Drawing.Point(489, 146);
            this.WhoPlacedCoverField.Name = "WhoPlacedCoverField";
            this.WhoPlacedCoverField.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.WhoPlacedCoverField.Properties.NullText = "";
            this.WhoPlacedCoverField.Size = new System.Drawing.Size(146, 20);
            this.WhoPlacedCoverField.StyleController = this.MainLayoutControl;
            this.WhoPlacedCoverField.TabIndex = 133;
            // 
            // HowPlacedCoverField
            // 
            this.HowPlacedCoverField.Location = new System.Drawing.Point(489, 170);
            this.HowPlacedCoverField.Name = "HowPlacedCoverField";
            this.HowPlacedCoverField.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.HowPlacedCoverField.Properties.NullText = "";
            this.HowPlacedCoverField.Size = new System.Drawing.Size(146, 20);
            this.HowPlacedCoverField.StyleController = this.MainLayoutControl;
            this.HowPlacedCoverField.TabIndex = 134;
            // 
            // HeardAboutTSIviaField
            // 
            this.HeardAboutTSIviaField.Location = new System.Drawing.Point(489, 194);
            this.HeardAboutTSIviaField.Name = "HeardAboutTSIviaField";
            this.HeardAboutTSIviaField.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.HeardAboutTSIviaField.Properties.NullText = "";
            this.HeardAboutTSIviaField.Size = new System.Drawing.Size(146, 20);
            this.HeardAboutTSIviaField.StyleController = this.MainLayoutControl;
            this.HeardAboutTSIviaField.TabIndex = 120;
            // 
            // MortgageeField
            // 
            this.MortgageeField.Location = new System.Drawing.Point(489, 218);
            this.MortgageeField.Name = "MortgageeField";
            this.MortgageeField.Size = new System.Drawing.Size(146, 38);
            this.MortgageeField.StyleController = this.MainLayoutControl;
            this.MortgageeField.TabIndex = 142;
            // 
            // PaymentMethodField
            // 
            this.PaymentMethodField.Location = new System.Drawing.Point(489, 327);
            this.PaymentMethodField.Name = "PaymentMethodField";
            this.PaymentMethodField.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.PaymentMethodField.Properties.NullText = "";
            this.PaymentMethodField.Size = new System.Drawing.Size(146, 20);
            this.PaymentMethodField.StyleController = this.MainLayoutControl;
            this.PaymentMethodField.TabIndex = 135;
            // 
            // PaymentReferenceField
            // 
            this.PaymentReferenceField.Location = new System.Drawing.Point(489, 351);
            this.PaymentReferenceField.Name = "PaymentReferenceField";
            this.PaymentReferenceField.Properties.ReadOnly = true;
            this.PaymentReferenceField.Size = new System.Drawing.Size(146, 20);
            this.PaymentReferenceField.StyleController = this.MainLayoutControl;
            this.PaymentReferenceField.TabIndex = 136;
            // 
            // BPayReceiptField
            // 
            this.BPayReceiptField.Location = new System.Drawing.Point(489, 375);
            this.BPayReceiptField.Name = "BPayReceiptField";
            this.BPayReceiptField.Properties.ReadOnly = true;
            this.BPayReceiptField.Size = new System.Drawing.Size(146, 20);
            this.BPayReceiptField.StyleController = this.MainLayoutControl;
            this.BPayReceiptField.TabIndex = 137;
            // 
            // OnlineReferenceField
            // 
            this.OnlineReferenceField.Location = new System.Drawing.Point(489, 399);
            this.OnlineReferenceField.Name = "OnlineReferenceField";
            this.OnlineReferenceField.Properties.ReadOnly = true;
            this.OnlineReferenceField.Size = new System.Drawing.Size(146, 20);
            this.OnlineReferenceField.StyleController = this.MainLayoutControl;
            this.OnlineReferenceField.TabIndex = 138;
            // 
            // CCTransactionIDField
            // 
            this.CCTransactionIDField.Location = new System.Drawing.Point(489, 423);
            this.CCTransactionIDField.Name = "CCTransactionIDField";
            this.CCTransactionIDField.Properties.ReadOnly = true;
            this.CCTransactionIDField.Size = new System.Drawing.Size(146, 20);
            this.CCTransactionIDField.StyleController = this.MainLayoutControl;
            this.CCTransactionIDField.TabIndex = 139;
            // 
            // VoucherTypeField
            // 
            this.VoucherTypeField.Location = new System.Drawing.Point(489, 447);
            this.VoucherTypeField.Name = "VoucherTypeField";
            this.VoucherTypeField.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.VoucherTypeField.Properties.NullText = "";
            this.VoucherTypeField.Size = new System.Drawing.Size(146, 20);
            this.VoucherTypeField.StyleController = this.MainLayoutControl;
            this.VoucherTypeField.TabIndex = 140;
            // 
            // PromoCodeField
            // 
            this.PromoCodeField.Location = new System.Drawing.Point(489, 471);
            this.PromoCodeField.Name = "PromoCodeField";
            this.PromoCodeField.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.PromoCodeField.Properties.NullText = "";
            this.PromoCodeField.Size = new System.Drawing.Size(146, 20);
            this.PromoCodeField.StyleController = this.MainLayoutControl;
            this.PromoCodeField.TabIndex = 141;
            // 
            // LinkCreditTransactionNumberField
            // 
            this.LinkCreditTransactionNumberField.Location = new System.Drawing.Point(796, 193);
            this.LinkCreditTransactionNumberField.Name = "LinkCreditTransactionNumberField";
            this.LinkCreditTransactionNumberField.Size = new System.Drawing.Size(146, 20);
            this.LinkCreditTransactionNumberField.StyleController = this.MainLayoutControl;
            this.LinkCreditTransactionNumberField.TabIndex = 167;
            // 
            // LinkParentTransactionNumberField
            // 
            this.LinkParentTransactionNumberField.Location = new System.Drawing.Point(796, 169);
            this.LinkParentTransactionNumberField.Name = "LinkParentTransactionNumberField";
            this.LinkParentTransactionNumberField.Size = new System.Drawing.Size(146, 20);
            this.LinkParentTransactionNumberField.StyleController = this.MainLayoutControl;
            this.LinkParentTransactionNumberField.TabIndex = 168;
            // 
            // LinkReferToTransactionNumberField
            // 
            this.LinkReferToTransactionNumberField.Location = new System.Drawing.Point(796, 121);
            this.LinkReferToTransactionNumberField.Name = "LinkReferToTransactionNumberField";
            this.LinkReferToTransactionNumberField.Size = new System.Drawing.Size(146, 20);
            this.LinkReferToTransactionNumberField.StyleController = this.MainLayoutControl;
            this.LinkReferToTransactionNumberField.TabIndex = 131;
            // 
            // LinkWelcomeLetterTransactionNumberField
            // 
            this.LinkWelcomeLetterTransactionNumberField.Location = new System.Drawing.Point(796, 145);
            this.LinkWelcomeLetterTransactionNumberField.Name = "LinkWelcomeLetterTransactionNumberField";
            this.LinkWelcomeLetterTransactionNumberField.Size = new System.Drawing.Size(146, 20);
            this.LinkWelcomeLetterTransactionNumberField.StyleController = this.MainLayoutControl;
            this.LinkWelcomeLetterTransactionNumberField.TabIndex = 132;
            // 
            // RecreatedField
            // 
            this.RecreatedField.AllowTriState = false;
            this.RecreatedField.DefaultValue = System.Windows.Forms.CheckState.Unchecked;
            this.RecreatedField.DisplayFocusCues = true;
            this.RecreatedField.EditValue = null;
            this.RecreatedField.IndeterminateBackColor = System.Drawing.Color.Silver;
            this.RecreatedField.IndeterminateToggleColor = System.Drawing.Color.Gainsboro;
            this.RecreatedField.Location = new System.Drawing.Point(489, 121);
            this.RecreatedField.MaximumSize = new System.Drawing.Size(0, 21);
            this.RecreatedField.MinimumSize = new System.Drawing.Size(20, 21);
            this.RecreatedField.Name = "RecreatedField";
            this.RecreatedField.OffBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(222)))), ((int)(((byte)(82)))), ((int)(((byte)(70)))));
            this.RecreatedField.OffToggleColor = System.Drawing.Color.WhiteSmoke;
            this.RecreatedField.OnBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(26)))), ((int)(((byte)(162)))), ((int)(((byte)(96)))));
            this.RecreatedField.OnToggleColor = System.Drawing.Color.WhiteSmoke;
            this.RecreatedField.Properties.AutoHeight = false;
            this.RecreatedField.Properties.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.Default;
            this.RecreatedField.Properties.Caption = "FancyCheckEdit";
            this.RecreatedField.Properties.ReadOnly = true;
            this.RecreatedField.Properties.UseDefaultMode = true;
            this.RecreatedField.Size = new System.Drawing.Size(48, 21);
            this.RecreatedField.StyleController = this.MainLayoutControl;
            this.RecreatedField.TabIndex = 129;
            // 
            // PaymentReceivedField
            // 
            this.PaymentReceivedField.AllowTriState = false;
            this.PaymentReceivedField.DefaultValue = System.Windows.Forms.CheckState.Unchecked;
            this.PaymentReceivedField.DisplayFocusCues = true;
            this.PaymentReceivedField.EditValue = null;
            this.PaymentReceivedField.IndeterminateBackColor = System.Drawing.Color.Silver;
            this.PaymentReceivedField.IndeterminateToggleColor = System.Drawing.Color.Gainsboro;
            this.PaymentReceivedField.Location = new System.Drawing.Point(489, 302);
            this.PaymentReceivedField.MaximumSize = new System.Drawing.Size(0, 21);
            this.PaymentReceivedField.MinimumSize = new System.Drawing.Size(20, 21);
            this.PaymentReceivedField.Name = "PaymentReceivedField";
            this.PaymentReceivedField.OffBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(222)))), ((int)(((byte)(82)))), ((int)(((byte)(70)))));
            this.PaymentReceivedField.OffToggleColor = System.Drawing.Color.WhiteSmoke;
            this.PaymentReceivedField.OnBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(26)))), ((int)(((byte)(162)))), ((int)(((byte)(96)))));
            this.PaymentReceivedField.OnToggleColor = System.Drawing.Color.WhiteSmoke;
            this.PaymentReceivedField.Properties.AutoHeight = false;
            this.PaymentReceivedField.Properties.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.Default;
            this.PaymentReceivedField.Properties.Caption = "FancyCheckEdit";
            this.PaymentReceivedField.Properties.UseDefaultMode = true;
            this.PaymentReceivedField.Size = new System.Drawing.Size(48, 21);
            this.PaymentReceivedField.StyleController = this.MainLayoutControl;
            this.PaymentReceivedField.TabIndex = 130;
            // 
            // ApprovedByField
            // 
            this.ApprovedByField.Location = new System.Drawing.Point(160, 523);
            this.ApprovedByField.Name = "ApprovedByField";
            this.ApprovedByField.Size = new System.Drawing.Size(137, 20);
            this.ApprovedByField.StyleController = this.MainLayoutControl;
            this.ApprovedByField.TabIndex = 72;
            // 
            // ApprovedOnField
            // 
            this.ApprovedOnField.Location = new System.Drawing.Point(412, 523);
            this.ApprovedOnField.Name = "ApprovedOnField";
            this.ApprovedOnField.Properties.DisplayFormat.FormatString = "d";
            this.ApprovedOnField.Properties.DisplayFormat.FormatType = DevExpress.Utils.FormatType.DateTime;
            this.ApprovedOnField.Properties.EditFormat.FormatString = "d";
            this.ApprovedOnField.Properties.EditFormat.FormatType = DevExpress.Utils.FormatType.DateTime;
            this.ApprovedOnField.Properties.Mask.EditMask = "d";
            this.ApprovedOnField.Properties.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.DateTime;
            this.ApprovedOnField.Size = new System.Drawing.Size(85, 20);
            this.ApprovedOnField.StyleController = this.MainLayoutControl;
            this.ApprovedOnField.TabIndex = 73;
            // 
            // RenewingTransactionsField
            // 
            this.RenewingTransactionsField.Location = new System.Drawing.Point(169, 378);
            this.RenewingTransactionsField.Name = "RenewingTransactionsField";
            this.RenewingTransactionsField.Size = new System.Drawing.Size(112, 20);
            this.RenewingTransactionsField.StyleController = this.MainLayoutControl;
            this.RenewingTransactionsField.TabIndex = 143;
            // 
            // RenewalReasonField
            // 
            this.RenewalReasonField.Location = new System.Drawing.Point(169, 453);
            this.RenewalReasonField.Name = "RenewalReasonField";
            this.RenewalReasonField.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.RenewalReasonField.Properties.NullText = "";
            this.RenewalReasonField.Size = new System.Drawing.Size(175, 20);
            this.RenewalReasonField.StyleController = this.MainLayoutControl;
            this.RenewalReasonField.TabIndex = 147;
            // 
            // RenewalPremiumPaidByField
            // 
            this.RenewalPremiumPaidByField.Location = new System.Drawing.Point(169, 477);
            this.RenewalPremiumPaidByField.Name = "RenewalPremiumPaidByField";
            this.RenewalPremiumPaidByField.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.RenewalPremiumPaidByField.Properties.NullText = "";
            this.RenewalPremiumPaidByField.Size = new System.Drawing.Size(175, 20);
            this.RenewalPremiumPaidByField.StyleController = this.MainLayoutControl;
            this.RenewalPremiumPaidByField.TabIndex = 148;
            // 
            // textEdit51
            // 
            this.textEdit51.Location = new System.Drawing.Point(169, 531);
            this.textEdit51.Name = "textEdit51";
            this.textEdit51.Size = new System.Drawing.Size(175, 20);
            this.textEdit51.StyleController = this.MainLayoutControl;
            this.textEdit51.TabIndex = 149;
            // 
            // RenewalDiscountAmountField
            // 
            this.RenewalDiscountAmountField.Location = new System.Drawing.Point(510, 453);
            this.RenewalDiscountAmountField.Name = "RenewalDiscountAmountField";
            this.RenewalDiscountAmountField.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.RenewalDiscountAmountField.Properties.NullText = "";
            this.RenewalDiscountAmountField.Size = new System.Drawing.Size(154, 20);
            this.RenewalDiscountAmountField.StyleController = this.MainLayoutControl;
            this.RenewalDiscountAmountField.TabIndex = 154;
            // 
            // RenewedTransactionField
            // 
            this.RenewedTransactionField.Location = new System.Drawing.Point(825, 378);
            this.RenewedTransactionField.Name = "RenewedTransactionField";
            this.RenewedTransactionField.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton()});
            this.RenewedTransactionField.Size = new System.Drawing.Size(108, 20);
            this.RenewedTransactionField.StyleController = this.MainLayoutControl;
            this.RenewedTransactionField.TabIndex = 159;
            // 
            // RenewedByTransactionField
            // 
            this.RenewedByTransactionField.Location = new System.Drawing.Point(825, 402);
            this.RenewedByTransactionField.Name = "RenewedByTransactionField";
            this.RenewedByTransactionField.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton()});
            this.RenewedByTransactionField.Size = new System.Drawing.Size(108, 20);
            this.RenewedByTransactionField.StyleController = this.MainLayoutControl;
            this.RenewedByTransactionField.TabIndex = 158;
            // 
            // LastRenewalErrorField
            // 
            this.LastRenewalErrorField.Location = new System.Drawing.Point(825, 426);
            this.LastRenewalErrorField.Name = "LastRenewalErrorField";
            this.LastRenewalErrorField.Size = new System.Drawing.Size(380, 62);
            this.LastRenewalErrorField.StyleController = this.MainLayoutControl;
            this.LastRenewalErrorField.TabIndex = 157;
            // 
            // RenewalBatchField
            // 
            this.RenewalBatchField.Location = new System.Drawing.Point(825, 492);
            this.RenewalBatchField.Name = "RenewalBatchField";
            this.RenewalBatchField.Size = new System.Drawing.Size(108, 20);
            this.RenewalBatchField.StyleController = this.MainLayoutControl;
            this.RenewalBatchField.TabIndex = 156;
            // 
            // LedgerHoldCoverField
            // 
            this.LedgerHoldCoverField.EditValue = null;
            this.LedgerHoldCoverField.Location = new System.Drawing.Point(115, 91);
            this.LedgerHoldCoverField.Name = "LedgerHoldCoverField";
            this.LedgerHoldCoverField.Properties.AllowDropDownWhenReadOnly = DevExpress.Utils.DefaultBoolean.False;
            this.LedgerHoldCoverField.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.LedgerHoldCoverField.Properties.CalendarTimeProperties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.LedgerHoldCoverField.Properties.DisplayFormat.FormatString = "";
            this.LedgerHoldCoverField.Properties.DisplayFormat.FormatType = DevExpress.Utils.FormatType.DateTime;
            this.LedgerHoldCoverField.Properties.EditFormat.FormatString = "";
            this.LedgerHoldCoverField.Properties.EditFormat.FormatType = DevExpress.Utils.FormatType.DateTime;
            this.LedgerHoldCoverField.Properties.Mask.EditMask = "";
            this.LedgerHoldCoverField.Properties.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.None;
            this.LedgerHoldCoverField.Size = new System.Drawing.Size(100, 20);
            this.LedgerHoldCoverField.StyleController = this.MainLayoutControl;
            this.LedgerHoldCoverField.TabIndex = 18;
            // 
            // LedgerPremiumPaidField
            // 
            this.LedgerPremiumPaidField.EditValue = null;
            this.LedgerPremiumPaidField.Location = new System.Drawing.Point(115, 115);
            this.LedgerPremiumPaidField.Name = "LedgerPremiumPaidField";
            this.LedgerPremiumPaidField.Properties.AllowDropDownWhenReadOnly = DevExpress.Utils.DefaultBoolean.False;
            this.LedgerPremiumPaidField.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.LedgerPremiumPaidField.Properties.CalendarTimeProperties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.LedgerPremiumPaidField.Size = new System.Drawing.Size(100, 20);
            this.LedgerPremiumPaidField.StyleController = this.MainLayoutControl;
            this.LedgerPremiumPaidField.TabIndex = 31;
            // 
            // LedgerCurrentOnField
            // 
            this.LedgerCurrentOnField.EditValue = null;
            this.LedgerCurrentOnField.Location = new System.Drawing.Point(350, 91);
            this.LedgerCurrentOnField.Name = "LedgerCurrentOnField";
            this.LedgerCurrentOnField.Properties.AllowDropDownWhenReadOnly = DevExpress.Utils.DefaultBoolean.False;
            this.LedgerCurrentOnField.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.LedgerCurrentOnField.Properties.CalendarTimeProperties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.LedgerCurrentOnField.Size = new System.Drawing.Size(98, 20);
            this.LedgerCurrentOnField.StyleController = this.MainLayoutControl;
            this.LedgerCurrentOnField.TabIndex = 29;
            // 
            // LedgerDistributeFundsField
            // 
            this.LedgerDistributeFundsField.EditValue = null;
            this.LedgerDistributeFundsField.Location = new System.Drawing.Point(350, 115);
            this.LedgerDistributeFundsField.Name = "LedgerDistributeFundsField";
            this.LedgerDistributeFundsField.Properties.AllowDropDownWhenReadOnly = DevExpress.Utils.DefaultBoolean.False;
            this.LedgerDistributeFundsField.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.LedgerDistributeFundsField.Properties.CalendarTimeProperties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.LedgerDistributeFundsField.Size = new System.Drawing.Size(98, 20);
            this.LedgerDistributeFundsField.StyleController = this.MainLayoutControl;
            this.LedgerDistributeFundsField.TabIndex = 30;
            // 
            // LedgerInvoiceOverdueOnField
            // 
            this.LedgerInvoiceOverdueOnField.EditValue = null;
            this.LedgerInvoiceOverdueOnField.Location = new System.Drawing.Point(583, 91);
            this.LedgerInvoiceOverdueOnField.Name = "LedgerInvoiceOverdueOnField";
            this.LedgerInvoiceOverdueOnField.Properties.AllowDropDownWhenReadOnly = DevExpress.Utils.DefaultBoolean.False;
            this.LedgerInvoiceOverdueOnField.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.LedgerInvoiceOverdueOnField.Properties.CalendarTimeProperties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.LedgerInvoiceOverdueOnField.Properties.DisplayFormat.FormatString = "";
            this.LedgerInvoiceOverdueOnField.Properties.DisplayFormat.FormatType = DevExpress.Utils.FormatType.DateTime;
            this.LedgerInvoiceOverdueOnField.Properties.EditFormat.FormatString = "";
            this.LedgerInvoiceOverdueOnField.Properties.EditFormat.FormatType = DevExpress.Utils.FormatType.DateTime;
            this.LedgerInvoiceOverdueOnField.Properties.Mask.EditMask = "";
            this.LedgerInvoiceOverdueOnField.Properties.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.None;
            this.LedgerInvoiceOverdueOnField.Size = new System.Drawing.Size(84, 20);
            this.LedgerInvoiceOverdueOnField.StyleController = this.MainLayoutControl;
            this.LedgerInvoiceOverdueOnField.TabIndex = 28;
            // 
            // LedgerExtendedOverdueOnField
            // 
            this.LedgerExtendedOverdueOnField.EditValue = null;
            this.LedgerExtendedOverdueOnField.Location = new System.Drawing.Point(583, 115);
            this.LedgerExtendedOverdueOnField.Name = "LedgerExtendedOverdueOnField";
            this.LedgerExtendedOverdueOnField.Properties.AllowDropDownWhenReadOnly = DevExpress.Utils.DefaultBoolean.False;
            this.LedgerExtendedOverdueOnField.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.LedgerExtendedOverdueOnField.Properties.CalendarTimeProperties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.LedgerExtendedOverdueOnField.Size = new System.Drawing.Size(84, 20);
            this.LedgerExtendedOverdueOnField.StyleController = this.MainLayoutControl;
            this.LedgerExtendedOverdueOnField.TabIndex = 33;
            // 
            // LedgerOverdueNoticeSentField
            // 
            this.LedgerOverdueNoticeSentField.EditValue = null;
            this.LedgerOverdueNoticeSentField.Location = new System.Drawing.Point(802, 91);
            this.LedgerOverdueNoticeSentField.Name = "LedgerOverdueNoticeSentField";
            this.LedgerOverdueNoticeSentField.Properties.AllowDropDownWhenReadOnly = DevExpress.Utils.DefaultBoolean.False;
            this.LedgerOverdueNoticeSentField.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.LedgerOverdueNoticeSentField.Properties.CalendarTimeProperties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.LedgerOverdueNoticeSentField.Size = new System.Drawing.Size(83, 20);
            this.LedgerOverdueNoticeSentField.StyleController = this.MainLayoutControl;
            this.LedgerOverdueNoticeSentField.TabIndex = 32;
            // 
            // LedgerExtendedLapsingOnField
            // 
            this.LedgerExtendedLapsingOnField.EditValue = null;
            this.LedgerExtendedLapsingOnField.Location = new System.Drawing.Point(1020, 91);
            this.LedgerExtendedLapsingOnField.Name = "LedgerExtendedLapsingOnField";
            this.LedgerExtendedLapsingOnField.Properties.AllowDropDownWhenReadOnly = DevExpress.Utils.DefaultBoolean.False;
            this.LedgerExtendedLapsingOnField.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.LedgerExtendedLapsingOnField.Properties.CalendarTimeProperties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.LedgerExtendedLapsingOnField.Size = new System.Drawing.Size(83, 20);
            this.LedgerExtendedLapsingOnField.StyleController = this.MainLayoutControl;
            this.LedgerExtendedLapsingOnField.TabIndex = 35;
            // 
            // LedgerFinalReminderSentField
            // 
            this.LedgerFinalReminderSentField.EditValue = null;
            this.LedgerFinalReminderSentField.Location = new System.Drawing.Point(802, 115);
            this.LedgerFinalReminderSentField.Name = "LedgerFinalReminderSentField";
            this.LedgerFinalReminderSentField.Properties.AllowDropDownWhenReadOnly = DevExpress.Utils.DefaultBoolean.False;
            this.LedgerFinalReminderSentField.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.LedgerFinalReminderSentField.Properties.CalendarTimeProperties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.LedgerFinalReminderSentField.Size = new System.Drawing.Size(83, 20);
            this.LedgerFinalReminderSentField.StyleController = this.MainLayoutControl;
            this.LedgerFinalReminderSentField.TabIndex = 34;
            // 
            // LedgerCancelledLapsedOnField
            // 
            this.LedgerCancelledLapsedOnField.EditValue = null;
            this.LedgerCancelledLapsedOnField.Location = new System.Drawing.Point(1020, 115);
            this.LedgerCancelledLapsedOnField.Name = "LedgerCancelledLapsedOnField";
            this.LedgerCancelledLapsedOnField.Properties.AllowDropDownWhenReadOnly = DevExpress.Utils.DefaultBoolean.False;
            this.LedgerCancelledLapsedOnField.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.LedgerCancelledLapsedOnField.Properties.CalendarTimeProperties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.LedgerCancelledLapsedOnField.Size = new System.Drawing.Size(83, 20);
            this.LedgerCancelledLapsedOnField.StyleController = this.MainLayoutControl;
            this.LedgerCancelledLapsedOnField.TabIndex = 36;
            // 
            // NotesLastModifiedOnField
            // 
            this.NotesLastModifiedOnField.EditValue = "01/01/1900 11:59:59";
            this.NotesLastModifiedOnField.Location = new System.Drawing.Point(1019, 636);
            this.NotesLastModifiedOnField.Name = "NotesLastModifiedOnField";
            this.NotesLastModifiedOnField.Properties.DisplayFormat.FormatString = "d";
            this.NotesLastModifiedOnField.Properties.DisplayFormat.FormatType = DevExpress.Utils.FormatType.DateTime;
            this.NotesLastModifiedOnField.Properties.EditFormat.FormatString = "d";
            this.NotesLastModifiedOnField.Properties.EditFormat.FormatType = DevExpress.Utils.FormatType.DateTime;
            this.NotesLastModifiedOnField.Properties.Mask.EditMask = "d";
            this.NotesLastModifiedOnField.Properties.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.DateTime;
            this.NotesLastModifiedOnField.Properties.ReadOnly = true;
            this.NotesLastModifiedOnField.Size = new System.Drawing.Size(186, 20);
            this.NotesLastModifiedOnField.StyleController = this.MainLayoutControl;
            this.NotesLastModifiedOnField.TabIndex = 61;
            // 
            // NotesLastModifiedByField
            // 
            this.NotesLastModifiedByField.Location = new System.Drawing.Point(1019, 612);
            this.NotesLastModifiedByField.Name = "NotesLastModifiedByField";
            this.NotesLastModifiedByField.Properties.ReadOnly = true;
            this.NotesLastModifiedByField.Size = new System.Drawing.Size(186, 20);
            this.NotesLastModifiedByField.StyleController = this.MainLayoutControl;
            this.NotesLastModifiedByField.TabIndex = 62;
            // 
            // NotesCreatedOnField
            // 
            this.NotesCreatedOnField.EditValue = "01/01/1900 11:59:59";
            this.NotesCreatedOnField.Location = new System.Drawing.Point(1019, 588);
            this.NotesCreatedOnField.Name = "NotesCreatedOnField";
            this.NotesCreatedOnField.Properties.DisplayFormat.FormatString = "d";
            this.NotesCreatedOnField.Properties.DisplayFormat.FormatType = DevExpress.Utils.FormatType.DateTime;
            this.NotesCreatedOnField.Properties.EditFormat.FormatString = "d";
            this.NotesCreatedOnField.Properties.EditFormat.FormatType = DevExpress.Utils.FormatType.DateTime;
            this.NotesCreatedOnField.Properties.Mask.EditMask = "d";
            this.NotesCreatedOnField.Properties.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.DateTime;
            this.NotesCreatedOnField.Properties.ReadOnly = true;
            this.NotesCreatedOnField.Size = new System.Drawing.Size(186, 20);
            this.NotesCreatedOnField.StyleController = this.MainLayoutControl;
            this.NotesCreatedOnField.TabIndex = 63;
            // 
            // NotesCreatedByField
            // 
            this.NotesCreatedByField.EditValue = "Chris Lamb";
            this.NotesCreatedByField.Location = new System.Drawing.Point(1019, 564);
            this.NotesCreatedByField.Name = "NotesCreatedByField";
            this.NotesCreatedByField.Properties.ReadOnly = true;
            this.NotesCreatedByField.Size = new System.Drawing.Size(186, 20);
            this.NotesCreatedByField.StyleController = this.MainLayoutControl;
            this.NotesCreatedByField.TabIndex = 64;
            // 
            // SchemePaidField
            // 
            this.SchemePaidField.EditValue = null;
            this.SchemePaidField.Location = new System.Drawing.Point(1019, 446);
            this.SchemePaidField.Name = "SchemePaidField";
            this.SchemePaidField.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.SchemePaidField.Properties.CalendarTimeProperties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.SchemePaidField.Properties.ReadOnly = true;
            this.SchemePaidField.Size = new System.Drawing.Size(186, 20);
            this.SchemePaidField.StyleController = this.MainLayoutControl;
            this.SchemePaidField.TabIndex = 65;
            // 
            // EasyDocsEmailAddressField
            // 
            this.EasyDocsEmailAddressField.Location = new System.Drawing.Point(492, 115);
            this.EasyDocsEmailAddressField.Name = "EasyDocsEmailAddressField";
            this.EasyDocsEmailAddressField.Size = new System.Drawing.Size(294, 20);
            this.EasyDocsEmailAddressField.StyleController = this.MainLayoutControl;
            this.EasyDocsEmailAddressField.TabIndex = 164;
            // 
            // EasyDocsPrimaryAssociateField
            // 
            this.EasyDocsPrimaryAssociateField.Location = new System.Drawing.Point(492, 91);
            this.EasyDocsPrimaryAssociateField.Name = "EasyDocsPrimaryAssociateField";
            this.EasyDocsPrimaryAssociateField.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Glyph)});
            this.EasyDocsPrimaryAssociateField.Size = new System.Drawing.Size(294, 20);
            this.EasyDocsPrimaryAssociateField.StyleController = this.MainLayoutControl;
            this.EasyDocsPrimaryAssociateField.TabIndex = 165;
            // 
            // EasyDocsMobilePhoneField
            // 
            this.EasyDocsMobilePhoneField.Location = new System.Drawing.Point(901, 115);
            this.EasyDocsMobilePhoneField.Name = "EasyDocsMobilePhoneField";
            this.EasyDocsMobilePhoneField.Size = new System.Drawing.Size(316, 20);
            this.EasyDocsMobilePhoneField.StyleController = this.MainLayoutControl;
            this.EasyDocsMobilePhoneField.TabIndex = 163;
            // 
            // LLReceivesElectronicCommunicationsField
            // 
            this.LLReceivesElectronicCommunicationsField.AllowTriState = false;
            this.LLReceivesElectronicCommunicationsField.DefaultValue = System.Windows.Forms.CheckState.Unchecked;
            this.LLReceivesElectronicCommunicationsField.DisplayFocusCues = true;
            this.LLReceivesElectronicCommunicationsField.EditValue = null;
            this.LLReceivesElectronicCommunicationsField.IndeterminateBackColor = System.Drawing.Color.Silver;
            this.LLReceivesElectronicCommunicationsField.IndeterminateToggleColor = System.Drawing.Color.Gainsboro;
            this.LLReceivesElectronicCommunicationsField.Location = new System.Drawing.Point(172, 91);
            this.LLReceivesElectronicCommunicationsField.MaximumSize = new System.Drawing.Size(0, 21);
            this.LLReceivesElectronicCommunicationsField.MinimumSize = new System.Drawing.Size(20, 21);
            this.LLReceivesElectronicCommunicationsField.Name = "LLReceivesElectronicCommunicationsField";
            this.LLReceivesElectronicCommunicationsField.OffBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(222)))), ((int)(((byte)(82)))), ((int)(((byte)(70)))));
            this.LLReceivesElectronicCommunicationsField.OffToggleColor = System.Drawing.Color.WhiteSmoke;
            this.LLReceivesElectronicCommunicationsField.OnBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(26)))), ((int)(((byte)(162)))), ((int)(((byte)(96)))));
            this.LLReceivesElectronicCommunicationsField.OnToggleColor = System.Drawing.Color.WhiteSmoke;
            this.LLReceivesElectronicCommunicationsField.Properties.AutoHeight = false;
            this.LLReceivesElectronicCommunicationsField.Properties.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.Default;
            this.LLReceivesElectronicCommunicationsField.Properties.Caption = "FancyCheckEdit";
            this.LLReceivesElectronicCommunicationsField.Properties.UseDefaultMode = true;
            this.LLReceivesElectronicCommunicationsField.Size = new System.Drawing.Size(48, 21);
            this.LLReceivesElectronicCommunicationsField.StyleController = this.MainLayoutControl;
            this.LLReceivesElectronicCommunicationsField.TabIndex = 21;
            // 
            // REAReceivesElectronicCommunicationsField
            // 
            this.REAReceivesElectronicCommunicationsField.AllowTriState = false;
            this.REAReceivesElectronicCommunicationsField.DefaultValue = System.Windows.Forms.CheckState.Unchecked;
            this.REAReceivesElectronicCommunicationsField.DisplayFocusCues = true;
            this.REAReceivesElectronicCommunicationsField.EditValue = null;
            this.REAReceivesElectronicCommunicationsField.IndeterminateBackColor = System.Drawing.Color.Silver;
            this.REAReceivesElectronicCommunicationsField.IndeterminateToggleColor = System.Drawing.Color.Gainsboro;
            this.REAReceivesElectronicCommunicationsField.Location = new System.Drawing.Point(172, 129);
            this.REAReceivesElectronicCommunicationsField.MaximumSize = new System.Drawing.Size(0, 21);
            this.REAReceivesElectronicCommunicationsField.MinimumSize = new System.Drawing.Size(20, 21);
            this.REAReceivesElectronicCommunicationsField.Name = "REAReceivesElectronicCommunicationsField";
            this.REAReceivesElectronicCommunicationsField.OffBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(222)))), ((int)(((byte)(82)))), ((int)(((byte)(70)))));
            this.REAReceivesElectronicCommunicationsField.OffToggleColor = System.Drawing.Color.WhiteSmoke;
            this.REAReceivesElectronicCommunicationsField.OnBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(26)))), ((int)(((byte)(162)))), ((int)(((byte)(96)))));
            this.REAReceivesElectronicCommunicationsField.OnToggleColor = System.Drawing.Color.WhiteSmoke;
            this.REAReceivesElectronicCommunicationsField.Properties.AutoHeight = false;
            this.REAReceivesElectronicCommunicationsField.Properties.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.Default;
            this.REAReceivesElectronicCommunicationsField.Properties.Caption = "FancyCheckEdit";
            this.REAReceivesElectronicCommunicationsField.Properties.UseDefaultMode = true;
            this.REAReceivesElectronicCommunicationsField.Size = new System.Drawing.Size(48, 21);
            this.REAReceivesElectronicCommunicationsField.StyleController = this.MainLayoutControl;
            this.REAReceivesElectronicCommunicationsField.TabIndex = 162;
            // 
            // DenialReasonField
            // 
            this.DenialReasonField.Location = new System.Drawing.Point(160, 147);
            this.DenialReasonField.Name = "DenialReasonField";
            this.DenialReasonField.Size = new System.Drawing.Size(455, 35);
            this.DenialReasonField.StyleController = this.MainLayoutControl;
            this.DenialReasonField.TabIndex = 74;
            // 
            // SpecialTermsField
            // 
            this.SpecialTermsField.Location = new System.Drawing.Point(730, 147);
            this.SpecialTermsField.Name = "SpecialTermsField";
            this.SpecialTermsField.Size = new System.Drawing.Size(475, 35);
            this.SpecialTermsField.StyleController = this.MainLayoutControl;
            this.SpecialTermsField.TabIndex = 75;
            // 
            // DisclosureItem1Field
            // 
            this.DisclosureItem1Field.AllowTriState = false;
            this.DisclosureItem1Field.DefaultValue = System.Windows.Forms.CheckState.Unchecked;
            this.DisclosureItem1Field.DisplayFocusCues = true;
            this.DisclosureItem1Field.EditValue = null;
            this.DisclosureItem1Field.IndeterminateBackColor = System.Drawing.Color.Silver;
            this.DisclosureItem1Field.IndeterminateToggleColor = System.Drawing.Color.Gainsboro;
            this.DisclosureItem1Field.Location = new System.Drawing.Point(879, 121);
            this.DisclosureItem1Field.MaximumSize = new System.Drawing.Size(48, 21);
            this.DisclosureItem1Field.MinimumSize = new System.Drawing.Size(48, 21);
            this.DisclosureItem1Field.Name = "DisclosureItem1Field";
            this.DisclosureItem1Field.OffBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(222)))), ((int)(((byte)(82)))), ((int)(((byte)(70)))));
            this.DisclosureItem1Field.OffToggleColor = System.Drawing.Color.WhiteSmoke;
            this.DisclosureItem1Field.OnBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(26)))), ((int)(((byte)(162)))), ((int)(((byte)(96)))));
            this.DisclosureItem1Field.OnToggleColor = System.Drawing.Color.WhiteSmoke;
            this.DisclosureItem1Field.Properties.AutoHeight = false;
            this.DisclosureItem1Field.Properties.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.Default;
            this.DisclosureItem1Field.Properties.Caption = "FancyCheckEdit";
            this.DisclosureItem1Field.Properties.UseDefaultMode = true;
            this.DisclosureItem1Field.Size = new System.Drawing.Size(48, 21);
            this.DisclosureItem1Field.StyleController = this.MainLayoutControl;
            this.DisclosureItem1Field.TabIndex = 16;
            // 
            // DisclosureItem2Field
            // 
            this.DisclosureItem2Field.AllowTriState = false;
            this.DisclosureItem2Field.DefaultValue = System.Windows.Forms.CheckState.Unchecked;
            this.DisclosureItem2Field.DisplayFocusCues = true;
            this.DisclosureItem2Field.EditValue = null;
            this.DisclosureItem2Field.IndeterminateBackColor = System.Drawing.Color.Silver;
            this.DisclosureItem2Field.IndeterminateToggleColor = System.Drawing.Color.Gainsboro;
            this.DisclosureItem2Field.Location = new System.Drawing.Point(879, 186);
            this.DisclosureItem2Field.MaximumSize = new System.Drawing.Size(48, 21);
            this.DisclosureItem2Field.MinimumSize = new System.Drawing.Size(48, 21);
            this.DisclosureItem2Field.Name = "DisclosureItem2Field";
            this.DisclosureItem2Field.OffBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(222)))), ((int)(((byte)(82)))), ((int)(((byte)(70)))));
            this.DisclosureItem2Field.OffToggleColor = System.Drawing.Color.WhiteSmoke;
            this.DisclosureItem2Field.OnBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(26)))), ((int)(((byte)(162)))), ((int)(((byte)(96)))));
            this.DisclosureItem2Field.OnToggleColor = System.Drawing.Color.WhiteSmoke;
            this.DisclosureItem2Field.Properties.AutoHeight = false;
            this.DisclosureItem2Field.Properties.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.Default;
            this.DisclosureItem2Field.Properties.Caption = "FancyCheckEdit";
            this.DisclosureItem2Field.Properties.UseDefaultMode = true;
            this.DisclosureItem2Field.Size = new System.Drawing.Size(48, 21);
            this.DisclosureItem2Field.StyleController = this.MainLayoutControl;
            this.DisclosureItem2Field.TabIndex = 17;
            // 
            // CircumstancesField
            // 
            this.CircumstancesField.Location = new System.Drawing.Point(160, 357);
            this.CircumstancesField.Name = "CircumstancesField";
            this.CircumstancesField.Size = new System.Drawing.Size(1045, 48);
            this.CircumstancesField.StyleController = this.MainLayoutControl;
            this.CircumstancesField.TabIndex = 70;
            // 
            // RestrictionsField
            // 
            this.RestrictionsField.Location = new System.Drawing.Point(160, 434);
            this.RestrictionsField.Name = "RestrictionsField";
            this.RestrictionsField.Size = new System.Drawing.Size(1045, 43);
            this.RestrictionsField.StyleController = this.MainLayoutControl;
            this.RestrictionsField.TabIndex = 71;
            // 
            // Disclosure3Field
            // 
            this.Disclosure3Field.AllowTriState = false;
            this.Disclosure3Field.DefaultValue = System.Windows.Forms.CheckState.Unchecked;
            this.Disclosure3Field.DisplayFocusCues = true;
            this.Disclosure3Field.EditValue = null;
            this.Disclosure3Field.IndeterminateBackColor = System.Drawing.Color.Silver;
            this.Disclosure3Field.IndeterminateToggleColor = System.Drawing.Color.Gainsboro;
            this.Disclosure3Field.Location = new System.Drawing.Point(879, 301);
            this.Disclosure3Field.MaximumSize = new System.Drawing.Size(48, 21);
            this.Disclosure3Field.MinimumSize = new System.Drawing.Size(48, 21);
            this.Disclosure3Field.Name = "Disclosure3Field";
            this.Disclosure3Field.OffBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(222)))), ((int)(((byte)(82)))), ((int)(((byte)(70)))));
            this.Disclosure3Field.OffToggleColor = System.Drawing.Color.WhiteSmoke;
            this.Disclosure3Field.OnBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(26)))), ((int)(((byte)(162)))), ((int)(((byte)(96)))));
            this.Disclosure3Field.OnToggleColor = System.Drawing.Color.WhiteSmoke;
            this.Disclosure3Field.Properties.AutoHeight = false;
            this.Disclosure3Field.Properties.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.Default;
            this.Disclosure3Field.Properties.Caption = "FancyCheckEdit";
            this.Disclosure3Field.Properties.UseDefaultMode = true;
            this.Disclosure3Field.Size = new System.Drawing.Size(48, 21);
            this.Disclosure3Field.StyleController = this.MainLayoutControl;
            this.Disclosure3Field.TabIndex = 66;
            // 
            // Disclosure4Field
            // 
            this.Disclosure4Field.AllowTriState = false;
            this.Disclosure4Field.DefaultValue = System.Windows.Forms.CheckState.Unchecked;
            this.Disclosure4Field.DisplayFocusCues = true;
            this.Disclosure4Field.EditValue = null;
            this.Disclosure4Field.IndeterminateBackColor = System.Drawing.Color.Silver;
            this.Disclosure4Field.IndeterminateToggleColor = System.Drawing.Color.Gainsboro;
            this.Disclosure4Field.Location = new System.Drawing.Point(879, 329);
            this.Disclosure4Field.MaximumSize = new System.Drawing.Size(48, 21);
            this.Disclosure4Field.MinimumSize = new System.Drawing.Size(48, 21);
            this.Disclosure4Field.Name = "Disclosure4Field";
            this.Disclosure4Field.OffBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(222)))), ((int)(((byte)(82)))), ((int)(((byte)(70)))));
            this.Disclosure4Field.OffToggleColor = System.Drawing.Color.WhiteSmoke;
            this.Disclosure4Field.OnBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(26)))), ((int)(((byte)(162)))), ((int)(((byte)(96)))));
            this.Disclosure4Field.OnToggleColor = System.Drawing.Color.WhiteSmoke;
            this.Disclosure4Field.Properties.AutoHeight = false;
            this.Disclosure4Field.Properties.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.Default;
            this.Disclosure4Field.Properties.Caption = "FancyCheckEdit";
            this.Disclosure4Field.Properties.UseDefaultMode = true;
            this.Disclosure4Field.Size = new System.Drawing.Size(48, 21);
            this.Disclosure4Field.StyleController = this.MainLayoutControl;
            this.Disclosure4Field.TabIndex = 67;
            // 
            // RenewalMultiPropertyDiscountField
            // 
            this.RenewalMultiPropertyDiscountField.AllowTriState = false;
            this.RenewalMultiPropertyDiscountField.DefaultValue = System.Windows.Forms.CheckState.Unchecked;
            this.RenewalMultiPropertyDiscountField.DisplayFocusCues = true;
            this.RenewalMultiPropertyDiscountField.EditValue = null;
            this.RenewalMultiPropertyDiscountField.IndeterminateBackColor = System.Drawing.Color.Silver;
            this.RenewalMultiPropertyDiscountField.IndeterminateToggleColor = System.Drawing.Color.Gainsboro;
            this.RenewalMultiPropertyDiscountField.Location = new System.Drawing.Point(510, 378);
            this.RenewalMultiPropertyDiscountField.MaximumSize = new System.Drawing.Size(0, 21);
            this.RenewalMultiPropertyDiscountField.MinimumSize = new System.Drawing.Size(20, 21);
            this.RenewalMultiPropertyDiscountField.Name = "RenewalMultiPropertyDiscountField";
            this.RenewalMultiPropertyDiscountField.OffBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(222)))), ((int)(((byte)(82)))), ((int)(((byte)(70)))));
            this.RenewalMultiPropertyDiscountField.OffToggleColor = System.Drawing.Color.WhiteSmoke;
            this.RenewalMultiPropertyDiscountField.OnBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(26)))), ((int)(((byte)(162)))), ((int)(((byte)(96)))));
            this.RenewalMultiPropertyDiscountField.OnToggleColor = System.Drawing.Color.WhiteSmoke;
            this.RenewalMultiPropertyDiscountField.Properties.AutoHeight = false;
            this.RenewalMultiPropertyDiscountField.Properties.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.Default;
            this.RenewalMultiPropertyDiscountField.Properties.Caption = "FancyCheckEdit";
            this.RenewalMultiPropertyDiscountField.Properties.UseDefaultMode = true;
            this.RenewalMultiPropertyDiscountField.Size = new System.Drawing.Size(48, 21);
            this.RenewalMultiPropertyDiscountField.StyleController = this.MainLayoutControl;
            this.RenewalMultiPropertyDiscountField.TabIndex = 150;
            // 
            // RenewalsOnHoldField
            // 
            this.RenewalsOnHoldField.AllowTriState = false;
            this.RenewalsOnHoldField.DefaultValue = System.Windows.Forms.CheckState.Unchecked;
            this.RenewalsOnHoldField.DisplayFocusCues = true;
            this.RenewalsOnHoldField.EditValue = null;
            this.RenewalsOnHoldField.IndeterminateBackColor = System.Drawing.Color.Silver;
            this.RenewalsOnHoldField.IndeterminateToggleColor = System.Drawing.Color.Gainsboro;
            this.RenewalsOnHoldField.Location = new System.Drawing.Point(169, 403);
            this.RenewalsOnHoldField.MaximumSize = new System.Drawing.Size(0, 21);
            this.RenewalsOnHoldField.MinimumSize = new System.Drawing.Size(20, 21);
            this.RenewalsOnHoldField.Name = "RenewalsOnHoldField";
            this.RenewalsOnHoldField.OffBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(222)))), ((int)(((byte)(82)))), ((int)(((byte)(70)))));
            this.RenewalsOnHoldField.OffToggleColor = System.Drawing.Color.WhiteSmoke;
            this.RenewalsOnHoldField.OnBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(26)))), ((int)(((byte)(162)))), ((int)(((byte)(96)))));
            this.RenewalsOnHoldField.OnToggleColor = System.Drawing.Color.WhiteSmoke;
            this.RenewalsOnHoldField.Properties.AutoHeight = false;
            this.RenewalsOnHoldField.Properties.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.Default;
            this.RenewalsOnHoldField.Properties.Caption = "FancyCheckEdit";
            this.RenewalsOnHoldField.Properties.UseDefaultMode = true;
            this.RenewalsOnHoldField.Size = new System.Drawing.Size(48, 21);
            this.RenewalsOnHoldField.StyleController = this.MainLayoutControl;
            this.RenewalsOnHoldField.TabIndex = 144;
            // 
            // DoNotRenewField
            // 
            this.DoNotRenewField.AllowTriState = false;
            this.DoNotRenewField.DefaultValue = System.Windows.Forms.CheckState.Unchecked;
            this.DoNotRenewField.DisplayFocusCues = true;
            this.DoNotRenewField.EditValue = null;
            this.DoNotRenewField.IndeterminateBackColor = System.Drawing.Color.Silver;
            this.DoNotRenewField.IndeterminateToggleColor = System.Drawing.Color.Gainsboro;
            this.DoNotRenewField.Location = new System.Drawing.Point(169, 428);
            this.DoNotRenewField.MaximumSize = new System.Drawing.Size(0, 21);
            this.DoNotRenewField.MinimumSize = new System.Drawing.Size(20, 21);
            this.DoNotRenewField.Name = "DoNotRenewField";
            this.DoNotRenewField.OffBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(222)))), ((int)(((byte)(82)))), ((int)(((byte)(70)))));
            this.DoNotRenewField.OffToggleColor = System.Drawing.Color.WhiteSmoke;
            this.DoNotRenewField.OnBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(26)))), ((int)(((byte)(162)))), ((int)(((byte)(96)))));
            this.DoNotRenewField.OnToggleColor = System.Drawing.Color.WhiteSmoke;
            this.DoNotRenewField.Properties.AutoHeight = false;
            this.DoNotRenewField.Properties.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.Default;
            this.DoNotRenewField.Properties.Caption = "FancyCheckEdit";
            this.DoNotRenewField.Properties.UseDefaultMode = true;
            this.DoNotRenewField.Size = new System.Drawing.Size(48, 21);
            this.DoNotRenewField.StyleController = this.MainLayoutControl;
            this.DoNotRenewField.TabIndex = 145;
            // 
            // RenewalImposedExcessField
            // 
            this.RenewalImposedExcessField.AllowTriState = false;
            this.RenewalImposedExcessField.DefaultValue = System.Windows.Forms.CheckState.Unchecked;
            this.RenewalImposedExcessField.DisplayFocusCues = true;
            this.RenewalImposedExcessField.EditValue = null;
            this.RenewalImposedExcessField.IndeterminateBackColor = System.Drawing.Color.Silver;
            this.RenewalImposedExcessField.IndeterminateToggleColor = System.Drawing.Color.Gainsboro;
            this.RenewalImposedExcessField.Location = new System.Drawing.Point(169, 501);
            this.RenewalImposedExcessField.MaximumSize = new System.Drawing.Size(0, 21);
            this.RenewalImposedExcessField.MinimumSize = new System.Drawing.Size(20, 21);
            this.RenewalImposedExcessField.Name = "RenewalImposedExcessField";
            this.RenewalImposedExcessField.OffBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(222)))), ((int)(((byte)(82)))), ((int)(((byte)(70)))));
            this.RenewalImposedExcessField.OffToggleColor = System.Drawing.Color.WhiteSmoke;
            this.RenewalImposedExcessField.OnBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(26)))), ((int)(((byte)(162)))), ((int)(((byte)(96)))));
            this.RenewalImposedExcessField.OnToggleColor = System.Drawing.Color.WhiteSmoke;
            this.RenewalImposedExcessField.Properties.AutoHeight = false;
            this.RenewalImposedExcessField.Properties.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.Default;
            this.RenewalImposedExcessField.Properties.Caption = "FancyCheckEdit";
            this.RenewalImposedExcessField.Properties.UseDefaultMode = true;
            this.RenewalImposedExcessField.Size = new System.Drawing.Size(37, 21);
            this.RenewalImposedExcessField.StyleController = this.MainLayoutControl;
            this.RenewalImposedExcessField.TabIndex = 146;
            // 
            // RealEstateDiscountField
            // 
            this.RealEstateDiscountField.AllowTriState = false;
            this.RealEstateDiscountField.DefaultValue = System.Windows.Forms.CheckState.Unchecked;
            this.RealEstateDiscountField.DisplayFocusCues = true;
            this.RealEstateDiscountField.EditValue = null;
            this.RealEstateDiscountField.IndeterminateBackColor = System.Drawing.Color.Silver;
            this.RealEstateDiscountField.IndeterminateToggleColor = System.Drawing.Color.Gainsboro;
            this.RealEstateDiscountField.Location = new System.Drawing.Point(510, 403);
            this.RealEstateDiscountField.MaximumSize = new System.Drawing.Size(0, 21);
            this.RealEstateDiscountField.MinimumSize = new System.Drawing.Size(20, 21);
            this.RealEstateDiscountField.Name = "RealEstateDiscountField";
            this.RealEstateDiscountField.OffBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(222)))), ((int)(((byte)(82)))), ((int)(((byte)(70)))));
            this.RealEstateDiscountField.OffToggleColor = System.Drawing.Color.WhiteSmoke;
            this.RealEstateDiscountField.OnBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(26)))), ((int)(((byte)(162)))), ((int)(((byte)(96)))));
            this.RealEstateDiscountField.OnToggleColor = System.Drawing.Color.WhiteSmoke;
            this.RealEstateDiscountField.Properties.AutoHeight = false;
            this.RealEstateDiscountField.Properties.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.Default;
            this.RealEstateDiscountField.Properties.Caption = "FancyCheckEdit";
            this.RealEstateDiscountField.Properties.UseDefaultMode = true;
            this.RealEstateDiscountField.Size = new System.Drawing.Size(48, 21);
            this.RealEstateDiscountField.StyleController = this.MainLayoutControl;
            this.RealEstateDiscountField.TabIndex = 151;
            // 
            // RenewalStaffDiscountField
            // 
            this.RenewalStaffDiscountField.AllowTriState = false;
            this.RenewalStaffDiscountField.DefaultValue = System.Windows.Forms.CheckState.Unchecked;
            this.RenewalStaffDiscountField.DisplayFocusCues = true;
            this.RenewalStaffDiscountField.EditValue = null;
            this.RenewalStaffDiscountField.IndeterminateBackColor = System.Drawing.Color.Silver;
            this.RenewalStaffDiscountField.IndeterminateToggleColor = System.Drawing.Color.Gainsboro;
            this.RenewalStaffDiscountField.Location = new System.Drawing.Point(510, 428);
            this.RenewalStaffDiscountField.MaximumSize = new System.Drawing.Size(0, 21);
            this.RenewalStaffDiscountField.MinimumSize = new System.Drawing.Size(20, 21);
            this.RenewalStaffDiscountField.Name = "RenewalStaffDiscountField";
            this.RenewalStaffDiscountField.OffBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(222)))), ((int)(((byte)(82)))), ((int)(((byte)(70)))));
            this.RenewalStaffDiscountField.OffToggleColor = System.Drawing.Color.WhiteSmoke;
            this.RenewalStaffDiscountField.OnBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(26)))), ((int)(((byte)(162)))), ((int)(((byte)(96)))));
            this.RenewalStaffDiscountField.OnToggleColor = System.Drawing.Color.WhiteSmoke;
            this.RenewalStaffDiscountField.Properties.AutoHeight = false;
            this.RenewalStaffDiscountField.Properties.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.Default;
            this.RenewalStaffDiscountField.Properties.Caption = "FancyCheckEdit";
            this.RenewalStaffDiscountField.Properties.UseDefaultMode = true;
            this.RenewalStaffDiscountField.Size = new System.Drawing.Size(48, 21);
            this.RenewalStaffDiscountField.StyleController = this.MainLayoutControl;
            this.RenewalStaffDiscountField.TabIndex = 152;
            // 
            // ApplyToRenewingPremiumField
            // 
            this.ApplyToRenewingPremiumField.AllowTriState = false;
            this.ApplyToRenewingPremiumField.DefaultValue = System.Windows.Forms.CheckState.Unchecked;
            this.ApplyToRenewingPremiumField.DisplayFocusCues = true;
            this.ApplyToRenewingPremiumField.EditValue = null;
            this.ApplyToRenewingPremiumField.IndeterminateBackColor = System.Drawing.Color.Silver;
            this.ApplyToRenewingPremiumField.IndeterminateToggleColor = System.Drawing.Color.Gainsboro;
            this.ApplyToRenewingPremiumField.Location = new System.Drawing.Point(510, 477);
            this.ApplyToRenewingPremiumField.MaximumSize = new System.Drawing.Size(0, 21);
            this.ApplyToRenewingPremiumField.MinimumSize = new System.Drawing.Size(20, 21);
            this.ApplyToRenewingPremiumField.Name = "ApplyToRenewingPremiumField";
            this.ApplyToRenewingPremiumField.OffBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(222)))), ((int)(((byte)(82)))), ((int)(((byte)(70)))));
            this.ApplyToRenewingPremiumField.OffToggleColor = System.Drawing.Color.WhiteSmoke;
            this.ApplyToRenewingPremiumField.OnBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(26)))), ((int)(((byte)(162)))), ((int)(((byte)(96)))));
            this.ApplyToRenewingPremiumField.OnToggleColor = System.Drawing.Color.WhiteSmoke;
            this.ApplyToRenewingPremiumField.Properties.AutoHeight = false;
            this.ApplyToRenewingPremiumField.Properties.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.Default;
            this.ApplyToRenewingPremiumField.Properties.Caption = "FancyCheckEdit";
            this.ApplyToRenewingPremiumField.Properties.UseDefaultMode = true;
            this.ApplyToRenewingPremiumField.Size = new System.Drawing.Size(48, 21);
            this.ApplyToRenewingPremiumField.StyleController = this.MainLayoutControl;
            this.ApplyToRenewingPremiumField.TabIndex = 153;
            // 
            // Disclosure5YesField
            // 
            this.Disclosure5YesField.EditValue = null;
            this.Disclosure5YesField.Location = new System.Drawing.Point(879, 409);
            this.Disclosure5YesField.MaximumSize = new System.Drawing.Size(0, 21);
            this.Disclosure5YesField.MinimumSize = new System.Drawing.Size(20, 21);
            this.Disclosure5YesField.Name = "Disclosure5YesField";
            this.Disclosure5YesField.Properties.AutoHeight = false;
            this.Disclosure5YesField.Properties.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.Default;
            this.Disclosure5YesField.Properties.Caption = "Yes";
            this.Disclosure5YesField.Properties.CheckBoxOptions.Style = DevExpress.XtraEditors.Controls.CheckBoxStyle.Radio;
            this.Disclosure5YesField.Size = new System.Drawing.Size(39, 21);
            this.Disclosure5YesField.StyleController = this.MainLayoutControl;
            this.Disclosure5YesField.TabIndex = 68;
            // 
            // ClassOfRiskField
            // 
            this.ClassOfRiskField.Location = new System.Drawing.Point(169, 121);
            this.ClassOfRiskField.Name = "ClassOfRiskField";
            this.ClassOfRiskField.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.ClassOfRiskField.Properties.NullText = "";
            this.ClassOfRiskField.Size = new System.Drawing.Size(151, 20);
            this.ClassOfRiskField.StyleController = this.MainLayoutControl;
            this.ClassOfRiskField.TabIndex = 13;
            // 
            // SelfManagedField
            // 
            this.SelfManagedField.Location = new System.Drawing.Point(169, 171);
            this.SelfManagedField.MenuManager = this.barManager1;
            this.SelfManagedField.Name = "SelfManagedField";
            this.SelfManagedField.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.SelfManagedField.Properties.NullText = "";
            this.SelfManagedField.Properties.PopupSizeable = false;
            this.SelfManagedField.Size = new System.Drawing.Size(68, 20);
            this.SelfManagedField.StyleController = this.MainLayoutControl;
            this.SelfManagedField.TabIndex = 204;
            // 
            // IsFurnishedField
            // 
            this.IsFurnishedField.Location = new System.Drawing.Point(169, 522);
            this.IsFurnishedField.MaximumSize = new System.Drawing.Size(0, 21);
            this.IsFurnishedField.MinimumSize = new System.Drawing.Size(30, 21);
            this.IsFurnishedField.Name = "IsFurnishedField";
            this.IsFurnishedField.Properties.AutoHeight = false;
            this.IsFurnishedField.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.IsFurnishedField.Properties.NullText = "";
            this.IsFurnishedField.Properties.PopupSizeable = false;
            this.IsFurnishedField.Size = new System.Drawing.Size(58, 21);
            this.IsFurnishedField.StyleController = this.MainLayoutControl;
            this.IsFurnishedField.TabIndex = 181;
            // 
            // IsCCTVField
            // 
            this.IsCCTVField.Location = new System.Drawing.Point(169, 548);
            this.IsCCTVField.MaximumSize = new System.Drawing.Size(0, 21);
            this.IsCCTVField.MinimumSize = new System.Drawing.Size(30, 21);
            this.IsCCTVField.Name = "IsCCTVField";
            this.IsCCTVField.Properties.AutoHeight = false;
            this.IsCCTVField.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.IsCCTVField.Properties.NullText = "";
            this.IsCCTVField.Properties.PopupSizeable = false;
            this.IsCCTVField.Size = new System.Drawing.Size(58, 21);
            this.IsCCTVField.StyleController = this.MainLayoutControl;
            this.IsCCTVField.TabIndex = 182;
            // 
            // IsElectronicAccessField
            // 
            this.IsElectronicAccessField.Location = new System.Drawing.Point(169, 573);
            this.IsElectronicAccessField.MaximumSize = new System.Drawing.Size(0, 21);
            this.IsElectronicAccessField.MinimumSize = new System.Drawing.Size(30, 21);
            this.IsElectronicAccessField.Name = "IsElectronicAccessField";
            this.IsElectronicAccessField.Properties.AutoHeight = false;
            this.IsElectronicAccessField.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.IsElectronicAccessField.Properties.NullText = "";
            this.IsElectronicAccessField.Properties.PopupSizeable = false;
            this.IsElectronicAccessField.Size = new System.Drawing.Size(58, 21);
            this.IsElectronicAccessField.StyleController = this.MainLayoutControl;
            this.IsElectronicAccessField.TabIndex = 183;
            // 
            // HolidayLetField
            // 
            this.HolidayLetField.Location = new System.Drawing.Point(169, 497);
            this.HolidayLetField.MaximumSize = new System.Drawing.Size(0, 21);
            this.HolidayLetField.MinimumSize = new System.Drawing.Size(30, 21);
            this.HolidayLetField.Name = "HolidayLetField";
            this.HolidayLetField.Properties.AutoHeight = false;
            this.HolidayLetField.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.HolidayLetField.Properties.NullText = "";
            this.HolidayLetField.Size = new System.Drawing.Size(58, 21);
            this.HolidayLetField.StyleController = this.MainLayoutControl;
            this.HolidayLetField.TabIndex = 180;
            // 
            // Root
            // 
            this.Root.EnableIndentsWithoutBorders = DevExpress.Utils.DefaultBoolean.True;
            this.Root.GroupBordersVisible = false;
            this.Root.Items.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.tabbedControlGroup1,
            this.ContactCodeHeaderLayoutControlItem,
            this.ContactNameHeaderLayoutControlItem,
            this.LandlordCodeHeaderLayoutControlItem,
            this.LandlordNameHeaderLayoutControlItem,
            this.TransactionNumberHeaderLayoutControlItem,
            this.TransactionStatusHeaderLayoutControlItem,
            this.TransactionRenewalHeaderLayoutControlItem,
            this.PremiumAmountHeaderLayoutControlItem,
            this.OutstandingAmountHeaderLayoutControlItem,
            this.ClaimCountHeaderLayoutControlItem});
            this.Root.Name = "Root";
            this.Root.Size = new System.Drawing.Size(1241, 692);
            this.Root.TextVisible = false;
            // 
            // tabbedControlGroup1
            // 
            this.tabbedControlGroup1.Location = new System.Drawing.Point(0, 42);
            this.tabbedControlGroup1.Name = "tabbedControlGroup1";
            this.tabbedControlGroup1.SelectedTabPage = this.PolicyTabGroup;
            this.tabbedControlGroup1.Size = new System.Drawing.Size(1221, 630);
            this.tabbedControlGroup1.TabPages.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.PolicyTabGroup,
            this.OtherDetailsTabGroup,
            this.DisclosureTabGroup,
            this.PremiumAndRenewalTabGroup,
            this.LedgerTabGroup,
            this.NotesTabGroup,
            this.EasyDocTabGroup,
            this.HistoryTabGroup,
            this.PDSAndFSGTabGroup});
            // 
            // PolicyTabGroup
            // 
            this.PolicyTabGroup.CaptionImageOptions.SvgImage = ((DevExpress.Utils.Svg.SvgImage)(resources.GetObject("PolicyTabGroup.CaptionImageOptions.SvgImage")));
            this.PolicyTabGroup.CaptionImageOptions.SvgImageSize = new System.Drawing.Size(16, 16);
            this.PolicyTabGroup.Items.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.PolicyDetailsGroup,
            this.PropertyGroup,
            this.DatesGroup,
            this.DiscountsGroup,
            this.emptySpaceItem35});
            this.PolicyTabGroup.Location = new System.Drawing.Point(0, 0);
            this.PolicyTabGroup.Name = "PolicyTabGroup";
            this.PolicyTabGroup.Size = new System.Drawing.Size(1197, 581);
            this.PolicyTabGroup.Text = "Policy";
            // 
            // PolicyDetailsGroup
            // 
            this.PolicyDetailsGroup.Items.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.PolicyClassLayoutControlItem,
            this.PremiumTypeLayoutControlItem,
            this.PolicyNumberLayoutControlItem,
            this.emptySpaceItem24,
            this.REACodeLayoutControlItem,
            this.NewBusinessArrangedByLayoutControlItem,
            this.DwellingTypeLayoutControlItem,
            this.ConstructionTypeLayoutControlItem,
            this.YearBuiltLayoutControlItem,
            this.CommonGroundsLayoutControlItem,
            this.emptySpaceItem34,
            this.CCTVLayoutControlItem,
            this.ElectronicAccessLayoutControlItem,
            this.BusinessOrTradeLayoutControlItem,
            this.FreeStandingLayoutControlItem,
            this.AcceptedBusinessUseLayoutControlItem,
            this.OnHoldLayoutControlItem,
            this.RewiredReplumbedLayoutControlItem,
            this.emptySpaceItem40,
            this.emptySpaceItem41,
            this.FurnishedLayoutControlItem,
            this.SelfManagedlayoutControlItem,
            this.HolidayLetLayoutControlItem});
            this.PolicyDetailsGroup.Location = new System.Drawing.Point(0, 0);
            this.PolicyDetailsGroup.Name = "PolicyDetailsGroup";
            this.PolicyDetailsGroup.Size = new System.Drawing.Size(312, 519);
            this.PolicyDetailsGroup.Text = "Policy Details";
            // 
            // PolicyClassLayoutControlItem
            // 
            this.PolicyClassLayoutControlItem.Control = this.ClassOfRiskField;
            this.PolicyClassLayoutControlItem.CustomizationFormText = "Class";
            this.PolicyClassLayoutControlItem.Location = new System.Drawing.Point(0, 0);
            this.PolicyClassLayoutControlItem.MaxSize = new System.Drawing.Size(288, 25);
            this.PolicyClassLayoutControlItem.MinSize = new System.Drawing.Size(288, 25);
            this.PolicyClassLayoutControlItem.Name = "PolicyClassLayoutControlItem";
            this.PolicyClassLayoutControlItem.Size = new System.Drawing.Size(288, 25);
            this.PolicyClassLayoutControlItem.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.PolicyClassLayoutControlItem.Text = "Class";
            this.PolicyClassLayoutControlItem.TextAlignMode = DevExpress.XtraLayout.TextAlignModeItem.CustomSize;
            this.PolicyClassLayoutControlItem.TextSize = new System.Drawing.Size(130, 13);
            this.PolicyClassLayoutControlItem.TextToControlDistance = 3;
            // 
            // PremiumTypeLayoutControlItem
            // 
            this.PremiumTypeLayoutControlItem.Control = this.PremiumTypeField;
            this.PremiumTypeLayoutControlItem.CustomizationFormText = "Premium Type";
            this.PremiumTypeLayoutControlItem.Location = new System.Drawing.Point(0, 25);
            this.PremiumTypeLayoutControlItem.MaxSize = new System.Drawing.Size(288, 25);
            this.PremiumTypeLayoutControlItem.MinSize = new System.Drawing.Size(288, 25);
            this.PremiumTypeLayoutControlItem.Name = "PremiumTypeLayoutControlItem";
            this.PremiumTypeLayoutControlItem.Size = new System.Drawing.Size(288, 25);
            this.PremiumTypeLayoutControlItem.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.PremiumTypeLayoutControlItem.Text = "Premium Type";
            this.PremiumTypeLayoutControlItem.TextAlignMode = DevExpress.XtraLayout.TextAlignModeItem.CustomSize;
            this.PremiumTypeLayoutControlItem.TextSize = new System.Drawing.Size(130, 13);
            this.PremiumTypeLayoutControlItem.TextToControlDistance = 3;
            // 
            // PolicyNumberLayoutControlItem
            // 
            this.PolicyNumberLayoutControlItem.Control = this.PolicyNumberField;
            this.PolicyNumberLayoutControlItem.CustomizationFormText = "Policy Number";
            this.PolicyNumberLayoutControlItem.Location = new System.Drawing.Point(0, 99);
            this.PolicyNumberLayoutControlItem.MinSize = new System.Drawing.Size(184, 24);
            this.PolicyNumberLayoutControlItem.Name = "PolicyNumberLayoutControlItem";
            this.PolicyNumberLayoutControlItem.Size = new System.Drawing.Size(288, 25);
            this.PolicyNumberLayoutControlItem.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.PolicyNumberLayoutControlItem.Text = "Policy Number";
            this.PolicyNumberLayoutControlItem.TextAlignMode = DevExpress.XtraLayout.TextAlignModeItem.CustomSize;
            this.PolicyNumberLayoutControlItem.TextSize = new System.Drawing.Size(130, 13);
            this.PolicyNumberLayoutControlItem.TextToControlDistance = 3;
            // 
            // emptySpaceItem24
            // 
            this.emptySpaceItem24.AllowHotTrack = false;
            this.emptySpaceItem24.CustomizationFormText = "emptySpaceItem24";
            this.emptySpaceItem24.Location = new System.Drawing.Point(205, 50);
            this.emptySpaceItem24.Name = "emptySpaceItem24";
            this.emptySpaceItem24.Size = new System.Drawing.Size(83, 24);
            this.emptySpaceItem24.TextSize = new System.Drawing.Size(0, 0);
            // 
            // REACodeLayoutControlItem
            // 
            this.REACodeLayoutControlItem.Control = this.REACodeField;
            this.REACodeLayoutControlItem.CustomizationFormText = "REA Code";
            this.REACodeLayoutControlItem.Location = new System.Drawing.Point(0, 124);
            this.REACodeLayoutControlItem.MinSize = new System.Drawing.Size(184, 26);
            this.REACodeLayoutControlItem.Name = "REACodeLayoutControlItem";
            this.REACodeLayoutControlItem.Size = new System.Drawing.Size(288, 27);
            this.REACodeLayoutControlItem.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.REACodeLayoutControlItem.Text = "REA Code";
            this.REACodeLayoutControlItem.TextAlignMode = DevExpress.XtraLayout.TextAlignModeItem.CustomSize;
            this.REACodeLayoutControlItem.TextSize = new System.Drawing.Size(130, 13);
            this.REACodeLayoutControlItem.TextToControlDistance = 3;
            // 
            // NewBusinessArrangedByLayoutControlItem
            // 
            this.NewBusinessArrangedByLayoutControlItem.Control = this.NewBusinessArrangedByCodeField;
            this.NewBusinessArrangedByLayoutControlItem.CustomizationFormText = "New Business Arranged By";
            this.NewBusinessArrangedByLayoutControlItem.Location = new System.Drawing.Point(0, 151);
            this.NewBusinessArrangedByLayoutControlItem.MinSize = new System.Drawing.Size(189, 26);
            this.NewBusinessArrangedByLayoutControlItem.Name = "NewBusinessArrangedByLayoutControlItem";
            this.NewBusinessArrangedByLayoutControlItem.Size = new System.Drawing.Size(288, 27);
            this.NewBusinessArrangedByLayoutControlItem.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.NewBusinessArrangedByLayoutControlItem.Text = "New Business Arranged By";
            this.NewBusinessArrangedByLayoutControlItem.TextAlignMode = DevExpress.XtraLayout.TextAlignModeItem.CustomSize;
            this.NewBusinessArrangedByLayoutControlItem.TextSize = new System.Drawing.Size(130, 13);
            this.NewBusinessArrangedByLayoutControlItem.TextToControlDistance = 3;
            // 
            // DwellingTypeLayoutControlItem
            // 
            this.DwellingTypeLayoutControlItem.Control = this.DwellingTypeField;
            this.DwellingTypeLayoutControlItem.CustomizationFormText = "Dwelling Type";
            this.DwellingTypeLayoutControlItem.Location = new System.Drawing.Point(0, 178);
            this.DwellingTypeLayoutControlItem.MinSize = new System.Drawing.Size(184, 24);
            this.DwellingTypeLayoutControlItem.Name = "DwellingTypeLayoutControlItem";
            this.DwellingTypeLayoutControlItem.Size = new System.Drawing.Size(288, 24);
            this.DwellingTypeLayoutControlItem.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.DwellingTypeLayoutControlItem.Text = "Dwelling Type";
            this.DwellingTypeLayoutControlItem.TextAlignMode = DevExpress.XtraLayout.TextAlignModeItem.CustomSize;
            this.DwellingTypeLayoutControlItem.TextSize = new System.Drawing.Size(130, 13);
            this.DwellingTypeLayoutControlItem.TextToControlDistance = 3;
            // 
            // ConstructionTypeLayoutControlItem
            // 
            this.ConstructionTypeLayoutControlItem.Control = this.ConstructionTypeField;
            this.ConstructionTypeLayoutControlItem.CustomizationFormText = "ConstructionType";
            this.ConstructionTypeLayoutControlItem.Location = new System.Drawing.Point(0, 202);
            this.ConstructionTypeLayoutControlItem.MinSize = new System.Drawing.Size(184, 24);
            this.ConstructionTypeLayoutControlItem.Name = "ConstructionTypeLayoutControlItem";
            this.ConstructionTypeLayoutControlItem.Size = new System.Drawing.Size(288, 24);
            this.ConstructionTypeLayoutControlItem.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.ConstructionTypeLayoutControlItem.Text = "ConstructionType";
            this.ConstructionTypeLayoutControlItem.TextAlignMode = DevExpress.XtraLayout.TextAlignModeItem.CustomSize;
            this.ConstructionTypeLayoutControlItem.TextSize = new System.Drawing.Size(130, 13);
            this.ConstructionTypeLayoutControlItem.TextToControlDistance = 3;
            // 
            // YearBuiltLayoutControlItem
            // 
            this.YearBuiltLayoutControlItem.Control = this.YearBuiltField;
            this.YearBuiltLayoutControlItem.CustomizationFormText = "Year Built";
            this.YearBuiltLayoutControlItem.Location = new System.Drawing.Point(0, 226);
            this.YearBuiltLayoutControlItem.MinSize = new System.Drawing.Size(184, 24);
            this.YearBuiltLayoutControlItem.Name = "YearBuiltLayoutControlItem";
            this.YearBuiltLayoutControlItem.Size = new System.Drawing.Size(288, 25);
            this.YearBuiltLayoutControlItem.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.YearBuiltLayoutControlItem.Text = "Year Built";
            this.YearBuiltLayoutControlItem.TextAlignMode = DevExpress.XtraLayout.TextAlignModeItem.CustomSize;
            this.YearBuiltLayoutControlItem.TextSize = new System.Drawing.Size(130, 13);
            this.YearBuiltLayoutControlItem.TextToControlDistance = 3;
            // 
            // CommonGroundsLayoutControlItem
            // 
            this.CommonGroundsLayoutControlItem.Control = this.CommonGroundsField;
            this.CommonGroundsLayoutControlItem.CustomizationFormText = "Common Grounds";
            this.CommonGroundsLayoutControlItem.Location = new System.Drawing.Point(0, 301);
            this.CommonGroundsLayoutControlItem.MaxSize = new System.Drawing.Size(185, 25);
            this.CommonGroundsLayoutControlItem.MinSize = new System.Drawing.Size(185, 25);
            this.CommonGroundsLayoutControlItem.Name = "CommonGroundsLayoutControlItem";
            this.CommonGroundsLayoutControlItem.Size = new System.Drawing.Size(185, 25);
            this.CommonGroundsLayoutControlItem.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.CommonGroundsLayoutControlItem.Text = "Common Grounds";
            this.CommonGroundsLayoutControlItem.TextAlignMode = DevExpress.XtraLayout.TextAlignModeItem.CustomSize;
            this.CommonGroundsLayoutControlItem.TextSize = new System.Drawing.Size(130, 13);
            this.CommonGroundsLayoutControlItem.TextToControlDistance = 3;
            // 
            // emptySpaceItem34
            // 
            this.emptySpaceItem34.AllowHotTrack = false;
            this.emptySpaceItem34.CustomizationFormText = "emptySpaceItem34";
            this.emptySpaceItem34.Location = new System.Drawing.Point(195, 251);
            this.emptySpaceItem34.Name = "emptySpaceItem34";
            this.emptySpaceItem34.Size = new System.Drawing.Size(93, 226);
            this.emptySpaceItem34.TextSize = new System.Drawing.Size(0, 0);
            // 
            // CCTVLayoutControlItem
            // 
            this.CCTVLayoutControlItem.Control = this.IsCCTVField;
            this.CCTVLayoutControlItem.CustomizationFormText = "CCTV";
            this.CCTVLayoutControlItem.Location = new System.Drawing.Point(0, 427);
            this.CCTVLayoutControlItem.MinSize = new System.Drawing.Size(50, 25);
            this.CCTVLayoutControlItem.Name = "CCTVLayoutControlItem";
            this.CCTVLayoutControlItem.Size = new System.Drawing.Size(195, 25);
            this.CCTVLayoutControlItem.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.CCTVLayoutControlItem.Text = "CCTV";
            this.CCTVLayoutControlItem.TextAlignMode = DevExpress.XtraLayout.TextAlignModeItem.CustomSize;
            this.CCTVLayoutControlItem.TextSize = new System.Drawing.Size(130, 13);
            this.CCTVLayoutControlItem.TextToControlDistance = 3;
            // 
            // ElectronicAccessLayoutControlItem
            // 
            this.ElectronicAccessLayoutControlItem.Control = this.IsElectronicAccessField;
            this.ElectronicAccessLayoutControlItem.CustomizationFormText = "Electronic Access";
            this.ElectronicAccessLayoutControlItem.Location = new System.Drawing.Point(0, 452);
            this.ElectronicAccessLayoutControlItem.MinSize = new System.Drawing.Size(50, 25);
            this.ElectronicAccessLayoutControlItem.Name = "ElectronicAccessLayoutControlItem";
            this.ElectronicAccessLayoutControlItem.Size = new System.Drawing.Size(195, 25);
            this.ElectronicAccessLayoutControlItem.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.ElectronicAccessLayoutControlItem.Text = "Electronic Access";
            this.ElectronicAccessLayoutControlItem.TextAlignMode = DevExpress.XtraLayout.TextAlignModeItem.CustomSize;
            this.ElectronicAccessLayoutControlItem.TextSize = new System.Drawing.Size(130, 13);
            this.ElectronicAccessLayoutControlItem.TextToControlDistance = 3;
            // 
            // BusinessOrTradeLayoutControlItem
            // 
            this.BusinessOrTradeLayoutControlItem.Control = this.BusinessOrTradeField;
            this.BusinessOrTradeLayoutControlItem.CustomizationFormText = "Business / Trade";
            this.BusinessOrTradeLayoutControlItem.Location = new System.Drawing.Point(0, 326);
            this.BusinessOrTradeLayoutControlItem.MaxSize = new System.Drawing.Size(185, 25);
            this.BusinessOrTradeLayoutControlItem.MinSize = new System.Drawing.Size(185, 25);
            this.BusinessOrTradeLayoutControlItem.Name = "BusinessOrTradeLayoutControlItem";
            this.BusinessOrTradeLayoutControlItem.Size = new System.Drawing.Size(185, 25);
            this.BusinessOrTradeLayoutControlItem.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.BusinessOrTradeLayoutControlItem.Text = "Business / Trade";
            this.BusinessOrTradeLayoutControlItem.TextAlignMode = DevExpress.XtraLayout.TextAlignModeItem.CustomSize;
            this.BusinessOrTradeLayoutControlItem.TextSize = new System.Drawing.Size(130, 13);
            this.BusinessOrTradeLayoutControlItem.TextToControlDistance = 3;
            // 
            // FreeStandingLayoutControlItem
            // 
            this.FreeStandingLayoutControlItem.Control = this.FreeStandingField;
            this.FreeStandingLayoutControlItem.CustomizationFormText = "Free Standing";
            this.FreeStandingLayoutControlItem.Location = new System.Drawing.Point(0, 276);
            this.FreeStandingLayoutControlItem.MaxSize = new System.Drawing.Size(185, 25);
            this.FreeStandingLayoutControlItem.MinSize = new System.Drawing.Size(185, 25);
            this.FreeStandingLayoutControlItem.Name = "FreeStandingLayoutControlItem";
            this.FreeStandingLayoutControlItem.Size = new System.Drawing.Size(185, 25);
            this.FreeStandingLayoutControlItem.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.FreeStandingLayoutControlItem.Text = "Free Standing";
            this.FreeStandingLayoutControlItem.TextAlignMode = DevExpress.XtraLayout.TextAlignModeItem.CustomSize;
            this.FreeStandingLayoutControlItem.TextSize = new System.Drawing.Size(130, 13);
            this.FreeStandingLayoutControlItem.TextToControlDistance = 3;
            // 
            // AcceptedBusinessUseLayoutControlItem
            // 
            this.AcceptedBusinessUseLayoutControlItem.Control = this.AcceptedBusinessUseField;
            this.AcceptedBusinessUseLayoutControlItem.CustomizationFormText = "Accepted Business Use";
            this.AcceptedBusinessUseLayoutControlItem.Location = new System.Drawing.Point(0, 351);
            this.AcceptedBusinessUseLayoutControlItem.MaxSize = new System.Drawing.Size(185, 25);
            this.AcceptedBusinessUseLayoutControlItem.MinSize = new System.Drawing.Size(185, 25);
            this.AcceptedBusinessUseLayoutControlItem.Name = "AcceptedBusinessUseLayoutControlItem";
            this.AcceptedBusinessUseLayoutControlItem.Size = new System.Drawing.Size(185, 25);
            this.AcceptedBusinessUseLayoutControlItem.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.AcceptedBusinessUseLayoutControlItem.Text = "Accepted Business Use";
            this.AcceptedBusinessUseLayoutControlItem.TextAlignMode = DevExpress.XtraLayout.TextAlignModeItem.CustomSize;
            this.AcceptedBusinessUseLayoutControlItem.TextSize = new System.Drawing.Size(130, 13);
            this.AcceptedBusinessUseLayoutControlItem.TextToControlDistance = 3;
            // 
            // OnHoldLayoutControlItem
            // 
            this.OnHoldLayoutControlItem.Control = this.OnHoldField;
            this.OnHoldLayoutControlItem.Location = new System.Drawing.Point(0, 74);
            this.OnHoldLayoutControlItem.MaxSize = new System.Drawing.Size(185, 25);
            this.OnHoldLayoutControlItem.MinSize = new System.Drawing.Size(185, 25);
            this.OnHoldLayoutControlItem.Name = "OnHoldLayoutControlItem";
            this.OnHoldLayoutControlItem.Size = new System.Drawing.Size(185, 25);
            this.OnHoldLayoutControlItem.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.OnHoldLayoutControlItem.Text = "On Hold";
            this.OnHoldLayoutControlItem.TextAlignMode = DevExpress.XtraLayout.TextAlignModeItem.CustomSize;
            this.OnHoldLayoutControlItem.TextSize = new System.Drawing.Size(130, 13);
            this.OnHoldLayoutControlItem.TextToControlDistance = 3;
            // 
            // RewiredReplumbedLayoutControlItem
            // 
            this.RewiredReplumbedLayoutControlItem.Control = this.RewiredReplumbedField;
            this.RewiredReplumbedLayoutControlItem.Location = new System.Drawing.Point(0, 251);
            this.RewiredReplumbedLayoutControlItem.MaxSize = new System.Drawing.Size(185, 25);
            this.RewiredReplumbedLayoutControlItem.MinSize = new System.Drawing.Size(185, 25);
            this.RewiredReplumbedLayoutControlItem.Name = "RewiredReplumbedLayoutControlItem";
            this.RewiredReplumbedLayoutControlItem.Size = new System.Drawing.Size(185, 25);
            this.RewiredReplumbedLayoutControlItem.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.RewiredReplumbedLayoutControlItem.Text = "Rewired && Replumbed";
            this.RewiredReplumbedLayoutControlItem.TextAlignMode = DevExpress.XtraLayout.TextAlignModeItem.CustomSize;
            this.RewiredReplumbedLayoutControlItem.TextSize = new System.Drawing.Size(130, 13);
            this.RewiredReplumbedLayoutControlItem.TextToControlDistance = 3;
            // 
            // emptySpaceItem40
            // 
            this.emptySpaceItem40.AllowHotTrack = false;
            this.emptySpaceItem40.Location = new System.Drawing.Point(185, 74);
            this.emptySpaceItem40.Name = "emptySpaceItem40";
            this.emptySpaceItem40.Size = new System.Drawing.Size(103, 25);
            this.emptySpaceItem40.TextSize = new System.Drawing.Size(0, 0);
            // 
            // emptySpaceItem41
            // 
            this.emptySpaceItem41.AllowHotTrack = false;
            this.emptySpaceItem41.Location = new System.Drawing.Point(185, 251);
            this.emptySpaceItem41.Name = "emptySpaceItem41";
            this.emptySpaceItem41.Size = new System.Drawing.Size(10, 125);
            this.emptySpaceItem41.TextSize = new System.Drawing.Size(0, 0);
            // 
            // FurnishedLayoutControlItem
            // 
            this.FurnishedLayoutControlItem.Control = this.IsFurnishedField;
            this.FurnishedLayoutControlItem.CustomizationFormText = "Furnished";
            this.FurnishedLayoutControlItem.Location = new System.Drawing.Point(0, 401);
            this.FurnishedLayoutControlItem.MinSize = new System.Drawing.Size(50, 25);
            this.FurnishedLayoutControlItem.Name = "FurnishedLayoutControlItem";
            this.FurnishedLayoutControlItem.Size = new System.Drawing.Size(195, 26);
            this.FurnishedLayoutControlItem.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.FurnishedLayoutControlItem.Text = "Furnished";
            this.FurnishedLayoutControlItem.TextAlignMode = DevExpress.XtraLayout.TextAlignModeItem.CustomSize;
            this.FurnishedLayoutControlItem.TextSize = new System.Drawing.Size(130, 13);
            this.FurnishedLayoutControlItem.TextToControlDistance = 3;
            // 
            // SelfManagedlayoutControlItem
            // 
            this.SelfManagedlayoutControlItem.Control = this.SelfManagedField;
            this.SelfManagedlayoutControlItem.Location = new System.Drawing.Point(0, 50);
            this.SelfManagedlayoutControlItem.Name = "SelfManagedlayoutControlItem";
            this.SelfManagedlayoutControlItem.Size = new System.Drawing.Size(205, 24);
            this.SelfManagedlayoutControlItem.Text = "Self Managed";
            this.SelfManagedlayoutControlItem.TextAlignMode = DevExpress.XtraLayout.TextAlignModeItem.CustomSize;
            this.SelfManagedlayoutControlItem.TextSize = new System.Drawing.Size(130, 13);
            this.SelfManagedlayoutControlItem.TextToControlDistance = 3;
            // 
            // HolidayLetLayoutControlItem
            // 
            this.HolidayLetLayoutControlItem.Control = this.HolidayLetField;
            this.HolidayLetLayoutControlItem.CustomizationFormText = "Holiday Let";
            this.HolidayLetLayoutControlItem.Location = new System.Drawing.Point(0, 376);
            this.HolidayLetLayoutControlItem.MinSize = new System.Drawing.Size(50, 25);
            this.HolidayLetLayoutControlItem.Name = "HolidayLetLayoutControlItem";
            this.HolidayLetLayoutControlItem.Size = new System.Drawing.Size(195, 25);
            this.HolidayLetLayoutControlItem.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.HolidayLetLayoutControlItem.Text = "Holiday Let";
            this.HolidayLetLayoutControlItem.TextAlignMode = DevExpress.XtraLayout.TextAlignModeItem.CustomSize;
            this.HolidayLetLayoutControlItem.TextSize = new System.Drawing.Size(130, 13);
            this.HolidayLetLayoutControlItem.TextToControlDistance = 3;
            // 
            // PropertyGroup
            // 
            this.PropertyGroup.Items.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.LandlordCodeLayoutControlItem,
            this.LandlordNameLayoutControlItem,
            this.InsuredPropertyAddressLayoutControlItem,
            this.BranchLayoutControlItem,
            this.BushfireLayoutControlItem,
            this.ContentsSumInsuredLayoutControlItem,
            this.BuildingSumInsuredLayoutControlItem,
            this.TenantedLayoutControlItem,
            this.emptySpaceItem3,
            this.emptySpaceItem4,
            this.FloodRiskLayoutControlItem,
            this.WeeklyRentSumInsuredLayoutControlItem,
            this.UnderwriterExcessLayoutControlItem,
            this.BuildingSumInsuredExcessLayoutControlItem,
            this.LandSizeLayoutControlItem,
            this.emptySpaceItem17,
            this.JobReducedCoronaVirusLayoutControlItem,
            this.ArrearsLayoutControlItem,
            this.WeeksInArrearsLayoutControlItem,
            this.emptySpaceItem1});
            this.PropertyGroup.Location = new System.Drawing.Point(312, 0);
            this.PropertyGroup.Name = "PropertyGroup";
            this.PropertyGroup.Size = new System.Drawing.Size(381, 519);
            this.PropertyGroup.Text = "Property";
            // 
            // LandlordCodeLayoutControlItem
            // 
            this.LandlordCodeLayoutControlItem.Control = this.LandlordCodeField;
            this.LandlordCodeLayoutControlItem.CustomizationFormText = "Landlord";
            this.LandlordCodeLayoutControlItem.Location = new System.Drawing.Point(0, 0);
            this.LandlordCodeLayoutControlItem.MaxSize = new System.Drawing.Size(357, 24);
            this.LandlordCodeLayoutControlItem.MinSize = new System.Drawing.Size(357, 24);
            this.LandlordCodeLayoutControlItem.Name = "LandlordCodeLayoutControlItem";
            this.LandlordCodeLayoutControlItem.Size = new System.Drawing.Size(357, 24);
            this.LandlordCodeLayoutControlItem.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.LandlordCodeLayoutControlItem.Text = "Landlord";
            this.LandlordCodeLayoutControlItem.TextAlignMode = DevExpress.XtraLayout.TextAlignModeItem.CustomSize;
            this.LandlordCodeLayoutControlItem.TextSize = new System.Drawing.Size(130, 13);
            this.LandlordCodeLayoutControlItem.TextToControlDistance = 3;
            // 
            // LandlordNameLayoutControlItem
            // 
            this.LandlordNameLayoutControlItem.Control = this.LandlordNameField;
            this.LandlordNameLayoutControlItem.CustomizationFormText = " ";
            this.LandlordNameLayoutControlItem.Location = new System.Drawing.Point(0, 24);
            this.LandlordNameLayoutControlItem.Name = "LandlordNameLayoutControlItem";
            this.LandlordNameLayoutControlItem.Size = new System.Drawing.Size(357, 24);
            this.LandlordNameLayoutControlItem.Text = " ";
            this.LandlordNameLayoutControlItem.TextAlignMode = DevExpress.XtraLayout.TextAlignModeItem.CustomSize;
            this.LandlordNameLayoutControlItem.TextSize = new System.Drawing.Size(130, 13);
            this.LandlordNameLayoutControlItem.TextToControlDistance = 3;
            // 
            // InsuredPropertyAddressLayoutControlItem
            // 
            this.InsuredPropertyAddressLayoutControlItem.CustomizationFormText = "Insured Property";
            this.InsuredPropertyAddressLayoutControlItem.Location = new System.Drawing.Point(0, 48);
            this.InsuredPropertyAddressLayoutControlItem.Name = "InsuredPropertyAddressLayoutControlItem";
            this.InsuredPropertyAddressLayoutControlItem.Size = new System.Drawing.Size(357, 27);
            this.InsuredPropertyAddressLayoutControlItem.Text = "Insured Property";
            this.InsuredPropertyAddressLayoutControlItem.TextAlignMode = DevExpress.XtraLayout.TextAlignModeItem.CustomSize;
            this.InsuredPropertyAddressLayoutControlItem.TextSize = new System.Drawing.Size(130, 13);
            this.InsuredPropertyAddressLayoutControlItem.TextToControlDistance = 3;
            // 
            // BranchLayoutControlItem
            // 
            this.BranchLayoutControlItem.Control = this.BranchField;
            this.BranchLayoutControlItem.CustomizationFormText = "Branch";
            this.BranchLayoutControlItem.Location = new System.Drawing.Point(0, 75);
            this.BranchLayoutControlItem.MaxSize = new System.Drawing.Size(267, 24);
            this.BranchLayoutControlItem.MinSize = new System.Drawing.Size(267, 24);
            this.BranchLayoutControlItem.Name = "BranchLayoutControlItem";
            this.BranchLayoutControlItem.Size = new System.Drawing.Size(267, 24);
            this.BranchLayoutControlItem.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.BranchLayoutControlItem.Text = "Branch";
            this.BranchLayoutControlItem.TextAlignMode = DevExpress.XtraLayout.TextAlignModeItem.CustomSize;
            this.BranchLayoutControlItem.TextSize = new System.Drawing.Size(130, 13);
            this.BranchLayoutControlItem.TextToControlDistance = 3;
            // 
            // BushfireLayoutControlItem
            // 
            this.BushfireLayoutControlItem.Control = this.BushfireRatingField;
            this.BushfireLayoutControlItem.CustomizationFormText = "Bushfire";
            this.BushfireLayoutControlItem.Location = new System.Drawing.Point(0, 123);
            this.BushfireLayoutControlItem.Name = "BushfireLayoutControlItem";
            this.BushfireLayoutControlItem.Size = new System.Drawing.Size(267, 24);
            this.BushfireLayoutControlItem.Text = "Bushfire";
            this.BushfireLayoutControlItem.TextAlignMode = DevExpress.XtraLayout.TextAlignModeItem.CustomSize;
            this.BushfireLayoutControlItem.TextSize = new System.Drawing.Size(130, 13);
            this.BushfireLayoutControlItem.TextToControlDistance = 3;
            // 
            // ContentsSumInsuredLayoutControlItem
            // 
            this.ContentsSumInsuredLayoutControlItem.Control = this.ContentsSumInsuredField;
            this.ContentsSumInsuredLayoutControlItem.CustomizationFormText = "Contents Sum Insured";
            this.ContentsSumInsuredLayoutControlItem.Location = new System.Drawing.Point(0, 171);
            this.ContentsSumInsuredLayoutControlItem.Name = "ContentsSumInsuredLayoutControlItem";
            this.ContentsSumInsuredLayoutControlItem.Size = new System.Drawing.Size(267, 24);
            this.ContentsSumInsuredLayoutControlItem.Text = "Contents Sum Insured";
            this.ContentsSumInsuredLayoutControlItem.TextAlignMode = DevExpress.XtraLayout.TextAlignModeItem.CustomSize;
            this.ContentsSumInsuredLayoutControlItem.TextSize = new System.Drawing.Size(130, 13);
            this.ContentsSumInsuredLayoutControlItem.TextToControlDistance = 3;
            // 
            // BuildingSumInsuredLayoutControlItem
            // 
            this.BuildingSumInsuredLayoutControlItem.Control = this.BuildingSumInsuredAmountField;
            this.BuildingSumInsuredLayoutControlItem.CustomizationFormText = "Building Sum Insured";
            this.BuildingSumInsuredLayoutControlItem.Location = new System.Drawing.Point(0, 195);
            this.BuildingSumInsuredLayoutControlItem.Name = "BuildingSumInsuredLayoutControlItem";
            this.BuildingSumInsuredLayoutControlItem.Size = new System.Drawing.Size(267, 24);
            this.BuildingSumInsuredLayoutControlItem.Text = "Building Sum Insured";
            this.BuildingSumInsuredLayoutControlItem.TextAlignMode = DevExpress.XtraLayout.TextAlignModeItem.CustomSize;
            this.BuildingSumInsuredLayoutControlItem.TextSize = new System.Drawing.Size(130, 13);
            this.BuildingSumInsuredLayoutControlItem.TextToControlDistance = 3;
            // 
            // TenantedLayoutControlItem
            // 
            this.TenantedLayoutControlItem.Control = this.TenantedField;
            this.TenantedLayoutControlItem.CustomizationFormText = "Tenanted";
            this.TenantedLayoutControlItem.Location = new System.Drawing.Point(0, 291);
            this.TenantedLayoutControlItem.MaxSize = new System.Drawing.Size(185, 25);
            this.TenantedLayoutControlItem.MinSize = new System.Drawing.Size(185, 25);
            this.TenantedLayoutControlItem.Name = "TenantedLayoutControlItem";
            this.TenantedLayoutControlItem.Size = new System.Drawing.Size(185, 25);
            this.TenantedLayoutControlItem.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.TenantedLayoutControlItem.Text = "Tenanted";
            this.TenantedLayoutControlItem.TextAlignMode = DevExpress.XtraLayout.TextAlignModeItem.CustomSize;
            this.TenantedLayoutControlItem.TextSize = new System.Drawing.Size(130, 13);
            this.TenantedLayoutControlItem.TextToControlDistance = 3;
            // 
            // emptySpaceItem3
            // 
            this.emptySpaceItem3.AllowHotTrack = false;
            this.emptySpaceItem3.CustomizationFormText = "emptySpaceItem3";
            this.emptySpaceItem3.Location = new System.Drawing.Point(267, 75);
            this.emptySpaceItem3.Name = "emptySpaceItem3";
            this.emptySpaceItem3.Size = new System.Drawing.Size(90, 216);
            this.emptySpaceItem3.TextSize = new System.Drawing.Size(0, 0);
            // 
            // emptySpaceItem4
            // 
            this.emptySpaceItem4.AllowHotTrack = false;
            this.emptySpaceItem4.CustomizationFormText = "emptySpaceItem4";
            this.emptySpaceItem4.Location = new System.Drawing.Point(0, 401);
            this.emptySpaceItem4.Name = "emptySpaceItem4";
            this.emptySpaceItem4.Size = new System.Drawing.Size(357, 76);
            this.emptySpaceItem4.TextSize = new System.Drawing.Size(0, 0);
            // 
            // FloodRiskLayoutControlItem
            // 
            this.FloodRiskLayoutControlItem.Control = this.FloodRiskRatingField;
            this.FloodRiskLayoutControlItem.CustomizationFormText = "Flood Risk";
            this.FloodRiskLayoutControlItem.Location = new System.Drawing.Point(0, 99);
            this.FloodRiskLayoutControlItem.Name = "FloodRiskLayoutControlItem";
            this.FloodRiskLayoutControlItem.Size = new System.Drawing.Size(267, 24);
            this.FloodRiskLayoutControlItem.Text = "Flood Risk";
            this.FloodRiskLayoutControlItem.TextAlignMode = DevExpress.XtraLayout.TextAlignModeItem.CustomSize;
            this.FloodRiskLayoutControlItem.TextSize = new System.Drawing.Size(130, 13);
            this.FloodRiskLayoutControlItem.TextToControlDistance = 3;
            // 
            // WeeklyRentSumInsuredLayoutControlItem
            // 
            this.WeeklyRentSumInsuredLayoutControlItem.Control = this.WeeklyRentSumInsuredAmountField;
            this.WeeklyRentSumInsuredLayoutControlItem.CustomizationFormText = "Weekly Rent Sum Insured";
            this.WeeklyRentSumInsuredLayoutControlItem.Location = new System.Drawing.Point(0, 147);
            this.WeeklyRentSumInsuredLayoutControlItem.Name = "WeeklyRentSumInsuredLayoutControlItem";
            this.WeeklyRentSumInsuredLayoutControlItem.Size = new System.Drawing.Size(267, 24);
            this.WeeklyRentSumInsuredLayoutControlItem.Text = "Weekly Rent Sum Insured";
            this.WeeklyRentSumInsuredLayoutControlItem.TextAlignMode = DevExpress.XtraLayout.TextAlignModeItem.CustomSize;
            this.WeeklyRentSumInsuredLayoutControlItem.TextSize = new System.Drawing.Size(130, 13);
            this.WeeklyRentSumInsuredLayoutControlItem.TextToControlDistance = 3;
            // 
            // UnderwriterExcessLayoutControlItem
            // 
            this.UnderwriterExcessLayoutControlItem.Control = this.UnderwriterExcessAmountField;
            this.UnderwriterExcessLayoutControlItem.CustomizationFormText = "Underwriter Excess";
            this.UnderwriterExcessLayoutControlItem.Location = new System.Drawing.Point(0, 243);
            this.UnderwriterExcessLayoutControlItem.Name = "UnderwriterExcessLayoutControlItem";
            this.UnderwriterExcessLayoutControlItem.Size = new System.Drawing.Size(267, 24);
            this.UnderwriterExcessLayoutControlItem.Text = "Underwriter Excess";
            this.UnderwriterExcessLayoutControlItem.TextAlignMode = DevExpress.XtraLayout.TextAlignModeItem.CustomSize;
            this.UnderwriterExcessLayoutControlItem.TextSize = new System.Drawing.Size(130, 13);
            this.UnderwriterExcessLayoutControlItem.TextToControlDistance = 3;
            // 
            // BuildingSumInsuredExcessLayoutControlItem
            // 
            this.BuildingSumInsuredExcessLayoutControlItem.Control = this.BuildingSumInsuredExcessAmountField;
            this.BuildingSumInsuredExcessLayoutControlItem.CustomizationFormText = "Building SI  Excess";
            this.BuildingSumInsuredExcessLayoutControlItem.Location = new System.Drawing.Point(0, 219);
            this.BuildingSumInsuredExcessLayoutControlItem.Name = "BuildingSumInsuredExcessLayoutControlItem";
            this.BuildingSumInsuredExcessLayoutControlItem.Size = new System.Drawing.Size(267, 24);
            this.BuildingSumInsuredExcessLayoutControlItem.Text = "Building SI  Excess";
            this.BuildingSumInsuredExcessLayoutControlItem.TextAlignMode = DevExpress.XtraLayout.TextAlignModeItem.CustomSize;
            this.BuildingSumInsuredExcessLayoutControlItem.TextSize = new System.Drawing.Size(130, 13);
            this.BuildingSumInsuredExcessLayoutControlItem.TextToControlDistance = 3;
            // 
            // LandSizeLayoutControlItem
            // 
            this.LandSizeLayoutControlItem.Control = this.LandSizeField;
            this.LandSizeLayoutControlItem.CustomizationFormText = "Land Size";
            this.LandSizeLayoutControlItem.Location = new System.Drawing.Point(0, 267);
            this.LandSizeLayoutControlItem.Name = "LandSizeLayoutControlItem";
            this.LandSizeLayoutControlItem.Size = new System.Drawing.Size(267, 24);
            this.LandSizeLayoutControlItem.Text = "Land Size";
            this.LandSizeLayoutControlItem.TextAlignMode = DevExpress.XtraLayout.TextAlignModeItem.CustomSize;
            this.LandSizeLayoutControlItem.TextSize = new System.Drawing.Size(130, 13);
            this.LandSizeLayoutControlItem.TextToControlDistance = 3;
            // 
            // emptySpaceItem17
            // 
            this.emptySpaceItem17.AllowHotTrack = false;
            this.emptySpaceItem17.CustomizationFormText = "emptySpaceItem17";
            this.emptySpaceItem17.Location = new System.Drawing.Point(185, 291);
            this.emptySpaceItem17.MinSize = new System.Drawing.Size(104, 24);
            this.emptySpaceItem17.Name = "emptySpaceItem17";
            this.emptySpaceItem17.Size = new System.Drawing.Size(172, 75);
            this.emptySpaceItem17.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.emptySpaceItem17.TextSize = new System.Drawing.Size(0, 0);
            // 
            // JobReducedCoronaVirusLayoutControlItem
            // 
            this.JobReducedCoronaVirusLayoutControlItem.Control = this.IsJobReducedDuePandemicField;
            this.JobReducedCoronaVirusLayoutControlItem.CustomizationFormText = "Job Reduced Due";
            this.JobReducedCoronaVirusLayoutControlItem.Location = new System.Drawing.Point(0, 316);
            this.JobReducedCoronaVirusLayoutControlItem.MaxSize = new System.Drawing.Size(185, 25);
            this.JobReducedCoronaVirusLayoutControlItem.MinSize = new System.Drawing.Size(185, 25);
            this.JobReducedCoronaVirusLayoutControlItem.Name = "JobReducedCoronaVirusLayoutControlItem";
            this.JobReducedCoronaVirusLayoutControlItem.Size = new System.Drawing.Size(185, 25);
            this.JobReducedCoronaVirusLayoutControlItem.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.JobReducedCoronaVirusLayoutControlItem.Text = "Job Reduced Due";
            this.JobReducedCoronaVirusLayoutControlItem.TextAlignMode = DevExpress.XtraLayout.TextAlignModeItem.CustomSize;
            this.JobReducedCoronaVirusLayoutControlItem.TextSize = new System.Drawing.Size(130, 13);
            this.JobReducedCoronaVirusLayoutControlItem.TextToControlDistance = 3;
            // 
            // ArrearsLayoutControlItem
            // 
            this.ArrearsLayoutControlItem.Control = this.IsArrearsField;
            this.ArrearsLayoutControlItem.CustomizationFormText = "Arrears";
            this.ArrearsLayoutControlItem.Location = new System.Drawing.Point(0, 341);
            this.ArrearsLayoutControlItem.MaxSize = new System.Drawing.Size(185, 25);
            this.ArrearsLayoutControlItem.MinSize = new System.Drawing.Size(185, 25);
            this.ArrearsLayoutControlItem.Name = "ArrearsLayoutControlItem";
            this.ArrearsLayoutControlItem.Size = new System.Drawing.Size(185, 25);
            this.ArrearsLayoutControlItem.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.ArrearsLayoutControlItem.Text = "Arrears";
            this.ArrearsLayoutControlItem.TextAlignMode = DevExpress.XtraLayout.TextAlignModeItem.CustomSize;
            this.ArrearsLayoutControlItem.TextSize = new System.Drawing.Size(130, 13);
            this.ArrearsLayoutControlItem.TextToControlDistance = 3;
            // 
            // WeeksInArrearsLayoutControlItem
            // 
            this.WeeksInArrearsLayoutControlItem.Control = this.WeeksInArrearsField;
            this.WeeksInArrearsLayoutControlItem.CustomizationFormText = "Weeks In Arrears";
            this.WeeksInArrearsLayoutControlItem.Location = new System.Drawing.Point(0, 366);
            this.WeeksInArrearsLayoutControlItem.MinSize = new System.Drawing.Size(183, 24);
            this.WeeksInArrearsLayoutControlItem.Name = "WeeksInArrearsLayoutControlItem";
            this.WeeksInArrearsLayoutControlItem.Size = new System.Drawing.Size(226, 35);
            this.WeeksInArrearsLayoutControlItem.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.WeeksInArrearsLayoutControlItem.Text = "Weeks In Arrears";
            this.WeeksInArrearsLayoutControlItem.TextAlignMode = DevExpress.XtraLayout.TextAlignModeItem.CustomSize;
            this.WeeksInArrearsLayoutControlItem.TextSize = new System.Drawing.Size(130, 13);
            this.WeeksInArrearsLayoutControlItem.TextToControlDistance = 3;
            // 
            // emptySpaceItem1
            // 
            this.emptySpaceItem1.AllowHotTrack = false;
            this.emptySpaceItem1.Location = new System.Drawing.Point(226, 366);
            this.emptySpaceItem1.Name = "emptySpaceItem1";
            this.emptySpaceItem1.Size = new System.Drawing.Size(131, 35);
            this.emptySpaceItem1.TextSize = new System.Drawing.Size(0, 0);
            // 
            // DatesGroup
            // 
            this.DatesGroup.Items.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.TransactionNumberLayoutControlItem,
            this.CreatedOnLayoutControlItem,
            this.InceptionFromLayoutControlItem,
            this.BalanceLayoutControlItem,
            this.EffectiveFromLayoutControlItem,
            this.BilledFromLayoutControlItem,
            this.ClosingOnLayoutControlItem,
            this.COISentOnLayoutControlItem,
            this.BilledToLayoutControlItem,
            this.RenewalOnLayoutControlItem,
            this.PolicySentOnLayoutControlItem,
            this.InvoicePrintedOnLayoutControlItem});
            this.DatesGroup.Location = new System.Drawing.Point(693, 0);
            this.DatesGroup.Name = "DatesGroup";
            this.DatesGroup.Size = new System.Drawing.Size(504, 186);
            this.DatesGroup.Text = "Dates";
            // 
            // TransactionNumberLayoutControlItem
            // 
            this.TransactionNumberLayoutControlItem.Control = this.TransactionNumberField;
            this.TransactionNumberLayoutControlItem.CustomizationFormText = "Transaction Number";
            this.TransactionNumberLayoutControlItem.Location = new System.Drawing.Point(0, 0);
            this.TransactionNumberLayoutControlItem.Name = "TransactionNumberLayoutControlItem";
            this.TransactionNumberLayoutControlItem.Size = new System.Drawing.Size(233, 24);
            this.TransactionNumberLayoutControlItem.Text = "Transaction Number";
            this.TransactionNumberLayoutControlItem.TextAlignMode = DevExpress.XtraLayout.TextAlignModeItem.CustomSize;
            this.TransactionNumberLayoutControlItem.TextSize = new System.Drawing.Size(130, 13);
            this.TransactionNumberLayoutControlItem.TextToControlDistance = 3;
            // 
            // CreatedOnLayoutControlItem
            // 
            this.CreatedOnLayoutControlItem.Control = this.CreatedOnField;
            this.CreatedOnLayoutControlItem.CustomizationFormText = "Created On";
            this.CreatedOnLayoutControlItem.Location = new System.Drawing.Point(0, 24);
            this.CreatedOnLayoutControlItem.Name = "CreatedOnLayoutControlItem";
            this.CreatedOnLayoutControlItem.Size = new System.Drawing.Size(233, 24);
            this.CreatedOnLayoutControlItem.Text = "Created On";
            this.CreatedOnLayoutControlItem.TextAlignMode = DevExpress.XtraLayout.TextAlignModeItem.CustomSize;
            this.CreatedOnLayoutControlItem.TextSize = new System.Drawing.Size(130, 13);
            this.CreatedOnLayoutControlItem.TextToControlDistance = 3;
            // 
            // InceptionFromLayoutControlItem
            // 
            this.InceptionFromLayoutControlItem.Control = this.InceptionFromField;
            this.InceptionFromLayoutControlItem.CustomizationFormText = "Inception From";
            this.InceptionFromLayoutControlItem.Location = new System.Drawing.Point(0, 48);
            this.InceptionFromLayoutControlItem.Name = "InceptionFromLayoutControlItem";
            this.InceptionFromLayoutControlItem.Size = new System.Drawing.Size(233, 24);
            this.InceptionFromLayoutControlItem.Text = "Inception From";
            this.InceptionFromLayoutControlItem.TextAlignMode = DevExpress.XtraLayout.TextAlignModeItem.CustomSize;
            this.InceptionFromLayoutControlItem.TextSize = new System.Drawing.Size(130, 13);
            this.InceptionFromLayoutControlItem.TextToControlDistance = 3;
            // 
            // BalanceLayoutControlItem
            // 
            this.BalanceLayoutControlItem.Control = this.BalanceOutstandingAmountField;
            this.BalanceLayoutControlItem.CustomizationFormText = "Balance";
            this.BalanceLayoutControlItem.Location = new System.Drawing.Point(233, 0);
            this.BalanceLayoutControlItem.Name = "BalanceLayoutControlItem";
            this.BalanceLayoutControlItem.Size = new System.Drawing.Size(247, 24);
            this.BalanceLayoutControlItem.Spacing = new DevExpress.XtraLayout.Utils.Padding(20, 0, 0, 0);
            this.BalanceLayoutControlItem.Text = "Balance";
            this.BalanceLayoutControlItem.TextAlignMode = DevExpress.XtraLayout.TextAlignModeItem.CustomSize;
            this.BalanceLayoutControlItem.TextSize = new System.Drawing.Size(110, 13);
            this.BalanceLayoutControlItem.TextToControlDistance = 3;
            // 
            // EffectiveFromLayoutControlItem
            // 
            this.EffectiveFromLayoutControlItem.Control = this.EffectiveFromField;
            this.EffectiveFromLayoutControlItem.CustomizationFormText = "Effective From";
            this.EffectiveFromLayoutControlItem.Location = new System.Drawing.Point(233, 24);
            this.EffectiveFromLayoutControlItem.Name = "EffectiveFromLayoutControlItem";
            this.EffectiveFromLayoutControlItem.Size = new System.Drawing.Size(247, 24);
            this.EffectiveFromLayoutControlItem.Spacing = new DevExpress.XtraLayout.Utils.Padding(20, 0, 0, 0);
            this.EffectiveFromLayoutControlItem.Text = "Effective From";
            this.EffectiveFromLayoutControlItem.TextAlignMode = DevExpress.XtraLayout.TextAlignModeItem.CustomSize;
            this.EffectiveFromLayoutControlItem.TextSize = new System.Drawing.Size(110, 13);
            this.EffectiveFromLayoutControlItem.TextToControlDistance = 3;
            // 
            // BilledFromLayoutControlItem
            // 
            this.BilledFromLayoutControlItem.Control = this.BilledFromField;
            this.BilledFromLayoutControlItem.CustomizationFormText = "Billed From";
            this.BilledFromLayoutControlItem.Location = new System.Drawing.Point(233, 48);
            this.BilledFromLayoutControlItem.Name = "BilledFromLayoutControlItem";
            this.BilledFromLayoutControlItem.Size = new System.Drawing.Size(247, 24);
            this.BilledFromLayoutControlItem.Spacing = new DevExpress.XtraLayout.Utils.Padding(20, 0, 0, 0);
            this.BilledFromLayoutControlItem.Text = "Billed From";
            this.BilledFromLayoutControlItem.TextAlignMode = DevExpress.XtraLayout.TextAlignModeItem.CustomSize;
            this.BilledFromLayoutControlItem.TextSize = new System.Drawing.Size(110, 13);
            this.BilledFromLayoutControlItem.TextToControlDistance = 3;
            // 
            // ClosingOnLayoutControlItem
            // 
            this.ClosingOnLayoutControlItem.Control = this.ClosingOnField;
            this.ClosingOnLayoutControlItem.CustomizationFormText = "Closing On";
            this.ClosingOnLayoutControlItem.Location = new System.Drawing.Point(0, 72);
            this.ClosingOnLayoutControlItem.Name = "ClosingOnLayoutControlItem";
            this.ClosingOnLayoutControlItem.Size = new System.Drawing.Size(233, 24);
            this.ClosingOnLayoutControlItem.Text = "Closing On";
            this.ClosingOnLayoutControlItem.TextAlignMode = DevExpress.XtraLayout.TextAlignModeItem.CustomSize;
            this.ClosingOnLayoutControlItem.TextSize = new System.Drawing.Size(130, 13);
            this.ClosingOnLayoutControlItem.TextToControlDistance = 3;
            // 
            // COISentOnLayoutControlItem
            // 
            this.COISentOnLayoutControlItem.Control = this.COISentOnField;
            this.COISentOnLayoutControlItem.CustomizationFormText = "COI Sent On";
            this.COISentOnLayoutControlItem.Location = new System.Drawing.Point(0, 96);
            this.COISentOnLayoutControlItem.Name = "COISentOnLayoutControlItem";
            this.COISentOnLayoutControlItem.Size = new System.Drawing.Size(233, 24);
            this.COISentOnLayoutControlItem.Text = "COI Sent On";
            this.COISentOnLayoutControlItem.TextAlignMode = DevExpress.XtraLayout.TextAlignModeItem.CustomSize;
            this.COISentOnLayoutControlItem.TextSize = new System.Drawing.Size(130, 13);
            this.COISentOnLayoutControlItem.TextToControlDistance = 3;
            // 
            // BilledToLayoutControlItem
            // 
            this.BilledToLayoutControlItem.Control = this.BilledToField;
            this.BilledToLayoutControlItem.CustomizationFormText = "Billed To";
            this.BilledToLayoutControlItem.Location = new System.Drawing.Point(233, 72);
            this.BilledToLayoutControlItem.Name = "BilledToLayoutControlItem";
            this.BilledToLayoutControlItem.Size = new System.Drawing.Size(247, 24);
            this.BilledToLayoutControlItem.Spacing = new DevExpress.XtraLayout.Utils.Padding(20, 0, 0, 0);
            this.BilledToLayoutControlItem.Text = "Billed To";
            this.BilledToLayoutControlItem.TextAlignMode = DevExpress.XtraLayout.TextAlignModeItem.CustomSize;
            this.BilledToLayoutControlItem.TextSize = new System.Drawing.Size(110, 13);
            this.BilledToLayoutControlItem.TextToControlDistance = 3;
            // 
            // RenewalOnLayoutControlItem
            // 
            this.RenewalOnLayoutControlItem.Control = this.RenewalOnField;
            this.RenewalOnLayoutControlItem.CustomizationFormText = "Renewal On";
            this.RenewalOnLayoutControlItem.Location = new System.Drawing.Point(233, 96);
            this.RenewalOnLayoutControlItem.Name = "RenewalOnLayoutControlItem";
            this.RenewalOnLayoutControlItem.Size = new System.Drawing.Size(247, 24);
            this.RenewalOnLayoutControlItem.Spacing = new DevExpress.XtraLayout.Utils.Padding(20, 0, 0, 0);
            this.RenewalOnLayoutControlItem.Text = "Renewal On";
            this.RenewalOnLayoutControlItem.TextAlignMode = DevExpress.XtraLayout.TextAlignModeItem.CustomSize;
            this.RenewalOnLayoutControlItem.TextSize = new System.Drawing.Size(110, 13);
            this.RenewalOnLayoutControlItem.TextToControlDistance = 3;
            // 
            // PolicySentOnLayoutControlItem
            // 
            this.PolicySentOnLayoutControlItem.Control = this.PolicySentOnField;
            this.PolicySentOnLayoutControlItem.CustomizationFormText = "Policy Sent On";
            this.PolicySentOnLayoutControlItem.Location = new System.Drawing.Point(0, 120);
            this.PolicySentOnLayoutControlItem.Name = "PolicySentOnLayoutControlItem";
            this.PolicySentOnLayoutControlItem.Size = new System.Drawing.Size(233, 24);
            this.PolicySentOnLayoutControlItem.Text = "Policy Sent On";
            this.PolicySentOnLayoutControlItem.TextAlignMode = DevExpress.XtraLayout.TextAlignModeItem.CustomSize;
            this.PolicySentOnLayoutControlItem.TextSize = new System.Drawing.Size(130, 13);
            this.PolicySentOnLayoutControlItem.TextToControlDistance = 3;
            // 
            // InvoicePrintedOnLayoutControlItem
            // 
            this.InvoicePrintedOnLayoutControlItem.Control = this.InvoicePrintedOnField;
            this.InvoicePrintedOnLayoutControlItem.CustomizationFormText = "Invoice Printed On";
            this.InvoicePrintedOnLayoutControlItem.Location = new System.Drawing.Point(233, 120);
            this.InvoicePrintedOnLayoutControlItem.Name = "InvoicePrintedOnLayoutControlItem";
            this.InvoicePrintedOnLayoutControlItem.Size = new System.Drawing.Size(247, 24);
            this.InvoicePrintedOnLayoutControlItem.Spacing = new DevExpress.XtraLayout.Utils.Padding(20, 0, 0, 0);
            this.InvoicePrintedOnLayoutControlItem.Text = "Invoice Printed On";
            this.InvoicePrintedOnLayoutControlItem.TextAlignMode = DevExpress.XtraLayout.TextAlignModeItem.CustomSize;
            this.InvoicePrintedOnLayoutControlItem.TextSize = new System.Drawing.Size(110, 13);
            this.InvoicePrintedOnLayoutControlItem.TextToControlDistance = 3;
            // 
            // DiscountsGroup
            // 
            this.DiscountsGroup.Items.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.FivePercentDiscountLayoutControlItem,
            this.MultiplePropertyDiscountLayoutControlItem,
            this.READiscountLayoutControlItem,
            this.StaffDiscountLayoutControlItem,
            this.emptySpaceItem21,
            this.emptySpaceItem12,
            this.FifteenForTwelveBonusOfferLayoutControlItem});
            this.DiscountsGroup.Location = new System.Drawing.Point(693, 186);
            this.DiscountsGroup.Name = "DiscountsGroup";
            this.DiscountsGroup.Size = new System.Drawing.Size(504, 333);
            this.DiscountsGroup.Text = "Discounts";
            // 
            // FivePercentDiscountLayoutControlItem
            // 
            this.FivePercentDiscountLayoutControlItem.Control = this.IsFivePercentDiscountField;
            this.FivePercentDiscountLayoutControlItem.CustomizationFormText = "5% Discount";
            this.FivePercentDiscountLayoutControlItem.Location = new System.Drawing.Point(0, 75);
            this.FivePercentDiscountLayoutControlItem.MaxSize = new System.Drawing.Size(185, 25);
            this.FivePercentDiscountLayoutControlItem.MinSize = new System.Drawing.Size(185, 25);
            this.FivePercentDiscountLayoutControlItem.Name = "FivePercentDiscountLayoutControlItem";
            this.FivePercentDiscountLayoutControlItem.Size = new System.Drawing.Size(185, 25);
            this.FivePercentDiscountLayoutControlItem.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.FivePercentDiscountLayoutControlItem.Text = "5% Discount";
            this.FivePercentDiscountLayoutControlItem.TextAlignMode = DevExpress.XtraLayout.TextAlignModeItem.CustomSize;
            this.FivePercentDiscountLayoutControlItem.TextSize = new System.Drawing.Size(130, 13);
            this.FivePercentDiscountLayoutControlItem.TextToControlDistance = 3;
            // 
            // MultiplePropertyDiscountLayoutControlItem
            // 
            this.MultiplePropertyDiscountLayoutControlItem.Control = this.IsMultiplePropertyDiscountField;
            this.MultiplePropertyDiscountLayoutControlItem.CustomizationFormText = "Multiple Prop. Discount";
            this.MultiplePropertyDiscountLayoutControlItem.Location = new System.Drawing.Point(0, 50);
            this.MultiplePropertyDiscountLayoutControlItem.MaxSize = new System.Drawing.Size(185, 25);
            this.MultiplePropertyDiscountLayoutControlItem.MinSize = new System.Drawing.Size(185, 25);
            this.MultiplePropertyDiscountLayoutControlItem.Name = "MultiplePropertyDiscountLayoutControlItem";
            this.MultiplePropertyDiscountLayoutControlItem.Size = new System.Drawing.Size(185, 25);
            this.MultiplePropertyDiscountLayoutControlItem.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.MultiplePropertyDiscountLayoutControlItem.Text = "Multiple Prop. Discount";
            this.MultiplePropertyDiscountLayoutControlItem.TextAlignMode = DevExpress.XtraLayout.TextAlignModeItem.CustomSize;
            this.MultiplePropertyDiscountLayoutControlItem.TextSize = new System.Drawing.Size(130, 13);
            this.MultiplePropertyDiscountLayoutControlItem.TextToControlDistance = 3;
            // 
            // READiscountLayoutControlItem
            // 
            this.READiscountLayoutControlItem.Control = this.IsREADiscountField;
            this.READiscountLayoutControlItem.CustomizationFormText = "REA Discount";
            this.READiscountLayoutControlItem.Location = new System.Drawing.Point(0, 25);
            this.READiscountLayoutControlItem.MaxSize = new System.Drawing.Size(185, 25);
            this.READiscountLayoutControlItem.MinSize = new System.Drawing.Size(185, 25);
            this.READiscountLayoutControlItem.Name = "READiscountLayoutControlItem";
            this.READiscountLayoutControlItem.Size = new System.Drawing.Size(185, 25);
            this.READiscountLayoutControlItem.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.READiscountLayoutControlItem.Text = "REA Discount";
            this.READiscountLayoutControlItem.TextAlignMode = DevExpress.XtraLayout.TextAlignModeItem.CustomSize;
            this.READiscountLayoutControlItem.TextSize = new System.Drawing.Size(130, 13);
            this.READiscountLayoutControlItem.TextToControlDistance = 3;
            // 
            // StaffDiscountLayoutControlItem
            // 
            this.StaffDiscountLayoutControlItem.Control = this.StaffDiscountField;
            this.StaffDiscountLayoutControlItem.CustomizationFormText = "Staff Discount";
            this.StaffDiscountLayoutControlItem.Location = new System.Drawing.Point(0, 0);
            this.StaffDiscountLayoutControlItem.MaxSize = new System.Drawing.Size(185, 25);
            this.StaffDiscountLayoutControlItem.MinSize = new System.Drawing.Size(185, 25);
            this.StaffDiscountLayoutControlItem.Name = "StaffDiscountLayoutControlItem";
            this.StaffDiscountLayoutControlItem.Size = new System.Drawing.Size(185, 25);
            this.StaffDiscountLayoutControlItem.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.StaffDiscountLayoutControlItem.Text = "Staff Discount";
            this.StaffDiscountLayoutControlItem.TextAlignMode = DevExpress.XtraLayout.TextAlignModeItem.CustomSize;
            this.StaffDiscountLayoutControlItem.TextSize = new System.Drawing.Size(130, 13);
            this.StaffDiscountLayoutControlItem.TextToControlDistance = 3;
            // 
            // emptySpaceItem21
            // 
            this.emptySpaceItem21.AllowHotTrack = false;
            this.emptySpaceItem21.CustomizationFormText = "emptySpaceItem21";
            this.emptySpaceItem21.Location = new System.Drawing.Point(185, 0);
            this.emptySpaceItem21.MinSize = new System.Drawing.Size(104, 24);
            this.emptySpaceItem21.Name = "emptySpaceItem21";
            this.emptySpaceItem21.Size = new System.Drawing.Size(295, 125);
            this.emptySpaceItem21.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.emptySpaceItem21.TextSize = new System.Drawing.Size(0, 0);
            // 
            // emptySpaceItem12
            // 
            this.emptySpaceItem12.AllowHotTrack = false;
            this.emptySpaceItem12.CustomizationFormText = "emptySpaceItem12";
            this.emptySpaceItem12.Location = new System.Drawing.Point(0, 125);
            this.emptySpaceItem12.Name = "emptySpaceItem12";
            this.emptySpaceItem12.Size = new System.Drawing.Size(480, 166);
            this.emptySpaceItem12.TextSize = new System.Drawing.Size(0, 0);
            // 
            // FifteenForTwelveBonusOfferLayoutControlItem
            // 
            this.FifteenForTwelveBonusOfferLayoutControlItem.Control = this.Is15For12BonusOfferField;
            this.FifteenForTwelveBonusOfferLayoutControlItem.CustomizationFormText = "15 For 12 Bonus Offer";
            this.FifteenForTwelveBonusOfferLayoutControlItem.Location = new System.Drawing.Point(0, 100);
            this.FifteenForTwelveBonusOfferLayoutControlItem.MaxSize = new System.Drawing.Size(185, 25);
            this.FifteenForTwelveBonusOfferLayoutControlItem.MinSize = new System.Drawing.Size(185, 25);
            this.FifteenForTwelveBonusOfferLayoutControlItem.Name = "FifteenForTwelveBonusOfferLayoutControlItem";
            this.FifteenForTwelveBonusOfferLayoutControlItem.Size = new System.Drawing.Size(185, 25);
            this.FifteenForTwelveBonusOfferLayoutControlItem.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.FifteenForTwelveBonusOfferLayoutControlItem.Text = "15 For 12 Bonus Offer";
            this.FifteenForTwelveBonusOfferLayoutControlItem.TextAlignMode = DevExpress.XtraLayout.TextAlignModeItem.CustomSize;
            this.FifteenForTwelveBonusOfferLayoutControlItem.TextSize = new System.Drawing.Size(130, 13);
            this.FifteenForTwelveBonusOfferLayoutControlItem.TextToControlDistance = 3;
            // 
            // emptySpaceItem35
            // 
            this.emptySpaceItem35.AllowHotTrack = false;
            this.emptySpaceItem35.CustomizationFormText = "emptySpaceItem35";
            this.emptySpaceItem35.Location = new System.Drawing.Point(0, 519);
            this.emptySpaceItem35.MinSize = new System.Drawing.Size(104, 24);
            this.emptySpaceItem35.Name = "emptySpaceItem35";
            this.emptySpaceItem35.Size = new System.Drawing.Size(1197, 62);
            this.emptySpaceItem35.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.emptySpaceItem35.TextSize = new System.Drawing.Size(0, 0);
            // 
            // OtherDetailsTabGroup
            // 
            this.OtherDetailsTabGroup.CaptionImageOptions.SvgImage = ((DevExpress.Utils.Svg.SvgImage)(resources.GetObject("OtherDetailsTabGroup.CaptionImageOptions.SvgImage")));
            this.OtherDetailsTabGroup.CaptionImageOptions.SvgImageSize = new System.Drawing.Size(16, 16);
            this.OtherDetailsTabGroup.Items.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.PremiumDetailsGroup,
            this.QuoteDetailsGroup,
            this.CoverDetailsGroup,
            this.PaymentDetailsGroup,
            this.LinkedTransactionsGroup,
            this.emptySpaceItem5});
            this.OtherDetailsTabGroup.Location = new System.Drawing.Point(0, 0);
            this.OtherDetailsTabGroup.Name = "OtherDetailsTabGroup";
            this.OtherDetailsTabGroup.Size = new System.Drawing.Size(1197, 581);
            this.OtherDetailsTabGroup.Text = "Other Details";
            // 
            // PremiumDetailsGroup
            // 
            this.PremiumDetailsGroup.CustomizationFormText = "Premium Details";
            this.PremiumDetailsGroup.Items.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.PremiumPaidByLayoutControlItem,
            this.PreferredMailingAddressLayoutControlItem,
            this.InsurerCodeLayoutControlItem});
            this.PremiumDetailsGroup.Location = new System.Drawing.Point(0, 0);
            this.PremiumDetailsGroup.Name = "PremiumDetailsGroup";
            this.PremiumDetailsGroup.Size = new System.Drawing.Size(320, 114);
            this.PremiumDetailsGroup.Text = "Premium Details";
            // 
            // PremiumPaidByLayoutControlItem
            // 
            this.PremiumPaidByLayoutControlItem.Control = this.PremiumPaidByField;
            this.PremiumPaidByLayoutControlItem.CustomizationFormText = "Premium Paid By";
            this.PremiumPaidByLayoutControlItem.Location = new System.Drawing.Point(0, 0);
            this.PremiumPaidByLayoutControlItem.Name = "PremiumPaidByLayoutControlItem";
            this.PremiumPaidByLayoutControlItem.Size = new System.Drawing.Size(296, 24);
            this.PremiumPaidByLayoutControlItem.Text = "Premium Paid By";
            this.PremiumPaidByLayoutControlItem.TextAlignMode = DevExpress.XtraLayout.TextAlignModeItem.CustomSize;
            this.PremiumPaidByLayoutControlItem.TextSize = new System.Drawing.Size(130, 13);
            this.PremiumPaidByLayoutControlItem.TextToControlDistance = 3;
            // 
            // PreferredMailingAddressLayoutControlItem
            // 
            this.PreferredMailingAddressLayoutControlItem.Control = this.PreferredMailingAddressTypeField;
            this.PreferredMailingAddressLayoutControlItem.CustomizationFormText = "Preferred Mailing Address";
            this.PreferredMailingAddressLayoutControlItem.Location = new System.Drawing.Point(0, 24);
            this.PreferredMailingAddressLayoutControlItem.Name = "PreferredMailingAddressLayoutControlItem";
            this.PreferredMailingAddressLayoutControlItem.Size = new System.Drawing.Size(296, 24);
            this.PreferredMailingAddressLayoutControlItem.Text = "Preferred Mailing Address";
            this.PreferredMailingAddressLayoutControlItem.TextAlignMode = DevExpress.XtraLayout.TextAlignModeItem.CustomSize;
            this.PreferredMailingAddressLayoutControlItem.TextSize = new System.Drawing.Size(130, 13);
            this.PreferredMailingAddressLayoutControlItem.TextToControlDistance = 3;
            // 
            // InsurerCodeLayoutControlItem
            // 
            this.InsurerCodeLayoutControlItem.Control = this.InsurerCodeField;
            this.InsurerCodeLayoutControlItem.CustomizationFormText = "Insurer Code";
            this.InsurerCodeLayoutControlItem.Location = new System.Drawing.Point(0, 48);
            this.InsurerCodeLayoutControlItem.Name = "InsurerCodeLayoutControlItem";
            this.InsurerCodeLayoutControlItem.Size = new System.Drawing.Size(296, 24);
            this.InsurerCodeLayoutControlItem.Text = "Insurer Code";
            this.InsurerCodeLayoutControlItem.TextAlignMode = DevExpress.XtraLayout.TextAlignModeItem.CustomSize;
            this.InsurerCodeLayoutControlItem.TextSize = new System.Drawing.Size(130, 13);
            this.InsurerCodeLayoutControlItem.TextToControlDistance = 3;
            // 
            // QuoteDetailsGroup
            // 
            this.QuoteDetailsGroup.CustomizationFormText = "Quote Details";
            this.QuoteDetailsGroup.Items.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.OnlineQuoteNumberLayoutControlItem,
            this.QuoteNumberLayoutControlItem,
            this.AskedForQuoteLayoutControlItem,
            this.HowAskedLayoutControlItem,
            this.QuotedByLayoutControlItem,
            this.QuotedOnLayoutControlItem,
            this.ConvertedToCoverByLayoutControlItem,
            this.ConvertedToCoverOnLayoutControlItem,
            this.QuoteExpiresOnLayoutControlItem,
            this.OnlineQuoteEmailLayoutControlItem,
            this.emptySpaceItem7});
            this.QuoteDetailsGroup.Location = new System.Drawing.Point(0, 114);
            this.QuoteDetailsGroup.Name = "QuoteDetailsGroup";
            this.QuoteDetailsGroup.Size = new System.Drawing.Size(320, 302);
            this.QuoteDetailsGroup.Text = "Quote Details";
            // 
            // OnlineQuoteNumberLayoutControlItem
            // 
            this.OnlineQuoteNumberLayoutControlItem.Control = this.OnlineQuoteNumberField;
            this.OnlineQuoteNumberLayoutControlItem.CustomizationFormText = "Online Quote Number";
            this.OnlineQuoteNumberLayoutControlItem.Location = new System.Drawing.Point(0, 0);
            this.OnlineQuoteNumberLayoutControlItem.Name = "OnlineQuoteNumberLayoutControlItem";
            this.OnlineQuoteNumberLayoutControlItem.Size = new System.Drawing.Size(296, 24);
            this.OnlineQuoteNumberLayoutControlItem.Text = "Online Quote Number";
            this.OnlineQuoteNumberLayoutControlItem.TextAlignMode = DevExpress.XtraLayout.TextAlignModeItem.CustomSize;
            this.OnlineQuoteNumberLayoutControlItem.TextSize = new System.Drawing.Size(130, 13);
            this.OnlineQuoteNumberLayoutControlItem.TextToControlDistance = 3;
            // 
            // QuoteNumberLayoutControlItem
            // 
            this.QuoteNumberLayoutControlItem.Control = this.QuoteNumberField;
            this.QuoteNumberLayoutControlItem.CustomizationFormText = "Quote Number";
            this.QuoteNumberLayoutControlItem.Location = new System.Drawing.Point(0, 24);
            this.QuoteNumberLayoutControlItem.Name = "QuoteNumberLayoutControlItem";
            this.QuoteNumberLayoutControlItem.Size = new System.Drawing.Size(296, 24);
            this.QuoteNumberLayoutControlItem.Text = "Quote Number";
            this.QuoteNumberLayoutControlItem.TextAlignMode = DevExpress.XtraLayout.TextAlignModeItem.CustomSize;
            this.QuoteNumberLayoutControlItem.TextSize = new System.Drawing.Size(130, 13);
            this.QuoteNumberLayoutControlItem.TextToControlDistance = 3;
            // 
            // AskedForQuoteLayoutControlItem
            // 
            this.AskedForQuoteLayoutControlItem.Control = this.AskedForQuoteWhoField;
            this.AskedForQuoteLayoutControlItem.CustomizationFormText = "Asked for Quote";
            this.AskedForQuoteLayoutControlItem.Location = new System.Drawing.Point(0, 48);
            this.AskedForQuoteLayoutControlItem.Name = "AskedForQuoteLayoutControlItem";
            this.AskedForQuoteLayoutControlItem.Size = new System.Drawing.Size(296, 24);
            this.AskedForQuoteLayoutControlItem.Text = "Asked for Quote";
            this.AskedForQuoteLayoutControlItem.TextAlignMode = DevExpress.XtraLayout.TextAlignModeItem.CustomSize;
            this.AskedForQuoteLayoutControlItem.TextSize = new System.Drawing.Size(130, 13);
            this.AskedForQuoteLayoutControlItem.TextToControlDistance = 3;
            // 
            // HowAskedLayoutControlItem
            // 
            this.HowAskedLayoutControlItem.Control = this.HowAskedForQuoteField;
            this.HowAskedLayoutControlItem.CustomizationFormText = "How Asked";
            this.HowAskedLayoutControlItem.Location = new System.Drawing.Point(0, 72);
            this.HowAskedLayoutControlItem.Name = "HowAskedLayoutControlItem";
            this.HowAskedLayoutControlItem.Size = new System.Drawing.Size(296, 24);
            this.HowAskedLayoutControlItem.Text = "How Asked";
            this.HowAskedLayoutControlItem.TextAlignMode = DevExpress.XtraLayout.TextAlignModeItem.CustomSize;
            this.HowAskedLayoutControlItem.TextSize = new System.Drawing.Size(130, 13);
            this.HowAskedLayoutControlItem.TextToControlDistance = 3;
            // 
            // QuotedByLayoutControlItem
            // 
            this.QuotedByLayoutControlItem.Control = this.QuotedByField;
            this.QuotedByLayoutControlItem.CustomizationFormText = "Quoted By";
            this.QuotedByLayoutControlItem.Location = new System.Drawing.Point(0, 96);
            this.QuotedByLayoutControlItem.Name = "QuotedByLayoutControlItem";
            this.QuotedByLayoutControlItem.Size = new System.Drawing.Size(296, 24);
            this.QuotedByLayoutControlItem.Text = "Quoted By";
            this.QuotedByLayoutControlItem.TextAlignMode = DevExpress.XtraLayout.TextAlignModeItem.CustomSize;
            this.QuotedByLayoutControlItem.TextSize = new System.Drawing.Size(130, 13);
            this.QuotedByLayoutControlItem.TextToControlDistance = 3;
            // 
            // QuotedOnLayoutControlItem
            // 
            this.QuotedOnLayoutControlItem.Control = this.QuotedOnField;
            this.QuotedOnLayoutControlItem.CustomizationFormText = "Quoted On";
            this.QuotedOnLayoutControlItem.Location = new System.Drawing.Point(0, 120);
            this.QuotedOnLayoutControlItem.Name = "QuotedOnLayoutControlItem";
            this.QuotedOnLayoutControlItem.Size = new System.Drawing.Size(296, 24);
            this.QuotedOnLayoutControlItem.Text = "Quoted On";
            this.QuotedOnLayoutControlItem.TextAlignMode = DevExpress.XtraLayout.TextAlignModeItem.CustomSize;
            this.QuotedOnLayoutControlItem.TextSize = new System.Drawing.Size(130, 13);
            this.QuotedOnLayoutControlItem.TextToControlDistance = 3;
            // 
            // ConvertedToCoverByLayoutControlItem
            // 
            this.ConvertedToCoverByLayoutControlItem.Control = this.ConvertedToCoverByField;
            this.ConvertedToCoverByLayoutControlItem.CustomizationFormText = "Converted To Cover By";
            this.ConvertedToCoverByLayoutControlItem.Location = new System.Drawing.Point(0, 144);
            this.ConvertedToCoverByLayoutControlItem.Name = "ConvertedToCoverByLayoutControlItem";
            this.ConvertedToCoverByLayoutControlItem.Size = new System.Drawing.Size(296, 24);
            this.ConvertedToCoverByLayoutControlItem.Text = "Converted To Cover By";
            this.ConvertedToCoverByLayoutControlItem.TextAlignMode = DevExpress.XtraLayout.TextAlignModeItem.CustomSize;
            this.ConvertedToCoverByLayoutControlItem.TextSize = new System.Drawing.Size(130, 13);
            this.ConvertedToCoverByLayoutControlItem.TextToControlDistance = 3;
            // 
            // ConvertedToCoverOnLayoutControlItem
            // 
            this.ConvertedToCoverOnLayoutControlItem.Control = this.ConvertedToCoverOnField;
            this.ConvertedToCoverOnLayoutControlItem.CustomizationFormText = "Converted To Cover On";
            this.ConvertedToCoverOnLayoutControlItem.Location = new System.Drawing.Point(0, 168);
            this.ConvertedToCoverOnLayoutControlItem.Name = "ConvertedToCoverOnLayoutControlItem";
            this.ConvertedToCoverOnLayoutControlItem.Size = new System.Drawing.Size(296, 24);
            this.ConvertedToCoverOnLayoutControlItem.Text = "Converted To Cover On";
            this.ConvertedToCoverOnLayoutControlItem.TextAlignMode = DevExpress.XtraLayout.TextAlignModeItem.CustomSize;
            this.ConvertedToCoverOnLayoutControlItem.TextSize = new System.Drawing.Size(130, 13);
            this.ConvertedToCoverOnLayoutControlItem.TextToControlDistance = 3;
            // 
            // QuoteExpiresOnLayoutControlItem
            // 
            this.QuoteExpiresOnLayoutControlItem.Control = this.QuoteExpiresOnField;
            this.QuoteExpiresOnLayoutControlItem.CustomizationFormText = "Quote Expires On";
            this.QuoteExpiresOnLayoutControlItem.Location = new System.Drawing.Point(0, 192);
            this.QuoteExpiresOnLayoutControlItem.Name = "QuoteExpiresOnLayoutControlItem";
            this.QuoteExpiresOnLayoutControlItem.Size = new System.Drawing.Size(296, 24);
            this.QuoteExpiresOnLayoutControlItem.Text = "Quote Expires On";
            this.QuoteExpiresOnLayoutControlItem.TextAlignMode = DevExpress.XtraLayout.TextAlignModeItem.CustomSize;
            this.QuoteExpiresOnLayoutControlItem.TextSize = new System.Drawing.Size(130, 13);
            this.QuoteExpiresOnLayoutControlItem.TextToControlDistance = 3;
            // 
            // OnlineQuoteEmailLayoutControlItem
            // 
            this.OnlineQuoteEmailLayoutControlItem.Control = this.OnlineQuoteEmailAddressField;
            this.OnlineQuoteEmailLayoutControlItem.CustomizationFormText = "Online Quote Email";
            this.OnlineQuoteEmailLayoutControlItem.Location = new System.Drawing.Point(0, 216);
            this.OnlineQuoteEmailLayoutControlItem.Name = "OnlineQuoteEmailLayoutControlItem";
            this.OnlineQuoteEmailLayoutControlItem.Size = new System.Drawing.Size(296, 24);
            this.OnlineQuoteEmailLayoutControlItem.Text = "Online Quote Email";
            this.OnlineQuoteEmailLayoutControlItem.TextAlignMode = DevExpress.XtraLayout.TextAlignModeItem.CustomSize;
            this.OnlineQuoteEmailLayoutControlItem.TextSize = new System.Drawing.Size(130, 13);
            this.OnlineQuoteEmailLayoutControlItem.TextToControlDistance = 3;
            // 
            // emptySpaceItem7
            // 
            this.emptySpaceItem7.AllowHotTrack = false;
            this.emptySpaceItem7.CustomizationFormText = "emptySpaceItem7";
            this.emptySpaceItem7.Location = new System.Drawing.Point(0, 240);
            this.emptySpaceItem7.Name = "emptySpaceItem7";
            this.emptySpaceItem7.Size = new System.Drawing.Size(296, 20);
            this.emptySpaceItem7.TextSize = new System.Drawing.Size(0, 0);
            // 
            // CoverDetailsGroup
            // 
            this.CoverDetailsGroup.CustomizationFormText = "Cover Details";
            this.CoverDetailsGroup.Items.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.RecreatedLayoutControlItem,
            this.WhoPlacedCoverLayoutControlItem,
            this.HowPlacedCoverLayoutControlItem,
            this.emptySpaceItem2,
            this.HeardAboutTSIViaLayoutControlItem,
            this.MortgageeLayoutControlItem});
            this.CoverDetailsGroup.Location = new System.Drawing.Point(320, 0);
            this.CoverDetailsGroup.Name = "CoverDetailsGroup";
            this.CoverDetailsGroup.Size = new System.Drawing.Size(307, 181);
            this.CoverDetailsGroup.Text = "Cover Details";
            // 
            // RecreatedLayoutControlItem
            // 
            this.RecreatedLayoutControlItem.Control = this.RecreatedField;
            this.RecreatedLayoutControlItem.CustomizationFormText = "Recreated";
            this.RecreatedLayoutControlItem.Location = new System.Drawing.Point(0, 0);
            this.RecreatedLayoutControlItem.MaxSize = new System.Drawing.Size(185, 25);
            this.RecreatedLayoutControlItem.MinSize = new System.Drawing.Size(185, 25);
            this.RecreatedLayoutControlItem.Name = "RecreatedLayoutControlItem";
            this.RecreatedLayoutControlItem.Size = new System.Drawing.Size(185, 25);
            this.RecreatedLayoutControlItem.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.RecreatedLayoutControlItem.Text = "Recreated";
            this.RecreatedLayoutControlItem.TextAlignMode = DevExpress.XtraLayout.TextAlignModeItem.CustomSize;
            this.RecreatedLayoutControlItem.TextSize = new System.Drawing.Size(130, 13);
            this.RecreatedLayoutControlItem.TextToControlDistance = 3;
            // 
            // WhoPlacedCoverLayoutControlItem
            // 
            this.WhoPlacedCoverLayoutControlItem.Control = this.WhoPlacedCoverField;
            this.WhoPlacedCoverLayoutControlItem.CustomizationFormText = "Who Placed Cover";
            this.WhoPlacedCoverLayoutControlItem.Location = new System.Drawing.Point(0, 25);
            this.WhoPlacedCoverLayoutControlItem.Name = "WhoPlacedCoverLayoutControlItem";
            this.WhoPlacedCoverLayoutControlItem.Size = new System.Drawing.Size(283, 24);
            this.WhoPlacedCoverLayoutControlItem.Text = "Who Placed Cover";
            this.WhoPlacedCoverLayoutControlItem.TextAlignMode = DevExpress.XtraLayout.TextAlignModeItem.CustomSize;
            this.WhoPlacedCoverLayoutControlItem.TextSize = new System.Drawing.Size(130, 13);
            this.WhoPlacedCoverLayoutControlItem.TextToControlDistance = 3;
            // 
            // HowPlacedCoverLayoutControlItem
            // 
            this.HowPlacedCoverLayoutControlItem.Control = this.HowPlacedCoverField;
            this.HowPlacedCoverLayoutControlItem.CustomizationFormText = "How Placed Cover";
            this.HowPlacedCoverLayoutControlItem.Location = new System.Drawing.Point(0, 49);
            this.HowPlacedCoverLayoutControlItem.Name = "HowPlacedCoverLayoutControlItem";
            this.HowPlacedCoverLayoutControlItem.Size = new System.Drawing.Size(283, 24);
            this.HowPlacedCoverLayoutControlItem.Text = "How Placed Cover";
            this.HowPlacedCoverLayoutControlItem.TextAlignMode = DevExpress.XtraLayout.TextAlignModeItem.CustomSize;
            this.HowPlacedCoverLayoutControlItem.TextSize = new System.Drawing.Size(130, 13);
            this.HowPlacedCoverLayoutControlItem.TextToControlDistance = 3;
            // 
            // emptySpaceItem2
            // 
            this.emptySpaceItem2.AllowHotTrack = false;
            this.emptySpaceItem2.CustomizationFormText = "emptySpaceItem2";
            this.emptySpaceItem2.Location = new System.Drawing.Point(185, 0);
            this.emptySpaceItem2.Name = "emptySpaceItem2";
            this.emptySpaceItem2.Size = new System.Drawing.Size(98, 25);
            this.emptySpaceItem2.TextSize = new System.Drawing.Size(0, 0);
            // 
            // HeardAboutTSIViaLayoutControlItem
            // 
            this.HeardAboutTSIViaLayoutControlItem.Control = this.HeardAboutTSIviaField;
            this.HeardAboutTSIViaLayoutControlItem.CustomizationFormText = "Heard About TSI via";
            this.HeardAboutTSIViaLayoutControlItem.Location = new System.Drawing.Point(0, 73);
            this.HeardAboutTSIViaLayoutControlItem.Name = "HeardAboutTSIViaLayoutControlItem";
            this.HeardAboutTSIViaLayoutControlItem.Size = new System.Drawing.Size(283, 24);
            this.HeardAboutTSIViaLayoutControlItem.Text = "Heard About TSI via";
            this.HeardAboutTSIViaLayoutControlItem.TextAlignMode = DevExpress.XtraLayout.TextAlignModeItem.CustomSize;
            this.HeardAboutTSIViaLayoutControlItem.TextSize = new System.Drawing.Size(130, 13);
            this.HeardAboutTSIViaLayoutControlItem.TextToControlDistance = 3;
            // 
            // MortgageeLayoutControlItem
            // 
            this.MortgageeLayoutControlItem.Control = this.MortgageeField;
            this.MortgageeLayoutControlItem.CustomizationFormText = "Mortgagee";
            this.MortgageeLayoutControlItem.Location = new System.Drawing.Point(0, 97);
            this.MortgageeLayoutControlItem.MaxSize = new System.Drawing.Size(0, 42);
            this.MortgageeLayoutControlItem.MinSize = new System.Drawing.Size(149, 42);
            this.MortgageeLayoutControlItem.Name = "MortgageeLayoutControlItem";
            this.MortgageeLayoutControlItem.Size = new System.Drawing.Size(283, 42);
            this.MortgageeLayoutControlItem.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.MortgageeLayoutControlItem.Text = "Mortgagee";
            this.MortgageeLayoutControlItem.TextAlignMode = DevExpress.XtraLayout.TextAlignModeItem.CustomSize;
            this.MortgageeLayoutControlItem.TextSize = new System.Drawing.Size(130, 13);
            this.MortgageeLayoutControlItem.TextToControlDistance = 3;
            // 
            // PaymentDetailsGroup
            // 
            this.PaymentDetailsGroup.CustomizationFormText = "Payment Details";
            this.PaymentDetailsGroup.Items.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.PaymentReceivedLayoutControlItem,
            this.PaymentMethodLayoutControlItem,
            this.PaymentReferenceLayoutControlItem,
            this.BPayReceiptLayoutControlItem,
            this.OnlineReferenceLayoutControlItem,
            this.CCTransactionIDLayoutControlItem,
            this.VoucherLayoutControlItem,
            this.PromoCodeLayoutControlItem,
            this.emptySpaceItem25});
            this.PaymentDetailsGroup.Location = new System.Drawing.Point(320, 181);
            this.PaymentDetailsGroup.Name = "PaymentDetailsGroup";
            this.PaymentDetailsGroup.Size = new System.Drawing.Size(307, 235);
            this.PaymentDetailsGroup.Text = "Payment Details";
            // 
            // PaymentReceivedLayoutControlItem
            // 
            this.PaymentReceivedLayoutControlItem.Control = this.PaymentReceivedField;
            this.PaymentReceivedLayoutControlItem.CustomizationFormText = "Payment Received";
            this.PaymentReceivedLayoutControlItem.Location = new System.Drawing.Point(0, 0);
            this.PaymentReceivedLayoutControlItem.MaxSize = new System.Drawing.Size(185, 25);
            this.PaymentReceivedLayoutControlItem.MinSize = new System.Drawing.Size(185, 25);
            this.PaymentReceivedLayoutControlItem.Name = "PaymentReceivedLayoutControlItem";
            this.PaymentReceivedLayoutControlItem.Size = new System.Drawing.Size(185, 25);
            this.PaymentReceivedLayoutControlItem.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.PaymentReceivedLayoutControlItem.Text = "Payment Received";
            this.PaymentReceivedLayoutControlItem.TextAlignMode = DevExpress.XtraLayout.TextAlignModeItem.CustomSize;
            this.PaymentReceivedLayoutControlItem.TextSize = new System.Drawing.Size(130, 13);
            this.PaymentReceivedLayoutControlItem.TextToControlDistance = 3;
            // 
            // PaymentMethodLayoutControlItem
            // 
            this.PaymentMethodLayoutControlItem.Control = this.PaymentMethodField;
            this.PaymentMethodLayoutControlItem.CustomizationFormText = "Payment Method";
            this.PaymentMethodLayoutControlItem.Location = new System.Drawing.Point(0, 25);
            this.PaymentMethodLayoutControlItem.Name = "PaymentMethodLayoutControlItem";
            this.PaymentMethodLayoutControlItem.Size = new System.Drawing.Size(283, 24);
            this.PaymentMethodLayoutControlItem.Text = "Payment Method";
            this.PaymentMethodLayoutControlItem.TextAlignMode = DevExpress.XtraLayout.TextAlignModeItem.CustomSize;
            this.PaymentMethodLayoutControlItem.TextSize = new System.Drawing.Size(130, 13);
            this.PaymentMethodLayoutControlItem.TextToControlDistance = 3;
            // 
            // PaymentReferenceLayoutControlItem
            // 
            this.PaymentReferenceLayoutControlItem.Control = this.PaymentReferenceField;
            this.PaymentReferenceLayoutControlItem.CustomizationFormText = "Payment Reference";
            this.PaymentReferenceLayoutControlItem.Location = new System.Drawing.Point(0, 49);
            this.PaymentReferenceLayoutControlItem.Name = "PaymentReferenceLayoutControlItem";
            this.PaymentReferenceLayoutControlItem.Size = new System.Drawing.Size(283, 24);
            this.PaymentReferenceLayoutControlItem.Text = "Payment Reference";
            this.PaymentReferenceLayoutControlItem.TextAlignMode = DevExpress.XtraLayout.TextAlignModeItem.CustomSize;
            this.PaymentReferenceLayoutControlItem.TextSize = new System.Drawing.Size(130, 13);
            this.PaymentReferenceLayoutControlItem.TextToControlDistance = 3;
            // 
            // BPayReceiptLayoutControlItem
            // 
            this.BPayReceiptLayoutControlItem.Control = this.BPayReceiptField;
            this.BPayReceiptLayoutControlItem.CustomizationFormText = "BPay Receipt";
            this.BPayReceiptLayoutControlItem.Location = new System.Drawing.Point(0, 73);
            this.BPayReceiptLayoutControlItem.Name = "BPayReceiptLayoutControlItem";
            this.BPayReceiptLayoutControlItem.Size = new System.Drawing.Size(283, 24);
            this.BPayReceiptLayoutControlItem.Text = "BPay Receipt";
            this.BPayReceiptLayoutControlItem.TextAlignMode = DevExpress.XtraLayout.TextAlignModeItem.CustomSize;
            this.BPayReceiptLayoutControlItem.TextSize = new System.Drawing.Size(130, 13);
            this.BPayReceiptLayoutControlItem.TextToControlDistance = 3;
            // 
            // OnlineReferenceLayoutControlItem
            // 
            this.OnlineReferenceLayoutControlItem.Control = this.OnlineReferenceField;
            this.OnlineReferenceLayoutControlItem.CustomizationFormText = "Online Reference";
            this.OnlineReferenceLayoutControlItem.Location = new System.Drawing.Point(0, 97);
            this.OnlineReferenceLayoutControlItem.Name = "OnlineReferenceLayoutControlItem";
            this.OnlineReferenceLayoutControlItem.Size = new System.Drawing.Size(283, 24);
            this.OnlineReferenceLayoutControlItem.Text = "Online Reference";
            this.OnlineReferenceLayoutControlItem.TextAlignMode = DevExpress.XtraLayout.TextAlignModeItem.CustomSize;
            this.OnlineReferenceLayoutControlItem.TextSize = new System.Drawing.Size(130, 13);
            this.OnlineReferenceLayoutControlItem.TextToControlDistance = 3;
            // 
            // CCTransactionIDLayoutControlItem
            // 
            this.CCTransactionIDLayoutControlItem.Control = this.CCTransactionIDField;
            this.CCTransactionIDLayoutControlItem.CustomizationFormText = "CC Transaction ID";
            this.CCTransactionIDLayoutControlItem.Location = new System.Drawing.Point(0, 121);
            this.CCTransactionIDLayoutControlItem.Name = "CCTransactionIDLayoutControlItem";
            this.CCTransactionIDLayoutControlItem.Size = new System.Drawing.Size(283, 24);
            this.CCTransactionIDLayoutControlItem.Text = "CC Transaction ID";
            this.CCTransactionIDLayoutControlItem.TextAlignMode = DevExpress.XtraLayout.TextAlignModeItem.CustomSize;
            this.CCTransactionIDLayoutControlItem.TextSize = new System.Drawing.Size(130, 13);
            this.CCTransactionIDLayoutControlItem.TextToControlDistance = 3;
            // 
            // VoucherLayoutControlItem
            // 
            this.VoucherLayoutControlItem.Control = this.VoucherTypeField;
            this.VoucherLayoutControlItem.CustomizationFormText = "Voucher";
            this.VoucherLayoutControlItem.Location = new System.Drawing.Point(0, 145);
            this.VoucherLayoutControlItem.Name = "VoucherLayoutControlItem";
            this.VoucherLayoutControlItem.Size = new System.Drawing.Size(283, 24);
            this.VoucherLayoutControlItem.Text = "Voucher";
            this.VoucherLayoutControlItem.TextAlignMode = DevExpress.XtraLayout.TextAlignModeItem.CustomSize;
            this.VoucherLayoutControlItem.TextSize = new System.Drawing.Size(130, 13);
            this.VoucherLayoutControlItem.TextToControlDistance = 3;
            // 
            // PromoCodeLayoutControlItem
            // 
            this.PromoCodeLayoutControlItem.Control = this.PromoCodeField;
            this.PromoCodeLayoutControlItem.CustomizationFormText = "Promo Code";
            this.PromoCodeLayoutControlItem.Location = new System.Drawing.Point(0, 169);
            this.PromoCodeLayoutControlItem.Name = "PromoCodeLayoutControlItem";
            this.PromoCodeLayoutControlItem.Size = new System.Drawing.Size(283, 24);
            this.PromoCodeLayoutControlItem.Text = "Promo Code";
            this.PromoCodeLayoutControlItem.TextAlignMode = DevExpress.XtraLayout.TextAlignModeItem.CustomSize;
            this.PromoCodeLayoutControlItem.TextSize = new System.Drawing.Size(130, 13);
            this.PromoCodeLayoutControlItem.TextToControlDistance = 3;
            // 
            // emptySpaceItem25
            // 
            this.emptySpaceItem25.AllowHotTrack = false;
            this.emptySpaceItem25.CustomizationFormText = "emptySpaceItem25";
            this.emptySpaceItem25.Location = new System.Drawing.Point(185, 0);
            this.emptySpaceItem25.Name = "emptySpaceItem25";
            this.emptySpaceItem25.Size = new System.Drawing.Size(98, 25);
            this.emptySpaceItem25.TextSize = new System.Drawing.Size(0, 0);
            // 
            // LinkedTransactionsGroup
            // 
            this.LinkedTransactionsGroup.CustomizationFormText = "Linked Transactions";
            this.LinkedTransactionsGroup.Items.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.CreditTransactionLayoutControlItem,
            this.ParentTransactionLayoutControlItem,
            this.ReferToTransactionLayoutControlItem,
            this.WelcomeLetterJoinedLayoutControlItem,
            this.emptySpaceItem15,
            this.emptySpaceItem32});
            this.LinkedTransactionsGroup.Location = new System.Drawing.Point(627, 0);
            this.LinkedTransactionsGroup.Name = "LinkedTransactionsGroup";
            this.LinkedTransactionsGroup.Size = new System.Drawing.Size(570, 416);
            this.LinkedTransactionsGroup.Text = "Linked Transactions";
            // 
            // CreditTransactionLayoutControlItem
            // 
            this.CreditTransactionLayoutControlItem.Control = this.LinkCreditTransactionNumberField;
            this.CreditTransactionLayoutControlItem.CustomizationFormText = "Credit Transaction";
            this.CreditTransactionLayoutControlItem.Location = new System.Drawing.Point(0, 72);
            this.CreditTransactionLayoutControlItem.MaxSize = new System.Drawing.Size(0, 24);
            this.CreditTransactionLayoutControlItem.MinSize = new System.Drawing.Size(187, 24);
            this.CreditTransactionLayoutControlItem.Name = "CreditTransactionLayoutControlItem";
            this.CreditTransactionLayoutControlItem.Size = new System.Drawing.Size(283, 24);
            this.CreditTransactionLayoutControlItem.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.CreditTransactionLayoutControlItem.Text = "Credit Transaction";
            this.CreditTransactionLayoutControlItem.TextAlignMode = DevExpress.XtraLayout.TextAlignModeItem.CustomSize;
            this.CreditTransactionLayoutControlItem.TextSize = new System.Drawing.Size(130, 13);
            this.CreditTransactionLayoutControlItem.TextToControlDistance = 3;
            // 
            // ParentTransactionLayoutControlItem
            // 
            this.ParentTransactionLayoutControlItem.Control = this.LinkParentTransactionNumberField;
            this.ParentTransactionLayoutControlItem.CustomizationFormText = "Parent Transaction";
            this.ParentTransactionLayoutControlItem.Location = new System.Drawing.Point(0, 48);
            this.ParentTransactionLayoutControlItem.MaxSize = new System.Drawing.Size(0, 24);
            this.ParentTransactionLayoutControlItem.MinSize = new System.Drawing.Size(187, 24);
            this.ParentTransactionLayoutControlItem.Name = "ParentTransactionLayoutControlItem";
            this.ParentTransactionLayoutControlItem.Size = new System.Drawing.Size(283, 24);
            this.ParentTransactionLayoutControlItem.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.ParentTransactionLayoutControlItem.Text = "Parent Transaction";
            this.ParentTransactionLayoutControlItem.TextAlignMode = DevExpress.XtraLayout.TextAlignModeItem.CustomSize;
            this.ParentTransactionLayoutControlItem.TextSize = new System.Drawing.Size(130, 13);
            this.ParentTransactionLayoutControlItem.TextToControlDistance = 3;
            // 
            // ReferToTransactionLayoutControlItem
            // 
            this.ReferToTransactionLayoutControlItem.Control = this.LinkReferToTransactionNumberField;
            this.ReferToTransactionLayoutControlItem.CustomizationFormText = "Refer To Transaction";
            this.ReferToTransactionLayoutControlItem.Location = new System.Drawing.Point(0, 0);
            this.ReferToTransactionLayoutControlItem.MaxSize = new System.Drawing.Size(0, 24);
            this.ReferToTransactionLayoutControlItem.MinSize = new System.Drawing.Size(187, 24);
            this.ReferToTransactionLayoutControlItem.Name = "ReferToTransactionLayoutControlItem";
            this.ReferToTransactionLayoutControlItem.Size = new System.Drawing.Size(283, 24);
            this.ReferToTransactionLayoutControlItem.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.ReferToTransactionLayoutControlItem.Text = "Refer To Transaction";
            this.ReferToTransactionLayoutControlItem.TextAlignMode = DevExpress.XtraLayout.TextAlignModeItem.CustomSize;
            this.ReferToTransactionLayoutControlItem.TextSize = new System.Drawing.Size(130, 13);
            this.ReferToTransactionLayoutControlItem.TextToControlDistance = 3;
            // 
            // WelcomeLetterJoinedLayoutControlItem
            // 
            this.WelcomeLetterJoinedLayoutControlItem.Control = this.LinkWelcomeLetterTransactionNumberField;
            this.WelcomeLetterJoinedLayoutControlItem.CustomizationFormText = "Welcome Letter Joined";
            this.WelcomeLetterJoinedLayoutControlItem.Location = new System.Drawing.Point(0, 24);
            this.WelcomeLetterJoinedLayoutControlItem.MaxSize = new System.Drawing.Size(0, 24);
            this.WelcomeLetterJoinedLayoutControlItem.MinSize = new System.Drawing.Size(187, 24);
            this.WelcomeLetterJoinedLayoutControlItem.Name = "WelcomeLetterJoinedLayoutControlItem";
            this.WelcomeLetterJoinedLayoutControlItem.Size = new System.Drawing.Size(283, 24);
            this.WelcomeLetterJoinedLayoutControlItem.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.WelcomeLetterJoinedLayoutControlItem.Text = "Welcome Letter Joined";
            this.WelcomeLetterJoinedLayoutControlItem.TextAlignMode = DevExpress.XtraLayout.TextAlignModeItem.CustomSize;
            this.WelcomeLetterJoinedLayoutControlItem.TextSize = new System.Drawing.Size(130, 13);
            this.WelcomeLetterJoinedLayoutControlItem.TextToControlDistance = 3;
            // 
            // emptySpaceItem15
            // 
            this.emptySpaceItem15.AllowHotTrack = false;
            this.emptySpaceItem15.CustomizationFormText = "emptySpaceItem15";
            this.emptySpaceItem15.Location = new System.Drawing.Point(283, 0);
            this.emptySpaceItem15.Name = "emptySpaceItem15";
            this.emptySpaceItem15.Size = new System.Drawing.Size(263, 96);
            this.emptySpaceItem15.TextSize = new System.Drawing.Size(0, 0);
            // 
            // emptySpaceItem32
            // 
            this.emptySpaceItem32.AllowHotTrack = false;
            this.emptySpaceItem32.Location = new System.Drawing.Point(0, 96);
            this.emptySpaceItem32.Name = "emptySpaceItem32";
            this.emptySpaceItem32.Size = new System.Drawing.Size(546, 278);
            this.emptySpaceItem32.TextSize = new System.Drawing.Size(0, 0);
            // 
            // emptySpaceItem5
            // 
            this.emptySpaceItem5.AllowHotTrack = false;
            this.emptySpaceItem5.CustomizationFormText = "emptySpaceItem1";
            this.emptySpaceItem5.Location = new System.Drawing.Point(0, 416);
            this.emptySpaceItem5.Name = "emptySpaceItem5";
            this.emptySpaceItem5.Size = new System.Drawing.Size(1197, 165);
            this.emptySpaceItem5.Text = "emptySpaceItem1";
            this.emptySpaceItem5.TextSize = new System.Drawing.Size(0, 0);
            // 
            // DisclosureTabGroup
            // 
            this.DisclosureTabGroup.CaptionImageOptions.SvgImage = ((DevExpress.Utils.Svg.SvgImage)(resources.GetObject("DisclosureTabGroup.CaptionImageOptions.SvgImage")));
            this.DisclosureTabGroup.CaptionImageOptions.SvgImageSize = new System.Drawing.Size(16, 16);
            this.DisclosureTabGroup.Items.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.ApprovalGroup,
            this.QuesstionnaireGroup,
            this.emptySpaceItem20});
            this.DisclosureTabGroup.Location = new System.Drawing.Point(0, 0);
            this.DisclosureTabGroup.Name = "DisclosureTabGroup";
            this.DisclosureTabGroup.Size = new System.Drawing.Size(1197, 581);
            this.DisclosureTabGroup.Text = "Disclosure";
            // 
            // ApprovalGroup
            // 
            this.ApprovalGroup.CustomizationFormText = "Approval";
            this.ApprovalGroup.Items.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.ApprovedByLayoutControlItem,
            this.ApprovedOnLayoutControlItem,
            this.emptySpaceItem14});
            this.ApprovalGroup.Location = new System.Drawing.Point(0, 402);
            this.ApprovalGroup.Name = "ApprovalGroup";
            this.ApprovalGroup.Size = new System.Drawing.Size(1197, 66);
            this.ApprovalGroup.Text = "Approval";
            // 
            // ApprovedByLayoutControlItem
            // 
            this.ApprovedByLayoutControlItem.Control = this.ApprovedByField;
            this.ApprovedByLayoutControlItem.CustomizationFormText = "Approved By";
            this.ApprovedByLayoutControlItem.Location = new System.Drawing.Point(0, 0);
            this.ApprovedByLayoutControlItem.Name = "ApprovedByLayoutControlItem";
            this.ApprovedByLayoutControlItem.Size = new System.Drawing.Size(265, 24);
            this.ApprovedByLayoutControlItem.Text = "Approved By";
            this.ApprovedByLayoutControlItem.TextAlignMode = DevExpress.XtraLayout.TextAlignModeItem.CustomSize;
            this.ApprovedByLayoutControlItem.TextSize = new System.Drawing.Size(121, 13);
            this.ApprovedByLayoutControlItem.TextToControlDistance = 3;
            // 
            // ApprovedOnLayoutControlItem
            // 
            this.ApprovedOnLayoutControlItem.Control = this.ApprovedOnField;
            this.ApprovedOnLayoutControlItem.CustomizationFormText = "Approved On";
            this.ApprovedOnLayoutControlItem.Location = new System.Drawing.Point(265, 0);
            this.ApprovedOnLayoutControlItem.Name = "ApprovedOnLayoutControlItem";
            this.ApprovedOnLayoutControlItem.Size = new System.Drawing.Size(200, 24);
            this.ApprovedOnLayoutControlItem.Spacing = new DevExpress.XtraLayout.Utils.Padding(20, 0, 0, 0);
            this.ApprovedOnLayoutControlItem.Text = "Approved On";
            this.ApprovedOnLayoutControlItem.TextAlignMode = DevExpress.XtraLayout.TextAlignModeItem.CustomSize;
            this.ApprovedOnLayoutControlItem.TextSize = new System.Drawing.Size(88, 13);
            this.ApprovedOnLayoutControlItem.TextToControlDistance = 3;
            // 
            // emptySpaceItem14
            // 
            this.emptySpaceItem14.AllowHotTrack = false;
            this.emptySpaceItem14.CustomizationFormText = "emptySpaceItem14";
            this.emptySpaceItem14.Location = new System.Drawing.Point(465, 0);
            this.emptySpaceItem14.Name = "emptySpaceItem14";
            this.emptySpaceItem14.Size = new System.Drawing.Size(708, 24);
            this.emptySpaceItem14.TextSize = new System.Drawing.Size(0, 0);
            // 
            // QuesstionnaireGroup
            // 
            this.QuesstionnaireGroup.Items.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.DisclosureItem1LayoutControlItem,
            this.DenialReasonLayoutControlItem,
            this.SpecialTermsLayoutControlItem,
            this.DisclosureItem2LayoutControlItem,
            this.emptySpaceItem10,
            this.emptySpaceItem13,
            this.layoutControlItem5,
            this.DisclosureItem3LayoutControlItem,
            this.DisclosureItem4LayoutControlItem,
            this.CircumstancesLayoutControlItem,
            this.DisclosureItem5LayoutControlItem,
            this.RestrictionsLayoutControlItem,
            this.emptySpaceItem18,
            this.emptySpaceItem39,
            this.Disclosure5NoLayoutItem,
            this.Disclosure5AwaitingApprovalLayoutItem});
            this.QuesstionnaireGroup.Location = new System.Drawing.Point(0, 0);
            this.QuesstionnaireGroup.Name = "QuesstionnaireGroup";
            this.QuesstionnaireGroup.Size = new System.Drawing.Size(1197, 402);
            this.QuesstionnaireGroup.Text = "Questionnaire";
            // 
            // DisclosureItem1LayoutControlItem
            // 
            this.DisclosureItem1LayoutControlItem.Control = this.DisclosureItem1Field;
            this.DisclosureItem1LayoutControlItem.CustomizationFormText = "1. Have you or anyone to be insured under the policy ever been declined insurance" +
    ", declined renewal on a policy or had special terms or conditions imposed on ins" +
    "urance?";
            this.DisclosureItem1LayoutControlItem.Location = new System.Drawing.Point(0, 0);
            this.DisclosureItem1LayoutControlItem.MinSize = new System.Drawing.Size(50, 25);
            this.DisclosureItem1LayoutControlItem.Name = "DisclosureItem1LayoutControlItem";
            this.DisclosureItem1LayoutControlItem.Size = new System.Drawing.Size(880, 26);
            this.DisclosureItem1LayoutControlItem.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.DisclosureItem1LayoutControlItem.Text = "1. Have you or anyone to be insured under the policy ever been declined insurance" +
    ", declined renewal on a policy or had special terms or conditions imposed on ins" +
    "urance?";
            this.DisclosureItem1LayoutControlItem.TextAlignMode = DevExpress.XtraLayout.TextAlignModeItem.CustomSize;
            this.DisclosureItem1LayoutControlItem.TextSize = new System.Drawing.Size(840, 13);
            this.DisclosureItem1LayoutControlItem.TextToControlDistance = 3;
            // 
            // DenialReasonLayoutControlItem
            // 
            this.DenialReasonLayoutControlItem.Control = this.DenialReasonField;
            this.DenialReasonLayoutControlItem.CustomizationFormText = "Denial Reason";
            this.DenialReasonLayoutControlItem.Location = new System.Drawing.Point(0, 26);
            this.DenialReasonLayoutControlItem.MaxSize = new System.Drawing.Size(0, 39);
            this.DenialReasonLayoutControlItem.MinSize = new System.Drawing.Size(139, 39);
            this.DenialReasonLayoutControlItem.Name = "DenialReasonLayoutControlItem";
            this.DenialReasonLayoutControlItem.Padding = new DevExpress.XtraLayout.Utils.Padding(15, 2, 2, 2);
            this.DenialReasonLayoutControlItem.Size = new System.Drawing.Size(583, 39);
            this.DenialReasonLayoutControlItem.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.DenialReasonLayoutControlItem.Text = "Denial Reason";
            this.DenialReasonLayoutControlItem.TextAlignMode = DevExpress.XtraLayout.TextAlignModeItem.CustomSize;
            this.DenialReasonLayoutControlItem.TextSize = new System.Drawing.Size(108, 13);
            this.DenialReasonLayoutControlItem.TextToControlDistance = 3;
            // 
            // SpecialTermsLayoutControlItem
            // 
            this.SpecialTermsLayoutControlItem.Control = this.SpecialTermsField;
            this.SpecialTermsLayoutControlItem.CustomizationFormText = "Special Terms";
            this.SpecialTermsLayoutControlItem.Location = new System.Drawing.Point(583, 26);
            this.SpecialTermsLayoutControlItem.MaxSize = new System.Drawing.Size(0, 39);
            this.SpecialTermsLayoutControlItem.MinSize = new System.Drawing.Size(126, 39);
            this.SpecialTermsLayoutControlItem.Name = "SpecialTermsLayoutControlItem";
            this.SpecialTermsLayoutControlItem.Size = new System.Drawing.Size(590, 39);
            this.SpecialTermsLayoutControlItem.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.SpecialTermsLayoutControlItem.Spacing = new DevExpress.XtraLayout.Utils.Padding(20, 0, 0, 0);
            this.SpecialTermsLayoutControlItem.Text = "Special Terms";
            this.SpecialTermsLayoutControlItem.TextAlignMode = DevExpress.XtraLayout.TextAlignModeItem.CustomSize;
            this.SpecialTermsLayoutControlItem.TextSize = new System.Drawing.Size(88, 13);
            this.SpecialTermsLayoutControlItem.TextToControlDistance = 3;
            // 
            // DisclosureItem2LayoutControlItem
            // 
            this.DisclosureItem2LayoutControlItem.Control = this.DisclosureItem2Field;
            this.DisclosureItem2LayoutControlItem.CustomizationFormText = "2. During the past 5 years, have you or anyone to be insured under the policy had" +
    " 3 or more claims under a Landlord or home and contents insurance policy or made" +
    " a claim";
            this.DisclosureItem2LayoutControlItem.Location = new System.Drawing.Point(0, 65);
            this.DisclosureItem2LayoutControlItem.MinSize = new System.Drawing.Size(50, 25);
            this.DisclosureItem2LayoutControlItem.Name = "DisclosureItem2LayoutControlItem";
            this.DisclosureItem2LayoutControlItem.Size = new System.Drawing.Size(880, 26);
            this.DisclosureItem2LayoutControlItem.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.DisclosureItem2LayoutControlItem.Text = "2. During the past 5 years, have you or anyone to be insured under the policy had" +
    " 3 or more claims under a Landlord or home and contents insurance policy or made" +
    " a claim";
            this.DisclosureItem2LayoutControlItem.TextAlignMode = DevExpress.XtraLayout.TextAlignModeItem.CustomSize;
            this.DisclosureItem2LayoutControlItem.TextSize = new System.Drawing.Size(840, 13);
            this.DisclosureItem2LayoutControlItem.TextToControlDistance = 3;
            // 
            // emptySpaceItem10
            // 
            this.emptySpaceItem10.AllowHotTrack = false;
            this.emptySpaceItem10.CustomizationFormText = "emptySpaceItem6";
            this.emptySpaceItem10.Location = new System.Drawing.Point(880, 0);
            this.emptySpaceItem10.MinSize = new System.Drawing.Size(104, 24);
            this.emptySpaceItem10.Name = "emptySpaceItem10";
            this.emptySpaceItem10.Size = new System.Drawing.Size(293, 26);
            this.emptySpaceItem10.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.emptySpaceItem10.Text = "emptySpaceItem6";
            this.emptySpaceItem10.TextSize = new System.Drawing.Size(0, 0);
            // 
            // emptySpaceItem13
            // 
            this.emptySpaceItem13.AllowHotTrack = false;
            this.emptySpaceItem13.CustomizationFormText = "emptySpaceItem5";
            this.emptySpaceItem13.Location = new System.Drawing.Point(880, 65);
            this.emptySpaceItem13.Name = "emptySpaceItem13";
            this.emptySpaceItem13.Size = new System.Drawing.Size(293, 26);
            this.emptySpaceItem13.Text = "emptySpaceItem5";
            this.emptySpaceItem13.TextSize = new System.Drawing.Size(0, 0);
            // 
            // layoutControlItem5
            // 
            this.layoutControlItem5.Control = this.DisclosureClaimsGrid;
            this.layoutControlItem5.Location = new System.Drawing.Point(0, 91);
            this.layoutControlItem5.Name = "layoutControlItem5";
            this.layoutControlItem5.Size = new System.Drawing.Size(1173, 89);
            this.layoutControlItem5.TextSize = new System.Drawing.Size(0, 0);
            this.layoutControlItem5.TextVisible = false;
            // 
            // DisclosureItem3LayoutControlItem
            // 
            this.DisclosureItem3LayoutControlItem.Control = this.Disclosure3Field;
            this.DisclosureItem3LayoutControlItem.CustomizationFormText = "3. In the last 5 years, have you or anyone to be insured under the policy been co" +
    "nvicted of theft or fraud?";
            this.DisclosureItem3LayoutControlItem.Location = new System.Drawing.Point(0, 180);
            this.DisclosureItem3LayoutControlItem.MinSize = new System.Drawing.Size(50, 25);
            this.DisclosureItem3LayoutControlItem.Name = "DisclosureItem3LayoutControlItem";
            this.DisclosureItem3LayoutControlItem.Size = new System.Drawing.Size(880, 28);
            this.DisclosureItem3LayoutControlItem.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.DisclosureItem3LayoutControlItem.Text = "3. In the last 5 years, have you or anyone to be insured under the policy been co" +
    "nvicted of theft or fraud?";
            this.DisclosureItem3LayoutControlItem.TextAlignMode = DevExpress.XtraLayout.TextAlignModeItem.CustomSize;
            this.DisclosureItem3LayoutControlItem.TextSize = new System.Drawing.Size(840, 13);
            this.DisclosureItem3LayoutControlItem.TextToControlDistance = 3;
            // 
            // DisclosureItem4LayoutControlItem
            // 
            this.DisclosureItem4LayoutControlItem.Control = this.Disclosure4Field;
            this.DisclosureItem4LayoutControlItem.CustomizationFormText = "4. Are you or anyone to be insured under the policy aware of any existing circums" +
    "tances that may lead to a claim under this policy?";
            this.DisclosureItem4LayoutControlItem.Location = new System.Drawing.Point(0, 208);
            this.DisclosureItem4LayoutControlItem.MinSize = new System.Drawing.Size(50, 25);
            this.DisclosureItem4LayoutControlItem.Name = "DisclosureItem4LayoutControlItem";
            this.DisclosureItem4LayoutControlItem.Size = new System.Drawing.Size(880, 28);
            this.DisclosureItem4LayoutControlItem.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.DisclosureItem4LayoutControlItem.Text = "4. Are you or anyone to be insured under the policy aware of any existing circums" +
    "tances that may lead to a claim under this policy?";
            this.DisclosureItem4LayoutControlItem.TextAlignMode = DevExpress.XtraLayout.TextAlignModeItem.CustomSize;
            this.DisclosureItem4LayoutControlItem.TextSize = new System.Drawing.Size(840, 13);
            this.DisclosureItem4LayoutControlItem.TextToControlDistance = 3;
            // 
            // CircumstancesLayoutControlItem
            // 
            this.CircumstancesLayoutControlItem.Control = this.CircumstancesField;
            this.CircumstancesLayoutControlItem.CustomizationFormText = "Circumstances";
            this.CircumstancesLayoutControlItem.Location = new System.Drawing.Point(0, 236);
            this.CircumstancesLayoutControlItem.MaxSize = new System.Drawing.Size(0, 52);
            this.CircumstancesLayoutControlItem.MinSize = new System.Drawing.Size(138, 52);
            this.CircumstancesLayoutControlItem.Name = "CircumstancesLayoutControlItem";
            this.CircumstancesLayoutControlItem.Padding = new DevExpress.XtraLayout.Utils.Padding(15, 2, 2, 2);
            this.CircumstancesLayoutControlItem.Size = new System.Drawing.Size(1173, 52);
            this.CircumstancesLayoutControlItem.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.CircumstancesLayoutControlItem.Text = "Circumstances";
            this.CircumstancesLayoutControlItem.TextAlignMode = DevExpress.XtraLayout.TextAlignModeItem.CustomSize;
            this.CircumstancesLayoutControlItem.TextSize = new System.Drawing.Size(108, 13);
            this.CircumstancesLayoutControlItem.TextToControlDistance = 3;
            // 
            // DisclosureItem5LayoutControlItem
            // 
            this.DisclosureItem5LayoutControlItem.Control = this.Disclosure5YesField;
            this.DisclosureItem5LayoutControlItem.CustomizationFormText = "5. Underwriting approval granted?";
            this.DisclosureItem5LayoutControlItem.Location = new System.Drawing.Point(0, 288);
            this.DisclosureItem5LayoutControlItem.Name = "DisclosureItem5LayoutControlItem";
            this.DisclosureItem5LayoutControlItem.Size = new System.Drawing.Size(886, 25);
            this.DisclosureItem5LayoutControlItem.Text = "5. Underwriting approval granted?";
            this.DisclosureItem5LayoutControlItem.TextAlignMode = DevExpress.XtraLayout.TextAlignModeItem.CustomSize;
            this.DisclosureItem5LayoutControlItem.TextSize = new System.Drawing.Size(840, 13);
            this.DisclosureItem5LayoutControlItem.TextToControlDistance = 3;
            // 
            // RestrictionsLayoutControlItem
            // 
            this.RestrictionsLayoutControlItem.Control = this.RestrictionsField;
            this.RestrictionsLayoutControlItem.CustomizationFormText = "Restrictions";
            this.RestrictionsLayoutControlItem.Location = new System.Drawing.Point(0, 313);
            this.RestrictionsLayoutControlItem.MaxSize = new System.Drawing.Size(0, 47);
            this.RestrictionsLayoutControlItem.MinSize = new System.Drawing.Size(139, 47);
            this.RestrictionsLayoutControlItem.Name = "RestrictionsLayoutControlItem";
            this.RestrictionsLayoutControlItem.Padding = new DevExpress.XtraLayout.Utils.Padding(15, 2, 2, 2);
            this.RestrictionsLayoutControlItem.Size = new System.Drawing.Size(1173, 47);
            this.RestrictionsLayoutControlItem.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.RestrictionsLayoutControlItem.Text = "Restrictions";
            this.RestrictionsLayoutControlItem.TextAlignMode = DevExpress.XtraLayout.TextAlignModeItem.CustomSize;
            this.RestrictionsLayoutControlItem.TextSize = new System.Drawing.Size(108, 13);
            this.RestrictionsLayoutControlItem.TextToControlDistance = 3;
            // 
            // emptySpaceItem18
            // 
            this.emptySpaceItem18.AllowHotTrack = false;
            this.emptySpaceItem18.CustomizationFormText = "emptySpaceItem10";
            this.emptySpaceItem18.Location = new System.Drawing.Point(880, 180);
            this.emptySpaceItem18.Name = "emptySpaceItem18";
            this.emptySpaceItem18.Size = new System.Drawing.Size(293, 56);
            this.emptySpaceItem18.Text = "emptySpaceItem10";
            this.emptySpaceItem18.TextSize = new System.Drawing.Size(0, 0);
            // 
            // emptySpaceItem39
            // 
            this.emptySpaceItem39.AllowHotTrack = false;
            this.emptySpaceItem39.Location = new System.Drawing.Point(1095, 288);
            this.emptySpaceItem39.Name = "emptySpaceItem39";
            this.emptySpaceItem39.Size = new System.Drawing.Size(78, 25);
            this.emptySpaceItem39.TextSize = new System.Drawing.Size(0, 0);
            // 
            // Disclosure5NoLayoutItem
            // 
            this.Disclosure5NoLayoutItem.Control = this.Disclosure5NoField;
            this.Disclosure5NoLayoutItem.Location = new System.Drawing.Point(886, 288);
            this.Disclosure5NoLayoutItem.Name = "Disclosure5NoLayoutItem";
            this.Disclosure5NoLayoutItem.Size = new System.Drawing.Size(56, 25);
            this.Disclosure5NoLayoutItem.TextSize = new System.Drawing.Size(0, 0);
            this.Disclosure5NoLayoutItem.TextVisible = false;
            // 
            // Disclosure5AwaitingApprovalLayoutItem
            // 
            this.Disclosure5AwaitingApprovalLayoutItem.Control = this.Disclosure5AwaitingApprovalField;
            this.Disclosure5AwaitingApprovalLayoutItem.Location = new System.Drawing.Point(942, 288);
            this.Disclosure5AwaitingApprovalLayoutItem.Name = "Disclosure5AwaitingApprovalLayoutItem";
            this.Disclosure5AwaitingApprovalLayoutItem.Size = new System.Drawing.Size(153, 25);
            this.Disclosure5AwaitingApprovalLayoutItem.TextSize = new System.Drawing.Size(0, 0);
            this.Disclosure5AwaitingApprovalLayoutItem.TextVisible = false;
            // 
            // emptySpaceItem20
            // 
            this.emptySpaceItem20.AllowHotTrack = false;
            this.emptySpaceItem20.Location = new System.Drawing.Point(0, 468);
            this.emptySpaceItem20.Name = "emptySpaceItem20";
            this.emptySpaceItem20.Size = new System.Drawing.Size(1197, 113);
            this.emptySpaceItem20.TextSize = new System.Drawing.Size(0, 0);
            // 
            // PremiumAndRenewalTabGroup
            // 
            this.PremiumAndRenewalTabGroup.CaptionImageOptions.SvgImage = ((DevExpress.Utils.Svg.SvgImage)(resources.GetObject("PremiumAndRenewalTabGroup.CaptionImageOptions.SvgImage")));
            this.PremiumAndRenewalTabGroup.CaptionImageOptions.SvgImageSize = new System.Drawing.Size(16, 16);
            this.PremiumAndRenewalTabGroup.Items.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.ExistingPremiumDetailsGroup,
            this.emptySpaceItem37,
            this.RenewingTransactionSettingsGroup,
            this.RenewingDiscountsGroup,
            this.RenewalProcessingGroup,
            this.emptySpaceItem30});
            this.PremiumAndRenewalTabGroup.Location = new System.Drawing.Point(0, 0);
            this.PremiumAndRenewalTabGroup.Name = "PremiumAndRenewalTabGroup";
            this.PremiumAndRenewalTabGroup.Size = new System.Drawing.Size(1197, 581);
            this.PremiumAndRenewalTabGroup.Text = "Premium / Renewal";
            // 
            // ExistingPremiumDetailsGroup
            // 
            this.ExistingPremiumDetailsGroup.Items.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.layoutControlItem6,
            this.layoutControlItem7});
            this.ExistingPremiumDetailsGroup.Location = new System.Drawing.Point(0, 0);
            this.ExistingPremiumDetailsGroup.Name = "ExistingPremiumDetailsGroup";
            this.ExistingPremiumDetailsGroup.Size = new System.Drawing.Size(656, 257);
            this.ExistingPremiumDetailsGroup.Text = "Premium Details";
            // 
            // layoutControlItem6
            // 
            this.layoutControlItem6.Control = this.PremiumBaseRatesGrid;
            this.layoutControlItem6.Location = new System.Drawing.Point(0, 0);
            this.layoutControlItem6.MaxSize = new System.Drawing.Size(0, 46);
            this.layoutControlItem6.MinSize = new System.Drawing.Size(104, 46);
            this.layoutControlItem6.Name = "layoutControlItem6";
            this.layoutControlItem6.Size = new System.Drawing.Size(632, 46);
            this.layoutControlItem6.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.layoutControlItem6.TextSize = new System.Drawing.Size(0, 0);
            this.layoutControlItem6.TextVisible = false;
            // 
            // layoutControlItem7
            // 
            this.layoutControlItem7.Control = this.PremiumDetailsGrid;
            this.layoutControlItem7.Location = new System.Drawing.Point(0, 46);
            this.layoutControlItem7.Name = "layoutControlItem7";
            this.layoutControlItem7.Size = new System.Drawing.Size(632, 169);
            this.layoutControlItem7.TextSize = new System.Drawing.Size(0, 0);
            this.layoutControlItem7.TextVisible = false;
            // 
            // emptySpaceItem37
            // 
            this.emptySpaceItem37.AllowHotTrack = false;
            this.emptySpaceItem37.Location = new System.Drawing.Point(656, 0);
            this.emptySpaceItem37.Name = "emptySpaceItem37";
            this.emptySpaceItem37.Size = new System.Drawing.Size(541, 257);
            this.emptySpaceItem37.TextSize = new System.Drawing.Size(0, 0);
            // 
            // RenewingTransactionSettingsGroup
            // 
            this.RenewingTransactionSettingsGroup.CustomizationFormText = "Renewing Transaction Settings";
            this.RenewingTransactionSettingsGroup.Items.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.RenewingTransactionsLayoutControlItem,
            this.RenewalReasonLayoutControlItem,
            this.RenewalPremiumPaidByLayoutControlItem,
            this.RenewalImposedExcessLayoutControlItem,
            this.RenewExcessAmountLayoutControlItem,
            this.DoNotRenewLayoutControlItem,
            this.emptySpaceItem28,
            this.emptySpaceItem29,
            this.emptySpaceItem27,
            this.RenewalsOnHoldLayoutControlItem});
            this.RenewingTransactionSettingsGroup.Location = new System.Drawing.Point(0, 257);
            this.RenewingTransactionSettingsGroup.Name = "RenewingTransactionSettingsGroup";
            this.RenewingTransactionSettingsGroup.Size = new System.Drawing.Size(336, 219);
            this.RenewingTransactionSettingsGroup.Text = "Renewing Transaction Settings";
            // 
            // RenewingTransactionsLayoutControlItem
            // 
            this.RenewingTransactionsLayoutControlItem.Control = this.RenewingTransactionsField;
            this.RenewingTransactionsLayoutControlItem.CustomizationFormText = "Renewing Transaction\'s";
            this.RenewingTransactionsLayoutControlItem.Location = new System.Drawing.Point(0, 0);
            this.RenewingTransactionsLayoutControlItem.MinSize = new System.Drawing.Size(189, 24);
            this.RenewingTransactionsLayoutControlItem.Name = "RenewingTransactionsLayoutControlItem";
            this.RenewingTransactionsLayoutControlItem.Size = new System.Drawing.Size(249, 25);
            this.RenewingTransactionsLayoutControlItem.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.RenewingTransactionsLayoutControlItem.Text = "Renewing Transaction\'s";
            this.RenewingTransactionsLayoutControlItem.TextAlignMode = DevExpress.XtraLayout.TextAlignModeItem.CustomSize;
            this.RenewingTransactionsLayoutControlItem.TextSize = new System.Drawing.Size(130, 13);
            this.RenewingTransactionsLayoutControlItem.TextToControlDistance = 3;
            // 
            // RenewalReasonLayoutControlItem
            // 
            this.RenewalReasonLayoutControlItem.Control = this.RenewalReasonField;
            this.RenewalReasonLayoutControlItem.CustomizationFormText = "Reason";
            this.RenewalReasonLayoutControlItem.Location = new System.Drawing.Point(0, 75);
            this.RenewalReasonLayoutControlItem.MaxSize = new System.Drawing.Size(0, 24);
            this.RenewalReasonLayoutControlItem.MinSize = new System.Drawing.Size(189, 24);
            this.RenewalReasonLayoutControlItem.Name = "RenewalReasonLayoutControlItem";
            this.RenewalReasonLayoutControlItem.Size = new System.Drawing.Size(312, 24);
            this.RenewalReasonLayoutControlItem.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.RenewalReasonLayoutControlItem.Text = "Reason";
            this.RenewalReasonLayoutControlItem.TextAlignMode = DevExpress.XtraLayout.TextAlignModeItem.CustomSize;
            this.RenewalReasonLayoutControlItem.TextSize = new System.Drawing.Size(130, 13);
            this.RenewalReasonLayoutControlItem.TextToControlDistance = 3;
            // 
            // RenewalPremiumPaidByLayoutControlItem
            // 
            this.RenewalPremiumPaidByLayoutControlItem.Control = this.RenewalPremiumPaidByField;
            this.RenewalPremiumPaidByLayoutControlItem.CustomizationFormText = "Premium Paid By";
            this.RenewalPremiumPaidByLayoutControlItem.Location = new System.Drawing.Point(0, 99);
            this.RenewalPremiumPaidByLayoutControlItem.MaxSize = new System.Drawing.Size(0, 24);
            this.RenewalPremiumPaidByLayoutControlItem.MinSize = new System.Drawing.Size(189, 24);
            this.RenewalPremiumPaidByLayoutControlItem.Name = "RenewalPremiumPaidByLayoutControlItem";
            this.RenewalPremiumPaidByLayoutControlItem.Size = new System.Drawing.Size(312, 24);
            this.RenewalPremiumPaidByLayoutControlItem.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.RenewalPremiumPaidByLayoutControlItem.Text = "Premium Paid By";
            this.RenewalPremiumPaidByLayoutControlItem.TextAlignMode = DevExpress.XtraLayout.TextAlignModeItem.CustomSize;
            this.RenewalPremiumPaidByLayoutControlItem.TextSize = new System.Drawing.Size(130, 13);
            this.RenewalPremiumPaidByLayoutControlItem.TextToControlDistance = 3;
            // 
            // RenewalImposedExcessLayoutControlItem
            // 
            this.RenewalImposedExcessLayoutControlItem.Control = this.RenewalImposedExcessField;
            this.RenewalImposedExcessLayoutControlItem.CustomizationFormText = "Imposed Excess";
            this.RenewalImposedExcessLayoutControlItem.Location = new System.Drawing.Point(0, 123);
            this.RenewalImposedExcessLayoutControlItem.MinSize = new System.Drawing.Size(50, 25);
            this.RenewalImposedExcessLayoutControlItem.Name = "RenewalImposedExcessLayoutControlItem";
            this.RenewalImposedExcessLayoutControlItem.Size = new System.Drawing.Size(174, 30);
            this.RenewalImposedExcessLayoutControlItem.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.RenewalImposedExcessLayoutControlItem.Text = "Imposed Excess";
            this.RenewalImposedExcessLayoutControlItem.TextAlignMode = DevExpress.XtraLayout.TextAlignModeItem.CustomSize;
            this.RenewalImposedExcessLayoutControlItem.TextSize = new System.Drawing.Size(130, 13);
            this.RenewalImposedExcessLayoutControlItem.TextToControlDistance = 3;
            // 
            // RenewExcessAmountLayoutControlItem
            // 
            this.RenewExcessAmountLayoutControlItem.Control = this.textEdit51;
            this.RenewExcessAmountLayoutControlItem.CustomizationFormText = "Renew Excess Amount";
            this.RenewExcessAmountLayoutControlItem.Location = new System.Drawing.Point(0, 153);
            this.RenewExcessAmountLayoutControlItem.MaxSize = new System.Drawing.Size(0, 24);
            this.RenewExcessAmountLayoutControlItem.MinSize = new System.Drawing.Size(189, 24);
            this.RenewExcessAmountLayoutControlItem.Name = "RenewExcessAmountLayoutControlItem";
            this.RenewExcessAmountLayoutControlItem.Size = new System.Drawing.Size(312, 24);
            this.RenewExcessAmountLayoutControlItem.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.RenewExcessAmountLayoutControlItem.Text = "Renew Excess Amount";
            this.RenewExcessAmountLayoutControlItem.TextAlignMode = DevExpress.XtraLayout.TextAlignModeItem.CustomSize;
            this.RenewExcessAmountLayoutControlItem.TextSize = new System.Drawing.Size(130, 13);
            this.RenewExcessAmountLayoutControlItem.TextToControlDistance = 3;
            // 
            // DoNotRenewLayoutControlItem
            // 
            this.DoNotRenewLayoutControlItem.Control = this.DoNotRenewField;
            this.DoNotRenewLayoutControlItem.CustomizationFormText = "Do Not Renew";
            this.DoNotRenewLayoutControlItem.Location = new System.Drawing.Point(0, 50);
            this.DoNotRenewLayoutControlItem.MaxSize = new System.Drawing.Size(185, 25);
            this.DoNotRenewLayoutControlItem.MinSize = new System.Drawing.Size(185, 25);
            this.DoNotRenewLayoutControlItem.Name = "DoNotRenewLayoutControlItem";
            this.DoNotRenewLayoutControlItem.Size = new System.Drawing.Size(185, 25);
            this.DoNotRenewLayoutControlItem.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.DoNotRenewLayoutControlItem.Text = "Do Not Renew";
            this.DoNotRenewLayoutControlItem.TextAlignMode = DevExpress.XtraLayout.TextAlignModeItem.CustomSize;
            this.DoNotRenewLayoutControlItem.TextSize = new System.Drawing.Size(130, 13);
            this.DoNotRenewLayoutControlItem.TextToControlDistance = 3;
            // 
            // emptySpaceItem28
            // 
            this.emptySpaceItem28.AllowHotTrack = false;
            this.emptySpaceItem28.CustomizationFormText = "emptySpaceItem28";
            this.emptySpaceItem28.Location = new System.Drawing.Point(174, 123);
            this.emptySpaceItem28.Name = "emptySpaceItem28";
            this.emptySpaceItem28.Size = new System.Drawing.Size(138, 30);
            this.emptySpaceItem28.TextSize = new System.Drawing.Size(0, 0);
            // 
            // emptySpaceItem29
            // 
            this.emptySpaceItem29.AllowHotTrack = false;
            this.emptySpaceItem29.CustomizationFormText = "emptySpaceItem29";
            this.emptySpaceItem29.Location = new System.Drawing.Point(249, 0);
            this.emptySpaceItem29.Name = "emptySpaceItem29";
            this.emptySpaceItem29.Size = new System.Drawing.Size(63, 25);
            this.emptySpaceItem29.TextSize = new System.Drawing.Size(0, 0);
            // 
            // emptySpaceItem27
            // 
            this.emptySpaceItem27.AllowHotTrack = false;
            this.emptySpaceItem27.CustomizationFormText = "emptySpaceItem27";
            this.emptySpaceItem27.Location = new System.Drawing.Point(185, 25);
            this.emptySpaceItem27.Name = "emptySpaceItem27";
            this.emptySpaceItem27.Size = new System.Drawing.Size(127, 50);
            this.emptySpaceItem27.TextSize = new System.Drawing.Size(0, 0);
            // 
            // RenewalsOnHoldLayoutControlItem
            // 
            this.RenewalsOnHoldLayoutControlItem.Control = this.RenewalsOnHoldField;
            this.RenewalsOnHoldLayoutControlItem.CustomizationFormText = "Renewals On Hold";
            this.RenewalsOnHoldLayoutControlItem.Location = new System.Drawing.Point(0, 25);
            this.RenewalsOnHoldLayoutControlItem.MaxSize = new System.Drawing.Size(185, 25);
            this.RenewalsOnHoldLayoutControlItem.MinSize = new System.Drawing.Size(185, 25);
            this.RenewalsOnHoldLayoutControlItem.Name = "RenewalsOnHoldLayoutControlItem";
            this.RenewalsOnHoldLayoutControlItem.Size = new System.Drawing.Size(185, 25);
            this.RenewalsOnHoldLayoutControlItem.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.RenewalsOnHoldLayoutControlItem.Text = "Renewals On Hold";
            this.RenewalsOnHoldLayoutControlItem.TextAlignMode = DevExpress.XtraLayout.TextAlignModeItem.CustomSize;
            this.RenewalsOnHoldLayoutControlItem.TextSize = new System.Drawing.Size(130, 13);
            this.RenewalsOnHoldLayoutControlItem.TextToControlDistance = 3;
            // 
            // RenewingDiscountsGroup
            // 
            this.RenewingDiscountsGroup.CustomizationFormText = "Renewing Discounts";
            this.RenewingDiscountsGroup.Items.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.RenewalMultiplePropertyDiscountLayoutControlItem,
            this.ApplyToRenewingPremiumLayoutControlItem,
            this.RenewalStaffDiscountLayoutControlItem,
            this.RealEstateDiscountLayoutControlItem,
            this.emptySpaceItem31,
            this.RenewalDiscountAmountLayoutControlItem,
            this.emptySpaceItem11,
            this.emptySpaceItem36});
            this.RenewingDiscountsGroup.Location = new System.Drawing.Point(336, 257);
            this.RenewingDiscountsGroup.Name = "RenewingDiscountsGroup";
            this.RenewingDiscountsGroup.Size = new System.Drawing.Size(320, 219);
            this.RenewingDiscountsGroup.Text = "Renewing Discounts";
            // 
            // RenewalMultiplePropertyDiscountLayoutControlItem
            // 
            this.RenewalMultiplePropertyDiscountLayoutControlItem.Control = this.RenewalMultiPropertyDiscountField;
            this.RenewalMultiplePropertyDiscountLayoutControlItem.CustomizationFormText = "Multiple Property Discount";
            this.RenewalMultiplePropertyDiscountLayoutControlItem.Location = new System.Drawing.Point(0, 0);
            this.RenewalMultiplePropertyDiscountLayoutControlItem.MaxSize = new System.Drawing.Size(190, 25);
            this.RenewalMultiplePropertyDiscountLayoutControlItem.MinSize = new System.Drawing.Size(190, 25);
            this.RenewalMultiplePropertyDiscountLayoutControlItem.Name = "RenewalMultiplePropertyDiscountLayoutControlItem";
            this.RenewalMultiplePropertyDiscountLayoutControlItem.Size = new System.Drawing.Size(190, 25);
            this.RenewalMultiplePropertyDiscountLayoutControlItem.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.RenewalMultiplePropertyDiscountLayoutControlItem.Text = "Multiple Property Discount";
            this.RenewalMultiplePropertyDiscountLayoutControlItem.TextAlignMode = DevExpress.XtraLayout.TextAlignModeItem.CustomSize;
            this.RenewalMultiplePropertyDiscountLayoutControlItem.TextSize = new System.Drawing.Size(135, 13);
            this.RenewalMultiplePropertyDiscountLayoutControlItem.TextToControlDistance = 3;
            // 
            // ApplyToRenewingPremiumLayoutControlItem
            // 
            this.ApplyToRenewingPremiumLayoutControlItem.Control = this.ApplyToRenewingPremiumField;
            this.ApplyToRenewingPremiumLayoutControlItem.CustomizationFormText = "Apply to Renewing Premium";
            this.ApplyToRenewingPremiumLayoutControlItem.Location = new System.Drawing.Point(0, 99);
            this.ApplyToRenewingPremiumLayoutControlItem.MaxSize = new System.Drawing.Size(190, 25);
            this.ApplyToRenewingPremiumLayoutControlItem.MinSize = new System.Drawing.Size(190, 25);
            this.ApplyToRenewingPremiumLayoutControlItem.Name = "ApplyToRenewingPremiumLayoutControlItem";
            this.ApplyToRenewingPremiumLayoutControlItem.Size = new System.Drawing.Size(190, 25);
            this.ApplyToRenewingPremiumLayoutControlItem.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.ApplyToRenewingPremiumLayoutControlItem.Text = "Apply to Renewing Premium";
            this.ApplyToRenewingPremiumLayoutControlItem.TextAlignMode = DevExpress.XtraLayout.TextAlignModeItem.CustomSize;
            this.ApplyToRenewingPremiumLayoutControlItem.TextSize = new System.Drawing.Size(135, 13);
            this.ApplyToRenewingPremiumLayoutControlItem.TextToControlDistance = 3;
            // 
            // RenewalStaffDiscountLayoutControlItem
            // 
            this.RenewalStaffDiscountLayoutControlItem.Control = this.RenewalStaffDiscountField;
            this.RenewalStaffDiscountLayoutControlItem.CustomizationFormText = "Staff Discount";
            this.RenewalStaffDiscountLayoutControlItem.Location = new System.Drawing.Point(0, 50);
            this.RenewalStaffDiscountLayoutControlItem.MaxSize = new System.Drawing.Size(190, 25);
            this.RenewalStaffDiscountLayoutControlItem.MinSize = new System.Drawing.Size(190, 25);
            this.RenewalStaffDiscountLayoutControlItem.Name = "RenewalStaffDiscountLayoutControlItem";
            this.RenewalStaffDiscountLayoutControlItem.Size = new System.Drawing.Size(190, 25);
            this.RenewalStaffDiscountLayoutControlItem.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.RenewalStaffDiscountLayoutControlItem.Text = "Staff Discount";
            this.RenewalStaffDiscountLayoutControlItem.TextAlignMode = DevExpress.XtraLayout.TextAlignModeItem.CustomSize;
            this.RenewalStaffDiscountLayoutControlItem.TextSize = new System.Drawing.Size(135, 13);
            this.RenewalStaffDiscountLayoutControlItem.TextToControlDistance = 3;
            // 
            // RealEstateDiscountLayoutControlItem
            // 
            this.RealEstateDiscountLayoutControlItem.Control = this.RealEstateDiscountField;
            this.RealEstateDiscountLayoutControlItem.CustomizationFormText = "Real Estate Discount Amount";
            this.RealEstateDiscountLayoutControlItem.Location = new System.Drawing.Point(0, 25);
            this.RealEstateDiscountLayoutControlItem.MaxSize = new System.Drawing.Size(190, 25);
            this.RealEstateDiscountLayoutControlItem.MinSize = new System.Drawing.Size(190, 25);
            this.RealEstateDiscountLayoutControlItem.Name = "RealEstateDiscountLayoutControlItem";
            this.RealEstateDiscountLayoutControlItem.Size = new System.Drawing.Size(190, 25);
            this.RealEstateDiscountLayoutControlItem.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.RealEstateDiscountLayoutControlItem.Text = "Real Estate Discount";
            this.RealEstateDiscountLayoutControlItem.TextAlignMode = DevExpress.XtraLayout.TextAlignModeItem.CustomSize;
            this.RealEstateDiscountLayoutControlItem.TextSize = new System.Drawing.Size(135, 13);
            this.RealEstateDiscountLayoutControlItem.TextToControlDistance = 3;
            // 
            // emptySpaceItem31
            // 
            this.emptySpaceItem31.AllowHotTrack = false;
            this.emptySpaceItem31.Location = new System.Drawing.Point(0, 124);
            this.emptySpaceItem31.Name = "emptySpaceItem31";
            this.emptySpaceItem31.Size = new System.Drawing.Size(296, 53);
            this.emptySpaceItem31.TextSize = new System.Drawing.Size(0, 0);
            // 
            // RenewalDiscountAmountLayoutControlItem
            // 
            this.RenewalDiscountAmountLayoutControlItem.Control = this.RenewalDiscountAmountField;
            this.RenewalDiscountAmountLayoutControlItem.CustomizationFormText = "Discount Amount";
            this.RenewalDiscountAmountLayoutControlItem.Location = new System.Drawing.Point(0, 75);
            this.RenewalDiscountAmountLayoutControlItem.Name = "RenewalDiscountAmountLayoutControlItem";
            this.RenewalDiscountAmountLayoutControlItem.Size = new System.Drawing.Size(296, 24);
            this.RenewalDiscountAmountLayoutControlItem.Text = "Discount Amount";
            this.RenewalDiscountAmountLayoutControlItem.TextAlignMode = DevExpress.XtraLayout.TextAlignModeItem.CustomSize;
            this.RenewalDiscountAmountLayoutControlItem.TextSize = new System.Drawing.Size(135, 13);
            this.RenewalDiscountAmountLayoutControlItem.TextToControlDistance = 3;
            // 
            // emptySpaceItem11
            // 
            this.emptySpaceItem11.AllowHotTrack = false;
            this.emptySpaceItem11.Location = new System.Drawing.Point(190, 99);
            this.emptySpaceItem11.Name = "emptySpaceItem11";
            this.emptySpaceItem11.Size = new System.Drawing.Size(106, 25);
            this.emptySpaceItem11.TextSize = new System.Drawing.Size(0, 0);
            // 
            // emptySpaceItem36
            // 
            this.emptySpaceItem36.AllowHotTrack = false;
            this.emptySpaceItem36.Location = new System.Drawing.Point(190, 0);
            this.emptySpaceItem36.Name = "emptySpaceItem36";
            this.emptySpaceItem36.Size = new System.Drawing.Size(106, 75);
            this.emptySpaceItem36.TextSize = new System.Drawing.Size(0, 0);
            // 
            // RenewalProcessingGroup
            // 
            this.RenewalProcessingGroup.CustomizationFormText = "Renewal Processing";
            this.RenewalProcessingGroup.Items.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.RenewedTransactionLayoutControlItem,
            this.RenewedByTransactionLayoutControlItem,
            this.LastRenewalErrorLayoutControlItem,
            this.RenewalBatchLayoutControlItem,
            this.emptySpaceItem19,
            this.emptySpaceItem22,
            this.emptySpaceItem23});
            this.RenewalProcessingGroup.Location = new System.Drawing.Point(656, 257);
            this.RenewalProcessingGroup.Name = "RenewalProcessingGroup";
            this.RenewalProcessingGroup.Size = new System.Drawing.Size(541, 219);
            this.RenewalProcessingGroup.Text = "Renewal Processing";
            // 
            // RenewedTransactionLayoutControlItem
            // 
            this.RenewedTransactionLayoutControlItem.Control = this.RenewedTransactionField;
            this.RenewedTransactionLayoutControlItem.CustomizationFormText = "Renewed Transaction";
            this.RenewedTransactionLayoutControlItem.Location = new System.Drawing.Point(0, 0);
            this.RenewedTransactionLayoutControlItem.Name = "RenewedTransactionLayoutControlItem";
            this.RenewedTransactionLayoutControlItem.Size = new System.Drawing.Size(245, 24);
            this.RenewedTransactionLayoutControlItem.Text = "Renewed Transaction";
            this.RenewedTransactionLayoutControlItem.TextAlignMode = DevExpress.XtraLayout.TextAlignModeItem.CustomSize;
            this.RenewedTransactionLayoutControlItem.TextSize = new System.Drawing.Size(130, 13);
            this.RenewedTransactionLayoutControlItem.TextToControlDistance = 3;
            // 
            // RenewedByTransactionLayoutControlItem
            // 
            this.RenewedByTransactionLayoutControlItem.Control = this.RenewedByTransactionField;
            this.RenewedByTransactionLayoutControlItem.CustomizationFormText = "Renewed By Transaction";
            this.RenewedByTransactionLayoutControlItem.Location = new System.Drawing.Point(0, 24);
            this.RenewedByTransactionLayoutControlItem.Name = "RenewedByTransactionLayoutControlItem";
            this.RenewedByTransactionLayoutControlItem.Size = new System.Drawing.Size(245, 24);
            this.RenewedByTransactionLayoutControlItem.Text = "Renewed By Transaction";
            this.RenewedByTransactionLayoutControlItem.TextAlignMode = DevExpress.XtraLayout.TextAlignModeItem.CustomSize;
            this.RenewedByTransactionLayoutControlItem.TextSize = new System.Drawing.Size(130, 13);
            this.RenewedByTransactionLayoutControlItem.TextToControlDistance = 3;
            // 
            // LastRenewalErrorLayoutControlItem
            // 
            this.LastRenewalErrorLayoutControlItem.Control = this.LastRenewalErrorField;
            this.LastRenewalErrorLayoutControlItem.CustomizationFormText = "Last Renewal Error";
            this.LastRenewalErrorLayoutControlItem.Location = new System.Drawing.Point(0, 48);
            this.LastRenewalErrorLayoutControlItem.MaxSize = new System.Drawing.Size(0, 66);
            this.LastRenewalErrorLayoutControlItem.MinSize = new System.Drawing.Size(149, 66);
            this.LastRenewalErrorLayoutControlItem.Name = "LastRenewalErrorLayoutControlItem";
            this.LastRenewalErrorLayoutControlItem.Size = new System.Drawing.Size(517, 66);
            this.LastRenewalErrorLayoutControlItem.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.LastRenewalErrorLayoutControlItem.Text = "Last Renewal Error";
            this.LastRenewalErrorLayoutControlItem.TextAlignMode = DevExpress.XtraLayout.TextAlignModeItem.CustomSize;
            this.LastRenewalErrorLayoutControlItem.TextSize = new System.Drawing.Size(130, 13);
            this.LastRenewalErrorLayoutControlItem.TextToControlDistance = 3;
            // 
            // RenewalBatchLayoutControlItem
            // 
            this.RenewalBatchLayoutControlItem.Control = this.RenewalBatchField;
            this.RenewalBatchLayoutControlItem.CustomizationFormText = "Renewal Batch";
            this.RenewalBatchLayoutControlItem.Location = new System.Drawing.Point(0, 114);
            this.RenewalBatchLayoutControlItem.Name = "RenewalBatchLayoutControlItem";
            this.RenewalBatchLayoutControlItem.Size = new System.Drawing.Size(245, 24);
            this.RenewalBatchLayoutControlItem.Text = "Renewal Batch";
            this.RenewalBatchLayoutControlItem.TextAlignMode = DevExpress.XtraLayout.TextAlignModeItem.CustomSize;
            this.RenewalBatchLayoutControlItem.TextSize = new System.Drawing.Size(130, 13);
            this.RenewalBatchLayoutControlItem.TextToControlDistance = 3;
            // 
            // emptySpaceItem19
            // 
            this.emptySpaceItem19.AllowHotTrack = false;
            this.emptySpaceItem19.CustomizationFormText = "emptySpaceItem19";
            this.emptySpaceItem19.Location = new System.Drawing.Point(0, 138);
            this.emptySpaceItem19.MinSize = new System.Drawing.Size(104, 24);
            this.emptySpaceItem19.Name = "emptySpaceItem19";
            this.emptySpaceItem19.Size = new System.Drawing.Size(517, 39);
            this.emptySpaceItem19.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.emptySpaceItem19.TextSize = new System.Drawing.Size(0, 0);
            // 
            // emptySpaceItem22
            // 
            this.emptySpaceItem22.AllowHotTrack = false;
            this.emptySpaceItem22.CustomizationFormText = "emptySpaceItem22";
            this.emptySpaceItem22.Location = new System.Drawing.Point(245, 0);
            this.emptySpaceItem22.Name = "emptySpaceItem22";
            this.emptySpaceItem22.Size = new System.Drawing.Size(272, 48);
            this.emptySpaceItem22.TextSize = new System.Drawing.Size(0, 0);
            // 
            // emptySpaceItem23
            // 
            this.emptySpaceItem23.AllowHotTrack = false;
            this.emptySpaceItem23.CustomizationFormText = "emptySpaceItem23";
            this.emptySpaceItem23.Location = new System.Drawing.Point(245, 114);
            this.emptySpaceItem23.Name = "emptySpaceItem23";
            this.emptySpaceItem23.Size = new System.Drawing.Size(272, 24);
            this.emptySpaceItem23.TextSize = new System.Drawing.Size(0, 0);
            // 
            // emptySpaceItem30
            // 
            this.emptySpaceItem30.AllowHotTrack = false;
            this.emptySpaceItem30.CustomizationFormText = "emptySpaceItem30";
            this.emptySpaceItem30.Location = new System.Drawing.Point(0, 476);
            this.emptySpaceItem30.MinSize = new System.Drawing.Size(104, 24);
            this.emptySpaceItem30.Name = "emptySpaceItem30";
            this.emptySpaceItem30.Size = new System.Drawing.Size(1197, 105);
            this.emptySpaceItem30.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.emptySpaceItem30.TextSize = new System.Drawing.Size(0, 0);
            // 
            // LedgerTabGroup
            // 
            this.LedgerTabGroup.CaptionImageOptions.SvgImage = ((DevExpress.Utils.Svg.SvgImage)(resources.GetObject("LedgerTabGroup.CaptionImageOptions.SvgImage")));
            this.LedgerTabGroup.CaptionImageOptions.SvgImageSize = new System.Drawing.Size(16, 16);
            this.LedgerTabGroup.Items.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.LedgerHoldCoverLayoutControlItem,
            this.LedgerCurrentOnLayoutControlItem,
            this.LedgerInvoiceOverdueOnLayoutControlItem,
            this.LedgerOverdueNoticeSentLayoutControlItem,
            this.LedgerExtendedLapsingOnLayoutControlItem,
            this.LedgerPremiumPaidLayoutControlItem,
            this.LedgerDistributeFundsLayoutControlItem,
            this.LedgerExtendedOverdueOnLayoutControlItem,
            this.LedgerFinalReminderSentLayoutControlItem,
            this.LedgerCancelledLapsingOnLayoutControlItem,
            this.emptySpaceItem8,
            this.layoutControlItem8});
            this.LedgerTabGroup.Location = new System.Drawing.Point(0, 0);
            this.LedgerTabGroup.Name = "LedgerTabGroup";
            this.LedgerTabGroup.Size = new System.Drawing.Size(1197, 581);
            this.LedgerTabGroup.Text = "Ledger";
            // 
            // LedgerHoldCoverLayoutControlItem
            // 
            this.LedgerHoldCoverLayoutControlItem.Control = this.LedgerHoldCoverField;
            this.LedgerHoldCoverLayoutControlItem.CustomizationFormText = "Hold Cover";
            this.LedgerHoldCoverLayoutControlItem.Location = new System.Drawing.Point(0, 0);
            this.LedgerHoldCoverLayoutControlItem.Name = "LedgerHoldCoverLayoutControlItem";
            this.LedgerHoldCoverLayoutControlItem.Size = new System.Drawing.Size(195, 24);
            this.LedgerHoldCoverLayoutControlItem.Text = "Hold Cover";
            this.LedgerHoldCoverLayoutControlItem.TextAlignMode = DevExpress.XtraLayout.TextAlignModeItem.CustomSize;
            this.LedgerHoldCoverLayoutControlItem.TextSize = new System.Drawing.Size(88, 13);
            this.LedgerHoldCoverLayoutControlItem.TextToControlDistance = 3;
            // 
            // LedgerCurrentOnLayoutControlItem
            // 
            this.LedgerCurrentOnLayoutControlItem.Control = this.LedgerCurrentOnField;
            this.LedgerCurrentOnLayoutControlItem.CustomizationFormText = "Current On";
            this.LedgerCurrentOnLayoutControlItem.Location = new System.Drawing.Point(195, 0);
            this.LedgerCurrentOnLayoutControlItem.Name = "LedgerCurrentOnLayoutControlItem";
            this.LedgerCurrentOnLayoutControlItem.Size = new System.Drawing.Size(233, 24);
            this.LedgerCurrentOnLayoutControlItem.Spacing = new DevExpress.XtraLayout.Utils.Padding(20, 0, 0, 0);
            this.LedgerCurrentOnLayoutControlItem.Text = "Current On";
            this.LedgerCurrentOnLayoutControlItem.TextAlignMode = DevExpress.XtraLayout.TextAlignModeItem.CustomSize;
            this.LedgerCurrentOnLayoutControlItem.TextSize = new System.Drawing.Size(108, 13);
            this.LedgerCurrentOnLayoutControlItem.TextToControlDistance = 3;
            // 
            // LedgerInvoiceOverdueOnLayoutControlItem
            // 
            this.LedgerInvoiceOverdueOnLayoutControlItem.Control = this.LedgerInvoiceOverdueOnField;
            this.LedgerInvoiceOverdueOnLayoutControlItem.CustomizationFormText = "Invoice Overdue On";
            this.LedgerInvoiceOverdueOnLayoutControlItem.Location = new System.Drawing.Point(428, 0);
            this.LedgerInvoiceOverdueOnLayoutControlItem.Name = "LedgerInvoiceOverdueOnLayoutControlItem";
            this.LedgerInvoiceOverdueOnLayoutControlItem.Size = new System.Drawing.Size(219, 24);
            this.LedgerInvoiceOverdueOnLayoutControlItem.Spacing = new DevExpress.XtraLayout.Utils.Padding(20, 0, 0, 0);
            this.LedgerInvoiceOverdueOnLayoutControlItem.Text = "Invoice Overdue On";
            this.LedgerInvoiceOverdueOnLayoutControlItem.TextAlignMode = DevExpress.XtraLayout.TextAlignModeItem.CustomSize;
            this.LedgerInvoiceOverdueOnLayoutControlItem.TextSize = new System.Drawing.Size(108, 13);
            this.LedgerInvoiceOverdueOnLayoutControlItem.TextToControlDistance = 3;
            // 
            // LedgerOverdueNoticeSentLayoutControlItem
            // 
            this.LedgerOverdueNoticeSentLayoutControlItem.Control = this.LedgerOverdueNoticeSentField;
            this.LedgerOverdueNoticeSentLayoutControlItem.CustomizationFormText = "Overdue Notice Sent";
            this.LedgerOverdueNoticeSentLayoutControlItem.Location = new System.Drawing.Point(647, 0);
            this.LedgerOverdueNoticeSentLayoutControlItem.Name = "LedgerOverdueNoticeSentLayoutControlItem";
            this.LedgerOverdueNoticeSentLayoutControlItem.Size = new System.Drawing.Size(218, 24);
            this.LedgerOverdueNoticeSentLayoutControlItem.Spacing = new DevExpress.XtraLayout.Utils.Padding(20, 0, 0, 0);
            this.LedgerOverdueNoticeSentLayoutControlItem.Text = "Overdue Notice Sent";
            this.LedgerOverdueNoticeSentLayoutControlItem.TextAlignMode = DevExpress.XtraLayout.TextAlignModeItem.CustomSize;
            this.LedgerOverdueNoticeSentLayoutControlItem.TextSize = new System.Drawing.Size(108, 13);
            this.LedgerOverdueNoticeSentLayoutControlItem.TextToControlDistance = 3;
            // 
            // LedgerExtendedLapsingOnLayoutControlItem
            // 
            this.LedgerExtendedLapsingOnLayoutControlItem.Control = this.LedgerExtendedLapsingOnField;
            this.LedgerExtendedLapsingOnLayoutControlItem.CustomizationFormText = "Extended Lapsing On";
            this.LedgerExtendedLapsingOnLayoutControlItem.Location = new System.Drawing.Point(865, 0);
            this.LedgerExtendedLapsingOnLayoutControlItem.Name = "LedgerExtendedLapsingOnLayoutControlItem";
            this.LedgerExtendedLapsingOnLayoutControlItem.Size = new System.Drawing.Size(218, 24);
            this.LedgerExtendedLapsingOnLayoutControlItem.Spacing = new DevExpress.XtraLayout.Utils.Padding(20, 0, 0, 0);
            this.LedgerExtendedLapsingOnLayoutControlItem.Text = "Extended Lapsing On";
            this.LedgerExtendedLapsingOnLayoutControlItem.TextAlignMode = DevExpress.XtraLayout.TextAlignModeItem.CustomSize;
            this.LedgerExtendedLapsingOnLayoutControlItem.TextSize = new System.Drawing.Size(108, 13);
            this.LedgerExtendedLapsingOnLayoutControlItem.TextToControlDistance = 3;
            // 
            // LedgerPremiumPaidLayoutControlItem
            // 
            this.LedgerPremiumPaidLayoutControlItem.Control = this.LedgerPremiumPaidField;
            this.LedgerPremiumPaidLayoutControlItem.CustomizationFormText = "Premium Paid";
            this.LedgerPremiumPaidLayoutControlItem.Location = new System.Drawing.Point(0, 24);
            this.LedgerPremiumPaidLayoutControlItem.Name = "LedgerPremiumPaidLayoutControlItem";
            this.LedgerPremiumPaidLayoutControlItem.Size = new System.Drawing.Size(195, 24);
            this.LedgerPremiumPaidLayoutControlItem.Text = "Premium Paid";
            this.LedgerPremiumPaidLayoutControlItem.TextAlignMode = DevExpress.XtraLayout.TextAlignModeItem.CustomSize;
            this.LedgerPremiumPaidLayoutControlItem.TextSize = new System.Drawing.Size(88, 13);
            this.LedgerPremiumPaidLayoutControlItem.TextToControlDistance = 3;
            // 
            // LedgerDistributeFundsLayoutControlItem
            // 
            this.LedgerDistributeFundsLayoutControlItem.Control = this.LedgerDistributeFundsField;
            this.LedgerDistributeFundsLayoutControlItem.CustomizationFormText = "Distribute Funds";
            this.LedgerDistributeFundsLayoutControlItem.Location = new System.Drawing.Point(195, 24);
            this.LedgerDistributeFundsLayoutControlItem.Name = "LedgerDistributeFundsLayoutControlItem";
            this.LedgerDistributeFundsLayoutControlItem.Size = new System.Drawing.Size(233, 24);
            this.LedgerDistributeFundsLayoutControlItem.Spacing = new DevExpress.XtraLayout.Utils.Padding(20, 0, 0, 0);
            this.LedgerDistributeFundsLayoutControlItem.Text = "Distribute Funds";
            this.LedgerDistributeFundsLayoutControlItem.TextAlignMode = DevExpress.XtraLayout.TextAlignModeItem.CustomSize;
            this.LedgerDistributeFundsLayoutControlItem.TextSize = new System.Drawing.Size(108, 13);
            this.LedgerDistributeFundsLayoutControlItem.TextToControlDistance = 3;
            // 
            // LedgerExtendedOverdueOnLayoutControlItem
            // 
            this.LedgerExtendedOverdueOnLayoutControlItem.Control = this.LedgerExtendedOverdueOnField;
            this.LedgerExtendedOverdueOnLayoutControlItem.CustomizationFormText = "Extended Overdue On";
            this.LedgerExtendedOverdueOnLayoutControlItem.Location = new System.Drawing.Point(428, 24);
            this.LedgerExtendedOverdueOnLayoutControlItem.Name = "LedgerExtendedOverdueOnLayoutControlItem";
            this.LedgerExtendedOverdueOnLayoutControlItem.Size = new System.Drawing.Size(219, 24);
            this.LedgerExtendedOverdueOnLayoutControlItem.Spacing = new DevExpress.XtraLayout.Utils.Padding(20, 0, 0, 0);
            this.LedgerExtendedOverdueOnLayoutControlItem.Text = "Extended Overdue On";
            this.LedgerExtendedOverdueOnLayoutControlItem.TextAlignMode = DevExpress.XtraLayout.TextAlignModeItem.CustomSize;
            this.LedgerExtendedOverdueOnLayoutControlItem.TextSize = new System.Drawing.Size(108, 13);
            this.LedgerExtendedOverdueOnLayoutControlItem.TextToControlDistance = 3;
            // 
            // LedgerFinalReminderSentLayoutControlItem
            // 
            this.LedgerFinalReminderSentLayoutControlItem.Control = this.LedgerFinalReminderSentField;
            this.LedgerFinalReminderSentLayoutControlItem.CustomizationFormText = "Final Reminder Sent";
            this.LedgerFinalReminderSentLayoutControlItem.Location = new System.Drawing.Point(647, 24);
            this.LedgerFinalReminderSentLayoutControlItem.Name = "LedgerFinalReminderSentLayoutControlItem";
            this.LedgerFinalReminderSentLayoutControlItem.Size = new System.Drawing.Size(218, 24);
            this.LedgerFinalReminderSentLayoutControlItem.Spacing = new DevExpress.XtraLayout.Utils.Padding(20, 0, 0, 0);
            this.LedgerFinalReminderSentLayoutControlItem.Text = "Final Reminder Sent";
            this.LedgerFinalReminderSentLayoutControlItem.TextAlignMode = DevExpress.XtraLayout.TextAlignModeItem.CustomSize;
            this.LedgerFinalReminderSentLayoutControlItem.TextSize = new System.Drawing.Size(108, 13);
            this.LedgerFinalReminderSentLayoutControlItem.TextToControlDistance = 3;
            // 
            // LedgerCancelledLapsingOnLayoutControlItem
            // 
            this.LedgerCancelledLapsingOnLayoutControlItem.Control = this.LedgerCancelledLapsedOnField;
            this.LedgerCancelledLapsingOnLayoutControlItem.CustomizationFormText = "Cancelled Lapsed On";
            this.LedgerCancelledLapsingOnLayoutControlItem.Location = new System.Drawing.Point(865, 24);
            this.LedgerCancelledLapsingOnLayoutControlItem.Name = "LedgerCancelledLapsingOnLayoutControlItem";
            this.LedgerCancelledLapsingOnLayoutControlItem.Size = new System.Drawing.Size(218, 24);
            this.LedgerCancelledLapsingOnLayoutControlItem.Spacing = new DevExpress.XtraLayout.Utils.Padding(20, 0, 0, 0);
            this.LedgerCancelledLapsingOnLayoutControlItem.Text = "Cancelled Lapsed On";
            this.LedgerCancelledLapsingOnLayoutControlItem.TextAlignMode = DevExpress.XtraLayout.TextAlignModeItem.CustomSize;
            this.LedgerCancelledLapsingOnLayoutControlItem.TextSize = new System.Drawing.Size(108, 13);
            this.LedgerCancelledLapsingOnLayoutControlItem.TextToControlDistance = 3;
            // 
            // emptySpaceItem8
            // 
            this.emptySpaceItem8.AllowHotTrack = false;
            this.emptySpaceItem8.Location = new System.Drawing.Point(1083, 0);
            this.emptySpaceItem8.Name = "emptySpaceItem8";
            this.emptySpaceItem8.Size = new System.Drawing.Size(114, 48);
            this.emptySpaceItem8.TextSize = new System.Drawing.Size(0, 0);
            // 
            // layoutControlItem8
            // 
            this.layoutControlItem8.Control = this.LedgerGrid;
            this.layoutControlItem8.Location = new System.Drawing.Point(0, 48);
            this.layoutControlItem8.Name = "layoutControlItem8";
            this.layoutControlItem8.Size = new System.Drawing.Size(1197, 533);
            this.layoutControlItem8.TextSize = new System.Drawing.Size(0, 0);
            this.layoutControlItem8.TextVisible = false;
            // 
            // NotesTabGroup
            // 
            this.NotesTabGroup.CaptionImageOptions.SvgImage = ((DevExpress.Utils.Svg.SvgImage)(resources.GetObject("NotesTabGroup.CaptionImageOptions.SvgImage")));
            this.NotesTabGroup.CaptionImageOptions.SvgImageSize = new System.Drawing.Size(16, 16);
            this.NotesTabGroup.Items.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.LogGroup});
            this.NotesTabGroup.Location = new System.Drawing.Point(0, 0);
            this.NotesTabGroup.Name = "NotesTabGroup";
            this.NotesTabGroup.Size = new System.Drawing.Size(1197, 581);
            this.NotesTabGroup.Text = "Notes";
            // 
            // LogGroup
            // 
            this.LogGroup.Items.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.NotesAndSpecialConditionsGroup,
            this.SchemePaidLayoutControlItem,
            this.emptySpaceItem9,
            this.NotesCreatedByLayoutControlItem,
            this.NotesCreatedOnLayoutControlItem,
            this.NotesLastModifiedByLayoutControlItem,
            this.NotesLastModifiedOnLayoutControlItem,
            this.layoutControlItem2});
            this.LogGroup.Location = new System.Drawing.Point(0, 0);
            this.LogGroup.Name = "LogGroup";
            this.LogGroup.Size = new System.Drawing.Size(1197, 581);
            this.LogGroup.Text = "Log";
            // 
            // NotesAndSpecialConditionsGroup
            // 
            this.NotesAndSpecialConditionsGroup.Items.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.layoutControlItem1});
            this.NotesAndSpecialConditionsGroup.Location = new System.Drawing.Point(0, 325);
            this.NotesAndSpecialConditionsGroup.Name = "NotesAndSpecialConditionsGroup";
            this.NotesAndSpecialConditionsGroup.Size = new System.Drawing.Size(872, 214);
            this.NotesAndSpecialConditionsGroup.Text = "Notes / Special Conditions";
            // 
            // layoutControlItem1
            // 
            this.layoutControlItem1.Control = this.NotesField;
            this.layoutControlItem1.Location = new System.Drawing.Point(0, 0);
            this.layoutControlItem1.Name = "layoutControlItem1";
            this.layoutControlItem1.Size = new System.Drawing.Size(848, 172);
            this.layoutControlItem1.TextSize = new System.Drawing.Size(0, 0);
            this.layoutControlItem1.TextVisible = false;
            // 
            // SchemePaidLayoutControlItem
            // 
            this.SchemePaidLayoutControlItem.Control = this.SchemePaidField;
            this.SchemePaidLayoutControlItem.CustomizationFormText = "Scheme Paid";
            this.SchemePaidLayoutControlItem.Location = new System.Drawing.Point(872, 325);
            this.SchemePaidLayoutControlItem.Name = "SchemePaidLayoutControlItem";
            this.SchemePaidLayoutControlItem.Size = new System.Drawing.Size(301, 24);
            this.SchemePaidLayoutControlItem.Spacing = new DevExpress.XtraLayout.Utils.Padding(20, 0, 0, 0);
            this.SchemePaidLayoutControlItem.Text = "Scheme Paid";
            this.SchemePaidLayoutControlItem.TextAlignMode = DevExpress.XtraLayout.TextAlignModeItem.CustomSize;
            this.SchemePaidLayoutControlItem.TextSize = new System.Drawing.Size(88, 13);
            this.SchemePaidLayoutControlItem.TextToControlDistance = 3;
            // 
            // emptySpaceItem9
            // 
            this.emptySpaceItem9.AllowHotTrack = false;
            this.emptySpaceItem9.CustomizationFormText = "emptySpaceItem9";
            this.emptySpaceItem9.Location = new System.Drawing.Point(872, 349);
            this.emptySpaceItem9.Name = "emptySpaceItem9";
            this.emptySpaceItem9.Size = new System.Drawing.Size(301, 94);
            this.emptySpaceItem9.TextSize = new System.Drawing.Size(0, 0);
            // 
            // NotesCreatedByLayoutControlItem
            // 
            this.NotesCreatedByLayoutControlItem.Control = this.NotesCreatedByField;
            this.NotesCreatedByLayoutControlItem.CustomizationFormText = "Created By";
            this.NotesCreatedByLayoutControlItem.Location = new System.Drawing.Point(872, 443);
            this.NotesCreatedByLayoutControlItem.Name = "NotesCreatedByLayoutControlItem";
            this.NotesCreatedByLayoutControlItem.Size = new System.Drawing.Size(301, 24);
            this.NotesCreatedByLayoutControlItem.Spacing = new DevExpress.XtraLayout.Utils.Padding(20, 0, 0, 0);
            this.NotesCreatedByLayoutControlItem.Text = "Created By";
            this.NotesCreatedByLayoutControlItem.TextAlignMode = DevExpress.XtraLayout.TextAlignModeItem.CustomSize;
            this.NotesCreatedByLayoutControlItem.TextSize = new System.Drawing.Size(88, 13);
            this.NotesCreatedByLayoutControlItem.TextToControlDistance = 3;
            // 
            // NotesCreatedOnLayoutControlItem
            // 
            this.NotesCreatedOnLayoutControlItem.Control = this.NotesCreatedOnField;
            this.NotesCreatedOnLayoutControlItem.CustomizationFormText = "Created On";
            this.NotesCreatedOnLayoutControlItem.Location = new System.Drawing.Point(872, 467);
            this.NotesCreatedOnLayoutControlItem.Name = "NotesCreatedOnLayoutControlItem";
            this.NotesCreatedOnLayoutControlItem.Size = new System.Drawing.Size(301, 24);
            this.NotesCreatedOnLayoutControlItem.Spacing = new DevExpress.XtraLayout.Utils.Padding(20, 0, 0, 0);
            this.NotesCreatedOnLayoutControlItem.Text = "Created On";
            this.NotesCreatedOnLayoutControlItem.TextAlignMode = DevExpress.XtraLayout.TextAlignModeItem.CustomSize;
            this.NotesCreatedOnLayoutControlItem.TextSize = new System.Drawing.Size(88, 13);
            this.NotesCreatedOnLayoutControlItem.TextToControlDistance = 3;
            // 
            // NotesLastModifiedByLayoutControlItem
            // 
            this.NotesLastModifiedByLayoutControlItem.Control = this.NotesLastModifiedByField;
            this.NotesLastModifiedByLayoutControlItem.CustomizationFormText = "Last Modified By";
            this.NotesLastModifiedByLayoutControlItem.Location = new System.Drawing.Point(872, 491);
            this.NotesLastModifiedByLayoutControlItem.Name = "NotesLastModifiedByLayoutControlItem";
            this.NotesLastModifiedByLayoutControlItem.Size = new System.Drawing.Size(301, 24);
            this.NotesLastModifiedByLayoutControlItem.Spacing = new DevExpress.XtraLayout.Utils.Padding(20, 0, 0, 0);
            this.NotesLastModifiedByLayoutControlItem.Text = "Last Modified By";
            this.NotesLastModifiedByLayoutControlItem.TextAlignMode = DevExpress.XtraLayout.TextAlignModeItem.CustomSize;
            this.NotesLastModifiedByLayoutControlItem.TextSize = new System.Drawing.Size(88, 13);
            this.NotesLastModifiedByLayoutControlItem.TextToControlDistance = 3;
            // 
            // NotesLastModifiedOnLayoutControlItem
            // 
            this.NotesLastModifiedOnLayoutControlItem.Control = this.NotesLastModifiedOnField;
            this.NotesLastModifiedOnLayoutControlItem.CustomizationFormText = "Last Modified On";
            this.NotesLastModifiedOnLayoutControlItem.Location = new System.Drawing.Point(872, 515);
            this.NotesLastModifiedOnLayoutControlItem.Name = "NotesLastModifiedOnLayoutControlItem";
            this.NotesLastModifiedOnLayoutControlItem.Size = new System.Drawing.Size(301, 24);
            this.NotesLastModifiedOnLayoutControlItem.Spacing = new DevExpress.XtraLayout.Utils.Padding(20, 0, 0, 0);
            this.NotesLastModifiedOnLayoutControlItem.Text = "Last Modified On";
            this.NotesLastModifiedOnLayoutControlItem.TextAlignMode = DevExpress.XtraLayout.TextAlignModeItem.CustomSize;
            this.NotesLastModifiedOnLayoutControlItem.TextSize = new System.Drawing.Size(88, 13);
            this.NotesLastModifiedOnLayoutControlItem.TextToControlDistance = 3;
            // 
            // layoutControlItem2
            // 
            this.layoutControlItem2.Control = this.LogGrid;
            this.layoutControlItem2.Location = new System.Drawing.Point(0, 0);
            this.layoutControlItem2.Name = "layoutControlItem2";
            this.layoutControlItem2.Size = new System.Drawing.Size(1173, 325);
            this.layoutControlItem2.TextSize = new System.Drawing.Size(0, 0);
            this.layoutControlItem2.TextVisible = false;
            // 
            // EasyDocTabGroup
            // 
            this.EasyDocTabGroup.CaptionImageOptions.SvgImage = ((DevExpress.Utils.Svg.SvgImage)(resources.GetObject("EasyDocTabGroup.CaptionImageOptions.SvgImage")));
            this.EasyDocTabGroup.CaptionImageOptions.SvgImageSize = new System.Drawing.Size(16, 16);
            this.EasyDocTabGroup.Items.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.LLReceivesElectronicCommunicationLayoutControlItem,
            this.REAReceivesElectronicCommunicationLayoutControlItem,
            this.EasyDocsEmailAddressLayoutControlItem,
            this.EasyDocsPrimaryAssociateLayoutControlItem,
            this.emptySpaceItem16,
            this.EasyDocsMobileNumberLayoutControlItem,
            this.emptySpaceItem26,
            this.layoutControlItem4,
            this.emptySpaceItem33});
            this.EasyDocTabGroup.Location = new System.Drawing.Point(0, 0);
            this.EasyDocTabGroup.Name = "EasyDocTabGroup";
            this.EasyDocTabGroup.Size = new System.Drawing.Size(1197, 581);
            this.EasyDocTabGroup.Text = "Easy Doc";
            // 
            // LLReceivesElectronicCommunicationLayoutControlItem
            // 
            this.LLReceivesElectronicCommunicationLayoutControlItem.AppearanceItemCaption.Options.UseTextOptions = true;
            this.LLReceivesElectronicCommunicationLayoutControlItem.AppearanceItemCaption.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.LLReceivesElectronicCommunicationLayoutControlItem.Control = this.LLReceivesElectronicCommunicationsField;
            this.LLReceivesElectronicCommunicationLayoutControlItem.CustomizationFormText = "Landlord receives Electronic Communications";
            this.LLReceivesElectronicCommunicationLayoutControlItem.Location = new System.Drawing.Point(0, 0);
            this.LLReceivesElectronicCommunicationLayoutControlItem.MaxSize = new System.Drawing.Size(200, 0);
            this.LLReceivesElectronicCommunicationLayoutControlItem.MinSize = new System.Drawing.Size(200, 25);
            this.LLReceivesElectronicCommunicationLayoutControlItem.Name = "LLReceivesElectronicCommunicationLayoutControlItem";
            this.LLReceivesElectronicCommunicationLayoutControlItem.Size = new System.Drawing.Size(200, 38);
            this.LLReceivesElectronicCommunicationLayoutControlItem.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.LLReceivesElectronicCommunicationLayoutControlItem.Text = "Landlord receives Electronic Communications";
            this.LLReceivesElectronicCommunicationLayoutControlItem.TextAlignMode = DevExpress.XtraLayout.TextAlignModeItem.CustomSize;
            this.LLReceivesElectronicCommunicationLayoutControlItem.TextSize = new System.Drawing.Size(145, 13);
            this.LLReceivesElectronicCommunicationLayoutControlItem.TextToControlDistance = 3;
            // 
            // REAReceivesElectronicCommunicationLayoutControlItem
            // 
            this.REAReceivesElectronicCommunicationLayoutControlItem.AppearanceItemCaption.Options.UseTextOptions = true;
            this.REAReceivesElectronicCommunicationLayoutControlItem.AppearanceItemCaption.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.REAReceivesElectronicCommunicationLayoutControlItem.Control = this.REAReceivesElectronicCommunicationsField;
            this.REAReceivesElectronicCommunicationLayoutControlItem.CustomizationFormText = "Real Estate Agent receives Electronic Communications";
            this.REAReceivesElectronicCommunicationLayoutControlItem.Location = new System.Drawing.Point(0, 38);
            this.REAReceivesElectronicCommunicationLayoutControlItem.MaxSize = new System.Drawing.Size(200, 0);
            this.REAReceivesElectronicCommunicationLayoutControlItem.MinSize = new System.Drawing.Size(200, 25);
            this.REAReceivesElectronicCommunicationLayoutControlItem.Name = "REAReceivesElectronicCommunicationLayoutControlItem";
            this.REAReceivesElectronicCommunicationLayoutControlItem.Size = new System.Drawing.Size(200, 40);
            this.REAReceivesElectronicCommunicationLayoutControlItem.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.REAReceivesElectronicCommunicationLayoutControlItem.Text = "Real Estate Agent receives Electronic Communications";
            this.REAReceivesElectronicCommunicationLayoutControlItem.TextAlignMode = DevExpress.XtraLayout.TextAlignModeItem.CustomSize;
            this.REAReceivesElectronicCommunicationLayoutControlItem.TextSize = new System.Drawing.Size(145, 13);
            this.REAReceivesElectronicCommunicationLayoutControlItem.TextToControlDistance = 3;
            // 
            // EasyDocsEmailAddressLayoutControlItem
            // 
            this.EasyDocsEmailAddressLayoutControlItem.Control = this.EasyDocsEmailAddressField;
            this.EasyDocsEmailAddressLayoutControlItem.CustomizationFormText = "E-mail Address";
            this.EasyDocsEmailAddressLayoutControlItem.Location = new System.Drawing.Point(357, 24);
            this.EasyDocsEmailAddressLayoutControlItem.Name = "EasyDocsEmailAddressLayoutControlItem";
            this.EasyDocsEmailAddressLayoutControlItem.Size = new System.Drawing.Size(409, 24);
            this.EasyDocsEmailAddressLayoutControlItem.Spacing = new DevExpress.XtraLayout.Utils.Padding(20, 0, 0, 0);
            this.EasyDocsEmailAddressLayoutControlItem.Text = "Email Address";
            this.EasyDocsEmailAddressLayoutControlItem.TextAlignMode = DevExpress.XtraLayout.TextAlignModeItem.CustomSize;
            this.EasyDocsEmailAddressLayoutControlItem.TextSize = new System.Drawing.Size(88, 13);
            this.EasyDocsEmailAddressLayoutControlItem.TextToControlDistance = 3;
            // 
            // EasyDocsPrimaryAssociateLayoutControlItem
            // 
            this.EasyDocsPrimaryAssociateLayoutControlItem.Control = this.EasyDocsPrimaryAssociateField;
            this.EasyDocsPrimaryAssociateLayoutControlItem.CustomizationFormText = "Primary Associate";
            this.EasyDocsPrimaryAssociateLayoutControlItem.Location = new System.Drawing.Point(357, 0);
            this.EasyDocsPrimaryAssociateLayoutControlItem.Name = "EasyDocsPrimaryAssociateLayoutControlItem";
            this.EasyDocsPrimaryAssociateLayoutControlItem.Size = new System.Drawing.Size(409, 24);
            this.EasyDocsPrimaryAssociateLayoutControlItem.Spacing = new DevExpress.XtraLayout.Utils.Padding(20, 0, 0, 0);
            this.EasyDocsPrimaryAssociateLayoutControlItem.Text = "Primary Associate";
            this.EasyDocsPrimaryAssociateLayoutControlItem.TextAlignMode = DevExpress.XtraLayout.TextAlignModeItem.CustomSize;
            this.EasyDocsPrimaryAssociateLayoutControlItem.TextSize = new System.Drawing.Size(88, 13);
            this.EasyDocsPrimaryAssociateLayoutControlItem.TextToControlDistance = 3;
            // 
            // emptySpaceItem16
            // 
            this.emptySpaceItem16.AllowHotTrack = false;
            this.emptySpaceItem16.CustomizationFormText = "emptySpaceItem16";
            this.emptySpaceItem16.Location = new System.Drawing.Point(766, 0);
            this.emptySpaceItem16.Name = "emptySpaceItem16";
            this.emptySpaceItem16.Size = new System.Drawing.Size(431, 24);
            this.emptySpaceItem16.TextSize = new System.Drawing.Size(0, 0);
            // 
            // EasyDocsMobileNumberLayoutControlItem
            // 
            this.EasyDocsMobileNumberLayoutControlItem.Control = this.EasyDocsMobilePhoneField;
            this.EasyDocsMobileNumberLayoutControlItem.CustomizationFormText = "Mobile Phone";
            this.EasyDocsMobileNumberLayoutControlItem.Location = new System.Drawing.Point(766, 24);
            this.EasyDocsMobileNumberLayoutControlItem.Name = "EasyDocsMobileNumberLayoutControlItem";
            this.EasyDocsMobileNumberLayoutControlItem.Size = new System.Drawing.Size(431, 24);
            this.EasyDocsMobileNumberLayoutControlItem.Spacing = new DevExpress.XtraLayout.Utils.Padding(20, 0, 0, 0);
            this.EasyDocsMobileNumberLayoutControlItem.Text = "Mobile Phone";
            this.EasyDocsMobileNumberLayoutControlItem.TextAlignMode = DevExpress.XtraLayout.TextAlignModeItem.CustomSize;
            this.EasyDocsMobileNumberLayoutControlItem.TextSize = new System.Drawing.Size(88, 13);
            this.EasyDocsMobileNumberLayoutControlItem.TextToControlDistance = 3;
            // 
            // emptySpaceItem26
            // 
            this.emptySpaceItem26.AllowHotTrack = false;
            this.emptySpaceItem26.CustomizationFormText = "emptySpaceItem26";
            this.emptySpaceItem26.Location = new System.Drawing.Point(200, 0);
            this.emptySpaceItem26.MinSize = new System.Drawing.Size(104, 24);
            this.emptySpaceItem26.Name = "emptySpaceItem26";
            this.emptySpaceItem26.Size = new System.Drawing.Size(157, 78);
            this.emptySpaceItem26.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.emptySpaceItem26.TextSize = new System.Drawing.Size(0, 0);
            // 
            // layoutControlItem4
            // 
            this.layoutControlItem4.Control = this.EasyDocsGrid;
            this.layoutControlItem4.Location = new System.Drawing.Point(0, 78);
            this.layoutControlItem4.Name = "layoutControlItem4";
            this.layoutControlItem4.Size = new System.Drawing.Size(1197, 503);
            this.layoutControlItem4.TextSize = new System.Drawing.Size(0, 0);
            this.layoutControlItem4.TextVisible = false;
            // 
            // emptySpaceItem33
            // 
            this.emptySpaceItem33.AllowHotTrack = false;
            this.emptySpaceItem33.CustomizationFormText = "emptySpaceItem33";
            this.emptySpaceItem33.Location = new System.Drawing.Point(357, 48);
            this.emptySpaceItem33.MinSize = new System.Drawing.Size(104, 24);
            this.emptySpaceItem33.Name = "emptySpaceItem33";
            this.emptySpaceItem33.Size = new System.Drawing.Size(840, 30);
            this.emptySpaceItem33.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.emptySpaceItem33.TextSize = new System.Drawing.Size(0, 0);
            // 
            // HistoryTabGroup
            // 
            this.HistoryTabGroup.CaptionImageOptions.SvgImage = ((DevExpress.Utils.Svg.SvgImage)(resources.GetObject("HistoryTabGroup.CaptionImageOptions.SvgImage")));
            this.HistoryTabGroup.CaptionImageOptions.SvgImageSize = new System.Drawing.Size(16, 16);
            this.HistoryTabGroup.Items.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.layoutControlItem10});
            this.HistoryTabGroup.Location = new System.Drawing.Point(0, 0);
            this.HistoryTabGroup.Name = "HistoryTabGroup";
            this.HistoryTabGroup.Size = new System.Drawing.Size(1197, 581);
            this.HistoryTabGroup.Text = "History";
            // 
            // layoutControlItem10
            // 
            this.layoutControlItem10.Control = this.HistoryGrid;
            this.layoutControlItem10.Location = new System.Drawing.Point(0, 0);
            this.layoutControlItem10.Name = "layoutControlItem10";
            this.layoutControlItem10.Size = new System.Drawing.Size(1197, 581);
            this.layoutControlItem10.TextSize = new System.Drawing.Size(0, 0);
            this.layoutControlItem10.TextVisible = false;
            // 
            // PDSAndFSGTabGroup
            // 
            this.PDSAndFSGTabGroup.CaptionImageOptions.SvgImage = ((DevExpress.Utils.Svg.SvgImage)(resources.GetObject("PDSAndFSGTabGroup.CaptionImageOptions.SvgImage")));
            this.PDSAndFSGTabGroup.CaptionImageOptions.SvgImageSize = new System.Drawing.Size(16, 16);
            this.PDSAndFSGTabGroup.Items.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.PDSAndFSGGroup,
            this.CommunicationsGroup,
            this.emptySpaceItem6});
            this.PDSAndFSGTabGroup.Location = new System.Drawing.Point(0, 0);
            this.PDSAndFSGTabGroup.Name = "PDSAndFSGTabGroup";
            this.PDSAndFSGTabGroup.Size = new System.Drawing.Size(1197, 581);
            this.PDSAndFSGTabGroup.Text = "PDS && FSG";
            // 
            // PDSAndFSGGroup
            // 
            this.PDSAndFSGGroup.Items.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.layoutControlItem11});
            this.PDSAndFSGGroup.Location = new System.Drawing.Point(0, 0);
            this.PDSAndFSGGroup.Name = "PDSAndFSGGroup";
            this.PDSAndFSGGroup.Size = new System.Drawing.Size(512, 301);
            this.PDSAndFSGGroup.Text = "PDS && FSG";
            // 
            // layoutControlItem11
            // 
            this.layoutControlItem11.Control = this.PDSandFSGGrid;
            this.layoutControlItem11.Location = new System.Drawing.Point(0, 0);
            this.layoutControlItem11.Name = "layoutControlItem11";
            this.layoutControlItem11.Size = new System.Drawing.Size(488, 259);
            this.layoutControlItem11.TextSize = new System.Drawing.Size(0, 0);
            this.layoutControlItem11.TextVisible = false;
            // 
            // CommunicationsGroup
            // 
            this.CommunicationsGroup.Items.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.emptySpaceItem38});
            this.CommunicationsGroup.Location = new System.Drawing.Point(0, 301);
            this.CommunicationsGroup.Name = "CommunicationsGroup";
            this.CommunicationsGroup.Size = new System.Drawing.Size(1197, 280);
            this.CommunicationsGroup.Text = "Communications";
            // 
            // emptySpaceItem38
            // 
            this.emptySpaceItem38.AllowHotTrack = false;
            this.emptySpaceItem38.CustomizationFormText = "In future, we could present Transaction Communications  here.";
            this.emptySpaceItem38.Location = new System.Drawing.Point(0, 0);
            this.emptySpaceItem38.Name = "emptySpaceItem38";
            this.emptySpaceItem38.Size = new System.Drawing.Size(1173, 238);
            this.emptySpaceItem38.Text = "In future, we could present Transaction Communications  here.";
            this.emptySpaceItem38.TextSize = new System.Drawing.Size(96, 0);
            this.emptySpaceItem38.TextVisible = true;
            // 
            // emptySpaceItem6
            // 
            this.emptySpaceItem6.AllowHotTrack = false;
            this.emptySpaceItem6.Location = new System.Drawing.Point(512, 0);
            this.emptySpaceItem6.Name = "emptySpaceItem6";
            this.emptySpaceItem6.Size = new System.Drawing.Size(685, 301);
            this.emptySpaceItem6.TextSize = new System.Drawing.Size(0, 0);
            // 
            // ContactCodeHeaderLayoutControlItem
            // 
            this.ContactCodeHeaderLayoutControlItem.Control = this.ContactCodeHeaderField;
            this.ContactCodeHeaderLayoutControlItem.CustomizationFormText = "Contact";
            this.ContactCodeHeaderLayoutControlItem.Location = new System.Drawing.Point(0, 0);
            this.ContactCodeHeaderLayoutControlItem.Name = "ContactCodeHeaderLayoutControlItem";
            this.ContactCodeHeaderLayoutControlItem.Padding = new DevExpress.XtraLayout.Utils.Padding(2, 2, 2, 0);
            this.ContactCodeHeaderLayoutControlItem.Size = new System.Drawing.Size(117, 42);
            this.ContactCodeHeaderLayoutControlItem.Text = "Contact";
            this.ContactCodeHeaderLayoutControlItem.TextLocation = DevExpress.Utils.Locations.Top;
            this.ContactCodeHeaderLayoutControlItem.TextSize = new System.Drawing.Size(96, 13);
            // 
            // ContactNameHeaderLayoutControlItem
            // 
            this.ContactNameHeaderLayoutControlItem.Control = this.ContactNameHeaderField;
            this.ContactNameHeaderLayoutControlItem.CustomizationFormText = " ";
            this.ContactNameHeaderLayoutControlItem.Location = new System.Drawing.Point(117, 0);
            this.ContactNameHeaderLayoutControlItem.Name = "ContactNameHeaderLayoutControlItem";
            this.ContactNameHeaderLayoutControlItem.Size = new System.Drawing.Size(117, 42);
            this.ContactNameHeaderLayoutControlItem.Text = " ";
            this.ContactNameHeaderLayoutControlItem.TextLocation = DevExpress.Utils.Locations.Top;
            this.ContactNameHeaderLayoutControlItem.TextSize = new System.Drawing.Size(96, 13);
            // 
            // LandlordCodeHeaderLayoutControlItem
            // 
            this.LandlordCodeHeaderLayoutControlItem.Control = this.LandlordCodeHeaderField;
            this.LandlordCodeHeaderLayoutControlItem.CustomizationFormText = "Landlord";
            this.LandlordCodeHeaderLayoutControlItem.Location = new System.Drawing.Point(234, 0);
            this.LandlordCodeHeaderLayoutControlItem.Name = "LandlordCodeHeaderLayoutControlItem";
            this.LandlordCodeHeaderLayoutControlItem.Size = new System.Drawing.Size(117, 42);
            this.LandlordCodeHeaderLayoutControlItem.Text = "Landlord";
            this.LandlordCodeHeaderLayoutControlItem.TextLocation = DevExpress.Utils.Locations.Top;
            this.LandlordCodeHeaderLayoutControlItem.TextSize = new System.Drawing.Size(96, 13);
            // 
            // LandlordNameHeaderLayoutControlItem
            // 
            this.LandlordNameHeaderLayoutControlItem.Control = this.LandlordNameHeaderField;
            this.LandlordNameHeaderLayoutControlItem.CustomizationFormText = " ";
            this.LandlordNameHeaderLayoutControlItem.Location = new System.Drawing.Point(351, 0);
            this.LandlordNameHeaderLayoutControlItem.Name = "LandlordNameHeaderLayoutControlItem";
            this.LandlordNameHeaderLayoutControlItem.Size = new System.Drawing.Size(117, 42);
            this.LandlordNameHeaderLayoutControlItem.Text = " ";
            this.LandlordNameHeaderLayoutControlItem.TextLocation = DevExpress.Utils.Locations.Top;
            this.LandlordNameHeaderLayoutControlItem.TextSize = new System.Drawing.Size(96, 13);
            // 
            // TransactionNumberHeaderLayoutControlItem
            // 
            this.TransactionNumberHeaderLayoutControlItem.Control = this.TransactionNumberHeaderField;
            this.TransactionNumberHeaderLayoutControlItem.CustomizationFormText = "Transaction Number";
            this.TransactionNumberHeaderLayoutControlItem.Location = new System.Drawing.Point(468, 0);
            this.TransactionNumberHeaderLayoutControlItem.Name = "TransactionNumberHeaderLayoutControlItem";
            this.TransactionNumberHeaderLayoutControlItem.Size = new System.Drawing.Size(117, 42);
            this.TransactionNumberHeaderLayoutControlItem.Text = "Transaction Number";
            this.TransactionNumberHeaderLayoutControlItem.TextLocation = DevExpress.Utils.Locations.Top;
            this.TransactionNumberHeaderLayoutControlItem.TextSize = new System.Drawing.Size(96, 13);
            // 
            // TransactionStatusHeaderLayoutControlItem
            // 
            this.TransactionStatusHeaderLayoutControlItem.Control = this.TransactionStatusHeaderField;
            this.TransactionStatusHeaderLayoutControlItem.CustomizationFormText = "Status";
            this.TransactionStatusHeaderLayoutControlItem.Location = new System.Drawing.Point(585, 0);
            this.TransactionStatusHeaderLayoutControlItem.Name = "TransactionStatusHeaderLayoutControlItem";
            this.TransactionStatusHeaderLayoutControlItem.Size = new System.Drawing.Size(117, 42);
            this.TransactionStatusHeaderLayoutControlItem.Text = "Status";
            this.TransactionStatusHeaderLayoutControlItem.TextLocation = DevExpress.Utils.Locations.Top;
            this.TransactionStatusHeaderLayoutControlItem.TextSize = new System.Drawing.Size(96, 13);
            // 
            // TransactionRenewalHeaderLayoutControlItem
            // 
            this.TransactionRenewalHeaderLayoutControlItem.Control = this.TransactionRenewalHeaderField;
            this.TransactionRenewalHeaderLayoutControlItem.CustomizationFormText = "Renewal";
            this.TransactionRenewalHeaderLayoutControlItem.Location = new System.Drawing.Point(702, 0);
            this.TransactionRenewalHeaderLayoutControlItem.Name = "TransactionRenewalHeaderLayoutControlItem";
            this.TransactionRenewalHeaderLayoutControlItem.Size = new System.Drawing.Size(117, 42);
            this.TransactionRenewalHeaderLayoutControlItem.Text = "Renewal";
            this.TransactionRenewalHeaderLayoutControlItem.TextLocation = DevExpress.Utils.Locations.Top;
            this.TransactionRenewalHeaderLayoutControlItem.TextSize = new System.Drawing.Size(96, 13);
            // 
            // PremiumAmountHeaderLayoutControlItem
            // 
            this.PremiumAmountHeaderLayoutControlItem.Control = this.PremiumAmountHeaderField;
            this.PremiumAmountHeaderLayoutControlItem.CustomizationFormText = "Premium";
            this.PremiumAmountHeaderLayoutControlItem.Location = new System.Drawing.Point(819, 0);
            this.PremiumAmountHeaderLayoutControlItem.Name = "PremiumAmountHeaderLayoutControlItem";
            this.PremiumAmountHeaderLayoutControlItem.Size = new System.Drawing.Size(116, 42);
            this.PremiumAmountHeaderLayoutControlItem.Text = "Premium";
            this.PremiumAmountHeaderLayoutControlItem.TextLocation = DevExpress.Utils.Locations.Top;
            this.PremiumAmountHeaderLayoutControlItem.TextSize = new System.Drawing.Size(96, 13);
            // 
            // OutstandingAmountHeaderLayoutControlItem
            // 
            this.OutstandingAmountHeaderLayoutControlItem.Control = this.OutstandingAmountHeaderField;
            this.OutstandingAmountHeaderLayoutControlItem.CustomizationFormText = "Outstanding";
            this.OutstandingAmountHeaderLayoutControlItem.Location = new System.Drawing.Point(935, 0);
            this.OutstandingAmountHeaderLayoutControlItem.Name = "OutstandingAmountHeaderLayoutControlItem";
            this.OutstandingAmountHeaderLayoutControlItem.Size = new System.Drawing.Size(117, 42);
            this.OutstandingAmountHeaderLayoutControlItem.Text = "Outstanding";
            this.OutstandingAmountHeaderLayoutControlItem.TextLocation = DevExpress.Utils.Locations.Top;
            this.OutstandingAmountHeaderLayoutControlItem.TextSize = new System.Drawing.Size(96, 13);
            // 
            // ClaimCountHeaderLayoutControlItem
            // 
            this.ClaimCountHeaderLayoutControlItem.Control = this.ClaimCountHeaderField;
            this.ClaimCountHeaderLayoutControlItem.CustomizationFormText = "Claims";
            this.ClaimCountHeaderLayoutControlItem.Location = new System.Drawing.Point(1052, 0);
            this.ClaimCountHeaderLayoutControlItem.Name = "ClaimCountHeaderLayoutControlItem";
            this.ClaimCountHeaderLayoutControlItem.Size = new System.Drawing.Size(169, 42);
            this.ClaimCountHeaderLayoutControlItem.Text = "Claims";
            this.ClaimCountHeaderLayoutControlItem.TextLocation = DevExpress.Utils.Locations.Top;
            this.ClaimCountHeaderLayoutControlItem.TextSize = new System.Drawing.Size(96, 13);
            // 
            // popupMenu1
            // 
            this.popupMenu1.Manager = this.barManager1;
            this.popupMenu1.Name = "popupMenu1";
            // 
            // dxErrorProvider1
            // 
            this.dxErrorProvider1.ContainerControl = this;
            // 
            // TransactionForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1241, 716);
            this.Controls.Add(this.MainLayoutControl);
            this.Controls.Add(this.barDockControlLeft);
            this.Controls.Add(this.barDockControlRight);
            this.Controls.Add(this.barDockControlBottom);
            this.Controls.Add(this.barDockControlTop);
            this.Name = "TransactionForm";
            this.Text = "Policy: {TRANSACTIONNUMBER} - {STATUS} - {INSUREDADDRESS}";
            ((System.ComponentModel.ISupportInitialize)(this.HistoryDetailGridView)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.HistoryGrid)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.HistoryGridView)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.barManager1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.MainLayoutControl)).EndInit();
            this.MainLayoutControl.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.Disclosure5AwaitingApprovalField.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Disclosure5NoField.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.PDSandFSGGrid)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.PDSandFSGGridView)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.LogGrid)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.LogGridView)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemRichTextEdit1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.LedgerGrid)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.LedgerGridView)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.PremiumDetailsGrid)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.PremiumDetailsGridView)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.PremiumBaseRatesGrid)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.PremiumBaseRatesGridView)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.DisclosureClaimsGrid)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.DisclosureClaimsGridView)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.EasyDocsGrid)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.EasyDocsGridView)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.PremiumTypeField.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.PolicyNumberField.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.REACodeField.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.NewBusinessArrangedByCodeField.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.DwellingTypeField.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ConstructionTypeField.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.YearBuiltField.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.LandlordCodeField.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.LandlordNameField.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.BranchField.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.BushfireRatingField.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ContentsSumInsuredField.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.BuildingSumInsuredAmountField.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.WeeksInArrearsField.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.FloodRiskRatingField.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.WeeklyRentSumInsuredAmountField.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.UnderwriterExcessAmountField.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.BuildingSumInsuredExcessAmountField.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.LandSizeField.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TransactionNumberField.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.CreatedOnField.Properties.CalendarTimeProperties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.CreatedOnField.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.InceptionFromField.Properties.CalendarTimeProperties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.InceptionFromField.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.BalanceOutstandingAmountField.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.EffectiveFromField.Properties.CalendarTimeProperties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.EffectiveFromField.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.BilledFromField.Properties.CalendarTimeProperties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.BilledFromField.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ClosingOnField.Properties.CalendarTimeProperties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ClosingOnField.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.COISentOnField.Properties.CalendarTimeProperties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.COISentOnField.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.BilledToField.Properties.CalendarTimeProperties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.BilledToField.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.RenewalOnField.Properties.CalendarTimeProperties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.RenewalOnField.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.PolicySentOnField.Properties.CalendarTimeProperties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.PolicySentOnField.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.InvoicePrintedOnField.Properties.CalendarTimeProperties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.InvoicePrintedOnField.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ContactCodeHeaderField.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ContactNameHeaderField.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.LandlordCodeHeaderField.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.LandlordNameHeaderField.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TransactionNumberHeaderField.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TransactionStatusHeaderField.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TransactionRenewalHeaderField.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.PremiumAmountHeaderField.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.OutstandingAmountHeaderField.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ClaimCountHeaderField.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.OnHoldField.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.RewiredReplumbedField.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.FreeStandingField.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.CommonGroundsField.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.BusinessOrTradeField.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.AcceptedBusinessUseField.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.IsJobReducedDuePandemicField.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.IsArrearsField.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TenantedField.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.StaffDiscountField.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.IsREADiscountField.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.IsMultiplePropertyDiscountField.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.IsFivePercentDiscountField.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Is15For12BonusOfferField.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.PremiumPaidByField.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.PreferredMailingAddressTypeField.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.InsurerCodeField.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.OnlineQuoteNumberField.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.QuoteNumberField.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.AskedForQuoteWhoField.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.HowAskedForQuoteField.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.QuotedByField.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.QuotedOnField.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ConvertedToCoverByField.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ConvertedToCoverOnField.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.QuoteExpiresOnField.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.OnlineQuoteEmailAddressField.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.WhoPlacedCoverField.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.HowPlacedCoverField.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.HeardAboutTSIviaField.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.MortgageeField.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.PaymentMethodField.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.PaymentReferenceField.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.BPayReceiptField.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.OnlineReferenceField.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.CCTransactionIDField.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.VoucherTypeField.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.PromoCodeField.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.LinkCreditTransactionNumberField.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.LinkParentTransactionNumberField.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.LinkReferToTransactionNumberField.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.LinkWelcomeLetterTransactionNumberField.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.RecreatedField.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.PaymentReceivedField.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ApprovedByField.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ApprovedOnField.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.RenewingTransactionsField.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.RenewalReasonField.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.RenewalPremiumPaidByField.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEdit51.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.RenewalDiscountAmountField.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.RenewedTransactionField.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.RenewedByTransactionField.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.LastRenewalErrorField.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.RenewalBatchField.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.LedgerHoldCoverField.Properties.CalendarTimeProperties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.LedgerHoldCoverField.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.LedgerPremiumPaidField.Properties.CalendarTimeProperties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.LedgerPremiumPaidField.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.LedgerCurrentOnField.Properties.CalendarTimeProperties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.LedgerCurrentOnField.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.LedgerDistributeFundsField.Properties.CalendarTimeProperties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.LedgerDistributeFundsField.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.LedgerInvoiceOverdueOnField.Properties.CalendarTimeProperties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.LedgerInvoiceOverdueOnField.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.LedgerExtendedOverdueOnField.Properties.CalendarTimeProperties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.LedgerExtendedOverdueOnField.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.LedgerOverdueNoticeSentField.Properties.CalendarTimeProperties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.LedgerOverdueNoticeSentField.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.LedgerExtendedLapsingOnField.Properties.CalendarTimeProperties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.LedgerExtendedLapsingOnField.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.LedgerFinalReminderSentField.Properties.CalendarTimeProperties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.LedgerFinalReminderSentField.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.LedgerCancelledLapsedOnField.Properties.CalendarTimeProperties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.LedgerCancelledLapsedOnField.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.NotesLastModifiedOnField.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.NotesLastModifiedByField.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.NotesCreatedOnField.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.NotesCreatedByField.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.SchemePaidField.Properties.CalendarTimeProperties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.SchemePaidField.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.EasyDocsEmailAddressField.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.EasyDocsPrimaryAssociateField.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.EasyDocsMobilePhoneField.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.LLReceivesElectronicCommunicationsField.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.REAReceivesElectronicCommunicationsField.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.DenialReasonField.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.SpecialTermsField.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.DisclosureItem1Field.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.DisclosureItem2Field.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.CircumstancesField.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.RestrictionsField.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Disclosure3Field.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Disclosure4Field.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.RenewalMultiPropertyDiscountField.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.RenewalsOnHoldField.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.DoNotRenewField.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.RenewalImposedExcessField.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.RealEstateDiscountField.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.RenewalStaffDiscountField.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ApplyToRenewingPremiumField.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Disclosure5YesField.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ClassOfRiskField.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.SelfManagedField.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.IsFurnishedField.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.IsCCTVField.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.IsElectronicAccessField.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.HolidayLetField.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Root)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.tabbedControlGroup1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.PolicyTabGroup)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.PolicyDetailsGroup)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.PolicyClassLayoutControlItem)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.PremiumTypeLayoutControlItem)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.PolicyNumberLayoutControlItem)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem24)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.REACodeLayoutControlItem)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.NewBusinessArrangedByLayoutControlItem)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.DwellingTypeLayoutControlItem)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ConstructionTypeLayoutControlItem)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.YearBuiltLayoutControlItem)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.CommonGroundsLayoutControlItem)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem34)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.CCTVLayoutControlItem)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ElectronicAccessLayoutControlItem)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.BusinessOrTradeLayoutControlItem)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.FreeStandingLayoutControlItem)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.AcceptedBusinessUseLayoutControlItem)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.OnHoldLayoutControlItem)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.RewiredReplumbedLayoutControlItem)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem40)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem41)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.FurnishedLayoutControlItem)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.SelfManagedlayoutControlItem)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.HolidayLetLayoutControlItem)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.PropertyGroup)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.LandlordCodeLayoutControlItem)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.LandlordNameLayoutControlItem)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.InsuredPropertyAddressLayoutControlItem)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.BranchLayoutControlItem)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.BushfireLayoutControlItem)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ContentsSumInsuredLayoutControlItem)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.BuildingSumInsuredLayoutControlItem)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TenantedLayoutControlItem)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem4)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.FloodRiskLayoutControlItem)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.WeeklyRentSumInsuredLayoutControlItem)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.UnderwriterExcessLayoutControlItem)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.BuildingSumInsuredExcessLayoutControlItem)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.LandSizeLayoutControlItem)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem17)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.JobReducedCoronaVirusLayoutControlItem)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ArrearsLayoutControlItem)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.WeeksInArrearsLayoutControlItem)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.DatesGroup)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TransactionNumberLayoutControlItem)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.CreatedOnLayoutControlItem)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.InceptionFromLayoutControlItem)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.BalanceLayoutControlItem)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.EffectiveFromLayoutControlItem)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.BilledFromLayoutControlItem)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ClosingOnLayoutControlItem)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.COISentOnLayoutControlItem)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.BilledToLayoutControlItem)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.RenewalOnLayoutControlItem)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.PolicySentOnLayoutControlItem)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.InvoicePrintedOnLayoutControlItem)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.DiscountsGroup)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.FivePercentDiscountLayoutControlItem)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.MultiplePropertyDiscountLayoutControlItem)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.READiscountLayoutControlItem)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.StaffDiscountLayoutControlItem)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem21)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem12)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.FifteenForTwelveBonusOfferLayoutControlItem)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem35)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.OtherDetailsTabGroup)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.PremiumDetailsGroup)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.PremiumPaidByLayoutControlItem)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.PreferredMailingAddressLayoutControlItem)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.InsurerCodeLayoutControlItem)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.QuoteDetailsGroup)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.OnlineQuoteNumberLayoutControlItem)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.QuoteNumberLayoutControlItem)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.AskedForQuoteLayoutControlItem)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.HowAskedLayoutControlItem)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.QuotedByLayoutControlItem)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.QuotedOnLayoutControlItem)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ConvertedToCoverByLayoutControlItem)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ConvertedToCoverOnLayoutControlItem)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.QuoteExpiresOnLayoutControlItem)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.OnlineQuoteEmailLayoutControlItem)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem7)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.CoverDetailsGroup)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.RecreatedLayoutControlItem)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.WhoPlacedCoverLayoutControlItem)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.HowPlacedCoverLayoutControlItem)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.HeardAboutTSIViaLayoutControlItem)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.MortgageeLayoutControlItem)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.PaymentDetailsGroup)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.PaymentReceivedLayoutControlItem)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.PaymentMethodLayoutControlItem)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.PaymentReferenceLayoutControlItem)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.BPayReceiptLayoutControlItem)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.OnlineReferenceLayoutControlItem)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.CCTransactionIDLayoutControlItem)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.VoucherLayoutControlItem)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.PromoCodeLayoutControlItem)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem25)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.LinkedTransactionsGroup)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.CreditTransactionLayoutControlItem)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ParentTransactionLayoutControlItem)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ReferToTransactionLayoutControlItem)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.WelcomeLetterJoinedLayoutControlItem)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem15)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem32)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem5)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.DisclosureTabGroup)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ApprovalGroup)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ApprovedByLayoutControlItem)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ApprovedOnLayoutControlItem)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem14)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.QuesstionnaireGroup)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.DisclosureItem1LayoutControlItem)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.DenialReasonLayoutControlItem)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.SpecialTermsLayoutControlItem)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.DisclosureItem2LayoutControlItem)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem10)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem13)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem5)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.DisclosureItem3LayoutControlItem)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.DisclosureItem4LayoutControlItem)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.CircumstancesLayoutControlItem)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.DisclosureItem5LayoutControlItem)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.RestrictionsLayoutControlItem)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem18)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem39)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Disclosure5NoLayoutItem)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Disclosure5AwaitingApprovalLayoutItem)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem20)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.PremiumAndRenewalTabGroup)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ExistingPremiumDetailsGroup)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem6)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem7)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem37)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.RenewingTransactionSettingsGroup)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.RenewingTransactionsLayoutControlItem)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.RenewalReasonLayoutControlItem)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.RenewalPremiumPaidByLayoutControlItem)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.RenewalImposedExcessLayoutControlItem)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.RenewExcessAmountLayoutControlItem)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.DoNotRenewLayoutControlItem)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem28)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem29)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem27)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.RenewalsOnHoldLayoutControlItem)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.RenewingDiscountsGroup)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.RenewalMultiplePropertyDiscountLayoutControlItem)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ApplyToRenewingPremiumLayoutControlItem)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.RenewalStaffDiscountLayoutControlItem)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.RealEstateDiscountLayoutControlItem)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem31)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.RenewalDiscountAmountLayoutControlItem)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem11)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem36)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.RenewalProcessingGroup)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.RenewedTransactionLayoutControlItem)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.RenewedByTransactionLayoutControlItem)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.LastRenewalErrorLayoutControlItem)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.RenewalBatchLayoutControlItem)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem19)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem22)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem23)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem30)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.LedgerTabGroup)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.LedgerHoldCoverLayoutControlItem)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.LedgerCurrentOnLayoutControlItem)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.LedgerInvoiceOverdueOnLayoutControlItem)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.LedgerOverdueNoticeSentLayoutControlItem)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.LedgerExtendedLapsingOnLayoutControlItem)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.LedgerPremiumPaidLayoutControlItem)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.LedgerDistributeFundsLayoutControlItem)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.LedgerExtendedOverdueOnLayoutControlItem)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.LedgerFinalReminderSentLayoutControlItem)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.LedgerCancelledLapsingOnLayoutControlItem)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem8)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem8)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.NotesTabGroup)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.LogGroup)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.NotesAndSpecialConditionsGroup)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.SchemePaidLayoutControlItem)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem9)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.NotesCreatedByLayoutControlItem)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.NotesCreatedOnLayoutControlItem)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.NotesLastModifiedByLayoutControlItem)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.NotesLastModifiedOnLayoutControlItem)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.EasyDocTabGroup)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.LLReceivesElectronicCommunicationLayoutControlItem)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.REAReceivesElectronicCommunicationLayoutControlItem)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.EasyDocsEmailAddressLayoutControlItem)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.EasyDocsPrimaryAssociateLayoutControlItem)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem16)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.EasyDocsMobileNumberLayoutControlItem)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem26)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem4)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem33)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.HistoryTabGroup)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem10)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.PDSAndFSGTabGroup)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.PDSAndFSGGroup)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem11)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.CommunicationsGroup)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem38)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem6)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ContactCodeHeaderLayoutControlItem)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ContactNameHeaderLayoutControlItem)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.LandlordCodeHeaderLayoutControlItem)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.LandlordNameHeaderLayoutControlItem)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TransactionNumberHeaderLayoutControlItem)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TransactionStatusHeaderLayoutControlItem)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TransactionRenewalHeaderLayoutControlItem)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.PremiumAmountHeaderLayoutControlItem)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.OutstandingAmountHeaderLayoutControlItem)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ClaimCountHeaderLayoutControlItem)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.popupMenu1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dxErrorProvider1)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private DevExpress.XtraLayout.LayoutControl MainLayoutControl;
        private DevExpress.XtraLayout.LayoutControlGroup Root;
        private DevExpress.XtraBars.BarManager barManager1;
        private DevExpress.XtraBars.Bar PolicyMenuBar;
        private DevExpress.XtraBars.BarButtonItem RefreshButton;
        private DevExpress.XtraBars.BarButtonItem ExtendCreditTermsButton;
        private DevExpress.XtraBars.BarButtonItem RecalcBalanceButton;
        private DevExpress.XtraBars.BarButtonItem CreateClaimButton;
        private DevExpress.XtraBars.BarButtonItem SendPolicyDocumentButton;
        private DevExpress.XtraBars.BarButtonItem PremiumButton;
        private DevExpress.XtraBars.BarButtonItem NotesAndLogsButton;
        private DevExpress.XtraBars.BarButtonItem EndorsementButton;
        private DevExpress.XtraBars.BarButtonItem EditPaymentButton;
        private DevExpress.XtraBars.BarButtonItem EditButton;
        private DevExpress.XtraBars.BarButtonItem SaveButton;
        private DevExpress.XtraBars.BarButtonItem CancelButton;
        private DevExpress.XtraBars.BarDockControl barDockControlTop;
        private DevExpress.XtraBars.BarDockControl barDockControlBottom;
        private DevExpress.XtraBars.BarDockControl barDockControlLeft;
        private DevExpress.XtraBars.BarDockControl barDockControlRight;
        private DevExpress.XtraLayout.TabbedControlGroup tabbedControlGroup1;
        private DevExpress.XtraLayout.LayoutControlGroup PolicyTabGroup;
        private DevExpress.XtraLayout.LayoutControlGroup OtherDetailsTabGroup;
        private DevExpress.XtraLayout.LayoutControlGroup DisclosureTabGroup;
        private DevExpress.XtraLayout.LayoutControlGroup PremiumAndRenewalTabGroup;
        private DevExpress.XtraLayout.LayoutControlGroup LedgerTabGroup;
        private DevExpress.XtraLayout.LayoutControlGroup NotesTabGroup;
        private DevExpress.XtraLayout.LayoutControlGroup EasyDocTabGroup;
        private DevExpress.XtraLayout.LayoutControlGroup HistoryTabGroup;
        private DevExpress.XtraLayout.LayoutControlGroup PDSAndFSGTabGroup;
        private DevExpress.XtraEditors.LookUpEdit PremiumTypeField;
        private DevExpress.XtraEditors.TextEdit PolicyNumberField;
        private DevExpress.XtraEditors.ButtonEdit REACodeField;
        private DevExpress.XtraEditors.ButtonEdit NewBusinessArrangedByCodeField;
        private DevExpress.XtraEditors.LookUpEdit DwellingTypeField;
        private DevExpress.XtraEditors.LookUpEdit ConstructionTypeField;
        private DevExpress.XtraEditors.LookUpEdit YearBuiltField;
        private DevExpress.XtraLayout.LayoutControlGroup PolicyDetailsGroup;
        private DevExpress.XtraLayout.LayoutControlItem PolicyClassLayoutControlItem;
        private DevExpress.XtraLayout.LayoutControlItem PremiumTypeLayoutControlItem;
        private DevExpress.XtraLayout.LayoutControlItem PolicyNumberLayoutControlItem;
        private DevExpress.XtraLayout.EmptySpaceItem emptySpaceItem24;
        private DevExpress.XtraLayout.LayoutControlItem REACodeLayoutControlItem;
        private DevExpress.XtraLayout.LayoutControlItem NewBusinessArrangedByLayoutControlItem;
        private DevExpress.XtraLayout.LayoutControlItem DwellingTypeLayoutControlItem;
        private DevExpress.XtraLayout.LayoutControlItem ConstructionTypeLayoutControlItem;
        private DevExpress.XtraLayout.LayoutControlItem YearBuiltLayoutControlItem;
        private DevExpress.XtraLayout.LayoutControlItem CommonGroundsLayoutControlItem;
        private DevExpress.XtraLayout.LayoutControlItem HolidayLetLayoutControlItem;
        private DevExpress.XtraLayout.LayoutControlItem FurnishedLayoutControlItem;
        private DevExpress.XtraLayout.EmptySpaceItem emptySpaceItem34;
        private DevExpress.XtraLayout.LayoutControlItem CCTVLayoutControlItem;
        private DevExpress.XtraLayout.LayoutControlItem ElectronicAccessLayoutControlItem;
        private DevExpress.XtraLayout.EmptySpaceItem emptySpaceItem35;
        private DevExpress.XtraLayout.LayoutControlItem BusinessOrTradeLayoutControlItem;
        private DevExpress.XtraLayout.LayoutControlItem FreeStandingLayoutControlItem;
        private DevExpress.XtraLayout.LayoutControlItem AcceptedBusinessUseLayoutControlItem;
        private DevExpress.XtraEditors.ButtonEdit LandlordCodeField;
        private DevExpress.XtraEditors.TextEdit LandlordNameField;
        private DevExpress.XtraEditors.LookUpEdit BranchField;
        private DevExpress.XtraEditors.TextEdit BushfireRatingField;
        private DevExpress.XtraEditors.LookUpEdit ContentsSumInsuredField;
        private DevExpress.XtraEditors.TextEdit BuildingSumInsuredAmountField;
        private DevExpress.XtraEditors.LookUpEdit WeeksInArrearsField;
        private DevExpress.XtraEditors.TextEdit FloodRiskRatingField;
        private DevExpress.XtraEditors.TextEdit WeeklyRentSumInsuredAmountField;
        private DevExpress.XtraEditors.TextEdit UnderwriterExcessAmountField;
        private DevExpress.XtraEditors.TextEdit BuildingSumInsuredExcessAmountField;
        private DevExpress.XtraEditors.LookUpEdit LandSizeField;
        private DevExpress.XtraLayout.LayoutControlGroup PropertyGroup;
        private DevExpress.XtraLayout.LayoutControlItem LandlordCodeLayoutControlItem;
        private DevExpress.XtraLayout.LayoutControlItem LandlordNameLayoutControlItem;
        private DevExpress.XtraLayout.LayoutControlItem InsuredPropertyAddressLayoutControlItem;
        private DevExpress.XtraLayout.LayoutControlItem BranchLayoutControlItem;
        private DevExpress.XtraLayout.LayoutControlItem BushfireLayoutControlItem;
        private DevExpress.XtraLayout.LayoutControlItem ContentsSumInsuredLayoutControlItem;
        private DevExpress.XtraLayout.LayoutControlItem BuildingSumInsuredLayoutControlItem;
        private DevExpress.XtraLayout.LayoutControlItem TenantedLayoutControlItem;
        private DevExpress.XtraLayout.LayoutControlItem JobReducedCoronaVirusLayoutControlItem;
        private DevExpress.XtraLayout.LayoutControlItem ArrearsLayoutControlItem;
        private DevExpress.XtraLayout.EmptySpaceItem emptySpaceItem3;
        private DevExpress.XtraLayout.LayoutControlItem WeeksInArrearsLayoutControlItem;
        private DevExpress.XtraLayout.EmptySpaceItem emptySpaceItem4;
        private DevExpress.XtraLayout.EmptySpaceItem emptySpaceItem17;
        private DevExpress.XtraLayout.LayoutControlItem FloodRiskLayoutControlItem;
        private DevExpress.XtraLayout.LayoutControlItem WeeklyRentSumInsuredLayoutControlItem;
        private DevExpress.XtraLayout.LayoutControlItem UnderwriterExcessLayoutControlItem;
        private DevExpress.XtraLayout.LayoutControlItem BuildingSumInsuredExcessLayoutControlItem;
        private DevExpress.XtraLayout.LayoutControlItem LandSizeLayoutControlItem;
        private DevExpress.XtraEditors.TextEdit TransactionNumberField;
        private DevExpress.XtraEditors.DateEdit CreatedOnField;
        private DevExpress.XtraEditors.DateEdit InceptionFromField;
        private DevExpress.XtraEditors.TextEdit BalanceOutstandingAmountField;
        private DevExpress.XtraEditors.DateEdit EffectiveFromField;
        private DevExpress.XtraEditors.DateEdit BilledFromField;
        private DevExpress.XtraEditors.DateEdit ClosingOnField;
        private DevExpress.XtraEditors.DateEdit COISentOnField;
        private DevExpress.XtraEditors.DateEdit BilledToField;
        private DevExpress.XtraEditors.DateEdit RenewalOnField;
        private DevExpress.XtraEditors.DateEdit PolicySentOnField;
        private DevExpress.XtraEditors.DateEdit InvoicePrintedOnField;
        private DevExpress.XtraLayout.LayoutControlGroup DatesGroup;
        private DevExpress.XtraLayout.LayoutControlItem TransactionNumberLayoutControlItem;
        private DevExpress.XtraLayout.LayoutControlItem CreatedOnLayoutControlItem;
        private DevExpress.XtraLayout.LayoutControlItem InceptionFromLayoutControlItem;
        private DevExpress.XtraLayout.LayoutControlItem BalanceLayoutControlItem;
        private DevExpress.XtraLayout.LayoutControlItem EffectiveFromLayoutControlItem;
        private DevExpress.XtraLayout.LayoutControlItem BilledFromLayoutControlItem;
        private DevExpress.XtraLayout.LayoutControlItem ClosingOnLayoutControlItem;
        private DevExpress.XtraLayout.LayoutControlItem COISentOnLayoutControlItem;
        private DevExpress.XtraLayout.LayoutControlItem BilledToLayoutControlItem;
        private DevExpress.XtraLayout.LayoutControlItem RenewalOnLayoutControlItem;
        private DevExpress.XtraLayout.LayoutControlItem PolicySentOnLayoutControlItem;
        private DevExpress.XtraLayout.LayoutControlItem InvoicePrintedOnLayoutControlItem;
        private DevExpress.XtraLayout.LayoutControlGroup DiscountsGroup;
        private DevExpress.XtraLayout.LayoutControlItem FivePercentDiscountLayoutControlItem;
        private DevExpress.XtraLayout.LayoutControlItem MultiplePropertyDiscountLayoutControlItem;
        private DevExpress.XtraLayout.LayoutControlItem READiscountLayoutControlItem;
        private DevExpress.XtraLayout.LayoutControlItem StaffDiscountLayoutControlItem;
        private DevExpress.XtraLayout.EmptySpaceItem emptySpaceItem21;
        private DevExpress.XtraLayout.EmptySpaceItem emptySpaceItem12;
        private DevExpress.XtraLayout.LayoutControlItem FifteenForTwelveBonusOfferLayoutControlItem;
        private DevExpress.XtraEditors.TextEdit ContactCodeHeaderField;
        private DevExpress.XtraEditors.ButtonEdit ContactNameHeaderField;
        private DevExpress.XtraEditors.TextEdit LandlordCodeHeaderField;
        private DevExpress.XtraEditors.ButtonEdit LandlordNameHeaderField;
        private DevExpress.XtraEditors.TextEdit TransactionNumberHeaderField;
        private DevExpress.XtraEditors.TextEdit TransactionStatusHeaderField;
        private DevExpress.XtraEditors.TextEdit TransactionRenewalHeaderField;
        private DevExpress.XtraEditors.TextEdit PremiumAmountHeaderField;
        private DevExpress.XtraEditors.TextEdit OutstandingAmountHeaderField;
        private DevExpress.XtraEditors.ButtonEdit ClaimCountHeaderField;
        private DevExpress.XtraLayout.LayoutControlItem ContactCodeHeaderLayoutControlItem;
        private DevExpress.XtraLayout.LayoutControlItem ContactNameHeaderLayoutControlItem;
        private DevExpress.XtraLayout.LayoutControlItem LandlordCodeHeaderLayoutControlItem;
        private DevExpress.XtraLayout.LayoutControlItem LandlordNameHeaderLayoutControlItem;
        private DevExpress.XtraLayout.LayoutControlItem TransactionNumberHeaderLayoutControlItem;
        private DevExpress.XtraLayout.LayoutControlItem TransactionStatusHeaderLayoutControlItem;
        private DevExpress.XtraLayout.LayoutControlItem TransactionRenewalHeaderLayoutControlItem;
        private DevExpress.XtraLayout.LayoutControlItem PremiumAmountHeaderLayoutControlItem;
        private DevExpress.XtraLayout.LayoutControlItem OutstandingAmountHeaderLayoutControlItem;
        private DevExpress.XtraLayout.LayoutControlItem ClaimCountHeaderLayoutControlItem;
        private DevExpress.XtraLayout.LayoutControlItem OnHoldLayoutControlItem;
        private DevExpress.XtraLayout.LayoutControlItem RewiredReplumbedLayoutControlItem;
        private Controls.FancyCheckEdit OnHoldField;
        private Controls.FancyCheckEdit RewiredReplumbedField;
        private Controls.FancyCheckEdit FreeStandingField;
        private Controls.FancyCheckEdit CommonGroundsField;
        private Controls.FancyCheckEdit BusinessOrTradeField;
        private Controls.FancyCheckEdit AcceptedBusinessUseField;
        private Controls.FancyCheckEdit IsJobReducedDuePandemicField;
        private Controls.FancyCheckEdit IsArrearsField;
        private Controls.FancyCheckEdit TenantedField;
        private Controls.FancyCheckEdit StaffDiscountField;
        private Controls.FancyCheckEdit IsREADiscountField;
        private Controls.FancyCheckEdit IsMultiplePropertyDiscountField;
        private Controls.FancyCheckEdit IsFivePercentDiscountField;
        private Controls.FancyCheckEdit Is15For12BonusOfferField;
        private DevExpress.XtraEditors.LookUpEdit PremiumPaidByField;
        private DevExpress.XtraEditors.LookUpEdit PreferredMailingAddressTypeField;
        private DevExpress.XtraEditors.TextEdit InsurerCodeField;
        private DevExpress.XtraEditors.ButtonEdit OnlineQuoteNumberField;
        private DevExpress.XtraEditors.TextEdit QuoteNumberField;
        private DevExpress.XtraEditors.LookUpEdit AskedForQuoteWhoField;
        private DevExpress.XtraEditors.LookUpEdit HowAskedForQuoteField;
        private DevExpress.XtraEditors.TextEdit QuotedByField;
        private DevExpress.XtraEditors.TextEdit QuotedOnField;
        private DevExpress.XtraEditors.TextEdit ConvertedToCoverByField;
        private DevExpress.XtraEditors.TextEdit ConvertedToCoverOnField;
        private DevExpress.XtraEditors.TextEdit QuoteExpiresOnField;
        private DevExpress.XtraEditors.TextEdit OnlineQuoteEmailAddressField;
        private DevExpress.XtraLayout.LayoutControlGroup PremiumDetailsGroup;
        private DevExpress.XtraLayout.LayoutControlItem PremiumPaidByLayoutControlItem;
        private DevExpress.XtraLayout.LayoutControlItem PreferredMailingAddressLayoutControlItem;
        private DevExpress.XtraLayout.LayoutControlItem InsurerCodeLayoutControlItem;
        private DevExpress.XtraLayout.LayoutControlGroup QuoteDetailsGroup;
        private DevExpress.XtraLayout.LayoutControlItem OnlineQuoteNumberLayoutControlItem;
        private DevExpress.XtraLayout.LayoutControlItem QuoteNumberLayoutControlItem;
        private DevExpress.XtraLayout.LayoutControlItem AskedForQuoteLayoutControlItem;
        private DevExpress.XtraLayout.LayoutControlItem HowAskedLayoutControlItem;
        private DevExpress.XtraLayout.LayoutControlItem QuotedByLayoutControlItem;
        private DevExpress.XtraLayout.LayoutControlItem QuotedOnLayoutControlItem;
        private DevExpress.XtraLayout.LayoutControlItem ConvertedToCoverByLayoutControlItem;
        private DevExpress.XtraLayout.LayoutControlItem ConvertedToCoverOnLayoutControlItem;
        private DevExpress.XtraLayout.LayoutControlItem QuoteExpiresOnLayoutControlItem;
        private DevExpress.XtraLayout.LayoutControlItem OnlineQuoteEmailLayoutControlItem;
        private DevExpress.XtraLayout.EmptySpaceItem emptySpaceItem7;
        private DevExpress.XtraLayout.EmptySpaceItem emptySpaceItem1;
        private DevExpress.XtraEditors.LookUpEdit WhoPlacedCoverField;
        private DevExpress.XtraEditors.LookUpEdit HowPlacedCoverField;
        private DevExpress.XtraEditors.LookUpEdit HeardAboutTSIviaField;
        private DevExpress.XtraEditors.MemoEdit MortgageeField;
        private DevExpress.XtraEditors.LookUpEdit PaymentMethodField;
        private DevExpress.XtraEditors.TextEdit PaymentReferenceField;
        private DevExpress.XtraEditors.TextEdit BPayReceiptField;
        private DevExpress.XtraEditors.TextEdit OnlineReferenceField;
        private DevExpress.XtraEditors.TextEdit CCTransactionIDField;
        private DevExpress.XtraEditors.LookUpEdit VoucherTypeField;
        private DevExpress.XtraEditors.LookUpEdit PromoCodeField;
        private DevExpress.XtraEditors.TextEdit LinkCreditTransactionNumberField;
        private DevExpress.XtraEditors.TextEdit LinkParentTransactionNumberField;
        private DevExpress.XtraEditors.TextEdit LinkReferToTransactionNumberField;
        private DevExpress.XtraEditors.TextEdit LinkWelcomeLetterTransactionNumberField;
        private Controls.FancyCheckEdit RecreatedField;
        private Controls.FancyCheckEdit PaymentReceivedField;
        private DevExpress.XtraEditors.TextEdit ApprovedByField;
        private DevExpress.XtraEditors.TextEdit ApprovedOnField;
        private DevExpress.XtraEditors.TextEdit RenewingTransactionsField;
        private DevExpress.XtraEditors.LookUpEdit RenewalReasonField;
        private DevExpress.XtraEditors.LookUpEdit RenewalPremiumPaidByField;
        private DevExpress.XtraEditors.TextEdit textEdit51;
        private DevExpress.XtraEditors.LookUpEdit RenewalDiscountAmountField;
        private DevExpress.XtraLayout.LayoutControlGroup RenewingTransactionSettingsGroup;
        private DevExpress.XtraLayout.LayoutControlItem RenewingTransactionsLayoutControlItem;
        private DevExpress.XtraLayout.LayoutControlItem RenewalsOnHoldLayoutControlItem;
        private DevExpress.XtraLayout.LayoutControlItem RenewalReasonLayoutControlItem;
        private DevExpress.XtraLayout.LayoutControlItem RenewalPremiumPaidByLayoutControlItem;
        private DevExpress.XtraLayout.LayoutControlItem RenewalImposedExcessLayoutControlItem;
        private DevExpress.XtraLayout.LayoutControlItem RenewExcessAmountLayoutControlItem;
        private DevExpress.XtraLayout.EmptySpaceItem emptySpaceItem27;
        private DevExpress.XtraLayout.LayoutControlItem DoNotRenewLayoutControlItem;
        private DevExpress.XtraLayout.EmptySpaceItem emptySpaceItem28;
        private DevExpress.XtraLayout.EmptySpaceItem emptySpaceItem29;
        private DevExpress.XtraLayout.LayoutControlGroup RenewingDiscountsGroup;
        private DevExpress.XtraLayout.LayoutControlItem RenewalMultiplePropertyDiscountLayoutControlItem;
        private DevExpress.XtraLayout.LayoutControlItem RealEstateDiscountLayoutControlItem;
        private DevExpress.XtraLayout.LayoutControlItem RenewalStaffDiscountLayoutControlItem;
        private DevExpress.XtraLayout.LayoutControlItem ApplyToRenewingPremiumLayoutControlItem;
        private DevExpress.XtraLayout.EmptySpaceItem emptySpaceItem30;
        private DevExpress.XtraLayout.LayoutControlItem RenewalDiscountAmountLayoutControlItem;
        private DevExpress.XtraLayout.LayoutControlGroup CoverDetailsGroup;
        private DevExpress.XtraLayout.LayoutControlItem RecreatedLayoutControlItem;
        private DevExpress.XtraLayout.LayoutControlItem WhoPlacedCoverLayoutControlItem;
        private DevExpress.XtraLayout.LayoutControlItem HowPlacedCoverLayoutControlItem;
        private DevExpress.XtraLayout.EmptySpaceItem emptySpaceItem2;
        private DevExpress.XtraLayout.LayoutControlItem HeardAboutTSIViaLayoutControlItem;
        private DevExpress.XtraLayout.LayoutControlItem MortgageeLayoutControlItem;
        private DevExpress.XtraLayout.LayoutControlGroup PaymentDetailsGroup;
        private DevExpress.XtraLayout.LayoutControlItem PaymentReceivedLayoutControlItem;
        private DevExpress.XtraLayout.LayoutControlItem PaymentMethodLayoutControlItem;
        private DevExpress.XtraLayout.LayoutControlItem PaymentReferenceLayoutControlItem;
        private DevExpress.XtraLayout.LayoutControlItem BPayReceiptLayoutControlItem;
        private DevExpress.XtraLayout.LayoutControlItem OnlineReferenceLayoutControlItem;
        private DevExpress.XtraLayout.LayoutControlItem CCTransactionIDLayoutControlItem;
        private DevExpress.XtraLayout.LayoutControlItem VoucherLayoutControlItem;
        private DevExpress.XtraLayout.LayoutControlItem PromoCodeLayoutControlItem;
        private DevExpress.XtraLayout.EmptySpaceItem emptySpaceItem25;
        private DevExpress.XtraLayout.LayoutControlGroup LinkedTransactionsGroup;
        private DevExpress.XtraLayout.LayoutControlItem CreditTransactionLayoutControlItem;
        private DevExpress.XtraLayout.LayoutControlItem ParentTransactionLayoutControlItem;
        private DevExpress.XtraLayout.EmptySpaceItem emptySpaceItem5;
        private DevExpress.XtraLayout.LayoutControlItem ReferToTransactionLayoutControlItem;
        private DevExpress.XtraLayout.LayoutControlItem WelcomeLetterJoinedLayoutControlItem;
        private DevExpress.XtraLayout.EmptySpaceItem emptySpaceItem15;
        private DevExpress.XtraLayout.LayoutControlGroup ApprovalGroup;
        private DevExpress.XtraLayout.LayoutControlItem ApprovedByLayoutControlItem;
        private DevExpress.XtraLayout.LayoutControlItem ApprovedOnLayoutControlItem;
        private DevExpress.XtraLayout.EmptySpaceItem emptySpaceItem14;
        private DevExpress.XtraRichEdit.RichEditControl NotesField;
        private DevExpress.XtraEditors.ButtonEdit RenewedTransactionField;
        private DevExpress.XtraEditors.ButtonEdit RenewedByTransactionField;
        private DevExpress.XtraEditors.MemoEdit LastRenewalErrorField;
        private DevExpress.XtraEditors.TextEdit RenewalBatchField;
        private DevExpress.XtraEditors.DateEdit LedgerHoldCoverField;
        private DevExpress.XtraEditors.DateEdit LedgerPremiumPaidField;
        private DevExpress.XtraEditors.DateEdit LedgerCurrentOnField;
        private DevExpress.XtraEditors.DateEdit LedgerDistributeFundsField;
        private DevExpress.XtraEditors.DateEdit LedgerInvoiceOverdueOnField;
        private DevExpress.XtraEditors.DateEdit LedgerExtendedOverdueOnField;
        private DevExpress.XtraEditors.DateEdit LedgerOverdueNoticeSentField;
        private DevExpress.XtraEditors.DateEdit LedgerExtendedLapsingOnField;
        private DevExpress.XtraEditors.DateEdit LedgerFinalReminderSentField;
        private DevExpress.XtraEditors.DateEdit LedgerCancelledLapsedOnField;
        private DevExpress.XtraEditors.TextEdit NotesLastModifiedOnField;
        private DevExpress.XtraEditors.TextEdit NotesLastModifiedByField;
        private DevExpress.XtraEditors.TextEdit NotesCreatedOnField;
        private DevExpress.XtraEditors.TextEdit NotesCreatedByField;
        private DevExpress.XtraEditors.DateEdit SchemePaidField;
        private DevExpress.XtraLayout.LayoutControlGroup LogGroup;
        private DevExpress.XtraLayout.LayoutControlGroup NotesAndSpecialConditionsGroup;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem1;
        private DevExpress.XtraLayout.LayoutControlItem SchemePaidLayoutControlItem;
        private DevExpress.XtraLayout.EmptySpaceItem emptySpaceItem9;
        private DevExpress.XtraLayout.LayoutControlItem NotesCreatedByLayoutControlItem;
        private DevExpress.XtraLayout.LayoutControlItem NotesCreatedOnLayoutControlItem;
        private DevExpress.XtraLayout.LayoutControlItem NotesLastModifiedByLayoutControlItem;
        private DevExpress.XtraLayout.LayoutControlItem NotesLastModifiedOnLayoutControlItem;
        private DevExpress.XtraLayout.LayoutControlGroup RenewalProcessingGroup;
        private DevExpress.XtraLayout.LayoutControlItem RenewedTransactionLayoutControlItem;
        private DevExpress.XtraLayout.LayoutControlItem RenewedByTransactionLayoutControlItem;
        private DevExpress.XtraLayout.LayoutControlItem LastRenewalErrorLayoutControlItem;
        private DevExpress.XtraLayout.LayoutControlItem RenewalBatchLayoutControlItem;
        private DevExpress.XtraLayout.EmptySpaceItem emptySpaceItem19;
        private DevExpress.XtraLayout.EmptySpaceItem emptySpaceItem22;
        private DevExpress.XtraLayout.EmptySpaceItem emptySpaceItem23;
        private DevExpress.XtraLayout.LayoutControlItem LedgerHoldCoverLayoutControlItem;
        private DevExpress.XtraLayout.LayoutControlItem LedgerCurrentOnLayoutControlItem;
        private DevExpress.XtraLayout.LayoutControlItem LedgerInvoiceOverdueOnLayoutControlItem;
        private DevExpress.XtraLayout.LayoutControlItem LedgerOverdueNoticeSentLayoutControlItem;
        private DevExpress.XtraLayout.LayoutControlItem LedgerExtendedLapsingOnLayoutControlItem;
        private DevExpress.XtraLayout.LayoutControlItem LedgerPremiumPaidLayoutControlItem;
        private DevExpress.XtraLayout.LayoutControlItem LedgerDistributeFundsLayoutControlItem;
        private DevExpress.XtraLayout.LayoutControlItem LedgerExtendedOverdueOnLayoutControlItem;
        private DevExpress.XtraLayout.LayoutControlItem LedgerFinalReminderSentLayoutControlItem;
        private DevExpress.XtraLayout.LayoutControlItem LedgerCancelledLapsingOnLayoutControlItem;
        private DevExpress.XtraLayout.EmptySpaceItem emptySpaceItem8;
        private DevExpress.Utils.ToolTipController LookupEditToolTipController;
        private DevExpress.XtraBars.PopupMenu popupMenu1;
        private DevExpress.XtraGrid.GridControl DisclosureClaimsGrid;
        private DevExpress.XtraGrid.Views.Grid.GridView DisclosureClaimsGridView;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn32;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn33;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn34;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn35;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn36;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn37;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn38;
        private DevExpress.XtraGrid.GridControl EasyDocsGrid;
        private DevExpress.XtraGrid.Views.Grid.GridView EasyDocsGridView;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn39;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn40;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn41;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn42;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn43;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn44;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn45;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn46;
        private DevExpress.XtraEditors.TextEdit EasyDocsEmailAddressField;
        private DevExpress.XtraEditors.ButtonEdit EasyDocsPrimaryAssociateField;
        private DevExpress.XtraEditors.TextEdit EasyDocsMobilePhoneField;
        private Controls.FancyCheckEdit LLReceivesElectronicCommunicationsField;
        private Controls.FancyCheckEdit REAReceivesElectronicCommunicationsField;
        private DevExpress.XtraEditors.MemoEdit DenialReasonField;
        private DevExpress.XtraEditors.MemoEdit SpecialTermsField;
        private Controls.FancyCheckEdit DisclosureItem1Field;
        private Controls.FancyCheckEdit DisclosureItem2Field;
        private DevExpress.XtraEditors.MemoEdit CircumstancesField;
        private DevExpress.XtraEditors.MemoEdit RestrictionsField;
        private Controls.FancyCheckEdit Disclosure3Field;
        private Controls.FancyCheckEdit Disclosure4Field;
        private DevExpress.XtraLayout.EmptySpaceItem emptySpaceItem32;
        private DevExpress.XtraLayout.LayoutControlGroup QuesstionnaireGroup;
        private DevExpress.XtraLayout.LayoutControlItem DisclosureItem1LayoutControlItem;
        private DevExpress.XtraLayout.LayoutControlItem DenialReasonLayoutControlItem;
        private DevExpress.XtraLayout.LayoutControlItem SpecialTermsLayoutControlItem;
        private DevExpress.XtraLayout.LayoutControlItem DisclosureItem2LayoutControlItem;
        private DevExpress.XtraLayout.EmptySpaceItem emptySpaceItem10;
        private DevExpress.XtraLayout.EmptySpaceItem emptySpaceItem13;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem5;
        private DevExpress.XtraLayout.LayoutControlItem DisclosureItem3LayoutControlItem;
        private DevExpress.XtraLayout.LayoutControlItem DisclosureItem4LayoutControlItem;
        private DevExpress.XtraLayout.LayoutControlItem CircumstancesLayoutControlItem;
        private DevExpress.XtraLayout.LayoutControlItem DisclosureItem5LayoutControlItem;
        private DevExpress.XtraLayout.LayoutControlItem RestrictionsLayoutControlItem;
        private DevExpress.XtraLayout.EmptySpaceItem emptySpaceItem18;
        private DevExpress.XtraLayout.EmptySpaceItem emptySpaceItem20;
        private DevExpress.XtraLayout.LayoutControlItem LLReceivesElectronicCommunicationLayoutControlItem;
        private DevExpress.XtraLayout.LayoutControlItem REAReceivesElectronicCommunicationLayoutControlItem;
        private DevExpress.XtraLayout.LayoutControlItem EasyDocsEmailAddressLayoutControlItem;
        private DevExpress.XtraLayout.LayoutControlItem EasyDocsPrimaryAssociateLayoutControlItem;
        private DevExpress.XtraLayout.EmptySpaceItem emptySpaceItem16;
        private DevExpress.XtraLayout.LayoutControlItem EasyDocsMobileNumberLayoutControlItem;
        private DevExpress.XtraLayout.EmptySpaceItem emptySpaceItem26;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem4;
        private DevExpress.XtraLayout.EmptySpaceItem emptySpaceItem33;
        private DevExpress.XtraGrid.GridControl LogGrid;
        private DevExpress.XtraGrid.Views.Grid.GridView LogGridView;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn28;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn29;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn30;
        private DevExpress.XtraEditors.Repository.RepositoryItemRichTextEdit repositoryItemRichTextEdit1;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn31;
        private DevExpress.XtraGrid.GridControl LedgerGrid;
        private DevExpress.XtraGrid.Views.Grid.GridView LedgerGridView;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn27;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn13;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn14;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn15;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn16;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn17;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn18;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn19;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn20;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn21;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn22;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn23;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn24;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn25;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn26;
        private DevExpress.XtraGrid.GridControl PremiumDetailsGrid;
        private DevExpress.XtraGrid.Views.Grid.GridView PremiumDetailsGridView;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn7;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn8;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn9;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn10;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn11;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn12;
        private DevExpress.XtraGrid.GridControl PremiumBaseRatesGrid;
        private DevExpress.XtraGrid.Views.Grid.GridView PremiumBaseRatesGridView;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn6;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn1;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn2;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn3;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn4;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn5;
        private Controls.FancyCheckEdit RenewalMultiPropertyDiscountField;
        private Controls.FancyCheckEdit RenewalsOnHoldField;
        private Controls.FancyCheckEdit DoNotRenewField;
        private Controls.FancyCheckEdit RenewalImposedExcessField;
        private Controls.FancyCheckEdit RealEstateDiscountField;
        private Controls.FancyCheckEdit RenewalStaffDiscountField;
        private Controls.FancyCheckEdit ApplyToRenewingPremiumField;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem2;
        private DevExpress.XtraLayout.LayoutControlGroup ExistingPremiumDetailsGroup;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem6;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem7;
        private DevExpress.XtraLayout.EmptySpaceItem emptySpaceItem37;
        private DevExpress.XtraLayout.EmptySpaceItem emptySpaceItem11;
        private DevExpress.XtraLayout.EmptySpaceItem emptySpaceItem36;
        private DevExpress.XtraLayout.EmptySpaceItem emptySpaceItem31;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem8;
        private DevExpress.XtraGrid.GridControl PDSandFSGGrid;
        private DevExpress.XtraGrid.Views.Grid.GridView PDSandFSGGridView;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn71;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn72;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn73;
        private DevExpress.XtraGrid.GridControl HistoryGrid;
        private DevExpress.XtraGrid.Views.Grid.GridView HistoryDetailGridView;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn59;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn60;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn61;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn62;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn63;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn64;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn65;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn66;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn67;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn68;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn69;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn70;
        private DevExpress.XtraGrid.Views.Grid.GridView HistoryGridView;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn47;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn48;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn49;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn50;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn51;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn52;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn53;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn54;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn55;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn56;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn57;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn58;
        private DevExpress.XtraLayout.LayoutControlGroup PDSAndFSGGroup;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem11;
        private DevExpress.XtraLayout.LayoutControlGroup CommunicationsGroup;
        private DevExpress.XtraLayout.EmptySpaceItem emptySpaceItem38;
        private DevExpress.XtraLayout.EmptySpaceItem emptySpaceItem6;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem10;
        private DevExpress.XtraLayout.EmptySpaceItem emptySpaceItem39;
        private DevExpress.XtraEditors.CheckEdit Disclosure5AwaitingApprovalField;
        private DevExpress.XtraEditors.CheckEdit Disclosure5NoField;
        private DevExpress.XtraEditors.CheckEdit Disclosure5YesField;
        private DevExpress.XtraLayout.LayoutControlItem Disclosure5NoLayoutItem;
        private DevExpress.XtraLayout.LayoutControlItem Disclosure5AwaitingApprovalLayoutItem;
        private DevExpress.XtraEditors.DXErrorProvider.DXErrorProvider dxErrorProvider1;
        private DevExpress.XtraEditors.LookUpEdit ClassOfRiskField;
        private DevExpress.XtraLayout.EmptySpaceItem emptySpaceItem40;
        private DevExpress.XtraLayout.EmptySpaceItem emptySpaceItem41;
        private DevExpress.XtraLayout.LayoutControlItem SelfManagedlayoutControlItem;
        private DevExpress.XtraEditors.LookUpEdit SelfManagedField;
        private DevExpress.XtraEditors.LookUpEdit IsFurnishedField;
        private DevExpress.XtraEditors.LookUpEdit IsCCTVField;
        private DevExpress.XtraEditors.LookUpEdit IsElectronicAccessField;
        private DevExpress.XtraEditors.LookUpEdit HolidayLetField;
    }
}
