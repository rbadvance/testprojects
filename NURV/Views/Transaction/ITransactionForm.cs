﻿using System.Windows.Forms;
using System.Threading.Tasks;
using DevExpress.XtraEditors;

using ReactiveUI;

namespace NURV.Views.Transaction
{
    internal interface ITransactionForm : IViewFor
    {
       void ViewModel_MessageBoxRequest(object sender, ViewModels.Common.MvvmMessageBoxEventArgs e);

        DialogResult ShowDialog(IWin32Window owner);

        Task DisplayTransactionAsync(string transactionNumber);
    }
}
