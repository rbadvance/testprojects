﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NURV.Types
{
    internal enum ScreenMode
    {
        ReadOnly,
        New,
        Edit
    }
}
