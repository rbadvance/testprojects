﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Windows.Forms;
using Microsoft.Extensions.Logging;

using Splat;

namespace NURV
{
    public static class Program
    {
        public static IServiceProvider ServiceProvider;

        /// <summary>
        /// The main entry point for the application.
        /// </summary>
        [STAThread]
        public static void Main()
        {
            // Register dependencies for Dependency Injection (Splat)
            ConfigureServices();

            Application.EnableVisualStyles();
            Application.SetCompatibleTextRenderingDefault(false);
            Application.Run(new Views.Transaction.TransactionForm());
        }

        private static void ConfigureServices()
        {
            // register views
            Locator.CurrentMutable.Register(() => new Views.Transaction.TransactionForm(), typeof(Views.Transaction.ITransactionForm));

            // register services
            Locator.CurrentMutable.Register(() => Core.Services.Transaction.TransactionService.CreateService(), typeof(Core.Services.Transaction.ITransactionService));
        }
    }
}
