﻿IF OBJECT_ID(N'[__EFMigrationsHistory]') IS NULL
BEGIN
    CREATE TABLE [__EFMigrationsHistory] (
        [MigrationId] nvarchar(150) NOT NULL,
        [ProductVersion] nvarchar(32) NOT NULL,
        CONSTRAINT [PK___EFMigrationsHistory] PRIMARY KEY ([MigrationId])
    );
END;

GO

IF NOT EXISTS(SELECT * FROM [__EFMigrationsHistory] WHERE [MigrationId] = N'20210820025420_InitMigration')
BEGIN
    INSERT INTO [__EFMigrationsHistory] ([MigrationId], [ProductVersion])
    VALUES (N'20210820025420_InitMigration', N'3.1.18');
END;

GO

IF NOT EXISTS(SELECT * FROM [__EFMigrationsHistory] WHERE [MigrationId] = N'20210910072301_Init000')
BEGIN
    INSERT INTO [__EFMigrationsHistory] ([MigrationId], [ProductVersion])
    VALUES (N'20210910072301_Init000', N'3.1.18');
END;

GO

IF NOT EXISTS(SELECT * FROM [__EFMigrationsHistory] WHERE [MigrationId] = N'20210915222917_AddedPaymentMethodTable')
BEGIN
    CREATE TABLE [tblPaymentMethod] (
        [ID] int NOT NULL IDENTITY,
        [Data] nvarchar(50) NULL,
        [InCancellation] bit NOT NULL,
        [InHoldCover] bit NOT NULL,
        [InReceipts] bit NOT NULL,
        [InOverpayments] bit NOT NULL,
        [InManualRefunds] bit NOT NULL,
        [Type] nvarchar(25) NULL,
        [Method] nvarchar(10) NULL,
        [SortOrder] int NOT NULL,
        CONSTRAINT [PK_tblPaymentMethod] PRIMARY KEY ([ID])
    );
END;

GO

IF NOT EXISTS(SELECT * FROM [__EFMigrationsHistory] WHERE [MigrationId] = N'20210915222917_AddedPaymentMethodTable')
BEGIN

    SET IDENTITY_INSERT tblPaymentMethod ON;
    INSERT INTO dbo.tblPaymentMethod (ID,Data,InCancellation,InHoldCover,InReceipts,InOverpayments,InManualRefunds,Type,Method,SortOrder) 
        (SELECT ID,Data,InCancellation,InHoldCover,InReceipts,InOverpayments,InManualRefunds,Type,Method,SortOrder FROM viewList_PaymentMethod);
    SET IDENTITY_INSERT tblPaymentMethod OFF;

END;

GO

IF NOT EXISTS(SELECT * FROM [__EFMigrationsHistory] WHERE [MigrationId] = N'20210915222917_AddedPaymentMethodTable')
BEGIN
    INSERT INTO [__EFMigrationsHistory] ([MigrationId], [ProductVersion])
    VALUES (N'20210915222917_AddedPaymentMethodTable', N'3.1.18');
END;

GO

