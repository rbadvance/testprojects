﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

namespace NURV.Migrations.Migrations
{
    public partial class AddedPaymentMethodTable : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.CreateTable(
                name: "tblPaymentMethod",
                columns: table => new
                {
                    ID = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    Data = table.Column<string>(maxLength: 50, nullable: true),
                    InCancellation = table.Column<bool>(nullable: false),
                    InHoldCover = table.Column<bool>(nullable: false),
                    InReceipts = table.Column<bool>(nullable: false),
                    InOverpayments = table.Column<bool>(nullable: false),
                    InManualRefunds = table.Column<bool>(nullable: false),
                    Type = table.Column<string>(maxLength: 25, nullable: true),
                    Method = table.Column<string>(maxLength: 10, nullable: true),
                    SortOrder = table.Column<int>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_tblPaymentMethod", x => x.ID);
                });

            //insert data from original view
            migrationBuilder.Sql(@"
SET IDENTITY_INSERT tblPaymentMethod ON;
INSERT INTO dbo.tblPaymentMethod (ID,Data,InCancellation,InHoldCover,InReceipts,InOverpayments,InManualRefunds,Type,Method,SortOrder) 
    (SELECT ID,Data,InCancellation,InHoldCover,InReceipts,InOverpayments,InManualRefunds,Type,Method,SortOrder FROM viewList_PaymentMethod);
SET IDENTITY_INSERT tblPaymentMethod OFF;
");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "tblPaymentMethod");
        }
    }
}
