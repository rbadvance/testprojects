﻿
To identify schema changes and create a new schema migration...

1. Set MigrationBuilder as the startup project 
2. Open Package Manager Console
3. Ensure Default project is set to NURV.Migration
4. Add the new migration with command where xxx is the name of the new migration
   PM> Add-Migration xxx

For further info see https://www.entityframeworktutorial.net/efcore/entity-framework-core-migration.aspx

--------------------------------------------------------------------------------------------------

update-database > applies migrations to the data source in NURVDbContext.OnConfiguring()

--------------------------------------------------------------------------------------------------

Information on how to apply migrations to a production system...
https://docs.microsoft.com/en-us/ef/core/managing-schemas/migrations/applying?tabs=dotnet-core-cli

Example would be using Idempotent SQL Scripts.

** EF Core supports generating idempotent scripts, which internally check which migrations have already been applied (via the migrations history table), 
** and only apply missing ones. This is useful if you don't exactly know what the last migration applied to the database was, or if you are deploying to multiple 
** databases that may each be at a different migration.

1. Open Package Manager Console
2. Ensure Default project is set to NURV.Migration
3. run the following... (where 'xxx' is the name of the script)
   dotnet ef migrations script --idempotent --project NURV.Data --output NURV.Migrations\Scripts\xxx.sql --startup-project MigrationBuilder

--------------------------------------------------------------------------------------------------------------------------------------------------

Usage: dotnet ef migrations script [arguments] [options]

Arguments:
  <FROM>  The starting migration. Defaults to '0' (the initial database).
  <TO>    The target migration. Defaults to the last migration.

Options:
  -o|--output <FILE>                     The file to write the result to.
  -i|--idempotent                        Generate a script that can be used on a database at any migration.
  --no-transactions                      Don't generate SQL transaction statements.
  -c|--context <DBCONTEXT>               The DbContext to use.
  -p|--project <PROJECT>                 The project to use. Defaults to the current working directory.
  -s|--startup-project <PROJECT>         The startup project to use. Defaults to the current working directory.
  --framework <FRAMEWORK>                The target framework. Defaults to the first one in the project.
  --configuration <CONFIGURATION>        The configuration to use.
  --runtime <RUNTIME_IDENTIFIER>         The runtime to use.
  --msbuildprojectextensionspath <PATH>  The MSBuild project extensions path. Defaults to "obj".
  --no-build                             Don't build the project. Intended to be used when the build is up-to-date.
  -h|--help                              Show help information
  -v|--verbose                           Show verbose output.
  --no-color                             Don't colorize output.
  --prefix-output                        Prefix output with level.