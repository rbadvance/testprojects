﻿using System;
using System.Threading.Tasks;

using ReactiveUI;
using Splat;
using NUnit.Framework;
using NURV.ViewModels.Transaction;

namespace NURV.UnitTest.Tests
{
    [TestFixture]
    public class TransactionTests
    {
        [OneTimeSetUp]
        public virtual void OneTimeSetUp()
        {
            // register dependencies
            //Locator.CurrentMutable.Register(() => new Views.Transaction.CoverForm(), typeof(Views.Transaction.ICoverForm));
            //Locator.CurrentMutable.Register(() => new Views.Transaction.QuoteForm(), typeof(Views.Transaction.IQuoteForm));
            //Locator.CurrentMutable.Register(() => new ViewModels.Transaction.Tabs.Disclosure.DisclosureTab(), typeof(ViewModels.Transaction.Tabs.Disclosure.IDisclosureTab));
            //Locator.CurrentMutable.Register(() => new ViewModels.Transaction.Tabs.EasyDoc.EasyDocTab(), typeof(ViewModels.Transaction.Tabs.EasyDoc.IEasyDocTab));
            //Locator.CurrentMutable.Register(() => new ViewModels.Transaction.Tabs.History.HistoryTab(), typeof(ViewModels.Transaction.Tabs.History.IHistoryTab));
            //Locator.CurrentMutable.Register(() => new ViewModels.Transaction.Tabs.History.HistoryTab(), typeof(ViewModels.Transaction.Tabs.History.IHistoryTab));
            //Locator.CurrentMutable.Register(() => new ViewModels.Transaction.Tabs.Ledger.LedgerTab(), typeof(ViewModels.Transaction.Tabs.Ledger.ILedgerTab));
            //Locator.CurrentMutable.Register(() => new ViewModels.Transaction.Tabs.Notes.NotesTab(), typeof(ViewModels.Transaction.Tabs.Notes.INotesTab));
            //Locator.CurrentMutable.Register(() => new ViewModels.Transaction.Tabs.OtherDetails.OtherDetailsTab(), typeof(ViewModels.Transaction.Tabs.OtherDetails.IOtherDetailsTab));
            //Locator.CurrentMutable.Register(() => new ViewModels.Transaction.Tabs.PDSandFSG.PDSandFSGTab(), typeof(ViewModels.Transaction.Tabs.PDSandFSG.IPDSandFSGTab));
            //Locator.CurrentMutable.Register(() => new ViewModels.Transaction.Tabs.Policy.PolicyTab(), typeof(ViewModels.Transaction.Tabs.Policy.IPolicyTab));
            //Locator.CurrentMutable.Register(() => new ViewModels.Transaction.Tabs.PremiumRenewal.PremiumRenewalTab(), typeof(ViewModels.Transaction.Tabs.PremiumRenewal.IPremiumRenewalTab));
        }

        [OneTimeTearDown]
        public virtual void OneTimeTearDown()
        {

        }

        [Test]
        public async Task TransactionViewModelTest()
        {
            var viewModel = new TransactionViewModel();
            await viewModel.InitAsync();
            await viewModel.DisplayTransactionAsync("5000000");
        }
    }
}
