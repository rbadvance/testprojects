﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

using NURV.Data.DataAccess.Transactions.DTO;

namespace NURV.Core.Services.Transaction
{
    public interface ITransactionService
    {
        /// <remarks>Centralised Validate Transaction logic ...to be called from multiple places.</remarks>
        Task<Common.IResult<bool>> ValidateTransactionAsync(IDTTransaction transaction);

        /// <remarks>Centralised Save Transaction logic ...to be called from multiple places.</remarks>
        Task<Common.IResult<bool>> SaveTransactionAsync(IDTTransaction transaction);
    }
}
