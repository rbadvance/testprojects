﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

using NURV.Data.DataAccess.Transactions.DTO;

namespace NURV.Core.Services.Transaction
{
    public class TransactionService : ITransactionService
    {
        internal TransactionService()
        {

        }

        public static ITransactionService CreateService() => new TransactionService();

        /// <inheritdoc/>
        public Task<Common.IResult<bool>> ValidateTransactionAsync(IDTTransaction transaction)
        {
            throw new NotImplementedException();
        }

        /// <inheritdoc/>
        public Task<Common.IResult<bool>> SaveTransactionAsync(IDTTransaction transaction)
        {
            Common.IResult<bool> result = new Common.Result<bool>();

            using (var context = new NURV.Data.Context.NURVContext(true, useTransaction: true))
            {
                // FYI here we can call multiple command updates on different contexts, but as they are all
                // under a single transaction we are in control of any rollback requirements.

                //TODO: Implement Save logic
                context.Commit();
            }

            //return result;

            // TODO: remove this when method is fixed up...
            result.Message = "BooooooooooooooooooooooooooooooooooooM!";
            return Task.FromResult(result);
        }
    }
}
