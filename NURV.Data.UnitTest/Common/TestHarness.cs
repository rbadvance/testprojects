﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using NUnit.Framework;

namespace NURV.Data.UnitTest.Common
{
    [TestFixture]
    public abstract class TestHarness
    {
        private protected Mock.MockContext _context;

        [OneTimeSetUp]
        public virtual void OneTimeSetUp()
        {
            _context = new Mock.MockContext();

            // set up testing environment
            NURV.Data.Context.NURVDbContext.MockInit(_context.CreateMockContext);
        }

        [OneTimeTearDown]
        public virtual void OneTimeTearDown()
        {
            _context = null;
        }
    }
}
