﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Reflection;
using System.ComponentModel.DataAnnotations.Schema;
using Castle.Components.DictionaryAdapter;
using System.Net;
using System.Threading;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;
using Moq;

namespace NURV.Data.UnitTest.Mock
{
    internal class Setup
    {
        /// <summary>
        /// Static method to mock a IEnumerable<> to a DbSet<>
        /// </summary>
        public static Mock<DbSet<T>> CreateDbSetMock<T>(List<T> elements) where T : Model.Common.EntityObjectBase
        {
            var elementsAsQueryable = elements.AsQueryable();
            var dbSetMock = new Mock<DbSet<T>>();

            // following are required to support mock async call operations
            dbSetMock.As<IAsyncEnumerable<T>>()
                .Setup(m => m.GetAsyncEnumerator(CancellationToken.None))
                .Returns(new TestAsyncEnumerator<T>(elementsAsQueryable.GetEnumerator()));
            dbSetMock.As<IQueryable<T>>()
                .Setup(m => m.Provider)
                .Returns(new TestAsyncQueryProvider<T>(elementsAsQueryable.Provider));

            dbSetMock.As<IQueryable<T>>().Setup(m => m.Expression).Returns(elementsAsQueryable.Expression);
            dbSetMock.As<IQueryable<T>>().Setup(m => m.ElementType).Returns(elementsAsQueryable.ElementType);
            dbSetMock.As<IQueryable<T>>().Setup(m => m.GetEnumerator()).Returns(() => elementsAsQueryable.GetEnumerator());
            dbSetMock.Setup(d => d.Add(It.IsAny<T>())).Callback<T>(s => elements.Add(s));
            dbSetMock.Setup(d => d.Remove(It.IsAny<T>())).Callback<T>(s => elements.Remove(s));
            dbSetMock.Setup(d => d.RemoveRange(It.IsAny<IEnumerable<T>>())).Callback<IEnumerable<T>>(s => elements.RemoveAll(x => s.Contains(x)));
            //dbSetMock.Setup(d => d.Attach(It.IsAny<T>())).Callback<T>(s => elements.Add(s)).Returns<T>(s => s);

            //// The following line bypasses the Include call.
            //dbSetMock.Setup(d => d.Include(It.IsAny<string>())).Returns(dbSetMock.Object);

            // Generically mock the Find() method 
            dbSetMock.Setup(x => x.Find(It.IsAny<object[]>())).Returns((object[] id) =>
            {
                var param = Expression.Parameter(typeof(T), "t");
                var col = Expression.Property(param, GetPrimaryKeyInfo<T>().Name);
                var body = Expression.Equal(col, Expression.Constant(id[0]));
                var lambda = Expression.Lambda<Func<T, bool>>(body, param);
                return elementsAsQueryable.FirstOrDefault(lambda);
            });

            return dbSetMock;
        }

        /// <summary>
        /// Method to identify the first Key column (for generic handling of the Find() method)
        /// Note: limitations currently mean only the first stated key is used
        /// </summary>
        private static PropertyInfo GetPrimaryKeyInfo<T>()
        {
            PropertyInfo[] properties = typeof(T).GetProperties();
            foreach (PropertyInfo pI in properties)
            {
                System.Object[] attributes = pI.GetCustomAttributes(true);
                foreach (object attribute in attributes)
                {
                    if (attribute is System.ComponentModel.DataAnnotations.KeyAttribute)
                        return pI;
                }
            }
            return null;
        }
    }
}
