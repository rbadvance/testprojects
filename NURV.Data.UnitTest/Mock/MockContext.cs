﻿using System;
using System.Collections.Generic;
using Microsoft.EntityFrameworkCore;
using Moq;

namespace NURV.Data.UnitTest.Mock
{
    internal class MockContext
    {
        private readonly DbSet<Model.Branch> _mockBranches;
        private readonly DbSet<Model.BuildingSumInsuredExcess> _mockBuildingSumInsuredExcesses;
        private readonly DbSet<Model.CCTV> _mockCCTVs;
        private readonly DbSet<Model.ClassOfRisk> _mockClassOfRisks;
        private readonly DbSet<Model.ConstructionType> _mockConstructionTypes;
        private readonly DbSet<Model.ContentsCoverExcess> _mockContentCoverExcesses;
        private readonly DbSet<Model.ContentsCover> _mockContentCovers;
        private readonly DbSet<Model.DwellingType> _mockDwellingTypes;
        private readonly DbSet<Model.ElectronicAccess> _mockElectronicAccesses;
        private readonly DbSet<Model.Furnished> _mockFurnished;
        private readonly DbSet<Model.HeardAboutTSIB> _mockHeardAboutTSIBs;
        private readonly DbSet<Model.Holiday> _mockHolidays;
        private readonly DbSet<Model.InformedByWho> _mockInformedByWhos;
        private readonly DbSet<Model.InformedHow> _mockInformedHows;
        private readonly DbSet<Model.LandSize> _mockLandSizes;
        private readonly DbSet<Model.OtherClaimsExcess> _mockOtherClaimsExcesses;
        private readonly DbSet<Model.PremiumPaidBy> _mockPremiumPaidBys;
        //private readonly DbSet<Model.PremiumType> _mockPremiumTypes;
        private readonly DbSet<Model.PromoCodeState> _mockPromoCodeStates;
        private readonly DbSet<Model.PromoCode> _mockPromoCodes;
        private readonly DbSet<Model.SelfManaged> _mockSelfManaged;
        private readonly DbSet<Model.Tenanted> _mockTenanted;

        public MockContext()
        {
            _mockBranches = CreateMockBranches();
            _mockBuildingSumInsuredExcesses = CreateMockBuildingSumInsuredExcesses();
            _mockCCTVs = CreateMockCCTVs();
            _mockClassOfRisks = CreateMockClassOfRisks();
            _mockConstructionTypes = CreateMockConstructionTypes();
            _mockContentCoverExcesses = CreateMockContentCoverExcesses();
            _mockContentCovers = CreateMockContentCovers();
            _mockDwellingTypes = CreateMockDwellingTypes();
            _mockElectronicAccesses = CreateMockElectronicAccesses();
            _mockFurnished = CreateMockFurnished();
            _mockHeardAboutTSIBs = CreateMockHeardAboutTSIBs();
            _mockHolidays = CreateMockHolidays();
            _mockInformedByWhos = CreateMockInformedByWhos();
            _mockInformedHows = CreateMockinformedHows();
            _mockLandSizes = CreateMockLandSizes();
            _mockOtherClaimsExcesses = CreateMockOtherClaimsExcesses();
            _mockPremiumPaidBys = CreateMockPremiumPaidBys();
            //_mockPremiumTypes = CreateMockPremiumTypes();
            _mockPromoCodeStates = CreateMockPromoCodeStates();
            _mockPromoCodes = CreateMockPromoCodes();
            _mockSelfManaged = CreateMockSelfManaged();
            _mockTenanted = CreateMockTenanted();
        }

        public Context.INURVDbContext CreateMockContext(bool trackingOn)
        {
            var mockContext = new Mock<Context.INURVDbContext>();

            // Add mock DbSets
            mockContext.Setup(c => c.Branches).Returns(_mockBranches);
            mockContext.Setup(c => c.BuildingSumInsuredExcesses).Returns(_mockBuildingSumInsuredExcesses);
            mockContext.Setup(c => c.CCTVs).Returns(_mockCCTVs);
            mockContext.Setup(c => c.ClassOfRisks).Returns(_mockClassOfRisks);
            mockContext.Setup(c => c.ConstructionTypes).Returns(_mockConstructionTypes);
            mockContext.Setup(c => c.ContentCoverExcesses).Returns(_mockContentCoverExcesses);
            mockContext.Setup(c => c.ContentCovers).Returns(_mockContentCovers);
            mockContext.Setup(c => c.DwellingTypes).Returns(_mockDwellingTypes);
            mockContext.Setup(c => c.ElectronicAccesses).Returns(_mockElectronicAccesses);
            mockContext.Setup(c => c.Furnished).Returns(_mockFurnished);
            mockContext.Setup(c => c.HeardAboutTSIBs).Returns(_mockHeardAboutTSIBs);
            mockContext.Setup(c => c.Holidays).Returns(_mockHolidays);
            mockContext.Setup(c => c.InformedByWhos).Returns(_mockInformedByWhos);
            mockContext.Setup(c => c.InformedHows).Returns(_mockInformedHows);
            mockContext.Setup(c => c.LandSizes).Returns(_mockLandSizes);
            mockContext.Setup(c => c.OtherClaimsExcesses).Returns(_mockOtherClaimsExcesses);
            mockContext.Setup(c => c.PremiumPaidBys).Returns(_mockPremiumPaidBys);
            //mockContext.Setup(c => c.PremiumTypes).Returns(_mockPremiumTypes);
            mockContext.Setup(c => c.PromoCodes).Returns(_mockPromoCodes);
            mockContext.Setup(c => c.SelfManaged).Returns(_mockSelfManaged);
            mockContext.Setup(c => c.Tenanted).Returns(_mockTenanted);

            return mockContext.Object;

        }

        private static DbSet<Model.Branch> CreateMockBranches()
        {
            var data = new List<Model.Branch>
            {

            };

            return Setup.CreateDbSetMock(data).Object;
        }

        private static DbSet<Model.BuildingSumInsuredExcess> CreateMockBuildingSumInsuredExcesses()
        {
            var data = new List<Model.BuildingSumInsuredExcess>
            {

            };

            return Setup.CreateDbSetMock(data).Object;
        }

        private static DbSet<Model.CCTV> CreateMockCCTVs()
        {
            var data = new List<Model.CCTV>
            {
                new Model.CCTV() { ID=1, CCTVName="YES", Rate=1M, ClassOfRiskID=11, PremiumTypeID=3, StartEffectiveDate=new DateTime(1900,01,01), EndEffectiveDate=new DateTime(2011,11,20,23,59,59), IsUnknown=false, State="ACT" },
                new Model.CCTV() { ID=2, CCTVName="YES", Rate=0.85M, ClassOfRiskID=11, PremiumTypeID=3, StartEffectiveDate=new DateTime(2011,11,21), EndEffectiveDate=new DateTime(2999,12,31,23,59,59), IsUnknown=false, State="ACT" },
                new Model.CCTV() { ID=3, CCTVName="NO", Rate=1M, ClassOfRiskID=11, PremiumTypeID=3, StartEffectiveDate=new DateTime(1900,01,01), EndEffectiveDate=new DateTime(2011,11,20,23,59,59), IsUnknown=false, State="ACT" },
                new Model.CCTV() { ID=4, CCTVName="NO", Rate=1M, ClassOfRiskID=11, PremiumTypeID=3, StartEffectiveDate=new DateTime(2011,11,21), EndEffectiveDate=new DateTime(2999,12,31,23,59,59), IsUnknown=false, State="ACT" },
                new Model.CCTV() { ID=5, CCTVName="Unknown", Rate=1M, ClassOfRiskID=11, PremiumTypeID=3, StartEffectiveDate=new DateTime(1900,01,01), EndEffectiveDate=new DateTime(2011,11,20,23,59,59), IsUnknown=true, State="ACT" },
                new Model.CCTV() { ID=6, CCTVName="Unknown", Rate=1M, ClassOfRiskID=11, PremiumTypeID=3, StartEffectiveDate=new DateTime(2011,11,21), EndEffectiveDate=new DateTime(2999,12,31,23,59,59), IsUnknown=true, State="ACT" },
                new Model.CCTV() { ID=7, CCTVName="YES", Rate=1M, ClassOfRiskID=11, PremiumTypeID=4, StartEffectiveDate=new DateTime(1900,01,01), EndEffectiveDate=new DateTime(2012,01,31,23,59,59), IsUnknown=false, State="ACT" },
                new Model.CCTV() { ID=8, CCTVName="YES", Rate=0.85M, ClassOfRiskID=11, PremiumTypeID=4, StartEffectiveDate=new DateTime(2012,02,01), EndEffectiveDate=new DateTime(2999,12,31,23,59,59), IsUnknown=false, State="ACT" },
                new Model.CCTV() { ID=9, CCTVName="NO", Rate=1M, ClassOfRiskID=11, PremiumTypeID=4, StartEffectiveDate=new DateTime(1900,01,01), EndEffectiveDate=new DateTime(2012,01,31,23,59,59), IsUnknown=false, State="ACT" },
                new Model.CCTV() { ID=10, CCTVName="NO", Rate=1M, ClassOfRiskID=11, PremiumTypeID=4, StartEffectiveDate=new DateTime(2012,02,01), EndEffectiveDate=new DateTime(2999,12,31,23,59,59), IsUnknown=false, State="ACT" },
                new Model.CCTV() { ID=11, CCTVName="Unknown", Rate=1M, ClassOfRiskID=11, PremiumTypeID=4, StartEffectiveDate=new DateTime(1900,01,01), EndEffectiveDate=new DateTime(2012,01,31,23,59,59), IsUnknown=true, State="ACT" },
                new Model.CCTV() { ID=12, CCTVName="Unknown", Rate=1M, ClassOfRiskID=11, PremiumTypeID=4, StartEffectiveDate=new DateTime(2012,02,01), EndEffectiveDate=new DateTime(2999,12,31,23,59,59), IsUnknown=true, State="ACT" },
                new Model.CCTV() { ID=13, CCTVName="YES", Rate=1M, ClassOfRiskID=11, PremiumTypeID=3, StartEffectiveDate=new DateTime(1900,01,01), EndEffectiveDate=new DateTime(2011,11,20,23,59,59), IsUnknown=false, State="NSW" },
                new Model.CCTV() { ID=14, CCTVName="YES", Rate=0.85M, ClassOfRiskID=11, PremiumTypeID=3, StartEffectiveDate=new DateTime(2011,11,21), EndEffectiveDate=new DateTime(2999,12,31,23,59,59), IsUnknown=false, State="NSW" },
                new Model.CCTV() { ID=15, CCTVName="NO", Rate=1M, ClassOfRiskID=11, PremiumTypeID=3, StartEffectiveDate=new DateTime(1900,01,01), EndEffectiveDate=new DateTime(2011,11,20,23,59,59), IsUnknown=false, State="NSW" },
                new Model.CCTV() { ID=16, CCTVName="NO", Rate=1M, ClassOfRiskID=11, PremiumTypeID=3, StartEffectiveDate=new DateTime(2011,11,21), EndEffectiveDate=new DateTime(2999,12,31,23,59,59), IsUnknown=false, State="NSW" },
                new Model.CCTV() { ID=17, CCTVName="Unknown", Rate=1M, ClassOfRiskID=11, PremiumTypeID=3, StartEffectiveDate=new DateTime(1900,01,01), EndEffectiveDate=new DateTime(2011,11,20,23,59,59), IsUnknown=true, State="NSW" },
                new Model.CCTV() { ID=18, CCTVName="Unknown", Rate=1M, ClassOfRiskID=11, PremiumTypeID=3, StartEffectiveDate=new DateTime(2011,11,21), EndEffectiveDate=new DateTime(2999,12,31,23,59,59), IsUnknown=true, State="NSW" },
                new Model.CCTV() { ID=19, CCTVName="YES", Rate=1M, ClassOfRiskID=11, PremiumTypeID=4, StartEffectiveDate=new DateTime(1900,01,01), EndEffectiveDate=new DateTime(2012,01,31,23,59,59), IsUnknown=false, State="NSW" },
                new Model.CCTV() { ID=20, CCTVName="YES", Rate=0.85M, ClassOfRiskID=11, PremiumTypeID=4, StartEffectiveDate=new DateTime(2012,02,01), EndEffectiveDate=new DateTime(2999,12,31,23,59,59), IsUnknown=false, State="NSW" },
                new Model.CCTV() { ID=21, CCTVName="NO", Rate=1M, ClassOfRiskID=11, PremiumTypeID=4, StartEffectiveDate=new DateTime(1900,01,01), EndEffectiveDate=new DateTime(2012,01,31,23,59,59), IsUnknown=false, State="NSW" },
                new Model.CCTV() { ID=22, CCTVName="NO", Rate=1M, ClassOfRiskID=11, PremiumTypeID=4, StartEffectiveDate=new DateTime(2012,02,01), EndEffectiveDate=new DateTime(2999,12,31,23,59,59), IsUnknown=false, State="NSW" },
                new Model.CCTV() { ID=23, CCTVName="Unknown", Rate=1M, ClassOfRiskID=11, PremiumTypeID=4, StartEffectiveDate=new DateTime(1900,01,01), EndEffectiveDate=new DateTime(2012,01,31,23,59,59), IsUnknown=true, State="NSW" },
                new Model.CCTV() { ID=24, CCTVName="Unknown", Rate=1M, ClassOfRiskID=11, PremiumTypeID=4, StartEffectiveDate=new DateTime(2012,02,01), EndEffectiveDate=new DateTime(2999,12,31,23,59,59), IsUnknown=true, State="NSW" }
            };

            return Setup.CreateDbSetMock(data).Object;
        }

        private static DbSet<Model.ClassOfRisk> CreateMockClassOfRisks()
        {
            var data = new List<Model.ClassOfRisk>
            {

            };

            return Setup.CreateDbSetMock(data).Object;
        }

        private static DbSet<Model.ConstructionType> CreateMockConstructionTypes()
        {
            var data = new List<Model.ConstructionType>
            {
                new  Model.ConstructionType() { ConstructionTypeID=1, ConstructionTypeName="BRICK", Rate=1M, ClassOfRiskID=3, StartEffectiveDate=new DateTime(1900,01,01), EndEffectiveDate=new DateTime(2012,01,31,23,59,59), IsUnknown=false },
                new  Model.ConstructionType() { ConstructionTypeID=2, ConstructionTypeName="OTHER", Rate=1M, ClassOfRiskID=3, StartEffectiveDate=new DateTime(1900,01,01), EndEffectiveDate=new DateTime(2011,11,20,23,59,59), IsUnknown=false },
                new  Model.ConstructionType() { ConstructionTypeID=3, ConstructionTypeName="Brick Veneer", Rate=1M, ClassOfRiskID=3, StartEffectiveDate=new DateTime(2011,11,21), EndEffectiveDate=new DateTime(2999,12,31,23,59,59), IsUnknown=false },
                new  Model.ConstructionType() { ConstructionTypeID=4, ConstructionTypeName="Double Brick", Rate=1M, ClassOfRiskID=3, StartEffectiveDate=new DateTime(2011,11,21), EndEffectiveDate=new DateTime(2999,12,31,23,59,59), IsUnknown=false },
                new  Model.ConstructionType() { ConstructionTypeID=11, ConstructionTypeName="Unknown", Rate=1M, ClassOfRiskID=3, StartEffectiveDate=new DateTime(2011,11,21), EndEffectiveDate=new DateTime(2999,12,31,23,59,59), IsUnknown=true },
                new  Model.ConstructionType() { ConstructionTypeID=12, ConstructionTypeName="Brick Veneer", Rate=1M, ClassOfRiskID=11, StartEffectiveDate=new DateTime(1900,01,01), EndEffectiveDate=new DateTime(2999,12,31,23,59,59), IsUnknown=false },
                new  Model.ConstructionType() { ConstructionTypeID=13, ConstructionTypeName="Double Brick", Rate=1M, ClassOfRiskID=11, StartEffectiveDate=new DateTime(1900,01,01), EndEffectiveDate=new DateTime(2999,12,31,23,59,59), IsUnknown=false },
                new  Model.ConstructionType() { ConstructionTypeID=14, ConstructionTypeName="Concrete/Hebel", Rate=1M, ClassOfRiskID=11, StartEffectiveDate=new DateTime(1900,01,01), EndEffectiveDate=new DateTime(2999,12,31,23,59,59), IsUnknown=false },
                new  Model.ConstructionType() { ConstructionTypeID=20, ConstructionTypeName="Unknown", Rate=1M, ClassOfRiskID=11, StartEffectiveDate=new DateTime(1900,01,01), EndEffectiveDate=new DateTime(2999,12,31,23,59,59), IsUnknown=true }
            };

            return Setup.CreateDbSetMock(data).Object;
        }

        private static DbSet<Model.ContentsCoverExcess> CreateMockContentCoverExcesses()
        {
            var data = new List<Model.ContentsCoverExcess>
            {

            };

            return Setup.CreateDbSetMock(data).Object;
        }

        private static DbSet<Model.Furnished> CreateMockFurnished()
        {
            var data = new List<Model.Furnished>
            {
                new Model.Furnished() { ID=1, FurnishedName="YES", Rate=1M, ClassOfRiskID=11, PremiumTypeID=3, StartEffectiveDate=new DateTime(1900,01,01), EndEffectiveDate=new DateTime(2011,11,20,23,59,59), IsUnknown=false, State="ACT" },
                new Model.Furnished() { ID=2, FurnishedName="YES", Rate=1.15M, ClassOfRiskID=11, PremiumTypeID=3, StartEffectiveDate=new DateTime(2011,11,21), EndEffectiveDate=new DateTime(2999,12,31,23,59,59), IsUnknown=false, State="ACT" },
                new Model.Furnished() { ID=3, FurnishedName="NO", Rate=1M, ClassOfRiskID=11, PremiumTypeID=3, StartEffectiveDate=new DateTime(1900,01,01), EndEffectiveDate=new DateTime(2011,11,20,23,59,59), IsUnknown=false, State="ACT" },
                new Model.Furnished() { ID=4, FurnishedName="NO", Rate=1M, ClassOfRiskID=11, PremiumTypeID=3, StartEffectiveDate=new DateTime(2011,11,21), EndEffectiveDate=new DateTime(2999,12,31,23,59,59), IsUnknown=false, State="ACT" },
                new Model.Furnished() { ID=5, FurnishedName="Unknown", Rate=1M, ClassOfRiskID=11, PremiumTypeID=3, StartEffectiveDate=new DateTime(1900,01,01), EndEffectiveDate=new DateTime(2011,11,20,23,59,59), IsUnknown=true, State="ACT" },
                new Model.Furnished() { ID=6, FurnishedName="Unknown", Rate=1M, ClassOfRiskID=11, PremiumTypeID=3, StartEffectiveDate=new DateTime(2011,11,21), EndEffectiveDate=new DateTime(2999,12,31,23,59,59), IsUnknown=true, State="ACT" },
                new Model.Furnished() { ID=7, FurnishedName="YES", Rate=1M, ClassOfRiskID=11, PremiumTypeID=4, StartEffectiveDate=new DateTime(1900,01,01), EndEffectiveDate=new DateTime(2012,01,31,23,59,59), IsUnknown=false, State="ACT" },
                new Model.Furnished() { ID=8, FurnishedName="YES", Rate=1.15M, ClassOfRiskID=11, PremiumTypeID=4, StartEffectiveDate=new DateTime(2012,02,01), EndEffectiveDate=new DateTime(2999,12,31,23,59,59), IsUnknown=false, State="ACT" },
                new Model.Furnished() { ID=9, FurnishedName="NO", Rate=1M, ClassOfRiskID=11, PremiumTypeID=4, StartEffectiveDate=new DateTime(1900,01,01), EndEffectiveDate=new DateTime(2012,01,31,23,59,59), IsUnknown=false, State="ACT" },
                new Model.Furnished() { ID=10, FurnishedName="NO", Rate=1M, ClassOfRiskID=11, PremiumTypeID=4, StartEffectiveDate=new DateTime(2012,02,01), EndEffectiveDate=new DateTime(2999,12,31,23,59,59), IsUnknown=false, State="ACT" },
                new Model.Furnished() { ID=11, FurnishedName="Unknown", Rate=1M, ClassOfRiskID=11, PremiumTypeID=4, StartEffectiveDate=new DateTime(1900,01,01), EndEffectiveDate=new DateTime(2012,01,31,23,59,59), IsUnknown=true, State="ACT" },
                new Model.Furnished() { ID=12, FurnishedName="Unknown", Rate=1M, ClassOfRiskID=11, PremiumTypeID=4, StartEffectiveDate=new DateTime(2012,02,01), EndEffectiveDate=new DateTime(2999,12,31,23,59,59), IsUnknown=true, State="ACT" },
                new Model.Furnished() { ID=13, FurnishedName="YES", Rate=1M, ClassOfRiskID=11, PremiumTypeID=3, StartEffectiveDate=new DateTime(1900,01,01), EndEffectiveDate=new DateTime(2011,11,20,23,59,59), IsUnknown=false, State="NSW" },
                new Model.Furnished() { ID=14, FurnishedName="YES", Rate=1.15M, ClassOfRiskID=11, PremiumTypeID=3, StartEffectiveDate=new DateTime(2011,11,21), EndEffectiveDate=new DateTime(2999,12,31,23,59,59), IsUnknown=false, State="NSW" },
                new Model.Furnished() { ID=15, FurnishedName="NO", Rate=1M, ClassOfRiskID=11, PremiumTypeID=3, StartEffectiveDate=new DateTime(1900,01,01), EndEffectiveDate=new DateTime(2011,11,20,23,59,59), IsUnknown=false, State="NSW" },
                new Model.Furnished() { ID=16, FurnishedName="NO", Rate=1M, ClassOfRiskID=11, PremiumTypeID=3, StartEffectiveDate=new DateTime(2011,11,21), EndEffectiveDate=new DateTime(2999,12,31,23,59,59), IsUnknown=false, State="NSW" },
                new Model.Furnished() { ID=17, FurnishedName="Unknown", Rate=1M, ClassOfRiskID=11, PremiumTypeID=3, StartEffectiveDate=new DateTime(1900,01,01), EndEffectiveDate=new DateTime(2011,11,20,23,59,59), IsUnknown=true, State="NSW" },
                new Model.Furnished() { ID=18, FurnishedName="Unknown", Rate=1M, ClassOfRiskID=11, PremiumTypeID=3, StartEffectiveDate=new DateTime(2011,11,21), EndEffectiveDate=new DateTime(2999,12,31,23,59,59), IsUnknown=true, State="NSW" },
                new Model.Furnished() { ID=19, FurnishedName="YES", Rate=1M, ClassOfRiskID=11, PremiumTypeID=4, StartEffectiveDate=new DateTime(1900,01,01), EndEffectiveDate=new DateTime(2012,01,31,23,59,59), IsUnknown=false, State="NSW" },
                new Model.Furnished() { ID=20, FurnishedName="YES", Rate=1.15M, ClassOfRiskID=11, PremiumTypeID=4, StartEffectiveDate=new DateTime(2012,02,01), EndEffectiveDate=new DateTime(2999,12,31,23,59,59), IsUnknown=false, State="NSW" },
                new Model.Furnished() { ID=21, FurnishedName="NO", Rate=1M, ClassOfRiskID=11, PremiumTypeID=4, StartEffectiveDate=new DateTime(1900,01,01), EndEffectiveDate=new DateTime(2012,01,31,23,59,59), IsUnknown=false, State="NSW" },
                new Model.Furnished() { ID=22, FurnishedName="NO", Rate=1M, ClassOfRiskID=11, PremiumTypeID=4, StartEffectiveDate=new DateTime(2012,02,01), EndEffectiveDate=new DateTime(2999,12,31,23,59,59), IsUnknown=false, State="NSW" },
                new Model.Furnished() { ID=23, FurnishedName="Unknown", Rate=1M, ClassOfRiskID=11, PremiumTypeID=4, StartEffectiveDate=new DateTime(1900,01,01), EndEffectiveDate=new DateTime(2012,01,31,23,59,59), IsUnknown=true, State="NSW" },
                new Model.Furnished() { ID=24, FurnishedName="Unknown", Rate=1M, ClassOfRiskID=11, PremiumTypeID=4, StartEffectiveDate=new DateTime(2012,02,01), EndEffectiveDate=new DateTime(2999,12,31,23,59,59), IsUnknown=true, State="NSW" }
            };

            return Setup.CreateDbSetMock(data).Object;
        }

        private static DbSet<Model.ContentsCover> CreateMockContentCovers()
        {
            var data = new List<Model.ContentsCover>
            {

            };

            return Setup.CreateDbSetMock(data).Object;
        }

        private static DbSet<Model.DwellingType> CreateMockDwellingTypes()
        {
            var data = new List<Model.DwellingType>
            {

            };

            return Setup.CreateDbSetMock(data).Object;
        }

        private static DbSet<Model.ElectronicAccess> CreateMockElectronicAccesses()
        {
            var data = new List<Model.ElectronicAccess>
            {
                new Model.ElectronicAccess() { ID=1, ElectronicAccessName="YES", Rate=1M, ClassOfRiskID=11, PremiumTypeID=3, StartEffectiveDate=new DateTime(1900,01,01), EndEffectiveDate=new DateTime(2011,11,20,23,59,59), IsUnknown=false, State="ACT" },
                new Model.ElectronicAccess() { ID=2, ElectronicAccessName="YES", Rate=1M, ClassOfRiskID=11, PremiumTypeID=3, StartEffectiveDate=new DateTime(2011,11,21), EndEffectiveDate=new DateTime(2999,12,31,23,59,59), IsUnknown=false, State="ACT" },
                new Model.ElectronicAccess() { ID=3, ElectronicAccessName="NO", Rate=1M, ClassOfRiskID=11, PremiumTypeID=3, StartEffectiveDate=new DateTime(1900,01,01), EndEffectiveDate=new DateTime(2011,11,20,23,59,59), IsUnknown=false, State="ACT" },
                new Model.ElectronicAccess() { ID=4, ElectronicAccessName="NO", Rate=1M, ClassOfRiskID=11, PremiumTypeID=3, StartEffectiveDate=new DateTime(2011,11,21), EndEffectiveDate=new DateTime(2999,12,31,23,59,59), IsUnknown=false, State="ACT" },
                new Model.ElectronicAccess() { ID=5, ElectronicAccessName="Unknown", Rate=1M, ClassOfRiskID=11, PremiumTypeID=3, StartEffectiveDate=new DateTime(1900,01,01), EndEffectiveDate=new DateTime(2011,11,20,23,59,59), IsUnknown=true, State="ACT" },
                new Model.ElectronicAccess() { ID=6, ElectronicAccessName="Unknown", Rate=1M, ClassOfRiskID=11, PremiumTypeID=3, StartEffectiveDate=new DateTime(2011,11,21), EndEffectiveDate=new DateTime(2999,12,31,23,59,59), IsUnknown=true, State="ACT" },
                new Model.ElectronicAccess() { ID=7, ElectronicAccessName="YES", Rate=1M, ClassOfRiskID=11, PremiumTypeID=4, StartEffectiveDate=new DateTime(1900,01,01), EndEffectiveDate=new DateTime(2012,01,31,23,59,59), IsUnknown=false, State="ACT" },
                new Model.ElectronicAccess() { ID=8, ElectronicAccessName="YES", Rate=1M, ClassOfRiskID=11, PremiumTypeID=4, StartEffectiveDate=new DateTime(2012,02,01), EndEffectiveDate=new DateTime(2999,12,31,23,59,59), IsUnknown=false, State="ACT" },
                new Model.ElectronicAccess() { ID=9, ElectronicAccessName="NO", Rate=1M, ClassOfRiskID=11, PremiumTypeID=4, StartEffectiveDate=new DateTime(1900,01,01), EndEffectiveDate=new DateTime(2012,01,31,23,59,59), IsUnknown=false, State="ACT" },
                new Model.ElectronicAccess() { ID=10, ElectronicAccessName="NO", Rate=1M, ClassOfRiskID=11, PremiumTypeID=4, StartEffectiveDate=new DateTime(2012,02,01), EndEffectiveDate=new DateTime(2999,12,31,23,59,59), IsUnknown=false, State="ACT" },
                new Model.ElectronicAccess() { ID=11, ElectronicAccessName="Unknown", Rate=1M, ClassOfRiskID=11, PremiumTypeID=4, StartEffectiveDate=new DateTime(1900,01,01), EndEffectiveDate=new DateTime(2012,01,31,23,59,59), IsUnknown=true, State="ACT" },
                new Model.ElectronicAccess() { ID=12, ElectronicAccessName="Unknown", Rate=1M, ClassOfRiskID=11, PremiumTypeID=4, StartEffectiveDate=new DateTime(2012,02,01), EndEffectiveDate=new DateTime(2999,12,31,23,59,59), IsUnknown=true, State="ACT" },
                new Model.ElectronicAccess() { ID=13, ElectronicAccessName="YES", Rate=1M, ClassOfRiskID=11, PremiumTypeID=3, StartEffectiveDate=new DateTime(1900,01,01), EndEffectiveDate=new DateTime(2011,11,20,23,59,59), IsUnknown=false, State="NSW" },
                new Model.ElectronicAccess() { ID=14, ElectronicAccessName="YES", Rate=1M, ClassOfRiskID=11, PremiumTypeID=3, StartEffectiveDate=new DateTime(2011,11,21), EndEffectiveDate=new DateTime(2999,12,31,23,59,59), IsUnknown=false, State="NSW" },
                new Model.ElectronicAccess() { ID=15, ElectronicAccessName="NO", Rate=1M, ClassOfRiskID=11, PremiumTypeID=3, StartEffectiveDate=new DateTime(1900,01,01), EndEffectiveDate=new DateTime(2011,11,20,23,59,59), IsUnknown=false, State="NSW" },
                new Model.ElectronicAccess() { ID=16, ElectronicAccessName="NO", Rate=1M, ClassOfRiskID=11, PremiumTypeID=3, StartEffectiveDate=new DateTime(2011,11,21), EndEffectiveDate=new DateTime(2999,12,31,23,59,59), IsUnknown=false, State="NSW" },
                new Model.ElectronicAccess() { ID=17, ElectronicAccessName="Unknown", Rate=1M, ClassOfRiskID=11, PremiumTypeID=3, StartEffectiveDate=new DateTime(1900,01,01), EndEffectiveDate=new DateTime(2011,11,20,23,59,59), IsUnknown=true, State="NSW" },
                new Model.ElectronicAccess() { ID=18, ElectronicAccessName="Unknown", Rate=1M, ClassOfRiskID=11, PremiumTypeID=3, StartEffectiveDate=new DateTime(2011,11,21), EndEffectiveDate=new DateTime(2999,12,31,23,59,59), IsUnknown=true, State="NSW" },
                new Model.ElectronicAccess() { ID=19, ElectronicAccessName="YES", Rate=1M, ClassOfRiskID=11, PremiumTypeID=4, StartEffectiveDate=new DateTime(1900,01,01), EndEffectiveDate=new DateTime(2012,01,31,23,59,59), IsUnknown=false, State="NSW" },
                new Model.ElectronicAccess() { ID=20, ElectronicAccessName="YES", Rate=1M, ClassOfRiskID=11, PremiumTypeID=4, StartEffectiveDate=new DateTime(2012,02,01), EndEffectiveDate=new DateTime(2999,12,31,23,59,59), IsUnknown=false, State="NSW" },
                new Model.ElectronicAccess() { ID=21, ElectronicAccessName="NO", Rate=1M, ClassOfRiskID=11, PremiumTypeID=4, StartEffectiveDate=new DateTime(1900,01,01), EndEffectiveDate=new DateTime(2012,01,31,23,59,59), IsUnknown=false, State="NSW" },
                new Model.ElectronicAccess() { ID=22, ElectronicAccessName="NO", Rate=1M, ClassOfRiskID=11, PremiumTypeID=4, StartEffectiveDate=new DateTime(2012,02,01), EndEffectiveDate=new DateTime(2999,12,31,23,59,59), IsUnknown=false, State="NSW" },
                new Model.ElectronicAccess() { ID=23, ElectronicAccessName="Unknown", Rate=1M, ClassOfRiskID=11, PremiumTypeID=4, StartEffectiveDate=new DateTime(1900,01,01), EndEffectiveDate=new DateTime(2012,01,31,23,59,59), IsUnknown=true, State="NSW" },
                new Model.ElectronicAccess() { ID=24, ElectronicAccessName="Unknown", Rate=1M, ClassOfRiskID=11, PremiumTypeID=4, StartEffectiveDate=new DateTime(2012,02,01), EndEffectiveDate=new DateTime(2999,12,31,23,59,59), IsUnknown=true, State="NSW" }
            };

            return Setup.CreateDbSetMock(data).Object;
        }

        private static DbSet<Model.HeardAboutTSIB> CreateMockHeardAboutTSIBs()
        {
            var data = new List<Model.HeardAboutTSIB>
            {

            };

            return Setup.CreateDbSetMock(data).Object;
        }

        private static DbSet<Model.Holiday> CreateMockHolidays()
        {
            var data = new List<Model.Holiday>
            {
                new Model.Holiday() { ID=1, HolidayName="YES", Rate=1M, ClassOfRiskID=11, PremiumTypeID=3, StartEffectiveDate=new DateTime(1900,01,01), EndEffectiveDate=new DateTime(2011,11,20,23,59,59), IsUnknown=false, State="ACT" },
                new Model.Holiday() { ID=2, HolidayName="YES", Rate=1.2M, ClassOfRiskID=11, PremiumTypeID=3, StartEffectiveDate=new DateTime(2011,11,21), EndEffectiveDate=new DateTime(2013,06,30,23,59,59), IsUnknown=false, State="ACT" },
                new Model.Holiday() { ID=3, HolidayName="NO", Rate=1M, ClassOfRiskID=11, PremiumTypeID=3, StartEffectiveDate=new DateTime(1900,01,01), EndEffectiveDate=new DateTime(2011,11,20,23,59,59), IsUnknown=false, State="ACT" },
                new Model.Holiday() { ID=4, HolidayName="NO", Rate=1M, ClassOfRiskID=11, PremiumTypeID=3, StartEffectiveDate=new DateTime(2011,11,21), EndEffectiveDate=new DateTime(2013,06,30,23,59,59), IsUnknown=false, State="ACT" },
                new Model.Holiday() { ID=5, HolidayName="Unknown", Rate=1M, ClassOfRiskID=11, PremiumTypeID=3, StartEffectiveDate=new DateTime(1900,01,01), EndEffectiveDate=new DateTime(2011,11,20,23,59,59), IsUnknown=true, State="ACT" },
                new Model.Holiday() { ID=6, HolidayName="Unknown", Rate=1M, ClassOfRiskID=11, PremiumTypeID=3, StartEffectiveDate=new DateTime(2011,11,21), EndEffectiveDate=new DateTime(2013,06,30,23,59,59), IsUnknown=true, State="ACT" },
                new Model.Holiday() { ID=7, HolidayName="YES", Rate=1M, ClassOfRiskID=11, PremiumTypeID=4, StartEffectiveDate=new DateTime(1900,01,01), EndEffectiveDate=new DateTime(2011,11,20,23,59,59), IsUnknown=false, State="ACT" },
                new Model.Holiday() { ID=8, HolidayName="YES", Rate=1.2M, ClassOfRiskID=11, PremiumTypeID=4, StartEffectiveDate=new DateTime(2011,11,21), EndEffectiveDate=new DateTime(2013,06,30,23,59,59), IsUnknown=false, State="ACT" },
                new Model.Holiday() { ID=9, HolidayName="NO", Rate=1M, ClassOfRiskID=11, PremiumTypeID=4, StartEffectiveDate=new DateTime(1900,01,01), EndEffectiveDate=new DateTime(2011,11,20,23,59,59), IsUnknown=false, State="ACT" },
                new Model.Holiday() { ID=10, HolidayName="NO", Rate=1M, ClassOfRiskID=11, PremiumTypeID=4, StartEffectiveDate=new DateTime(2011,11,21), EndEffectiveDate=new DateTime(2013,06,30,23,59,59), IsUnknown=false, State="ACT" },
                new Model.Holiday() { ID=11, HolidayName="Unknown", Rate=1M, ClassOfRiskID=11, PremiumTypeID=4, StartEffectiveDate=new DateTime(1900,01,01), EndEffectiveDate=new DateTime(2011,11,20,23,59,59), IsUnknown=true, State="ACT" },
                new Model.Holiday() { ID=12, HolidayName="Unknown", Rate=1M, ClassOfRiskID=11, PremiumTypeID=4, StartEffectiveDate=new DateTime(2011,11,21), EndEffectiveDate=new DateTime(2013,06,30,23,59,59), IsUnknown=true, State="ACT" },
                new Model.Holiday() { ID=13, HolidayName="YES", Rate=1M, ClassOfRiskID=11, PremiumTypeID=3, StartEffectiveDate=new DateTime(1900,01,01), EndEffectiveDate=new DateTime(2011,11,20,23,59,59), IsUnknown=false, State="NSW" },
                new Model.Holiday() { ID=14, HolidayName="YES", Rate=1.2M, ClassOfRiskID=11, PremiumTypeID=3, StartEffectiveDate=new DateTime(2011,11,21), EndEffectiveDate=new DateTime(2013,06,30,23,59,59), IsUnknown=false, State="NSW" },
                new Model.Holiday() { ID=15, HolidayName="NO", Rate=1M, ClassOfRiskID=11, PremiumTypeID=3, StartEffectiveDate=new DateTime(1900,01,01), EndEffectiveDate=new DateTime(2011,11,20,23,59,59), IsUnknown=false, State="NSW" },
                new Model.Holiday() { ID=16, HolidayName="NO", Rate=1M, ClassOfRiskID=11, PremiumTypeID=3, StartEffectiveDate=new DateTime(2011,11,21), EndEffectiveDate=new DateTime(2013,06,30,23,59,59), IsUnknown=false, State="NSW" },
                new Model.Holiday() { ID=17, HolidayName="Unknown", Rate=1M, ClassOfRiskID=11, PremiumTypeID=3, StartEffectiveDate=new DateTime(1900,01,01), EndEffectiveDate=new DateTime(2011,11,20,23,59,59), IsUnknown=true, State="NSW" },
                new Model.Holiday() { ID=18, HolidayName="Unknown", Rate=1M, ClassOfRiskID=11, PremiumTypeID=3, StartEffectiveDate=new DateTime(2011,11,21), EndEffectiveDate=new DateTime(2013,06,30,23,59,59), IsUnknown=true, State="NSW" },
                new Model.Holiday() { ID=19, HolidayName="YES", Rate=1M, ClassOfRiskID=11, PremiumTypeID=4, StartEffectiveDate=new DateTime(1900,01,01), EndEffectiveDate=new DateTime(2011,11,20,23,59,59), IsUnknown=false, State="NSW" },
                new Model.Holiday() { ID=20, HolidayName="YES", Rate=1.2M, ClassOfRiskID=11, PremiumTypeID=4, StartEffectiveDate=new DateTime(2011,11,21), EndEffectiveDate=new DateTime(2013,06,30,23,59,59), IsUnknown=false, State="NSW" },
                new Model.Holiday() { ID=21, HolidayName="NO", Rate=1M, ClassOfRiskID=11, PremiumTypeID=4, StartEffectiveDate=new DateTime(1900,01,01), EndEffectiveDate=new DateTime(2011,11,20,23,59,59), IsUnknown=false, State="NSW" },
                new Model.Holiday() { ID=22, HolidayName="NO", Rate=1M, ClassOfRiskID=11, PremiumTypeID=4, StartEffectiveDate=new DateTime(2011,11,21), EndEffectiveDate=new DateTime(2013,06,30,23,59,59), IsUnknown=false, State="NSW" },
                new Model.Holiday() { ID=23, HolidayName="Unknown", Rate=1M, ClassOfRiskID=11, PremiumTypeID=4, StartEffectiveDate=new DateTime(1900,01,01), EndEffectiveDate=new DateTime(2011,11,20,23,59,59), IsUnknown=true, State="NSW" },
                new Model.Holiday() { ID=24, HolidayName="Unknown", Rate=1M, ClassOfRiskID=11, PremiumTypeID=4, StartEffectiveDate=new DateTime(2011,11,21), EndEffectiveDate=new DateTime(2013,06,30,23,59,59), IsUnknown=true, State="NSW" },
                new Model.Holiday() { ID=97, HolidayName="YES", Rate=1M, ClassOfRiskID=3, PremiumTypeID=3, StartEffectiveDate=new DateTime(1900,01,01), EndEffectiveDate=new DateTime(2011,11,20,23,59,59), IsUnknown=false, State="ACT" },
                new Model.Holiday() { ID=98, HolidayName="YES", Rate=1.2M, ClassOfRiskID=3, PremiumTypeID=3, StartEffectiveDate=new DateTime(2011,11,21), EndEffectiveDate=new DateTime(2013,06,30,23,59,59), IsUnknown=false, State="ACT" },
                new Model.Holiday() { ID=99, HolidayName="NO", Rate=1M, ClassOfRiskID=3, PremiumTypeID=3, StartEffectiveDate=new DateTime(1900,01,01), EndEffectiveDate=new DateTime(2011,11,20,23,59,59), IsUnknown=false, State="ACT" },
                new Model.Holiday() { ID=100, HolidayName="NO", Rate=1M, ClassOfRiskID=3, PremiumTypeID=3, StartEffectiveDate=new DateTime(2011,11,21), EndEffectiveDate=new DateTime(2013,06,30,23,59,59), IsUnknown=false, State="ACT" },
                new Model.Holiday() { ID=101, HolidayName="Unknown", Rate=1M, ClassOfRiskID=3, PremiumTypeID=3, StartEffectiveDate=new DateTime(1900,01,01), EndEffectiveDate=new DateTime(2011,11,20,23,59,59), IsUnknown=true, State="ACT" },
                new Model.Holiday() { ID=102, HolidayName="Unknown", Rate=1M, ClassOfRiskID=3, PremiumTypeID=3, StartEffectiveDate=new DateTime(2011,11,21), EndEffectiveDate=new DateTime(2013,06,30,23,59,59), IsUnknown=true, State="ACT" },
                new Model.Holiday() { ID=103, HolidayName="YES", Rate=1M, ClassOfRiskID=3, PremiumTypeID=4, StartEffectiveDate=new DateTime(1900,01,01), EndEffectiveDate=new DateTime(2011,11,20,23,59,59), IsUnknown=false, State="ACT" },
                new Model.Holiday() { ID=104, HolidayName="YES", Rate=1.2M, ClassOfRiskID=3, PremiumTypeID=4, StartEffectiveDate=new DateTime(2011,11,21), EndEffectiveDate=new DateTime(2013,06,30,23,59,59), IsUnknown=false, State="ACT" },
                new Model.Holiday() { ID=105, HolidayName="NO", Rate=1M, ClassOfRiskID=3, PremiumTypeID=4, StartEffectiveDate=new DateTime(1900,01,01), EndEffectiveDate=new DateTime(2011,11,20,23,59,59), IsUnknown=false, State="ACT" },
                new Model.Holiday() { ID=106, HolidayName="NO", Rate=1M, ClassOfRiskID=3, PremiumTypeID=4, StartEffectiveDate=new DateTime(2011,11,21), EndEffectiveDate=new DateTime(2013,06,30,23,59,59), IsUnknown=false, State="ACT" },
                new Model.Holiday() { ID=107, HolidayName="Unknown", Rate=1M, ClassOfRiskID=3, PremiumTypeID=4, StartEffectiveDate=new DateTime(1900,01,01), EndEffectiveDate=new DateTime(2011,11,20,23,59,59), IsUnknown=true, State="ACT" },
                new Model.Holiday() { ID=108, HolidayName="Unknown", Rate=1M, ClassOfRiskID=3, PremiumTypeID=4, StartEffectiveDate=new DateTime(2011,11,21), EndEffectiveDate=new DateTime(2013,06,30,23,59,59), IsUnknown=true, State="ACT" },
                new Model.Holiday() { ID=109, HolidayName="YES", Rate=1M, ClassOfRiskID=3, PremiumTypeID=3, StartEffectiveDate=new DateTime(1900,01,01), EndEffectiveDate=new DateTime(2011,11,20,23,59,59), IsUnknown=false, State="NSW" },
                new Model.Holiday() { ID=110, HolidayName="YES", Rate=1.2M, ClassOfRiskID=3, PremiumTypeID=3, StartEffectiveDate=new DateTime(2011,11,21), EndEffectiveDate=new DateTime(2013,06,30,23,59,59), IsUnknown=false, State="NSW" },
                new Model.Holiday() { ID=111, HolidayName="NO", Rate=1M, ClassOfRiskID=3, PremiumTypeID=3, StartEffectiveDate=new DateTime(1900,01,01), EndEffectiveDate=new DateTime(2011,11,20,23,59,59), IsUnknown=false, State="NSW" },
                new Model.Holiday() { ID=112, HolidayName="NO", Rate=1M, ClassOfRiskID=3, PremiumTypeID=3, StartEffectiveDate=new DateTime(2011,11,21), EndEffectiveDate=new DateTime(2013,06,30,23,59,59), IsUnknown=false, State="NSW" },
                new Model.Holiday() { ID=113, HolidayName="Unknown", Rate=1M, ClassOfRiskID=3, PremiumTypeID=3, StartEffectiveDate=new DateTime(1900,01,01), EndEffectiveDate=new DateTime(2011,11,20,23,59,59), IsUnknown=true, State="NSW" },
                new Model.Holiday() { ID=114, HolidayName="Unknown", Rate=1M, ClassOfRiskID=3, PremiumTypeID=3, StartEffectiveDate=new DateTime(2011,11,21), EndEffectiveDate=new DateTime(2013,06,30,23,59,59), IsUnknown=true, State="NSW" },
                new Model.Holiday() { ID=115, HolidayName="YES", Rate=1M, ClassOfRiskID=3, PremiumTypeID=4, StartEffectiveDate=new DateTime(1900,01,01), EndEffectiveDate=new DateTime(2011,11,20,23,59,59), IsUnknown=false, State="NSW" },
                new Model.Holiday() { ID=116, HolidayName="YES", Rate=1.2M, ClassOfRiskID=3, PremiumTypeID=4, StartEffectiveDate=new DateTime(2011,11,21), EndEffectiveDate=new DateTime(2013,06,30,23,59,59), IsUnknown=false, State="NSW" },
                new Model.Holiday() { ID=117, HolidayName="NO", Rate=1M, ClassOfRiskID=3, PremiumTypeID=4, StartEffectiveDate=new DateTime(1900,01,01), EndEffectiveDate=new DateTime(2011,11,20,23,59,59), IsUnknown=false, State="NSW" },
                new Model.Holiday() { ID=118, HolidayName="NO", Rate=1M, ClassOfRiskID=3, PremiumTypeID=4, StartEffectiveDate=new DateTime(2011,11,21), EndEffectiveDate=new DateTime(2013,06,30,23,59,59), IsUnknown=false, State="NSW" },
                new Model.Holiday() { ID=119, HolidayName="Unknown", Rate=1M, ClassOfRiskID=3, PremiumTypeID=4, StartEffectiveDate=new DateTime(1900,01,01), EndEffectiveDate=new DateTime(2011,11,20,23,59,59), IsUnknown=true, State="NSW" },
                new Model.Holiday() { ID=120, HolidayName="Unknown", Rate=1M, ClassOfRiskID=3, PremiumTypeID=4, StartEffectiveDate=new DateTime(2011,11,21), EndEffectiveDate=new DateTime(2013,06,30,23,59,59), IsUnknown=true, State="NSW" }
            };

            return Setup.CreateDbSetMock(data).Object;
        }

        private static DbSet<Model.InformedByWho> CreateMockInformedByWhos()
        {
            var data = new List<Model.InformedByWho>
            {

            };

            return Setup.CreateDbSetMock(data).Object;
        }

        private static DbSet<Model.InformedHow> CreateMockinformedHows()
        {
            var data = new List<Model.InformedHow>
            {

            };

            return Setup.CreateDbSetMock(data).Object;
        }

        private static DbSet<Model.LandSize> CreateMockLandSizes()
        {
            var data = new List<Model.LandSize>
            {
                new Model.LandSize { LandSizeId=1, LandSizeName="Less than 2 acres", Rate=1, ClassOfRiskID=1, IsUnknown=false, StartEffectiveDate=new DateTime(1900,01,01), EndEffectiveDate=new DateTime(2011,11,20,23,59,59)},
                new Model.LandSize { LandSizeId=2, LandSizeName="Between 2 and 5 acres", Rate=1, ClassOfRiskID=1, IsUnknown=false, StartEffectiveDate=new DateTime(1900,01,01), EndEffectiveDate=new DateTime(2011,11,20,23,59,59)},
                new Model.LandSize { LandSizeId=3, LandSizeName="Unknown", Rate=1, ClassOfRiskID=1, IsUnknown=true, StartEffectiveDate=new DateTime(1900,01,01), EndEffectiveDate=new DateTime(2011,11,20,23,59,59)},
                new Model.LandSize { LandSizeId=4, LandSizeName="Less that 2 acres", Rate=1, ClassOfRiskID=1, IsUnknown=false, StartEffectiveDate=new DateTime(2011,11,21), EndEffectiveDate=new DateTime(2999,12,31,23,59,59)},
                new Model.LandSize { LandSizeId=5, LandSizeName="Between 2 and 5 acres", Rate=1, ClassOfRiskID=1, IsUnknown=false, StartEffectiveDate=new DateTime(2011,11,21), EndEffectiveDate=new DateTime(2999,12,31,23,59,59)},
                new Model.LandSize { LandSizeId=6, LandSizeName="Unknown", Rate=1, ClassOfRiskID=1, IsUnknown=true, StartEffectiveDate=new DateTime(2011,11,21), EndEffectiveDate=new DateTime(2999,12,31,23,59,59)},
                new Model.LandSize { LandSizeId=7, LandSizeName="Less than 2 acres", Rate=1, ClassOfRiskID=3, IsUnknown=false, StartEffectiveDate=new DateTime(1900,01,01), EndEffectiveDate=new DateTime(2011,11,20,23,59,59)},
                new Model.LandSize { LandSizeId=8, LandSizeName="Between 2 and 5 acres", Rate=1, ClassOfRiskID=3, IsUnknown=false, StartEffectiveDate=new DateTime(1900,01,01), EndEffectiveDate=new DateTime(2011,11,20,23,59,59)},
                new Model.LandSize { LandSizeId=9, LandSizeName="Unknown", Rate=1, ClassOfRiskID=3, IsUnknown=true, StartEffectiveDate=new DateTime(1900,01,01), EndEffectiveDate=new DateTime(2011,11,20,23,59,59)},
                new Model.LandSize { LandSizeId=10, LandSizeName="Less that 2 acres", Rate=1, ClassOfRiskID=3, IsUnknown=false, StartEffectiveDate=new DateTime(2011,11,21), EndEffectiveDate=new DateTime(2999,12,31,23,59,59)},
                new Model.LandSize { LandSizeId=11, LandSizeName="Between 2 and 5 acres", Rate=1, ClassOfRiskID=3, IsUnknown=false, StartEffectiveDate=new DateTime(2011,11,21), EndEffectiveDate=new DateTime(2999,12,31,23,59,59)},
                new Model.LandSize { LandSizeId=12, LandSizeName="Unknown", Rate=1, ClassOfRiskID=3, IsUnknown=true, StartEffectiveDate=new DateTime(2011,11,21), EndEffectiveDate=new DateTime(2999,12,31,23,59,59)}
            };

            return Setup.CreateDbSetMock(data).Object;
        }

        private static DbSet<Model.OtherClaimsExcess> CreateMockOtherClaimsExcesses()
        {
            var data = new List<Model.OtherClaimsExcess>
            {
                new Model.OtherClaimsExcess() { OtherClaimsExcessID=9, ClassOfRiskID=11, PremiumTypeID=3, State="ACT", ClaimExcess=100M, Default=true, Rate=1M, StartEffectiveDate=new DateTime(2011,11,21), EndEffectiveDate=new DateTime(2999,12,31) },
                new Model.OtherClaimsExcess() { OtherClaimsExcessID=10, ClassOfRiskID=11, PremiumTypeID=3, State="NSW", ClaimExcess=100M, Default=true, Rate=1M, StartEffectiveDate=new DateTime(2011,11,21), EndEffectiveDate=new DateTime(2999,12,31) },
                new Model.OtherClaimsExcess() { OtherClaimsExcessID=25, ClassOfRiskID=11, PremiumTypeID=4, State="ACT", ClaimExcess=100M, Default=true, Rate=1M, StartEffectiveDate=new DateTime(2011,11,21), EndEffectiveDate=new DateTime(2999,12,31) },
                new Model.OtherClaimsExcess() { OtherClaimsExcessID=26, ClassOfRiskID=11, PremiumTypeID=4, State="NSW", ClaimExcess=100M, Default=true, Rate=1M, StartEffectiveDate=new DateTime(2011,11,21), EndEffectiveDate=new DateTime(2999,12,31) },
                new Model.OtherClaimsExcess() { OtherClaimsExcessID=33, ClassOfRiskID=3, PremiumTypeID=3, State="ACT", ClaimExcess=250M, Default=true, Rate=1M, StartEffectiveDate=new DateTime(2011,11,21), EndEffectiveDate=new DateTime(2999,12,31) },
                new Model.OtherClaimsExcess() { OtherClaimsExcessID=34, ClassOfRiskID=3, PremiumTypeID=3, State="ACT", ClaimExcess=500M, Default=false, Rate=1M, StartEffectiveDate=new DateTime(2011,11,21), EndEffectiveDate=new DateTime(2999,12,31) },
                new Model.OtherClaimsExcess() { OtherClaimsExcessID=35, ClassOfRiskID=3, PremiumTypeID=3, State="ACT", ClaimExcess=1000M, Default=false, Rate=1M, StartEffectiveDate=new DateTime(2011,11,21), EndEffectiveDate=new DateTime(2999,12,31) },
                new Model.OtherClaimsExcess() { OtherClaimsExcessID=36, ClassOfRiskID=3, PremiumTypeID=3, State="NSW", ClaimExcess=250M, Default=true, Rate=1M, StartEffectiveDate=new DateTime(2011,11,21), EndEffectiveDate=new DateTime(2999,12,31) },
                new Model.OtherClaimsExcess() { OtherClaimsExcessID=37, ClassOfRiskID=3, PremiumTypeID=3, State="NSW", ClaimExcess=500M, Default=false, Rate=1M, StartEffectiveDate=new DateTime(2011,11,21), EndEffectiveDate=new DateTime(2999,12,31) },
                new Model.OtherClaimsExcess() { OtherClaimsExcessID=38, ClassOfRiskID=3, PremiumTypeID=3, State="NSW", ClaimExcess=1000M, Default=false, Rate=1M, StartEffectiveDate=new DateTime(2011,11,21), EndEffectiveDate=new DateTime(2999,12,31) },
                new Model.OtherClaimsExcess() { OtherClaimsExcessID=57, ClassOfRiskID=3, PremiumTypeID=4, State="ACT", ClaimExcess=250M, Default=true, Rate=1M, StartEffectiveDate=new DateTime(2011,11,21), EndEffectiveDate=new DateTime(2999,12,31) },
                new Model.OtherClaimsExcess() { OtherClaimsExcessID=58, ClassOfRiskID=3, PremiumTypeID=4, State="ACT", ClaimExcess=500M, Default=false, Rate=1M, StartEffectiveDate=new DateTime(2011,11,21), EndEffectiveDate=new DateTime(2999,12,31) },
                new Model.OtherClaimsExcess() { OtherClaimsExcessID=59, ClassOfRiskID=3, PremiumTypeID=4, State="ACT", ClaimExcess=1000M, Default=false, Rate=1M, StartEffectiveDate=new DateTime(2011,11,21), EndEffectiveDate=new DateTime(2999,12,31) },
                new Model.OtherClaimsExcess() { OtherClaimsExcessID=60, ClassOfRiskID=3, PremiumTypeID=4, State="NSW", ClaimExcess=250M, Default=true, Rate=1M, StartEffectiveDate=new DateTime(2011,11,21), EndEffectiveDate=new DateTime(2999,12,31) },
                new Model.OtherClaimsExcess() { OtherClaimsExcessID=61, ClassOfRiskID=3, PremiumTypeID=4, State="NSW", ClaimExcess=500M, Default=false, Rate=1M, StartEffectiveDate=new DateTime(2011,11,21), EndEffectiveDate=new DateTime(2999,12,31) },
                new Model.OtherClaimsExcess() { OtherClaimsExcessID=62, ClassOfRiskID=3, PremiumTypeID=4, State="NSW", ClaimExcess=1000M, Default=false, Rate=1M, StartEffectiveDate=new DateTime(2011,11,21), EndEffectiveDate=new DateTime(2999,12,31) }
            };

            return Setup.CreateDbSetMock(data).Object;
        }

        private static DbSet<Model.PremiumPaidBy> CreateMockPremiumPaidBys()
        {
            var data = new List<Model.PremiumPaidBy>
            {

            };

            return Setup.CreateDbSetMock(data).Object;
        }

        //private static DbSet<Model.PremiumType> CreateMockPremiumTypes()
        //{
        //    var data = new List<Model.PremiumType>
        //    {

        //    };

        //    return Setup.CreateDbSetMock(data).Object;
        //}

        private static DbSet<Model.PromoCodeState> CreateMockPromoCodeStates()
        {
            var data = new List<Model.PromoCodeState>
            {

            };

            return Setup.CreateDbSetMock(data).Object;
        }

        private static DbSet<Model.PromoCode> CreateMockPromoCodes()
        {
            var data = new List<Model.PromoCode>
            {

            };

            return Setup.CreateDbSetMock(data).Object;
        }

        private static DbSet<Model.SelfManaged> CreateMockSelfManaged()
        {
            var data = new List<Model.SelfManaged>
            {
                new Model.SelfManaged() { SelfManagedID=1, SelfManagedName="NO", Rate=1M, ClassOfRiskID=3, PremiumTypeID=3, State="ACT", StartEffectiveDate=new DateTime(1900,01,01), EndEffectiveDate=new DateTime(2011,11,20,23,59,59) },
                new Model.SelfManaged() { SelfManagedID=2, SelfManagedName="NO", Rate=1M, ClassOfRiskID=3, PremiumTypeID=3, State="ACT", StartEffectiveDate=new DateTime(2011,11,21), EndEffectiveDate=new DateTime(2999,12,31,23,59,59) },
                new Model.SelfManaged() { SelfManagedID=3, SelfManagedName="YES", Rate=1M, ClassOfRiskID=3, PremiumTypeID=3, State="ACT", StartEffectiveDate=new DateTime(2011,11,21), EndEffectiveDate=new DateTime(2999,12,31,20,23,59,59) },
                new Model.SelfManaged() { SelfManagedID=4, SelfManagedName="NO", Rate=1M, ClassOfRiskID=3, PremiumTypeID=4, State="ACT", StartEffectiveDate=new DateTime(1900,01,01), EndEffectiveDate=new DateTime(2012,01,31,23,59,59) },
                new Model.SelfManaged() { SelfManagedID=5, SelfManagedName="NO", Rate=1M, ClassOfRiskID=3, PremiumTypeID=4, State="ACT", StartEffectiveDate=new DateTime(2012,01,01), EndEffectiveDate=new DateTime(2999,12,31,20,23,59,59) },
                new Model.SelfManaged() { SelfManagedID=6, SelfManagedName="YES", Rate=1M, ClassOfRiskID=3, PremiumTypeID=4, State="ACT", StartEffectiveDate=new DateTime(2012,02,01), EndEffectiveDate=new DateTime(2999,12,31,20,23,59,59) },
                new Model.SelfManaged() { SelfManagedID=7, SelfManagedName="NO", Rate=1M, ClassOfRiskID=3, PremiumTypeID=3, State="NSW", StartEffectiveDate=new DateTime(1900,01,01), EndEffectiveDate=new DateTime(2011,11,20,23,59,59) },
                new Model.SelfManaged() { SelfManagedID=8, SelfManagedName="NO", Rate=1M, ClassOfRiskID=3, PremiumTypeID=3, State="NSW", StartEffectiveDate=new DateTime(2011,11,21), EndEffectiveDate=new DateTime(2999,12,31,23,59,59) },
                new Model.SelfManaged() { SelfManagedID=9, SelfManagedName="YES", Rate=1M, ClassOfRiskID=3, PremiumTypeID=3, State="NSW", StartEffectiveDate=new DateTime(2011,11,21), EndEffectiveDate=new DateTime(2999,12,31,20,23,59,59) },
                new Model.SelfManaged() { SelfManagedID=10, SelfManagedName="NO", Rate=1M, ClassOfRiskID=3, PremiumTypeID=4, State="NSW", StartEffectiveDate=new DateTime(1900,01,01), EndEffectiveDate=new DateTime(2012,01,31,23,59,59) },
                new Model.SelfManaged() { SelfManagedID=11, SelfManagedName="NO", Rate=1M, ClassOfRiskID=3, PremiumTypeID=4, State="NSW", StartEffectiveDate=new DateTime(2012,02,01), EndEffectiveDate=new DateTime(2999,12,31,20,23,59,59) },
                new Model.SelfManaged() { SelfManagedID=12, SelfManagedName="YES", Rate=1M, ClassOfRiskID=3, PremiumTypeID=4, State="NSW", StartEffectiveDate=new DateTime(2012,02,01), EndEffectiveDate=new DateTime(2999,12,31,20,23,59,59) }
            };

            return Setup.CreateDbSetMock(data).Object;
        }

        private static DbSet<Model.Tenanted> CreateMockTenanted()
        {
            var data = new List<Model.Tenanted>
            {
                new Model.Tenanted() { TenantedID=7, TenantedName="YES", Rate=1M, ClassOfRiskID=3, StartEffectiveDate=new DateTime(1900,01,01), EndEffectiveDate=new DateTime(2011,11,20,23,59,59), IsUnknown=false },
                new Model.Tenanted() { TenantedID=8, TenantedName="NO", Rate=1M, ClassOfRiskID=3, StartEffectiveDate=new DateTime(1900,01,01), EndEffectiveDate=new DateTime(2011,11,20,23,59,59), IsUnknown=false },
                new Model.Tenanted() { TenantedID=9, TenantedName="Unknown", Rate=1M, ClassOfRiskID=3, StartEffectiveDate=new DateTime(1900,01,01), EndEffectiveDate=new DateTime(2011,11,20,23,59,59), IsUnknown=true },
                new Model.Tenanted() { TenantedID=10, TenantedName="YES", Rate=1M, ClassOfRiskID=3, StartEffectiveDate=new DateTime(2011,11,21), EndEffectiveDate=new DateTime(2999,12,31,23,59,59), IsUnknown=false },
                new Model.Tenanted() { TenantedID=11, TenantedName="NO", Rate=1M, ClassOfRiskID=3, StartEffectiveDate=new DateTime(2011,11,21), EndEffectiveDate=new DateTime(2999,12,31,23,59,59), IsUnknown=false },
                new Model.Tenanted() { TenantedID=12, TenantedName="Unknown", Rate=1M, ClassOfRiskID=3, StartEffectiveDate=new DateTime(2011,11,21), EndEffectiveDate=new DateTime(2999,12,31,23,59,59), IsUnknown=true },
                new Model.Tenanted() { TenantedID=19, TenantedName="YES", Rate=1M, ClassOfRiskID=11, StartEffectiveDate=new DateTime(1900,01,01), EndEffectiveDate=new DateTime(2011,11,20,23,59,59), IsUnknown=false },
                new Model.Tenanted() { TenantedID=20, TenantedName="NO", Rate=1M, ClassOfRiskID=11, StartEffectiveDate=new DateTime(1900,01,01), EndEffectiveDate=new DateTime(2011,11,20,23,59,59), IsUnknown=false },
                new Model.Tenanted() { TenantedID=21, TenantedName="Unknown", Rate=1M, ClassOfRiskID=11, StartEffectiveDate=new DateTime(1900,01,01), EndEffectiveDate=new DateTime(2011,11,20,23,59,59), IsUnknown=true },
                new Model.Tenanted() { TenantedID=22, TenantedName="YES", Rate=1M, ClassOfRiskID=11, StartEffectiveDate=new DateTime(2011,11,21), EndEffectiveDate=new DateTime(2999,12,31,23,59,59), IsUnknown=false },
                new Model.Tenanted() { TenantedID=23, TenantedName="NO", Rate=1M, ClassOfRiskID=11, StartEffectiveDate=new DateTime(2011,11,21), EndEffectiveDate=new DateTime(2999,12,31,23,59,59), IsUnknown=false },
                new Model.Tenanted() { TenantedID=24, TenantedName="Unknown", Rate=1M, ClassOfRiskID=11, StartEffectiveDate=new DateTime(2011,11,21), EndEffectiveDate=new DateTime(2999,12,31,23,59,59), IsUnknown=true }
            };

            return Setup.CreateDbSetMock(data).Object;
        }
    }
}
