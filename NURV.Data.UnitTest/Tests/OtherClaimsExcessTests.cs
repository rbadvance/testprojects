﻿using System;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using NUnit.Framework;

namespace NURV.Data.UnitTest.Tests
{
    [TestFixture]
    public class OtherClaimsExcessTests : Common.TestHarness
    {
        [Test, Order(1), Description("Get Other Claims Excess for Lookup test")]
        [TestCase(11, "2012-01-01", "ACT", Types.PremiumType.NewBusiness, ExpectedResult = "9")]
        [TestCase(3, "2012-01-01", "ACT", Types.PremiumType.NewBusiness, ExpectedResult = "33,34,35")]
        [TestCase(11, "2012-01-01", "ACT", Types.PremiumType.Renewal, ExpectedResult = "25")]
        [TestCase(3, "2012-01-01", "ACT", Types.PremiumType.Renewal, ExpectedResult = "57,58,59")]
        [TestCase(11, "2012-01-01", "NSW", Types.PremiumType.NewBusiness, ExpectedResult = "10")]
        [TestCase(3, "2012-01-01", "NSW", Types.PremiumType.NewBusiness, ExpectedResult = "36,37,38")]
        [TestCase(11, "2012-01-01", "NSW", Types.PremiumType.Renewal, ExpectedResult = "26")]
        [TestCase(3, "2012-01-01", "NSW", Types.PremiumType.Renewal, ExpectedResult = "60,61,62")]
        public async Task<string> GetOtherClaimsExcessForLookupTest(int classOfRiskId, DateTime? effectiveDate, string state, Types.PremiumType premiumType)
        {
            using (var context = new Data.Context.NURVContext(true))
            {
                var result = await context.OtherClaimsExcessQuery.Value.GetOtherClaimsExcessForLookupAsync(classOfRiskId, effectiveDate, state, premiumType, CancellationToken.None);

                Assert.That(result != null, "GetOtherClaimsExcessForLookupAsync() returned NULL");
                Assert.That(string.IsNullOrEmpty(result.Message), result.Message);

                foreach (DataAccess.OtherClaimsExcesses.DTO.DTOtherClaimsExcessForLookup row in result.Value)
                {
                    Console.WriteLine($"{row.Id}.{row.ClaimExcess}");
                }

                return result.HasSucceeded ? string.Join(",", result.Value.Select(s => s.Id).ToList()) : null;
            }
        }
    }
}
