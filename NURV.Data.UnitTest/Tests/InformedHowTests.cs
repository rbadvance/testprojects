﻿using System;
using System.Threading;
using System.Threading.Tasks;
using NUnit.Framework;

namespace NURV.Data.UnitTest.Tests
{
    [TestFixture]
    public class InformedHowTests : Common.TestHarness
    {
        [Test, Order(1), Description("Get InformedHow for Lookup test")]
        [TestCase(ExpectedResult = true)]
        public async Task<bool> GetInformedHowForLookupTest()
        {
            using (var context = new Context.NURVContext(true))
            {
                var result = await context.InformedHowQuery.GetInformedHowForLookupAsync(CancellationToken.None);

                Assert.That(result != null, "GetInformedHowForLookupAsync() returned NULL");
                Assert.That(string.IsNullOrEmpty(result.Message), result.Message);

                foreach (DataAccess.InformedHows.DTO.DTInformedHowForLookup row in result.Value)
                {
                    Console.WriteLine($"{row.Id}.{row.Data}");
                }

                return result.HasSucceeded;
            }
        }
    }
}
