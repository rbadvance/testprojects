﻿using System;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using NUnit.Framework;

namespace NURV.Data.UnitTest.Tests
{
    [TestFixture]
    public class HolidayTests : Common.TestHarness
    {
        [Test, Order(1), Description("Get Holidays for Lookup test")]
        [TestCase(11, false, "2010-01-01", "ACT", Types.PremiumType.NewBusiness, ExpectedResult = "1,3")]
        [TestCase(11, false, "2012-01-01", "ACT", Types.PremiumType.NewBusiness, ExpectedResult = "2,4")]
        [TestCase(11, true, "2010-01-01", "ACT", Types.PremiumType.NewBusiness, ExpectedResult = "1,5,3")]
        [TestCase(11, true, "2012-01-01", "ACT", Types.PremiumType.NewBusiness, ExpectedResult = "2,6,4")]
        [TestCase(3, false, "2010-01-01", "ACT", Types.PremiumType.NewBusiness, ExpectedResult = "97,99")]
        [TestCase(3, false, "2012-01-01", "ACT", Types.PremiumType.NewBusiness, ExpectedResult = "98,100")]
        [TestCase(3, true, "2010-01-01", "ACT", Types.PremiumType.NewBusiness, ExpectedResult = "97,101,99")]
        [TestCase(3, true, "2012-01-01", "ACT", Types.PremiumType.NewBusiness, ExpectedResult = "98,102,100")]
        [TestCase(11, false, "2010-01-01", "NSW", Types.PremiumType.NewBusiness, ExpectedResult = "13,15")]
        [TestCase(11, false, "2012-01-01", "NSW", Types.PremiumType.NewBusiness, ExpectedResult = "14,16")]
        [TestCase(11, true, "2010-01-01", "NSW", Types.PremiumType.NewBusiness, ExpectedResult = "13,17,15")]
        [TestCase(11, true, "2012-01-01", "NSW", Types.PremiumType.NewBusiness, ExpectedResult = "14,18,16")]
        [TestCase(3, false, "2010-01-01", "NSW", Types.PremiumType.NewBusiness, ExpectedResult = "109,111")]
        [TestCase(3, false, "2012-01-01", "NSW", Types.PremiumType.NewBusiness, ExpectedResult = "110,112")]
        [TestCase(3, true, "2010-01-01", "NSW", Types.PremiumType.NewBusiness, ExpectedResult = "109,113,111")]
        [TestCase(3, true, "2012-01-01", "NSW", Types.PremiumType.NewBusiness, ExpectedResult = "110,114,112")]
        [TestCase(11, false, "2010-01-01", "ACT", Types.PremiumType.Renewal, ExpectedResult = "7,9")]
        [TestCase(11, false, "2012-01-01", "ACT", Types.PremiumType.Renewal, ExpectedResult = "8,10")]
        [TestCase(11, true, "2010-01-01", "ACT", Types.PremiumType.Renewal, ExpectedResult = "7,11,9")]
        [TestCase(11, true, "2012-01-01", "ACT", Types.PremiumType.Renewal, ExpectedResult = "8,12,10")]
        public async Task<string> GetHolidayForLookupTest(int classOfRiskId, bool includeUnknown, DateTime? effectiveDate, string state, Types.PremiumType premiumType)
        {
            using (var context = new Data.Context.NURVContext(true))
            {
                var result = await context.HolidayQuery.Value.GetHolidayForLookupAsync(classOfRiskId, includeUnknown, effectiveDate, state, premiumType, CancellationToken.None);

                Assert.That(result != null, "GetHolidayForLookupAsync() returned NULL");
                Assert.That(string.IsNullOrEmpty(result.Message), result.Message);

                foreach (DataAccess.Holidays.DTO.DTHolidayForLookup row in result.Value)
                {
                    Console.WriteLine($"{row.Id}.{row.Name}");
                }

                return result.HasSucceeded ? string.Join(",", result.Value.Select(s => s.Id).ToList()) : null;
            }
        }
    }
}
