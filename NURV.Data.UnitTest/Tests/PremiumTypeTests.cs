﻿using System;
using System.Threading;
using System.Threading.Tasks;
using NUnit.Framework;

namespace NURV.Data.UnitTest.Tests
{
    [TestFixture]
    public class PremiumTypeTests : Common.TestHarness
    {
        [Test, Order(1), Description("Get PremiumType for Lookup test")]
        [TestCase(ExpectedResult = true)]
        public async Task<bool> GetPremiumTypeForLookupTest()
        {
            using (var context = new Context.NURVContext(true))
            {
                var result = await context.PremiumTypeQuery.GetPremiumTypeForLookupAsync(CancellationToken.None);

                Assert.That(result != null, "GetPremiumTypeForLookupAsync() returned NULL");
                Assert.That(string.IsNullOrEmpty(result.Message), result.Message);

                foreach (DataAccess.PremiumTypes.DTO.DTPremiumTypeForLookup row in result.Value)
                {
                    Console.WriteLine($"{row.Id}.{row.Data}");
                }

                return result.HasSucceeded;
            }
        }
    }
}
