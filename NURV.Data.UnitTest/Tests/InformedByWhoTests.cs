﻿using System;
using System.Threading;
using System.Threading.Tasks;
using NUnit.Framework;

namespace NURV.Data.UnitTest.Tests
{
    [TestFixture]
    public class InformedByWhoTests : Common.TestHarness
    {
        [Test, Order(1), Description("Get InformedByWho for Lookup test")]
        [TestCase(ExpectedResult = true)]
        public async Task<bool> GetInformedByWhoForLookupTest()
        {
            using (var context = new Context.NURVContext(true))
            {
                var result = await context.InformedByWhoQuery.GetInformedByWhoForLookupAsync(CancellationToken.None);

                Assert.That(result != null, "GetInformedByWhoForLookupAsync() returned NULL");
                Assert.That(string.IsNullOrEmpty(result.Message), result.Message);

                foreach (DataAccess.InformedByWhos.DTO.DTInformedByWhoForLookup row in result.Value)
                {
                    Console.WriteLine($"{row.Id}.{row.Data}");
                }

                return result.HasSucceeded;
            }
        }
    }
}
