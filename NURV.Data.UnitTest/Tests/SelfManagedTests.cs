﻿using System;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using NUnit.Framework;

namespace NURV.Data.UnitTest.Tests
{
    [TestFixture]
    public class SelfManagedTests : Common.TestHarness
    {
        [Test, Order(1), Description("Get Self Managed for Lookup test")]
        [TestCase(3, "2010-01-01", "ACT", Types.PremiumType.NewBusiness, ExpectedResult = "1")]
        [TestCase(3, "2012-01-01", "ACT", Types.PremiumType.NewBusiness, ExpectedResult = "3,2")]
        [TestCase(3, "2012-03-01", "ACT", Types.PremiumType.NewBusiness, ExpectedResult = "3,2")]
        [TestCase(3, "2010-01-01", "ACT", Types.PremiumType.Renewal, ExpectedResult = "4")]
        [TestCase(3, "2012-01-01", "ACT", Types.PremiumType.Renewal, ExpectedResult = "4,5")]
        [TestCase(3, "2012-03-01", "ACT", Types.PremiumType.Renewal, ExpectedResult = "6,5")]
        [TestCase(3, "2010-01-01", "NSW", Types.PremiumType.NewBusiness, ExpectedResult = "7")]
        [TestCase(3, "2012-01-01", "NSW", Types.PremiumType.NewBusiness, ExpectedResult = "9,8")]
        [TestCase(3, "2012-03-01", "NSW", Types.PremiumType.NewBusiness, ExpectedResult = "9,8")]
        [TestCase(3, "2010-01-01", "NSW", Types.PremiumType.Renewal, ExpectedResult = "10")]
        [TestCase(3, "2012-01-01", "NSW", Types.PremiumType.Renewal, ExpectedResult = "10")]
        [TestCase(3, "2012-03-01", "NSW", Types.PremiumType.Renewal, ExpectedResult = "12,11")]
        public async Task<string> GetSelfManagedForLookupTest(int classOfRiskId, DateTime? effectiveDate, string state, Types.PremiumType premiumType)
        {
            using (var context = new Data.Context.NURVContext(true))
            {
                var result = await context.SelfManagedQuery.Value.GetSelfManagedForLookupAsync(classOfRiskId, effectiveDate, state, premiumType, CancellationToken.None);

                Assert.That(result != null, "GetSelfManagedForLookupAsync() returned NULL");
                Assert.That(string.IsNullOrEmpty(result.Message), result.Message);

                foreach (DataAccess.SelfManaged.DTO.DTSelfManagedForLookup row in result.Value)
                {
                    Console.WriteLine($"{row.Id}.{row.Name}");
                }

                return result.HasSucceeded ? string.Join(",", result.Value.Select(s => s.Id).ToList()) : null;
            }
        }
    }
}
