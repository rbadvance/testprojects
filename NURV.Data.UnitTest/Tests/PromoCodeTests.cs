﻿using System;
using System.Threading;
using System.Threading.Tasks;
using NUnit.Framework;

namespace NURV.Data.UnitTest.Tests
{
    [TestFixture]
    public class PromoCodeTests : Common.TestHarness
    {
        [Test, Order(1), Description("Get PromoCode for Lookup test")]
        [TestCase(ExpectedResult = true)]
        public async Task<bool> GetPromoCodeForLookupTest()
        {
            using (var context = new Context.NURVContext(true))
            {
                var result = await context.PromoCodeQuery.GetPromoCodeForLookupAsync(CancellationToken.None);

                Assert.That(result != null, "GetPromoCodeForLookupAsync() returned NULL");
                Assert.That(string.IsNullOrEmpty(result.Message), result.Message);

                foreach (DataAccess.PromoCodes.DTO.DTPromoCodeForLookup row in result.Value)
                {
                    Console.WriteLine($"{row.Id}.{row.Name}");
                }

                return result.HasSucceeded;
            }
        }
    }
}
