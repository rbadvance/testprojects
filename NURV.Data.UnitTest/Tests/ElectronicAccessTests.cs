﻿using System;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using NUnit.Framework;

namespace NURV.Data.UnitTest.Tests
{
    [TestFixture]
    public class ElectronicAccessTests : Common.TestHarness
    {
        [Test, Order(1), Description("Get Electronic Access for Lookup test")]
        [TestCase(11, false, "2010-01-01", "ACT", Types.PremiumType.NewBusiness, ExpectedResult = "1,3")]
        [TestCase(11, false, "2012-03-01", "ACT", Types.PremiumType.NewBusiness, ExpectedResult = "2,4")]
        [TestCase(11, true, "2010-01-01", "ACT", Types.PremiumType.NewBusiness, ExpectedResult = "1,3,5")]
        [TestCase(11, false, "2010-01-01", "ACT", Types.PremiumType.Renewal, ExpectedResult = "7,9")]
        [TestCase(11, false, "2012-03-01", "ACT", Types.PremiumType.Renewal, ExpectedResult = "8,10")]
        [TestCase(11, true, "2010-01-01", "ACT", Types.PremiumType.Renewal, ExpectedResult = "7,9,11")]
        [TestCase(11, false, "2010-01-01", "NSW", Types.PremiumType.NewBusiness, ExpectedResult = "13,15")]
        [TestCase(11, false, "2012-03-01", "NSW", Types.PremiumType.NewBusiness, ExpectedResult = "14,16")]
        [TestCase(11, true, "2010-01-01", "NSW", Types.PremiumType.NewBusiness, ExpectedResult = "13,15,17")]
        [TestCase(11, false, "2010-01-01", "NSW", Types.PremiumType.Renewal, ExpectedResult = "19,21")]
        [TestCase(11, false, "2012-03-01", "NSW", Types.PremiumType.Renewal, ExpectedResult = "20,22")]
        [TestCase(11, true, "2010-01-01", "NSW", Types.PremiumType.Renewal, ExpectedResult = "19,21,23")]
        public async Task<string> GetElectronicAccessForLookupTest(int classOfRiskId, bool includeUnknown, DateTime? effectiveDate, string state, Types.PremiumType premiumType)
        {
            using (var context = new Data.Context.NURVContext(true))
            {
                var result = await context.ElectronicAccessQuery.Value.GetElectronicAccessForLookupAsync(classOfRiskId, includeUnknown, effectiveDate, state, premiumType, CancellationToken.None);

                Assert.That(result != null, "GetElectronicAccessForLookupAsync() returned NULL");
                Assert.That(string.IsNullOrEmpty(result.Message), result.Message);

                foreach (DataAccess.ElectronicAccesses.DTO.DTElectronicAccessForLookup row in result.Value)
                {
                    Console.WriteLine($"{row.Id}.{row.Name}");
                }

                return result.HasSucceeded ? string.Join(",", result.Value.Select(s => s.Id).ToList()) : null;
            }
        }
    }
}
