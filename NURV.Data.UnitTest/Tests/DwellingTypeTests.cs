﻿using System;
using System.Threading;
using System.Threading.Tasks;
using NUnit.Framework;

namespace NURV.Data.UnitTest.Tests
{
    [TestFixture]
    public class DwellingTypeTests : Common.TestHarness
    {
        [Test, Order(1), Description("Get DwellingType for Lookup test")]
        [TestCase(ExpectedResult = true)]
        public async Task<bool> GetDwellingTypeForLookupTest()
        {
            using (var context = new Context.NURVContext(true))
            {
                var result = await context.DwellingTypeQuery.GetDwellingTypeForLookupAsync(CancellationToken.None);

                Assert.That(result != null, "GetDwellingTypeForLookupAsync() returned NULL");
                Assert.That(string.IsNullOrEmpty(result.Message), result.Message);

                foreach (DataAccess.DwellingTypes.DTO.DTDwellingTypeForLookup row in result.Value)
                {
                    Console.WriteLine($"{row.Id}.{row.Name}");
                }

                return result.HasSucceeded;
            }
        }
    }
}
