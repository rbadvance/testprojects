﻿using System;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using NUnit.Framework;

namespace NURV.Data.UnitTest.Tests
{
    [TestFixture]
    public class LandSizeTests : Common.TestHarness
    {
        [Test, Order(1), Description("Get Land Size for Lookup test")]
        [TestCase(1, false, "2010-01-01", ExpectedResult = "1,2")]
        [TestCase(1, false, "2012-01-01", ExpectedResult = "4,5")]
        [TestCase(1, true, "2010-01-01", ExpectedResult = "1,2,3")]
        [TestCase(1, true, "2012-01-01", ExpectedResult = "4,5,6")]
        [TestCase(3, false, "2010-01-01", ExpectedResult = "7,8")]
        [TestCase(3, false, "2012-01-01", ExpectedResult = "10,11")]
        [TestCase(3, true, "2010-01-01", ExpectedResult = "7,8,9")]
        [TestCase(3, true, "2012-01-01", ExpectedResult = "10,11,12")]
        public async Task<string> GetLandSizeForLookupTest(int classOfRiskId, bool includeUnknown, DateTime? effectiveDate)
        {
            using (var context = new Data.Context.NURVContext(true))
            {
                var result = await context.LandSizeQuery.Value.GetLandSizeForLookupAsync(classOfRiskId, includeUnknown, effectiveDate, CancellationToken.None);

                Assert.That(result != null, "GetLandSizeForLookupAsync() returned NULL");
                Assert.That(string.IsNullOrEmpty(result.Message), result.Message);

                foreach (DataAccess.LandSizes.DTO.DTLandSizeForLookup row in result.Value)
                {
                    Console.WriteLine($"{row.Id}.{row.Name}");
                }

                return result.HasSucceeded ? string.Join(",",result.Value.Select(s=> s.Id).ToList()) : null;
            }
        }
    }
}
