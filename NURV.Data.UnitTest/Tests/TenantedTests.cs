﻿using System;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using NUnit.Framework;

namespace NURV.Data.UnitTest.Tests
{
    [TestFixture]
    public class TenantedTests : Common.TestHarness
    {
        [Test, Order(1), Description("Get Tenanted for Lookup test")]
        [TestCase(3, false, "2010-01-01", ExpectedResult = "7,8")]
        [TestCase(3, false, "2012-03-01", ExpectedResult = "10,11")]
        [TestCase(3, true, "2010-01-01", ExpectedResult = "7,8,9")]
        [TestCase(11, false, "2010-01-01", ExpectedResult = "19,20")]
        [TestCase(11, false, "2012-03-01", ExpectedResult = "22,23")]
        [TestCase(11, true, "2010-01-01", ExpectedResult = "19,20,21")]
        public async Task<string> GetTenantedForLookupTest(int classOfRiskId, bool includeUnknown, DateTime? effectiveDate)
        {
            using (var context = new Data.Context.NURVContext(true))
            {
                var result = await context.TenantedQuery.Value.GetTenantedForLookupAsync(classOfRiskId, includeUnknown, effectiveDate, CancellationToken.None);

                Assert.That(result != null, "GetTenantedForLookupAsync() returned NULL");
                Assert.That(string.IsNullOrEmpty(result.Message), result.Message);

                foreach (DataAccess.Tenanted.DTO.DTTenantedForLookup row in result.Value)
                {
                    Console.WriteLine($"{row.Id}.{row.Name}");
                }

                return result.HasSucceeded ? string.Join(",", result.Value.Select(s => s.Id).ToList()) : null;
            }
        }
    }
}
