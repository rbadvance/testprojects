﻿using System;
using System.Threading;
using System.Threading.Tasks;
using NUnit.Framework;

namespace NURV.Data.UnitTest.Tests
{
    [TestFixture]
    public class HeardAboutTSIBTests : Common.TestHarness
    {
        [Test, Order(1), Description("Get HeardAboutTSIB for Lookup test")]
        [TestCase(ExpectedResult = true)]
        public async Task<bool> GetHeardAboutTSIBForLookupTest()
        {
            using (var context = new Context.NURVContext(true))
            {
                var result = await context.HeardAboutTSIBQuery.GetHeardAboutTSIBForLookupAsync(CancellationToken.None);

                Assert.That(result != null, "GetHeardAboutTSIBForLookupAsync() returned NULL");
                Assert.That(string.IsNullOrEmpty(result.Message), result.Message);

                foreach (DataAccess.HeardAboutTSIBs.DTO.DTHeardAboutTSIBForLookup row in result.Value)
                {
                    Console.WriteLine($"{row.Id}.{row.Name}");
                }

                return result.HasSucceeded;
            }
        }
    }
}
