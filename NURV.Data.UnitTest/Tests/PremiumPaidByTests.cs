﻿using System;
using System.Threading;
using System.Threading.Tasks;
using NUnit.Framework;

namespace NURV.Data.UnitTest.Tests
{
    [TestFixture]
    public class PremiumPaidByTests : Common.TestHarness
    {
        [Test, Order(1), Description("Get PremiumPaidBy for Lookup test")]
        [TestCase(ExpectedResult = true)]
        public async Task<bool> GetPremiumPaidByForLookupTest()
        {
            using (var context = new Context.NURVContext(true))
            {
                var result = await context.PremiumPaidByQuery.GetPremiumPaidByForLookupAsync(CancellationToken.None);

                Assert.That(result != null, "GetPremiumPaidByForLookupAsync() returned NULL");
                Assert.That(string.IsNullOrEmpty(result.Message), result.Message);

                foreach (DataAccess.PremiumPaidBys.DTO.DTPremiumPaidByForLookup row in result.Value)
                {
                    Console.WriteLine($"{row.Id}.{row.Name}");
                }

                return result.HasSucceeded;
            }
        }
    }
}
