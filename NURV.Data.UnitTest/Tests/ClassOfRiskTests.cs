﻿using System;
using System.Threading;
using System.Threading.Tasks;
using NUnit.Framework;

namespace NURV.Data.UnitTest.Tests
{
    [TestFixture]
    public class ClassOfRiskTests : Common.TestHarness
    {
        [Test, Order(1), Description("Get ClassOfRisk for Lookup test")]
        [TestCase(ExpectedResult = true)]
        public async Task<bool> GetClassOfRiskForLookupTest()
        {
            using (var context = new Context.NURVContext(true))
            {
                var result = await context.ClassOfRiskQuery.GetClassOfRiskForLookupAsync(CancellationToken.None);

                Assert.That(result != null, "GetClassOfRiskForLookupAsync() returned NULL");
                Assert.That(string.IsNullOrEmpty(result.Message), result.Message);

                foreach (DataAccess.ClassesOfRisk.DTO.DTClassOfRiskForLookup row in result.Value)
                {
                    Console.WriteLine($"{row.Id}.{row.Data}");
                }

                return result.HasSucceeded;
            }
        }
    }
}
