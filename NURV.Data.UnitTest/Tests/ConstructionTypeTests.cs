﻿using System;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using NUnit.Framework;

namespace NURV.Data.UnitTest.Tests
{
    [TestFixture]
    public class ConstructionTypeTests : Common.TestHarness
    {
        [Test, Order(1), Description("Get ConstructionType for Lookup test")]
        [TestCase(3, false, "2010-01-01", ExpectedResult = "1,2")]
        [TestCase(3, false, "2012-02-01", ExpectedResult = "3,4")]
        [TestCase(3, true, "2010-01-01", ExpectedResult = "1,2")]
        [TestCase(3, true, "2012-02-01", ExpectedResult = "3,4,11")]
        [TestCase(11, false, "2010-01-01", ExpectedResult = "12,13,14")]
        [TestCase(11, false, "2012-02-01", ExpectedResult = "12,13,14")]
        [TestCase(11, true, "2010-01-01", ExpectedResult = "12,13,14,20")]
        [TestCase(11, true, "2012-02-01", ExpectedResult = "12,13,14,20")]
        public async Task<string> GetConstructionTypeForLookupTest(int classOfRiskId, bool includeUnknown, DateTime? effectiveDate)
        {
            using (var context = new Context.NURVContext(true))
            {
                var result = await context.ConstructionTypeQuery.Value.GetConstructionTypeForLookupAsync(classOfRiskId, includeUnknown, effectiveDate, CancellationToken.None);

                Assert.That(result != null, "GetConstructionTypeForLookupAsync() returned NULL");
                Assert.That(string.IsNullOrEmpty(result.Message), result.Message);

                foreach (DataAccess.ConstructionTypes.DTO.DTConstructionTypeForLookup row in result.Value)
                {
                    Console.WriteLine($"{row.Id}.{row.Name}");
                }

                return result.HasSucceeded ? string.Join(",", result.Value.Select(s => s.Id).ToList()) : null;
            }
        }
    }
}
