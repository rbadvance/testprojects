﻿using System;
using System.Threading;
using System.Threading.Tasks;
using NUnit.Framework;

namespace NURV.Data.UnitTest.Tests
{
    [TestFixture]
    public class ContentsCoverTests : Common.TestHarness
    {
        [Test, Order(1), Description("Get ContentsCover for Lookup test")]
        [TestCase(ExpectedResult = true)]
        public async Task<bool> GetContentsCoverForLookupTest()
        {
            using (var context = new Context.NURVContext(true))
            {
                var result = await context.ContentsCoverQuery.GetContentsCoverForLookupAsync(CancellationToken.None);

                Assert.That(result != null, "GetContentsCoverForLookupAsync() returned NULL");
                Assert.That(string.IsNullOrEmpty(result.Message), result.Message);

                foreach (DataAccess.ContentsCovers.DTO.DTContentsCoverForLookup row in result.Value)
                {
                    Console.WriteLine($"{row.Id}.{row.ContentsSumInsured}");
                }

                return result.HasSucceeded;
            }
        }
    }
}
